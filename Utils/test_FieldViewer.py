import numpy as np
from Utils.PlotsCondVAE import FieldViewer
import matplotlib.pyplot as plt

mems = 10
shape = (36, 60)

grid = np.random.randn(mems, 2, *shape)

x = np.expand_dims(grid[:,0], axis=1)
y = np.expand_dims(grid[:,1], axis=1)

u0 = -y#/np.sqrt(x**2 + y**2)
v0 = x#/np.sqrt(x**2 + y**2)
uv = np.concatenate([u0, v0], axis=1)
uv += 0.01 * np.random.randn(*uv.shape)

print(type(uv))

ll = np.exp(-(x**2 + y**2)/2)/np.sqrt(2*np.pi)

mask = np.random.binomial(1, 0.5, size=((mems,)+shape))

fv = FieldViewer(grid, mask)

fv.plot(
    background={'data': ll*x, 'colorbar':True, 'cmap':None},
    contours={'data': ll, 'colorbar':True},
    glyphs = {'data': uv, 'cmap': 'plasma', 'color': 'var', 'colorbar': True}
)
plt.show()