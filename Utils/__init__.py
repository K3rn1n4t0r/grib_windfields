import numpy as np
import datetime
from calendar import monthrange
import torch

def computeLossFunction(input, target, mode='L1'):
    if mode == 'L1':
        return np.mean(np.abs(target - input))#, 1, axis=0))
    elif mode == 'L2':
        return np.sum(np.linalg.norm(target - input, 2, axis=0))
    else:
        raise Exception()


def computeBatchLoss(inputs, targets, indices, channels, dataset, mode='L1', normalize=True):
    loss = 0
    batchSize = len(inputs)
    borderOffset = 0
    lenX = inputs[0].shape[2]
    lenY = inputs[0].shape[1]

    minX = borderOffset
    maxX = lenX - borderOffset
    minY = borderOffset
    maxY = lenY - borderOffset

    for f in range(batchSize):
        input = inputs[f]
        target = targets[f]
        idx = indices[f]

        loss += computeLossFunction(input[channels, minY:maxY, minX:maxX],
                                    target[channels, minY:maxY, minX:maxX], mode)

    return loss / batchSize


## ranges = [minRange, maxRange] --> [(2017, 1), (2018, 2)]
def checkTimesteps(timesteps, ranges):
    mask = np.full(len(timesteps), True, dtype=bool)

    minMonthRange = ranges[0]
    maxMonthRange = ranges[1]

    minDate = datetime.date(*minMonthRange, 1)
    maxDate = datetime.date(*maxMonthRange, monthrange(*maxMonthRange)[1])

    for i in range(len(timesteps)):
        timestep = timesteps[i]
        date = datetime.datetime.strptime(timestep, "%Y-%m-%d %H")

        curDate = datetime.date(date.year, date.month, date.day)

        if not minDate <= curDate <= maxDate:
            mask[i] = False
        # # check if in year range
        # if year:
        #     if date.year < minDate[1] or date.year > maxDate[2]:
        #         mask[i] = False
        #
        #
        #
        # if month and (date.month < range[0] or date.month > range[1]):
        #     mask[i] = False
        #
        # if day:
        #     raise NotImplementedError("Daily filtering is not implemented")

    return mask


def space_to_depth(x, patch_size):
    if isinstance(patch_size, int):
        patch_size = (patch_size, )
    dim = len(patch_size)
    assert len(x.size()) >= dim, "[ERROR] Input tensor must have at least as many dimensions as tuple <patch_size>."
    if len(x.size()) == dim:
        x = x.unsqueeze(0)
    for dim_curr in range(dim):
        x = torch.stack(x.split(patch_size[dim_curr], dim=-dim), dim=-1)
    x = x.flatten(start_dim=-(2*dim+1), end_dim=-(dim+1))
    return x


def depth_to_space(x, patch_size):
    if isinstance(patch_size, int):
        patch_size = (patch_size, )
    dim = len(patch_size)
    shape = x.size()
    assert len(shape) > dim, \
        "[ERROR] Input tensor must have at least one dimensions more than indicated by tuple <patch_size>."
    assert shape[-(dim + 1)] % np.prod(patch_size) == 0, \
        "[ERROR] Number of features must be a multiple of the number of pixels per patch."
    depth_new = int(shape[-(dim + 1)] / np.prod(patch_size))
    shape_new = shape[:-(dim+1)] + (depth_new,) + patch_size + shape[-dim:]
    x = x.view(*shape_new)
    shape_new = []
    for dim_curr in range(dim):
        x = torch.cat(x.split(1, dim=-(dim_curr+1)), dim=-(dim+dim_curr+1))
        shape_new += [patch_size[dim_curr] * shape[-(dim-dim_curr)]]
    shape_new = shape[:-(dim+1)] + (depth_new,) + tuple(shape_new)
    x = x.view(*shape_new)
    return x
