import torch
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.tri import Triangulation, TriAnalyzer


class FieldGrid():
    def __init__(self, grid, mask, viewpoint=None):
        if isinstance(mask, torch.Tensor):
            self.mask = mask.data.cpu().numpy()
        else:
            self.mask = mask
        while len(self.mask.shape) > 2:
            self.mask = self.mask[0]
        if isinstance(grid, torch.Tensor):
            self.padded = grid.data.cpu().numpy()
        else:
            self.padded = grid
        sz = self.padded.shape
        if len(sz) > 2:
            self.padded = self.padded[0]
        self.angular = self.unmask(self.padded).T
        self.cartesian = self.angular_to_cartesian(self.angular)
        self.viewpoint = None
        self.projected = None
        self.triangulation = None
        self.set_viewpoint(viewpoint)

    def set_viewpoint(self, viewpoint=None):
        if viewpoint is None:
            viewpoint = self.angular_to_cartesian([10, 45.5], r=100).squeeze()
        self.viewpoint = viewpoint
        self.compute_projection()
        self.compute_triangulation()

    def compute_projection(self):
        viewpoint = np.array(self.viewpoint)
        viewRight = np.cross(np.array([0, 0, 1]), viewpoint)
        viewUp = np.cross(viewpoint, viewRight)
        viewRight /= np.linalg.norm(viewRight)
        viewUp /= np.linalg.norm(viewUp)
        trafo = np.array([viewRight, viewUp]).T
        relative = self.cartesian - viewpoint
        scale = np.linalg.norm(viewpoint)**2 / np.dot(relative, viewpoint)
        scaled = viewpoint - (relative * np.expand_dims(scale, axis=-1))
        projected = np.dot(scaled, trafo)
        projected -= (projected.max(axis=0) + projected.min(axis=0)) / 2
        projected /= projected[:, 0].max()
        self.projected = projected
        return projected

    def compute_triangulation(self):
        nodes = self.projected
        tri = Triangulation(nodes[:, 0], nodes[:, 1])
        tri.set_mask(TriAnalyzer(tri).get_flat_tri_mask())
        self.triangulation = tri
        return tri

    def unmask(self, data):
        sz = data.shape
        if self.mask is not None:
            assert self.mask.shape == sz[-2:]
            if len(sz) > 2:
                data = data.reshape( (-1,) + sz[-2:] )
                data = data[:, self.mask == 0]
                data = data.reshape( sz[:-2] + (-1,) )
            else:
                data = data[self.mask == 0]
        else:
            if len(sz) > 2:
                data = np.reshape(data, (sz[0], -1))
            else:
                data = data.flatten()
        return data

    @staticmethod
    def angular_to_cartesian(angular, r=1):
        phi = np.take(angular, 0, axis=-1)
        the = np.take(angular, 1, axis=-1)
        phi = 2 * np.pi * phi / 360
        the = 2 * np.pi * (90 - the) / 360
        cPhi = np.cos(phi)
        sPhi = np.sin(phi)
        cThe = np.cos(the)
        sThe = np.sin(the)
        cartesian = np.array([r * sThe * cPhi, r * sThe * sPhi, r * cThe]).T
        return cartesian

    def __len__(self):
        return len(self.angular)

    def shape_padded(self):
        return self.mask.shape


class FieldData():
    def __init__(self, data, grid, transform=None):
        if isinstance(data, torch.Tensor):
            data = data.data.cpu().numpy()
        assert isinstance(data, np.ndarray), '[ERROR] Variable <data> must be of type torch.Tensor or numpy.array'
        self.data = None
        self.is_padded = None
        self.is_vector = None
        self.ndims = None
        self.is_ensemble = None
        self.nmems = None
        self._classify_data(data, grid)
        if transform is not None:
            self.transform_data(transform)

    def norm(self, keepdims=True):
        return np.linalg.norm(self.data, axis=-2, keepdims=keepdims)

    def log(self):
        return np.log(self.data)

    def log10(self):
        return np.log10(self.data)

    def mean(self, keepdims=True):
        return np.mean(self.data, axis=0, keepdims=keepdims)

    def var(self, keepdims=True):
        return np.var(self.data, axis=0, keepdims=keepdims)

    def cumVar(self, keepdims=True):
        return np.sum(self.var(keepdims=keepdims), axis=1, keepdims=keepdims)

    def logVar(self, keepdims=True):
        return np.log(self.var(keepdims=keepdims))

    def logCumVar(self, keepdims=True):
        return np.log(self.cumVar(keepdims=keepdims))

    def log10Var(self, keepdims=True):
        return np.log10(self.var(keepdims=keepdims))

    def log10CumVar(self, keepdims=True):
        return np.log10(self.cumVar(keepdims=keepdims))

    def _classify_data(self, data, grid):
        sz = data.shape
        dataDim = len(sz)
        print('[INFO] Processing {}D data array.'.format(dataDim))
        if dataDim == 1:
            assert sz[-1] == len(grid), self._message_assertion_error()
            self.is_padded = False
            self.is_vector = False
            self.is_ensemble = False
            self.ndims = 1
            self.nmems = 1
            print(self._message_data_type())
            self.data = data
        elif dataDim == 2:
            if sz == grid.shape_padded():
                self.is_padded = True
                self.is_vector = False
                self.is_ensemble = False
                self.ndims = 1
                self.nmems = 1
                print(self._message_data_type())
                self.data = grid.unmask(data)
            else:
                assert sz[-1] == len(grid), self._message_assertion_error()
                self.is_padded = False
                self.is_vector = (sz[0] > 1)
                self.is_ensemble = False
                self.ndims = sz[0]
                self.nmems = 1
                print(self._message_data_type())
                self.data = data
        elif dataDim == 3:
            if sz[-2:] == grid.shape_padded():
                self.is_padded = True
                self.is_vector = (sz[0] > 1)
                self.is_ensemble = False
                self.ndims = sz[0]
                self.nmems = 1
                print(self._message_data_type())
                self.data = grid.unmask(data)
            else:
                assert sz[-1] == len(grid), self._message_assertion_error()
                self.is_padded = False
                self.is_vector = (sz[1] > 1)
                self.is_ensemble = (sz[0] > 1)
                self.ndims = sz[1]
                self.nmems = sz[0]
                print(self._message_data_type())
                self.data = data
        elif dataDim == 4:
            assert sz[-2:] == grid.shape_padded(), self._message_assertion_error()
            self.is_padded = True
            self.is_vector = (sz[1] > 1)
            self.is_ensemble = (sz[0] > 1)
            self.ndims = sz[1]
            self.nmems = sz[0]
            print(self._message_data_type())
            self.data = grid.unmask(data)
        else:
            raise NotImplementedError('[ERROR] Handling of specified data dimension not implemented yet.')
        self.data = self.data.reshape((self.nmems, self.ndims, -1))

    def transform_data(self, transform):
        funcNames = ['norm', 'log', 'log10', 'mean', 'var', 'logVar', 'log10Var']
        if transform in funcNames:
            func = getattr(self, transform)
            self.data = func()
        else:
            raise NotImplementedError('Requested data transform <{}> has not been implemented yet.'.format(transform))
        return self.data

    def _message_data_type(self):
        padding = 'padded' if self.is_padded else 'unpadded'
        fieldType = 'vector' if self.is_vector else 'scalar'
        str1 = '[INFO] Found {} '.format(padding)
        if self.is_ensemble:
            str2 = 'ensemble of {} fields.'.format(fieldType)
        else:
            str2 = '{} field.'.format(fieldType)
        return str1 + str2

    @staticmethod
    def _message_assertion_error():
        return '[ERROR] Dimensions of <data> are incompatible with grid.'


class FieldViewer():
    def __init__(self, grid, mask, viewpoint=None):
        self.grid = FieldGrid(grid, mask, viewpoint)

    def plot_scalar(self, data=None, **kwargs):
        keys = kwargs.keys()
        if 'transform' in keys:
            transform = kwargs['transform']
        else:
            transform = None
        if 'axis' in keys:
            axis = kwargs['axis']
        else:
            axis = None
        if 'cbar' in keys:
            cbar = kwargs['cbar']
        else:
            cbar = False
        if 'cmap' in keys:
            cmap = kwargs['cmap']
        else:
            cmap = None
        if 'levels' in keys:
            levels = kwargs['levels']
        else:
            levels = None

        field = FieldData(data, self.grid, transform=transform)
        if field.is_ensemble and 'member' in keys:
            mem = kwargs['member']
            if mem >= field.nmems:
                mem = 0
                print(
                    '[WARNING] Given ensemble index exceeds number of ensemble members ({}).'.format(field.nmems),
                    '\nProceeding with member {} instead.'.format(mem)
                )
        else:
            mem = 0
        if field.is_vector and 'dimension' in keys:
            dim = kwargs['dimension']
            if dim >= field.ndims:
                dim = 0
                print(
                    '[WARNING] Given dimension exceeds dimension of vector field ({}).'.format(field.ndims),
                    '\nProceeding with dimension 0.'
                )
        else:
            dim = 0
        tri = self.grid.triangulation
        data = field.data[mem][dim]
        if axis is None:
            plt.tricontourf(tri, data, cmap=cmap, levels=levels)
            if cbar:
                plt.colorbar()
        else:
            view = axis.tricontourf(tri, data, cmap=cmap, levels=levels)
            if cbar:
                axis.figure.colorbar(view)

    def plot_contours(self, data=None, **kwargs):
        keys = kwargs.keys()
        if 'transform' in keys:
            transform = kwargs['transform']
        else:
            transform = None
        if 'axis' in keys:
            axis = kwargs['axis']
        else:
            axis =  None
        if 'cbar' in keys:
            cbar = kwargs['cbar']
        else:
            cbar = False
        if 'cmap' in keys:
            cmap = kwargs['cmap']
        else:
            cmap = None
        if 'levels' in keys:
            levels = kwargs['levels']
        else:
            levels = None

        field = FieldData(data, self.grid, transform=transform)
        if field.is_ensemble and 'member' in keys:
            mem = kwargs['member']
            if mem >= field.nmems:
                mem = 0
                print(
                    '[WARNING] Given ensemble index exceeds number of ensemble members ({}).'.format(field.nmems),
                    '\nProceeding with member {} instead.'.format(mem)
                )
        else:
            mem = 0
        if field.is_vector and 'dimension' in keys:
            dim = kwargs['dimension']
            if dim >= field.ndims:
                dim = 0
                print(
                    '[WARNING] Given dimension exceeds dimension of vector field ({}).'.format(field.ndims),
                    '\nProceeding with dimension 0.'
                )
        else:
            dim = 0
        tri = self.grid.triangulation
        data = field.data[mem][dim]
        if axis is None:
            plt.tricontour(tri, data, cmap=cmap, levels=levels)
            if cbar:
                plt.colorbar()
        else:
            view = axis.tricontour(tri, data, cmap=cmap, levels=levels)
            if cbar:
                axis.figure.colorbar(view)

    def plot_glyphs(self, data=None, **kwargs):
        field = FieldData(data, self.grid)
        keys = kwargs.keys()
        if 'axis' in keys:
            axis = kwargs['axis']
        else:
            axis = None
        if 'cbar' in keys:
            cbar = kwargs['cbar']
        else:
            cbar = False
        if 'cmap' in keys:
            cmap = kwargs['cmap']
        else:
            cmap = None
        if 'scale' in keys:
            scale = kwargs['scale']
        else:
            scale = None
        if field.is_ensemble and 'member' in keys:
            mem = kwargs['member']
            if mem >= field.nmems:
                mem = 0
                print(
                    '[WARNING] Given ensemble index exceeds number of ensemble members ({}).'.format(field.nmems),
                    '\nProceeding with member {} instead.'.format(mem)
                )
        else:
            mem = 0
        if not field.is_vector or field.ndims != 2:
            raise ValueError(
                '[ERROR] Glyph fields must be 2D vectors. Given field has dimension {}.'.format(field.ndims))
        coords = self.grid.projected
        if 'color_by' in keys:
            assert isinstance(kwargs['color_by'], str)
            func = getattr(field, kwargs['color_by'])
            color = func(keepdims=True)
        elif 'color' in keys:
            assert isinstance(kwargs['color'], (np.ndarray, torch.Tensor))
            color = FieldData(kwargs['color'], self.grid).data
        else:
            color = None
        if color is not None:
            sz = color.shape
            color = color[0 if sz[0]==1 else mem]
            if sz[1] > 1:
                if 'reduce_color' in keys:
                    reduction = kwargs['reduce_color']
                    if isinstance(reduction, int):
                        dim = reduction
                        if dim >= sz[1]:
                            print('[WARNING] Given dimension for coloring ({}) exceeds number of vector dimensions.'.format(dim))
                            dim = 0
                            print('Using dimension {} instead.'.format(dim))
                    elif isinstance(reduction, str):
                        try:
                            func = getattr(np, reduction)
                            color = func(color, axis=0, keepdims=True)
                            dim = 0
                        except:
                            raise NotImplementedError(
                                'Requested color reduction method <{}> has not been implemented yet.'.format(reduction)
                            )
                    else:
                        raise NotImplementedError(
                            'Requested color reduction method <{}> has not been implemented yet.'.format(reduction)
                        )
                else:
                    dim = 0
                color = color[dim]
        if axis is None:
            if color is None:
                plt.quiver(coords[:, 0], coords[:, 1], field.data[mem, 0], field.data[mem, 1], cmap=cmap, scale=scale)
            else:
                plt.quiver(coords[:, 0], coords[:, 1], field.data[mem, 0], field.data[mem, 1], color,
                           cmap=cmap, scale=scale)
                if cbar:
                    plt.colorbar()
        else:
            if color is None:
                view = axis.quiver(coords[:, 0], coords[:, 1], field.data[mem, 0], field.data[mem, 1])
            else:
                view = axis.quiver(coords[:, 0], coords[:, 1], field.data[mem, 0], field.data[mem, 1], color,
                                   cmap=cmap)
                if cbar:
                    axis.figure.colorbar(view)
