from DataGeneration.GridPadder import *
from DataGeneration.GribParser import *
import os


def createTimestepStr(timestep):
    validTime = timestep
    validTime = validTime.replace("-", "")
    validTime = validTime.replace(":", "_")
    return validTime.replace(" ", "-")


def exportOrographyGridSetToNPZ(inputSet, outdir, filename):
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    print("[INFO]: Exporting orography to {}".format(outdir))

    for timestep in inputSet.timesteps:
        grids = inputSet.grids[timestep]

        np.savez_compressed("{}/{}.npz".format(outdir, filename), z=grids['z'].values, seaMask=grids['lsm'].values)


def exportInputGridSetToNPZ(inputSet, outdir, filename):
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    print("[INFO]: Exporting input vectors to {}".format(outdir))

    for timestep in inputSet.timesteps:
        validTime = createTimestepStr(timestep)
        grids = inputSet.grids[timestep]

        lons = grids['100u'].desc.lons
        lats = grids['100u'].desc.lats

        np.savez_compressed("{}/{}_{}.npz".format(outdir, filename, validTime),
                            windU=grids['100u'].values, windV=grids['100v'].values,
                            blh=grids['blh'].values, fsr=grids['fsr'].values,
                            lons=lons, lats=lats)


def exportTargetGridSetToNPZ(inputSet, outdir, filename):
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    print("[INFO]: Exporting target vectors to {}".format(outdir))

    for timestep in inputSet.timesteps:
        validTime = createTimestepStr(timestep)
        grids = inputSet.grids[timestep]

        lons = grids['100u'].desc.lons
        lats = grids['100u'].desc.lats

        np.savez_compressed("{}/{}_{}.npz".format(outdir, filename, validTime),
                            windU=grids['100u'].values, windV=grids['100v'].values,
                            lons=lons, lats=lats)
