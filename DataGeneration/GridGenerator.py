from DataGeneration.GaussianGrid import *
from DataGeneration.GridPadder import GridPadder
import multiprocessing


class GridGenerator:
    def __init__(self, is_same_grid_type, is_static=False):
        self.is_same_grid_type = is_same_grid_type
        self.bbox = None
        self.precomputed_lonlat_points = None
        self.is_static = is_static

    # obtain next grids by message (thread sage)
    def get_grids_from_messages(self, message_list, grid_set, progress_bar):
        # print("[Thread {}] started process".format(thread_id))
        print("[INFO]: Create new grids from grib messages.")

        num_messages_processed = 0
        #while not message_queue.empty():
        for message in message_list:
            #message = message_queue.get()
            grid, timestep = self.create_new_grid_from_message(message)

            #mutex.acquire()
            if self.is_static:
                grid_set.addGridAtTimestep("static", grid)
            else:
                grid_set.addGridAtTimestep(timestep, grid)

            num_messages_processed += 1
            progress_bar.proceed(num_messages_processed)
            #mutex.release()

            #message_queue.task_done()

        return True

    # obtain next grids by message (thread sage)
    def get_padded_grids_from_messages(self, message_list, grid_set, progress_bar, padding_shape):
        # print("[Thread {}] started process".format(thread_id))
        print("[INFO]: Create new grids from grib messages and add paddings to boundaries.")

        num_messages_processed = 0
        #while not message_queue.empty():
        for message in message_list:
            #message = message_queue.get()
            grid, timestep = self.create_new_grid_from_message(message)

            # pad at the boundaries to target shape
            grid_padder = GridPadder(grid)
            # overwrite grid
            grid = grid_padder.pad_to_size(padding_shape)

            # mutex.acquire()
            # add to grid set
            if self.is_static:
                grid_set.addGridAtTimestep("static", grid)
            else:
                grid_set.addGridAtTimestep(timestep, grid)

            num_messages_processed += 1
            progress_bar.proceed(num_messages_processed)
            # mutex.release()

            #message_queue.task_done()

        return True

    def create_new_grid_from_message(self, message):
        grid_id = None

        if message.grid_type == 'reduced_gg':
            if message.is_octahedral:
                grid_id = GridType.REDUCED_GAUSSIAN
            else:
                grid_id = GridType.REDUCED_GAUSSIAN
        else:
            raise NotImplementedError("Parser does only support reduced Gaussian grids")

        return self.create_gaussian_grid(message, grid_id)

    def create_gaussian_grid(self, message, grid_id):
        if not self.is_same_grid_type or not self.bbox:
            self.bbox = BoundingBox(message.min_lon, message.min_lat,
                                    message.max_lon, message.max_lat)
        bbox = self.bbox

        if not self.is_same_grid_type or not self.precomputed_lonlat_points:
            # compute longitude / latitude points
            if message.grid_type == 'reduced_gg':
                self.precomputed_lonlat_points = GaussianScalarField.compute_grid_points(
                    bbox,
                    message.num_latitudes,
                    message.points_indices_longitude,
                    message.num_data_points)
            else:
                raise NotImplementedError("Cannot handle other grids")
        precomputed_lonlats = self.precomputed_lonlat_points

        # create descriptor for grid
        desc = GridDescriptor(grid_id, message.num_latitudes,
                              precomputed_lonlats.longitudes,
                              precomputed_lonlats.latitudes,
                              precomputed_lonlats.dim_lons,
                              precomputed_lonlats.deltas_lon,
                              precomputed_lonlats.delta_lat,
                              bbox)
        # convert geopotential to geopotential height
        if message.short_name is "z":
            message.values = [value / 9.81 for value in message.values]
        # create grid
        grid = GaussianScalarField(message.short_name, message.values, desc)

        return grid, message.timestep

