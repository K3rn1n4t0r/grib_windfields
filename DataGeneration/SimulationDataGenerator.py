from DataGeneration.GribReader import GribReader
from DataGeneration.GridGenerator import GridGenerator
from DataGeneration.GaussianGrid import *
from DataGeneration.GridPadder import *
from Utils.ProgressBar import ProgressBar
import multiprocessing


class SimulationDataGenerator:
    def __init__(self, filename, is_static=False):
        self.grib_reader = GribReader(filename)
        # region of data
        self.area = self.grib_reader.area
        # get queue of messages
        self.message_list = self.grib_reader.obtain_messages()

        self.grid_generator = GridGenerator(is_same_grid_type=True, is_static=is_static)

    def create_raw_data(self, grid_set=None):
        if grid_set is None:
            grid_set = GridSet()

        progress_bar = ProgressBar(len(self.message_list), displaySumCount=True)
        self.grid_generator.get_grids_from_messages(self.message_list,
                                                    grid_set, progress_bar)

        return grid_set

    def create_padded_data(self, padding_shape, grid_set=None):
        if grid_set is None:
            grid_set = GridSet()

        progress_bar = ProgressBar(len(self.message_list), displaySumCount=True)
        self.grid_generator.get_padded_grids_from_messages(self.message_list,
                                                           grid_set, progress_bar,
                                                           padding_shape)

        return grid_set

