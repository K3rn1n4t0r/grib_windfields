from eccodes import *
from datetime import datetime
from datetime import timedelta
from DataGeneration.GaussianGrid import *
from Utils.ProgressBar import ProgressBar
#import multiprocessing
import queue
from copy import deepcopy
import time


class GribMessage:
    def __init__(self, gid):
        self.min_lon = codes_get(gid, 'longitudeOfFirstGridPointInDegrees')
        self.max_lon = codes_get(gid, 'longitudeOfLastGridPointInDegrees')
        self.min_lat = codes_get(gid, 'latitudeOfFirstGridPointInDegrees')
        self.max_lat = codes_get(gid, 'latitudeOfLastGridPointInDegrees')
        #self.bbox = BoundingBox(min_lon, min_lat, max_lon, max_lat)

        self.num_latitudes = codes_get(gid, 'Nj')
        self.points_indices_longitude = codes_get_array(gid, 'pl')
        self.num_data_points = codes_get(gid, 'numberOfDataPoints')

        self.init_day = codes_get(gid, 'dataDate')
        self.init_time = codes_get(gid, 'dataTime')
        self.step_range = codes_get(gid, 'startStep')

        self.short_name = codes_get(gid, 'shortName')
        # name = codes_get(gid, 'cfVarName')
        # longName = codes_get(gid, 'name')
        # valueSize = codes_get_size(gid, 'values')
        self.grid_type = codes_get(gid, 'gridType')
        self.is_octahedral = codes_get_array(gid, 'isOctahedral')
        self.values = codes_get_values(gid)

        # read time step
        self.timestep = GribMessageParser.read_timestep(self.init_day,
                                                        self.init_time,
                                                        self.step_range)

    # only update values if grid type is the same
    def update(self, gid):
        self.values = codes_get_values(gid)
        self.short_name = codes_get(gid, 'shortName')

        self.init_day = codes_get(gid, 'dataDate')
        self.init_time = codes_get(gid, 'dataTime')
        self.step_range = codes_get(gid, 'startStep')

        # read time step
        self.timestep = GribMessageParser.read_timestep(self.init_day,
                                                        self.init_time,
                                                        self.step_range)


# Parse grib file messages and converts each data to a grid
# Converts grib message to (Gaussian/Any type) grid
class GribMessageParser:
    def __init__(self, assume_same_grid=True):
        self.file = None
        self.assume_same_grid = assume_same_grid
        self.bbox = None
        self.desc = None
        self.num_processed_files = 0
        self.progressBar = None
        self.filename = None
        self.num_messages = 0
        self.num_processed_messages = 0
        self.precomputed_lonlat_points = None

    # read file and obtain general information
    def read_file(self, filename):
        self.file = open(filename, 'rb')
        self.filename = filename
        print("[INFO]: Processing grib file: {}".format(filename))
        self.num_messages = codes_count_in_file(self.file)
        print("[INFO]: Number of grib messages: {}".format(self.num_messages))
        self.progressBar = ProgressBar(self.num_messages, displaySumCount=True)

    # obtain list of all messages (message ids)
    def get_message_queue(self, assume_same_grid=True):
        #message_queue = queue.Queue()
        message_list = []
        #messages = [None for _ in range(self.num_messages)]
        all_same_grid_type = True
        grid_type = None

        global_message = None
        while True:
            # get next message
            gid = codes_grib_new_from_file(self.file)
            if gid is None:
                codes_close_file(self.file)
                break
            # create message object --> read most important information
            if global_message is None or not assume_same_grid:
                global_message = GribMessage(gid)
            else:
                global_message = deepcopy(global_message)
                global_message.update(gid)
            # check if all messages contain grids of the same type
            if grid_type is None:
                grid_type = global_message.grid_type
            else:
                all_same_grid_type &= global_message.grid_type != grid_type
            #messages[int(self.num_processed_messages)] = global_message
            #message_queue.put(global_message)
            message_list.append(global_message)

            # release memory
            codes_release(gid)

            self.num_processed_messages += 1
            self.progressBar.proceed(self.num_processed_messages)

        return message_list, all_same_grid_type

    @staticmethod
    def read_timestep(init_day, init_time, step_range):
        if init_time == 0:
            init_time = str("0000")

        time_str = str(init_day) + " " + str(init_time)

        date_time_obj = datetime.strptime(time_str, "%Y%m%d %H%M")
        date_time_obj += timedelta(hours=step_range)
        date_key = str(date_time_obj)[:-6]
        return date_key
