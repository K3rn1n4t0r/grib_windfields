import numpy as np


def computeLNorm(inputGrid, targetGrid, norm=2):
    valuesI = inputGrid.values
    valuesT = targetGrid.values

    np.linalg.norm(valuesT - valuesI, norm)
