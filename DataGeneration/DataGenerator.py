from DataGeneration.GridPadder import *
from DataGeneration.GribParser import *

# from collections import namedtuple
#
# dataGenOptions = namedtuple('DataOptions', ['padde', 'lrOroName', 'hrDataName', 'hrOroName'])

# import threading
#
#
# class readMessageThread(threading.Thread):
#     def __init__(self, ID, reader):
#         threading.Thread.__init__(self)
#         self.ID = ID
#         self.reader = reader
#
#     def run(self):
#         print("Thread")
#
#         if self.gid is None:
#             return


class DataGenerator:
    def __init__(self, filename, padding=False, paddingShape=None):
        self.filename = filename
        self.padding = padding
        self.paddingShape = paddingShape
        self.gridSet = GridSet()

        # read file
        self.reader = GribReader()
        self.reader.readFile(self.filename)

    def generate(self):
        if self.padding:
            self._readAndPad()
        else:
            self._read()

        return self.gridSet

    def _read(self):
        while True:
            grid, timestep = self.reader.getNextGridAndTimestep()

            # read message by message
            if grid is None:
                break

            self.gridSet.addGridAtTimestep(timestep, grid)

    def _readAndPad(self):
        #counter = 0

        while True:
            grid, timestep = self.reader.getNextGridAndTimestep()

            # read message by message
            if grid is None:
                break

            # pad to output shape
            padder = GridPadder(grid)
            paddedGrid = padder.padToSize(self.paddingShape)

            self.gridSet.addGridAtTimestep(timestep, paddedGrid)

            #counter += 1
