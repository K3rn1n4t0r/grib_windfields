import os
import sys
from DataGeneration import DataPreprocessor
from Training.Parser import ArgParser
from DataGeneration.GribReader import GribReader
from DataGeneration.GaussianGrid import GridSet
from DataGeneration.SimulationDataGenerator import SimulationDataGenerator
from DataGeneration.DataExporter import DataExporter
# temp
import datetime
import numpy as np

# def checkTime(arr):
#     mask = np.full(arr.shape, False, dtype=bool)
#
#     for i in range(len(arr)):
#         timestep = arr[i]
#         date = datetime.datetime.strptime(timestep, "%Y-%m-%d %H")
#
#         if date.day % 2 == 0:
#             mask[i] = True
#
#     return mask

# f = np.load('data/training_new/input.npz')
#
# vtimes = f['valid_times']
# indices = np.argwhere(checkTime(vtimes))
# windsU = f['windU']
# windsV = f['windV']
# reducedU = windsU[indices]
# reducedV = windsV[indices]
#
# print(indices)


parser = ArgParser()
parser.parse(sys.argv[1:])
configOptions = parser.options

rawFilesBaseDir = '/home/hoehlein/data/grib'

area_mapping = {"area1": "alps", "area2": "canada"}

rawFilenames = {'LRES': os.path.join(rawFilesBaseDir, "ERA5_predictand_vars_area1_2018.grib"),
                'LRES_Sfc': os.path.join(rawFilesBaseDir, "ERA5_Auxil_Fixed_SurfaceVars_area1.grib"),
                'HRES': os.path.join(rawFilesBaseDir, "HRES_100m_wind_cpts_area1_2018.grib"),
                'HRES_Sfc': os.path.join(rawFilesBaseDir, "HRES_Auxil_Fixed_SurfaceVars_area1_2017_2018.grib")}

directories = {'data': {'training': os.path.join('data', 'training'),
                        'test': os.path.join('data', 'test'),
                        'raw': os.path.join('data', 'raw')}
               }

paddingHRES = (180, 144)
paddingLRES = (60, 36)

# print("Raw files::\n===================")
# printConfigs(rawFilenames)
# print("===================")

# generate raw data with plain timestep x point data array
def generateRawData(outDir, gridMapInput, gridMapTarget, inputFilename, outputFilename,
                    filterRange=None, outputComplex=True):
    filesArray = [[rawFilenames['LRES'], rawFilenames['LRES_Sfc']],
                  [rawFilenames['HRES'], rawFilenames['HRES_Sfc']]]

    filenames = [inputFilename, outputFilename]

    for f in range(len(filesArray)):
        files = filesArray[f]
        filename = filenames[f]
        # load data
        # cache name
        rawFilename = os.path.splitext(files[0])[0]
        preprocessor = DataPreprocessor(files,  # input filenames
                                        [gridMapInput, gridMapTarget],
                                        rawFilename + "_raw")  # for caching

        outFilename = os.path.basename(rawFilename) + "_packed_raw"
        preprocessor.exportEntireSet(outDir, outFilename,
                             [['windU', 'windV'], ['z', 'seaMask']],  # used grids
                             ['winds', ''],  # name for values ('' = no concatenation
                             filterRange=filterRange,) # filter range


        if outputComplex:
            preprocessor.exportEntireSet(outDir,"{}_complex".format(outFilename),
                                         [['windU', 'windV', 'complex'], ['z', 'seaMask']],  # used grids
                                         ['winds', ''],  # name for values ('' = no concatenation
                                         filterRange=filterRange,  # filter range
                                         )


def generateNetworkData(outDir, gridMapInput, gridMapTarget,
                        inputFilename, outputFilename,
                        exportGridsArray,
                        filterRange=None, paddingEnabled=False, paddingShapes=[]):

    filesArray = [[rawFilenames['LRES'], rawFilenames['LRES_Sfc']],
                  [rawFilenames['HRES'], rawFilenames['HRES_Sfc']]]

    filenames = [inputFilename, outputFilename]

    paddingOpts = [paddingEnabled, None]

    for f in range(len(filesArray)):
        files = filesArray[f]
        filename = filenames[f]
        outNames = [filenames, filename + "_orography"]
        exportGrids = exportGridsArray[f]

        assert(len(files) == len(exportGrids))

        if paddingEnabled:
            paddingOpts[1] = paddingShapes[f]
        # load data
        # cache name
        rawFilename = os.path.splitext(files[0])[0]
        preprocessor = DataPreprocessor(files,  # input filenames
                                        [gridMapInput, gridMapTarget],
                                        rawFilename + "_padded",  # for caching
                                        paddingOpts=paddingOpts)

        outFilename = os.path.basename(rawFilename) + "_packed"
        if paddingEnabled:
            outFilename += "_padded"
        preprocessor.exportEntireSet(outDir, outFilename,
                                     exportGrids,  # used grids
                                     ['' for _ in range(len(filename))],  # name for values ('' = no concatenation
                                     filterRange=filterRange,  # filter range
                                     )
        #preprocessor.exportDaily(outDir, outNames, exportGrids, filterRange)

import h5py
import numpy as np
from enum import Enum


class ExportMode(Enum):
    HDF5 = 1
    NPZ = 2


class TrainingDataGenerator:
    def __init__(self, path_to_data, grid_mappings, grid_mappings_sfc):
        self.path_to_data = path_to_data
        self.grid_mappings = grid_mappings
        self.grid_mappings_sfc = grid_mappings_sfc
        self.generator_set = {}
        # obtain all files from path_to_data

    def clear(self):
        self.generator_set = {}

    def create_data(self, out_dir, out_filename, strfmt_wind_data, strfmt_wind_data_sfc,
                    min_date, max_date, areas, padding=False, padding_shape=(24, 24), save_per_year=True):
        years = np.arange(min_date.year, max_date.year + 1, 1)

        for area in areas:
            files_data = []
            files_sfc = []
            area_mapped = area_mapping[area]

            sfc_filename = os.path.join(self.path_to_data, strfmt_wind_data_sfc.format(area))
            if os.path.isfile(sfc_filename):
                files_sfc.append(sfc_filename)
            else:
                print("[WARNING]: surface grib file <{}> not found! File is skipped".format(sfc_filename))
            for year in years:
                data_filename = os.path.join(self.path_to_data, strfmt_wind_data.format(area, year))
                if not os.path.isfile(data_filename):
                    print("[WARNING]: grib file <{}> not found! File is skipped".format(data_filename))
                    continue

                if save_per_year:
                    min_date_year = datetime.datetime.strptime('{}-01-01'.format(year), '%Y-%m-%d')
                    max_date_year = datetime.datetime.strptime('{}-12-31'.format(year), '%Y-%m-%d')
                    yearly_min_date = max(min_date, min_date_year)
                    yearly_max_date = min(max_date, max_date_year)

                    self._filter_and_export(data_filename, files_sfc, padding, padding_shape,
                                            yearly_min_date, yearly_max_date, out_dir, out_filename, area_mapped, year)

                else:
                    files_data.append(data_filename)

            if len(files_data) > 0:
                self._filter_and_export(files_data, files_sfc, padding, padding_shape,
                                        min_date, max_date, out_dir, out_filename, area_mapped)

    def _filter_and_export(self, files_data, files_sfc, padding, padding_shape, min_date, max_date,
                           out_dir, out_filename, area, year=None):
        num_file = 0
        for file_data in files_data:
            grid_set_data, grid_set_sfc = self.create_simulation_data([file_data], files_sfc, padding, padding_shape)
            exporter = DataExporter([grid_set_data, grid_set_sfc], self.grid_mappings, self.grid_mappings_sfc, area)
            print("[INFO]: Filter and prepare data for time range: ({} - {})"
                  .format(min_date.strftime("%Y-%m-%d"), max_date.strftime("%Y-%m-%d")))
            self.filter_data(exporter, min_date, max_date)

            filename = out_filename + "_{}".format(area)
            if year:
                filename = out_filename + "_{}".format(year)
            if num_file == 0:
                print("[INFO]: Export data to file <{}>".format(filename))
                self.export_data(exporter, out_dir, filename, mode=ExportMode.HDF5)
            else:
                self.append_data(exporter, out_dir, filename, mode=ExportMode.HDF5)

            num_file += 1

    def create_simulation_data(self, filenames, filenames_sfc, padding=False, padding_shape=(24, 24)):
        if not isinstance(filenames, (list, tuple)):
            filenames = [filenames]

        if not isinstance(filenames_sfc, (list, tuple)):
            filenames_sfc = [filenames_sfc]

        grid_set_data = self.create_grid_set(filenames, padding, padding_shape, is_static=False)
        grid_set_sfc = self.create_grid_set(filenames_sfc, padding, padding_shape, is_static=True)

        return grid_set_data, grid_set_sfc

    def create_grid_set(self, filenames, padding, padding_shape, is_static):
        grid_set = GridSet()

        for filename in filenames:
            # if filename not in self.generator_set:
            #     self.generator_set[filename] = SimulationDataGenerator(filename=filename, is_static=is_static)
            simulation_generator = SimulationDataGenerator(filename=filename, is_static=is_static)

            # simulation_generator = self.generator_set[filename]
            if padding:
                simulation_generator.create_padded_data(padding_shape, grid_set)
            else:
                simulation_generator.create_raw_data(grid_set)

        return grid_set

    def filter_data(self, exporter, min_date, max_date):
        assert(isinstance(exporter, DataExporter))
        exporter.prepare_and_filter_data(min_date, max_date)

    def export_data(self, exporter, out_dir, out_filename, mode=ExportMode.HDF5):
        assert (isinstance(exporter, DataExporter))
        assert(isinstance(mode, ExportMode))

        if not os.path.exists(out_dir):
            os.makedirs(out_dir)

        if mode == ExportMode.HDF5:
            exporter.export_variables_to_hdf5(out_dir, out_filename)
        elif mode == ExportMode.NPZ:
            raise NotImplementedError()
        else:
            raise NotImplementedError()

    def append_data(self, exporter, out_dir, out_filename, mode=ExportMode.HDF5):
        assert (isinstance(exporter, DataExporter))
        assert (isinstance(mode, ExportMode))

        if not os.path.exists(out_dir):
            os.makedirs(out_dir)

        if mode == ExportMode.HDF5:
            exporter.append_variables_to_hdf5(out_dir, out_filename)
        elif mode == ExportMode.NPZ:
            raise NotImplementedError()
        else:
            raise NotImplementedError()


if __name__ == "__main__":
    # test h5py
    # f = h5py.File('test.hdf5', 'r')
    # print(f.keys())
    # dyn = f['dynamic_variables']
    # print(dyn.keys())
    # print(dyn['100u'][()])
    # stat = f['dynamic_variables']

    configOpts = {
        "gridMappings":
            {"100u": "windU", "100v": "windV"},
        "gridMappingsSfc":
            {"z": "zOro", "lsm": "seaMask"},
        "pathData": "/home/kerninator/Uni/data/wind",
        "buildPath": "data/training_h5_new",
        "padding": True,
        "paddingShapeLR": (60, 36),
        "paddingShapeHR": (180, 144),
        "minDatetime": "2016-01-01",
        "maxDatetime": "2020-01-01",
        "export": "hdf5"  # "npz"
    }

    minDatetime = datetime.datetime.strptime(configOpts['minDatetime'], '%Y-%m-%d')
    maxDatetime = datetime.datetime.strptime(configOpts['maxDatetime'], '%Y-%m-%d')

    training_gen = TrainingDataGenerator(configOpts['pathData'], configOpts['gridMappings'],
                                         configOpts['gridMappingsSfc'])
    # training_gen.create_data(configOpts['buildPath'], 'input',
    #                          'ERA5_predictand_vars_{}_{}.grib', 'ERA5_Auxil_Fixed_SurfaceVars_{}.grib',
    #                          minDatetime, maxDatetime, areas=['area1', 'area2'],
    #                          padding=configOpts['padding'], padding_shape=configOpts['paddingShapeLR'],
    #                          save_per_year=False)
    # remove old grid sets to free memory
    # training_gen.clear()
    training_gen.create_data(configOpts['buildPath'], 'target',
                             'HRES_100m_wind_cpts_{}_{}.grib', 'HRES_Auxil_Fixed_SurfaceVars_{}_2017_2018.grib',
                             minDatetime, maxDatetime, areas=['area1', 'area2'],
                             padding=configOpts['padding'], padding_shape=configOpts['paddingShapeHR'],
                             save_per_year=False)


    # simulationData = SimulationDataGenerator(
    #     filename="/home/kerninator/Uni/data/wind/ERA5_predictand_vars_area2_2017.grib")
    # simulationData2 = SimulationDataGenerator(
    #     filename="/home/kerninator/Uni/data/wind/ERA5_predictand_vars_area2_2018.grib")
    #
    # grid_set = GridSet()
    # simulationData.create_padded_data(configOpts['paddingShapeLR'], grid_set)
    # simulationData2.create_padded_data(configOpts['paddingShapeLR'], grid_set)
    #
    # exporter = DataExporter(grid_set, configOpts['gridMappings'])
    # exporter.prepare_and_filter_data(datetime.datetime.strptime(configOpts['minDatetime'], '%Y-%m-%d'),
    #                                  datetime.datetime.strptime(configOpts['maxDatetime'], '%Y-%m-%d'))
    # exporter.export_variables_to_hdf5(configOpts['pathData'], 'test')
    #
    # f = h5py.File('test.hdf5', 'r')
    # print(f.keys())
    # dyn = f['dynamic_variables']
    # print(dyn.keys())
    # print(dyn['100u'][()])
    # stat = f['dynamic_variables']
    # t = f['valid_times']
    # print(t)
    # print(t[0].decode('utf-8'))
    # for att in f.attrs:
    #     print(f.attrs[att])

    # reader = GribReader(filename="/home/kerninator/Uni/data/wind/ERA5_predictand_vars_area2_2017.grib")
    # reader.read()

    # years = [2017, 2018]
    # areas = ['area1', 'area2']
    #
    # for year in years:
    #     for area in areas:
    #         rawFilenames = {'LRES': os.path.join(rawFilesBaseDir, "ERA5_predictand_vars_{}_{}.grib".format(area, year)),
    #                         'LRES_Sfc': os.path.join(rawFilesBaseDir, "ERA5_Auxil_Fixed_SurfaceVars_{}.grib".format(area)),
    #                         'HRES': os.path.join(rawFilesBaseDir, "HRES_100m_wind_cpts_{}_{}.grib".format(area, year)),
    #                         'HRES_Sfc': os.path.join(rawFilesBaseDir, "HRES_Auxil_Fixed_SurfaceVars_{}_2017_2018.grib".format(area))}
    #
    #         print("[INFO]: Process datasets:")
    #         print(rawFilenames)
    #
    #         generateRawData(directories['data']['raw'],
    #                         configOptions['preprocess']['gridMapInput'],
    #                         configOptions['preprocess']['gridMapSfc'],
    #                         'input_raw', 'target_raw',
    #                         None,
    #                         #configOptions['preprocess']['monthsTraining'],
    #                         outputComplex=False)
    #
    #         generateNetworkData(directories['data']['training'],
    #                             configOptions['preprocess']['gridMapInput'],
    #                             configOptions['preprocess']['gridMapSfc'],
    #                             'input', 'target',
    #                             [[configOptions['preprocess']['exportGridsInput'],
    #                              configOptions['preprocess']['exportGridsSfc']],
    #                              [configOptions['preprocess']['exportGridsTarget'],
    #                              configOptions['preprocess']['exportGridsSfc']]],
    #                             filterRange=None,
    #                             #filterRange=configOptions['preprocess']['monthsTraining'],
    #                             paddingEnabled=configOptions['preprocess']['padding'],
    #                             paddingShapes=[paddingLRES, paddingHRES])

