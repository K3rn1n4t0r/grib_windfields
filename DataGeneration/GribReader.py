from DataGeneration.GribParser import GribMessageParser
from DataGeneration.GaussianGrid import GridSet
import re
import queue
import multiprocessing
from Utils.ProgressBar import ProgressBar


area_mapping = {"area1": "alps", "area2": "canada"}


class GribReader:
    def __init__(self, filename):
        self.filename = filename
        self.grid_set = GridSet()
        self.area = area_mapping[re.search(r"(area[0-9])", filename).group(0)]

        # parser to parse message
        self.parser = GribMessageParser()
        # read grib file and obtain general information
        self.parser.read_file(self.filename)

    def obtain_messages(self):
        message_queue, all_same_grid_type = self.parser.get_message_queue()

        if all_same_grid_type:
            print("[WARNING]: Fields do not have the same grid type")

        return message_queue

        # TODO python threads for parallelization
        #queue_messages = multiprocessing.JoinableQueue()
        # set up queue to collect all message ids
        # messages, all_same_grid_type = self.parser.get_messages()
        # for message in messages:
        #     queue_messages.put(message)

        # set up threads
        # num_threads = 1  # multiprocessing.cpu_count()
        # num_processed = 0
        # mutex = multiprocessing.Lock()

        # progressBar = ProgressBar(len(messages), displaySumCount=True)

        # threads = []
        # for i in range(num_threads):
        #     t = multiprocessing.Process(target=self.parser.get_next_grid, args=(queue_messages, self.grid_set,
        #                                                                         mutex, progressBar,
        #                                                                         num_processed, i))
        #     # t.daemon = True
        #     threads.append(t)
        #     t.start()
        #
        # # block main thread until queue is empty
        # queue_messages.join()

        #print(self.grid_set.grids)

        # for t in threads:
        #     t.join()
