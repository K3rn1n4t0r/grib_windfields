from DataGeneration.DataExporter import DataExporter
from DataGeneration.DataGenerator import DataGenerator


def loadAndExport(inputFile, outFile, ourDirTraining, outDirTest, monthRangeTraining, monthRangeTest,
                     gridMapping, padding=False, paddingShape=None, formatTime=True):
    generator = DataGenerator(inputFile, padding=padding, paddingShape=paddingShape)
    grids = generator.generate()
    exporter = DataExporter(grids, gridMapping)
    exporter.exportMonths(ourDirTraining, outFile, monthRangeTraining, formatTime=formatTime)
    exporter.exportMonths(outDirTest, outFile, monthRangeTest, formatTime=formatTime)


def loadAndExportDataAndSfc(inputFiles, outFile, outDir,
                            gridMappings, varLists, valueNames,
                            isComplex=False, paddingOpts=[False, None]):
    grids = []

    for file in inputFiles:
        generator = DataGenerator(file, padding=paddingOpts[0], paddingShape=paddingOpts[1])
        grids += [generator.generate()]

    exporter = DataExporter(grids, gridMappings)
    exporter.exportVarsForEntireDataSet(outDir, outFile, varLists, valueNames)

import pickle
import os

class DataPreprocessor:
    def __init__(self, inputFiles, gridMappings, cacheFile, paddingOpts=[False, None]):
        if os.path.isfile(cacheFile + ".pkl"):
            obj = self.load(cacheFile)
            self.__dict__ = obj.__dict__

        else:
            print("[INFO]: Load grib files")
            self.grids = []
            self.gridMappings = gridMappings
            self.exporter = DataExporter(self.grids, self.gridMappings)

            for file in inputFiles:
                generator = DataGenerator(file, padding=paddingOpts[0], paddingShape=paddingOpts[1])
                self.grids += [generator.generate()]
            self.save(cacheFile)

    def save(self, filename):
        file = open(filename + ".pkl", 'wb')
        pickle.dump(self, file, pickle.HIGHEST_PROTOCOL)
        file.close()

    def load(self, filename):
        with open(filename + ".pkl", 'rb') as file:
            return pickle.load(file)

    def exportEntireSet(self, outDir, outFile, varLists, valueNames, filterRange=None):
        self.exporter.exportVarsForEntireDataSet(outDir, outFile, varLists, valueNames, filterRange)

    def exportDaily(self, outDir, outFile, filterRange, formatTime=True):
        self.exporter.exportDaily(outDir, outFile, filterRange, formatTime=formatTime)
