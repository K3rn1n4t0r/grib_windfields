import os
import datetime
import sys
import numpy as np
from DataGeneration.IOUtils import createTimestepStr
from DataGeneration.GaussianGrid import *
import h5py


class DataExporter:
    def __init__(self, grid_sets, grid_mappings, grid_mappings_sfc, area):
        if type(grid_sets) != list:
            self.grid_sets = [grid_sets]
        else:
            self.grid_sets = grid_sets

        assert(isinstance(grid_mappings, dict))
        self.grid_mappings = grid_mappings

        assert (isinstance(grid_mappings, dict))
        self.grid_mappings_sfc = grid_mappings_sfc

        self.version = 2.0
        self.static_variables = None
        self.dynamic_variables = None
        self.date_times = None
        self.area = area

    def prepare_and_filter_data(self, min_date, max_date):
        assert (isinstance(min_date, datetime.datetime))
        assert (isinstance(max_date, datetime.datetime))

        static_variables = {}
        dynamic_variables = {}

        self.date_times = []

        for s in range(len(self.grid_sets)):
            grid_set = self.grid_sets[s]
            # check if grid set is static (only one timestep)
            is_static = 'static' in grid_set.timesteps and len(grid_set.timesteps) == 1

            if is_static:
                filtered_timesteps = grid_set.timesteps
            else:
                filtered_timesteps = list(filter(
                    lambda x: min_date < datetime.datetime.strptime(x, "%Y-%m-%d %H") < max_date,
                    grid_set.timesteps))

            for t in filtered_timesteps:
                # update list of date times
                if t not in self.date_times and t != 'static':
                    self.date_times.append(t)

                grids = grid_set.getGridsAtTimestep(t)
                # date = datetime.datetime.strptime(t, "%Y-%m-%d %H")

                for grid_name in grids:
                    grid = grids[grid_name]
                    grid_desc = grid.desc

                    if 'lons' not in static_variables:
                        static_variables['lons'] = grid_desc.lons
                    if 'lats' not in static_variables:
                        static_variables['lats'] = grid_desc.lats
                    # for padded data, we also have the mask as static information
                    if isinstance(grid, RegularPaddedGrid):
                        if 'padding_mask' not in static_variables:
                            static_variables['padding_mask'] = grid.mask

                    # for orography
                    var_name = grid_name
                    # mapping original names to unique names
                    if is_static:
                        if var_name in self.grid_mappings_sfc:
                            var_name = self.grid_mappings_sfc[grid_name]
                    else:
                        if var_name in self.grid_mappings:
                            var_name = self.grid_mappings[grid_name]

                    if is_static:
                        if var_name not in static_variables:
                            static_variables[var_name] = grid.values
                        else:
                            print("[WARNING] duplicated grids found with variable <{}>".format(grid_name))
                    else:
                        if var_name not in dynamic_variables:
                            dynamic_variables[var_name] = (grid.values,)
                        else:
                            dynamic_variables[var_name] += (grid.values,)

        self.static_variables = static_variables
        self.dynamic_variables = dynamic_variables
        self.date_times = np.array(self.date_times, dtype=h5py.string_dtype(encoding='utf-8'))
        print("[INFO]: dynamic variables found: {}".format(self.dynamic_variables.keys()))
        print("[INFO]: static variables found: {}".format(self.static_variables.keys()))
        print("[INFO]: num timesteps: {}".format(len(self.date_times)))

    def export_variables_to_hdf5(self, out_dir, out_filename):
        f = h5py.File('{}.hdf5'.format(os.path.join(out_dir, out_filename)), 'w')

        static_group = f.create_group('static_variables')
        dynamic_group = f.create_group('dynamic_variables')

        # version of hdf5 file
        f.attrs.create('version', self.version)
        # area
        f.attrs.create('area', self.area)

        # information about data mode (raw, complex, padded)
        if 'padding_mask' in self.static_variables:
            f.attrs.create('mode', u'padded_at_boundaries')
        else:
            f.attrs.create('mode', u'raw')

        # valid_times for data
        data_set = f.create_dataset('valid_times', self.date_times.shape, data=self.date_times,
                                    maxshape=(None,), chunks=(1, ), dtype=h5py.string_dtype(encoding='utf-8'))

        for var in self.static_variables:
            values = np.array(self.static_variables[var], dtype=np.float32)
            data_set = static_group.create_dataset(var, values.shape, dtype='f', data=values,
                                                   chunks=values.shape,
                                                   maxshape=(None, None))
            data_set.attrs.create('data_shape', list(values.shape))

        for var in self.dynamic_variables:
            values = np.array(self.dynamic_variables[var], dtype=np.float32)
            chunk_size = (1,) + values.shape[1:] if len(values.shape) == 3 else values.shape
            data_set = dynamic_group.create_dataset(var, values.shape, chunks=chunk_size, dtype='f', data=values,
                                                    maxshape=(None,) + values.shape[1:])
            data_set.attrs.create('data_shape', list(values.shape))
            data_set.attrs.create('chunk_size', list(chunk_size))

        f.close()

    def append_variables_to_hdf5(self, out_dir, out_filename):
        f = h5py.File('{}.hdf5'.format(os.path.join(out_dir, out_filename)), mode='a')
        print(f.keys())

        static_group = f['static_variables']
        dynamic_group = f['dynamic_variables']

        # append valid_times to data
        #data_set = f.create_dataset('valid_times', self.date_times.shape, data=self.date_times)
        valid_times_set = f['valid_times']
        old_shape = valid_times_set.shape[0]
        new_shape = self.date_times.shape[0]
        test = valid_times_set[()]
        print(test.shape)
        valid_times_set.resize((old_shape + new_shape,))
        valid_times_set[old_shape:] = self.date_times
        test2 = valid_times_set[()]
        print(test2.shape)

        for var in self.static_variables:
            if var not in static_group.keys():
                values = np.array(self.static_variables[var], dtype=np.float32)
                data_set = static_group.create_dataset(var, values.shape, dtype='f', data=values)
                data_set.attrs.create('data_shape', list(values.shape))

        for var in self.dynamic_variables:
            values = np.array(self.dynamic_variables[var], dtype=np.float32)
            #chunk_size = (1,) + values.shape[1:] if len(values.shape) == 3 else values.shape
            #data_set = dynamic_group.create_dataset(var, values.shape, chunks=chunk_size, dtype='f', data=values)
            #data_set.attrs.create('data_shape', list(values.shape))
            #data_set.attrs.create('chunk_size', list(chunk_size))
            data_set = dynamic_group[var]
            old_shape = data_set.shape[0]
            new_shape = values.shape[0]
            # test = data_set[()]
            # print(test.shape)
            data_set.resize((old_shape + new_shape,) + data_set.shape[1:])
            data_set[old_shape:] = values
            # test2 = data_set[()]
            # print(test2.shape)

        f.close()

        # print(f.keys())
        # data_set = f.create_dataset("first_data", (100,), dtype='f', data=np.random.random((100,)))
        # print(f.keys())
        # print(data_set.value)

# class DataExporter:
#     def __init__(self, gridSets, gridMappings, mode='entire'):
#         if type(gridSets) != list:
#             self.gridSets = [gridSets]
#         else:
#             self.gridSets = gridSets
#
#         if type(gridMappings) != list:
#             self.gridMappings = [gridMappings]
#         else:
#             self.gridMappings = gridMappings
#
#         self.mode = mode
#         self.version = 1.1
#
#     def __checkTimestep(self, timestep, range, year=True, month=True, day=False):
#         date = datetime.datetime.strptime(timestep, "%Y-%m-%d %H")
#
#         if range is None:
#             return True
#
#         if year and date.year != range[2]:
#             return False
#
#         if month and (date.month < range[0] or date.month > range[1]):
#             return False
#
#         if day:
#             raise NotImplementedError("Daily filtering is not implemented")
#
#         return True
#
#     def exportVarsForEntireDataSet(self, outDir, outName, varLists, valueNames, filterRange=None):
#         if not os.path.exists(outDir):
#             os.makedirs(outDir)
#
#         # description of longitudes / latitudes
#         lons = []
#         lats = []
#         timesteps = []
#         # dictionary for npz export
#         npzDict = {}
#         mask = None
#
#         for s in range(len(self.gridSets)):
#             gridSet = self.gridSets[s]
#             gridMapping = self.gridMappings[s]
#             valueName = valueNames[s]
#             varList = varLists[s]
#             concatenateArrays = len(valueName) != 0
#
#             # set up list of values
#             #dayValueArr = [[] for _ in range(numFiles)]
#             dayValueTuple = ()
#             numValues = 0
#             convertToComplex = False
#
#             # filter timesteps by year, month, day
#             filteredTimesteps = list(filter(lambda x: self.__checkTimestep(x, filterRange), gridSet.timesteps))
#
#             # number of files
#             numFiles = len(filteredTimesteps)
#             # fill timestep array for export
#             if len(timesteps) == 0:
#                 timesteps = filteredTimesteps
#
#             # check if we should convert to complex values
#             convertToComplex = 'complex' in varList
#             if convertToComplex:
#                 varList.remove('complex')
#                 assert (len(varList) == 2)
#                 assert (len(valueName) != 0)
#                 concatenateArrays = True
#
#             # iterate over all files
#             for f in range(numFiles):
#             #for timestep in self.gridSet.timesteps:
#                 timestep = filteredTimesteps[f]
#
#                 grids = gridSet.grids[timestep]
#
#                 # obtain number of values an additional information
#                 if f == 0 and len(lons) == 0 and len(lats) == 0:
#                     keys = list(gridMapping.keys())
#                     gridTemp = grids[gridMapping[keys[0]]]
#                     numValues = len(gridTemp.values)
#                     lons = gridTemp.desc.lons
#                     lats = gridTemp.desc.lats
#                     if isinstance(gridTemp, RegularPaddedGrid):
#                         mask = gridTemp.mask
#
#                 valueArr = np.array([])
#
#                 if convertToComplex:
#                     valueArr = np.array([complex(0) for _ in range(numValues)])
#                     realPartGrid = grids[gridMapping[varList[0]]]
#                     imPartGrid = grids[gridMapping[varList[1]]]
#                     for i in range(numValues):
#                         realPart = realPartGrid.values[i]
#                         imPart = imPartGrid.values[i]
#
#                         valueArr[i] = complex(realPart, imPart)
#
#                     dayValueTuple += (valueArr,)
#                 else:
#                     for v in range(len(varList)):
#                         var = varList[v]
#
#                         if var not in gridMapping:
#                             print("[WARNING]: {} could not be found in dataset")
#                             continue
#
#                         grid = grids[gridMapping[var]]
#
#                         if concatenateArrays:
#                             valueArr = np.concatenate((valueArr, grid.values), axis=0)
#                         else:
#                             if var not in npzDict:
#                                 if numFiles == 1:
#                                     npzDict[var] = grid.values
#                                 else:
#                                     npzDict[var] = (grid.values,)
#                             else:
#                                 npzDict[var] += (grid.values, )
#
#                     if concatenateArrays:
#                         dayValueTuple += (valueArr,)
#
#                 progress = (f + 1) / float(numFiles)
#                 numChars = 30
#                 barProgress = int(numChars * progress)
#                 sys.stdout.write('\rProgress: |{}{}{}{}| {}%{}'.format('=' * barProgress,
#                                                                        '>' * int(progress < 1 and progress > 0),
#                                                                        '=' * int(progress == 1),
#                                                                        '.' * (numChars - barProgress),
#                                                                        int(progress * 100),
#                                                                        '\n' * int(progress == 1)))
#                 sys.stdout.flush()
#
#             # create numpy array
#             if concatenateArrays or convertToComplex:
#                 daysValueArray = np.stack(dayValueTuple)
#                 print(daysValueArray.shape)
#
#                 # output to compressed npz file
#                 npzDict[valueName] = daysValueArray
#             else:
#                 for var in npzDict:
#                     if isinstance(npzDict[var], tuple):
#                         npzDict[var] = np.array(npzDict[var])
#
#         npzDict.update({'lons': lons, 'lats': lats, 'valid_times': timesteps})
#         if mask is not None:
#             npzDict.update({'padding_mask': mask})
#
#         np.savez_compressed('{}/{}'.format(outDir, outName), **npzDict)
#
#     def exportDaily(self, outDir, outNames, varLists, monthRange=(0, 12, 2018), formatTime=True):
#         if not os.path.exists(outDir):
#             os.makedirs(outDir)
#
#         # testDict = {'windU': '100u', 'windV': '100v'}
#
#         for s in range(len(self.gridSets)):
#             gridSet = self.gridSets[s]
#             gridMapping = self.gridMappings[s]
#             outName = outNames[s]
#             varList = varLists[s]
#
#             filteredTimesteps = list(filter(lambda x: self.__checkTimestep(x, monthRange), gridSet.timesteps))
#             # skip filtering for sfc grids
#             if 'z' in gridMapping.keys():
#                 filteredTimesteps = gridSet.timesteps
#
#             for timestep in filteredTimesteps:
#                 # date = datetime.datetime.strptime(timestep, "%Y-%m-%d %H")
#
#                 # if date.year != monthRange[2] or date.month < monthRange[0] or date.month > monthRange[1]:
#                 #     continue
#
#                 validTime = createTimestepStr(timestep)
#                 grids = gridSet.grids[timestep]
#
#                 # define arrays to export in npz compressed zip format
#                 npzDict = {}
#
#                 keys = list(gridMapping.keys())
#
#                 # export grid (lons and lats)
#                 gridTemp = grids[gridMapping[keys[0]]]
#                 npzDict['lons'] = gridTemp.desc.lons
#                 npzDict['lats'] = gridTemp.desc.lats
#                 if isinstance(gridTemp, RegularPaddedGrid):
#                     npzDict['mask'] = gridTemp.mask
#
#                 for var in varList:
#                     grid = grids[gridMapping[var]]
#                     npzDict[var] = grid.values
#
#                 if formatTime:
#                     np.savez_compressed('{}/{}_{}'.format(outDir, outName, validTime), **npzDict)
#                 else:
#                     np.savez_compressed('{}/{}'.format(outDir, outName), **npzDict)


