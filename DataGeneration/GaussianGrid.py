import numpy as np
from enum import Enum
import collections

# 1) Simple padding with factor x.x
# 2) Regridding / resampling
# 3) Custom Grid convolution --> define mesh levels after convolution

# https://confluence.ecmwf.int/display/FCST/Gaussian+grids
class GridType(Enum):
    REGULAR_GAUSSIAN = 0  # Fxxx
    ORIG_RED_GAUSSIAN = 1  # Nxxx
    REDUCED_GAUSSIAN = 2  # Oxxx
    REGULAR_PADDED = 3


class BoundingBox:
    def __init__(self, minLon, minLat, maxLon, maxLat):
        self.minLon = minLon
        self.minLat = minLat
        self.maxLon = maxLon
        self.maxLat = maxLat


class GridDescriptor:
    def __init__(self, type, dimLat,
                       lons, lats,
                       dimLons,
                       deltaLons, deltaLats,
                       bbox):
        self.type = type
        self.dimLat = dimLat
        self.lons = np.array(lons)
        self.lats = np.array(lats)
        self.dimLons = dimLons
        self.deltaLons = np.array(deltaLons)
        self.deltaLats = np.array(deltaLats)
        self.bbox = bbox


class GaussianScalarField:
    def __init__(self, varname, values, descriptor):
        self.name = varname
        self.values = np.array(values)
        self.desc = descriptor

    def sampleAtPos(self, lon, lat):
        # get lon lat boundaries, assume lat is contained in the data
        idx, idxMax = self.getIDYAtLat(lat)

        idxLonN = idx
        lonN = 0

        while idxLonN <= idxMax:

            lonN = self.desc.lons[idxLonN]
            if lon >= lonN:
                break

            idxLonN += 1

        idxLonP = max(idx, idxLonN - 1)
        lonP = self.desc.lons(idxLonP)

        t = (lon - lonP) / (lonN - lonP)

        value = self.values[idxLonP] * (1 - t) + self.values[idxLonN] * t
        return value

    def sampleAtIndex(self, ilon, ilat):
        pass

    def getIDYAtLat(self, lat):
        try:
            ilat = self.desc.lats.index(lat)
            idx = 0
            for i in range(ilat):
                idx += self.desc.dimLons[i]

            dimLon = self.desc.dimLons[ilat]

            idxMax = idx + dimLon

            return idx, idxMax
        except ValueError as error:
            print("ERROR: could not find latitude {} in data").format(lat)
            return -1, -1

    @staticmethod
    def compute_grid_points(bbox, num_latitudes, points_indices_longitude, num_data_points):
        dim_lons = []
        latitudes = []
        longitudes = []

        delta_lat = (bbox.maxLat - bbox.minLat) / (num_latitudes - 1)
        deltas_lon = []

        # startPointID = 144
        num_points_total = 0

        for i in range(num_latitudes):
            num_lons_total = points_indices_longitude[i]  # int(ID * 4 + 16)
            cur_latitude = bbox.minLat + i * delta_lat

            delta_lon = 360.0 / num_lons_total
            deltas_lon += [delta_lon]

            num_longitudes = 0

            for p in range(num_lons_total):
                lon = -180.0 + p * delta_lon

                # fix for numerical rounding errors of floats
                # we know from data description that longitudinal point 0.0
                # is always present in the data
                if -delta_lon / 2 <= lon <= delta_lon / 2:
                    lon = 0

                # generate sequence of lon points
                # and count number of points within the bounding box
                # TODO: precompute sequence of lon-points per latitude
                if bbox.minLon <= lon <= bbox.maxLon:
                    latitudes += [cur_latitude]
                    longitudes += [lon]
                    num_longitudes += 1

            dim_lons += [num_longitudes]
            num_points_total += num_longitudes

        if num_points_total != num_data_points:
            raise Exception("Number of computed geo points ({})"
                            " does not match actual number of data points ({})!"
                            .format(num_points_total, num_data_points))

        return GridPoints(dim_lons, num_points_total, longitudes, latitudes, deltas_lon, delta_lat)


class GridPoints:
    def __init__(self, dim_lons, num_points_total, longitudes, latitudes, deltas_lon, delta_lat):
        self.dim_lons = dim_lons
        self.num_points_total = num_points_total
        self.longitudes = longitudes
        self.latitudes = latitudes
        self.deltas_lon = deltas_lon
        self.delta_lat = delta_lat


class RegularPaddedGrid:
    def __init__(self, varname, values, mask, descriptor):
        self.name = varname
        self.values = values
        self.mask = mask
        self.desc = descriptor


class GridSet:
    def __init__(self):
        self.timesteps = []
        self.vars = []
        self.grids = collections.OrderedDict()

    def addGridAtTimestep(self, timestep, grid):
        if timestep not in self.timesteps:
            self.timesteps += [timestep]

        if grid.name not in self.vars:
            self.vars += [grid.name]

        if timestep not in self.grids:
            self.grids[timestep] = {}

        self.grids[timestep][grid.name] = grid

    def getGridsAtTimestep(self, timestep):
        if timestep not in self.timesteps:
            raise Exception

        return self.grids[timestep]

    def getGridAtTimestep(self, timestep, name):
        if timestep not in self.timesteps:
            raise Exception

        if name not in self.vars:
            raise Exception

        return self.grids[timestep][name]




