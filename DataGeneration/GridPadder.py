from DataGeneration.GaussianGrid import *

class GridPadder:
    def __init__(self, grid):
        self.grid = grid

    def pad_to_size(self, shape):
        self.newDimX = shape[0]
        self.newDimY = shape[1]

        newValues = [] # 2D array of values with regular size
        maskValues = []
        newLons = []
        newLats = []

        x = 0
        for yy in range(self.newDimY):
            allValuesPadded = False

            if yy >= self.grid.desc.dimLat:
                allValuesPadded = True

            y = min(yy, self.grid.desc.dimLat - 1)
            x = min(x, len(self.grid.values) - self.grid.desc.dimLons[self.grid.desc.dimLat - 1])
            dimLon = self.grid.desc.dimLons[y]
            maxX = x + dimLon

            resDimX = self.newDimX - dimLon
            resDimXHalf = int(resDimX / 2)

            valuesX = []
            masksX = []
            lonsX = []
            latsX = []
            valuesX += [self.grid.values[x] for _ in range(resDimXHalf)]
            masksX += [1 for _ in range(resDimXHalf)]
            lonsX += [self.grid.desc.lons[x] for _ in range(resDimXHalf)]
            latsX += [self.grid.desc.lats[x] for _ in range(resDimXHalf)]

            while x < maxX:
                valuesX += [self.grid.values[x]]
                masksX += [1] if allValuesPadded else [0]
                latsX += [self.grid.desc.lats[x]]
                lonsX += [self.grid.desc.lons[x]]
                x += 1

            resDimX = self.newDimX - len(valuesX)
            valuesX += [self.grid.values[x - 1] for _ in range(resDimX)]
            masksX += [1 for _ in range(resDimX)]
            lonsX += [self.grid.desc.lons[x - 1] for _ in range(resDimX)]
            latsX += [self.grid.desc.lats[x - 1] for _ in range(resDimX)]

            # while len(valuesX) < self.newDimX:
            #     valuesX += [self.grid.values[maxX - 1]]

            newValues += [valuesX]
            maskValues += [masksX]
            newLons += [lonsX]
            newLats += [latsX]



        values2D = np.array(newValues)
        masks2D = np.array(maskValues)

        desc = GridDescriptor(GridType.REGULAR_PADDED, self.newDimY, newLons, newLats, self.newDimX,
                              self.grid.desc.deltaLons, self.grid.desc.deltaLats, self.grid.desc.bbox)

        # create new grid
        grid = RegularPaddedGrid(self.grid.name, values2D, masks2D, desc)

        return grid


def unpadToGrid(grid, outputGrid):
    values = np.delete(grid.values.flatten(), np.where(grid.mask.flatten() == 1))
    outputGrid.values = values


def unpadGrid(grid):
    grid.values = np.delete(grid.values.flatten(), np.where(grid.mask.flatten() == 1))

def unpadArrays(grids, mask):
    unpaddedGrids = []

    for grid in grids:
        unpaddedGrids += [np.delete(grid.flatten(), np.where(mask.flatten() == 1))]

    return unpaddedGrids
