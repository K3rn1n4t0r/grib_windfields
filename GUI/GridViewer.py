# README
# designer: sudo apt install qttools5-dev-tools
# ui to py: pyuic5 netviewer.ui > netviewer.py

from DataGeneration.GaussianGrid import *
from DataGeneration.GridPadder import unpadArrays
from GUI.GraphCreator import createVectorGraph
from Utils import computeBatchLoss

import sys
import os
from PyQt5.Qt import QMainWindow, QApplication
from PyQt5 import QtWidgets, QtCore
from PyQt5.QtCore import pyqtSlot
from GUI.netviewer import Ui_NetViewer
from GUI.GraphCreator import QuiverPlot
import torch
from DataPreprocessing import DataLoader as dl
from torch.nn import functional as F
from Networks.UNet import UNet

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.pyplot as plt

cudaAvailable = torch.cuda.is_available()
device = torch.device("cuda:0" if cudaAvailable else "cpu")


def loadVectorField(filename):
    file = np.load(filename)

    u = file['windU']
    v = file['windV']
    lons = file['lons']
    lats = file['lats']

    file.close()

    return u, v, lons, lats
#
#
# fig = plt.figure(figsize=(25.6, 14.4))
#
#
# u, v, lons, lats = loadVectorField('results/original/target_vectors_20180101-00_00_00.npz')
# ax0 = fig.add_subplot(1, 2, 1)
# ax0.set_title("Output")
# createVectorGraph(u, v, lons, lats)
# #plt.show()
# u, v, lons, lats = loadVectorField('results/original/input_vectors_20180101-00_00_00.npz')
# ax1 = fig.add_subplot(1, 2, 2)
# ax1.set_title("Input")
# createVectorGraph(u, v, lons, lats)
# plt.show()

from numpy import arange, sin, pi
import random

#https://matplotlib.org/examples/user_interfaces/embedding_in_qt5.html
from Training.Parser import ArgParser
from DataPreprocessing.DataScaler import DataScaler

parser = ArgParser()
parser.parse(sys.argv[1:])
configOptions = parser.options

# set up scaling for variable transformation
scalerUV = DataScaler(mode='normalize_neg')
scalerLSM = DataScaler(mode='normalize_neg')
scalerBLH = DataScaler(mode='normalize_neg')
scalerZ = DataScaler(mode='normalize_neg')
scalerZOro = DataScaler(mode='normalize_neg')
scalerLSMOro = DataScaler(mode='normalize_neg')

import pickle
# scalerUV.fit(np.array([-28.17633056640625, 33.49676513671875]))
# scalerZOro.fit(np.array([-112.20334798674821, 3063.7395674057084]))
# scalerLSMOro.fit(np.array([0.0, 1.0]))

# with open('scalarUV.obj', 'rb') as file:
#     print(pickle.load(file))
#
# with open('scalarZOro.obj', 'rb') as file:
#     scalerZOro = pickle.load(file)
#
# with open('scalarLSMOro.obj', 'rb') as file:
#     scalerLSMOro = pickle.load(file)

scaling = 1.5


class MainWindow(QMainWindow, Ui_NetViewer):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)

        # self.sc = MyStaticMplCanvas(self.InputImg, width=400, height=400, dpi=100)
        # self.dc = MyDynamicMplCanvas(self.InputImg, width=400, height=400, dpi=100)
        # self.LayoutImg.addWidget(self.sc)
        # self.LayoutImg.addWidget(self.dc)
        self.inputCanvas = QuiverPlot(self.InputImg)
        self.targetCanvas = QuiverPlot(self.TargetImg)
        self.predCanvas = QuiverPlot(self.PredImg)
        self.LayoutImg.addWidget(self.inputCanvas)
        self.LayoutTar.addWidget(self.targetCanvas)
        self.LayoutPred.addWidget(self.predCanvas)
        self.modelDir = 'saved_models/Server/UNet_area1_area2_2017_2018'

        self.PredictBtn.clicked.connect(self.onClick)
        self.InterpolateBtn.clicked.connect(self.onInterpolateClicked)
        self.FileSpinBox.valueChanged.connect(self.onFileChanged)
        self.FileSpinBox.setKeyboardTracking(False)
        self.SpinBoxLlcrnrlat.valueChanged.connect(self.onBboxChanged)
        self.SpinBoxUrcrnrlat.valueChanged.connect(self.onBboxChanged)
        self.SpinBoxLlcrnrlon.valueChanged.connect(self.onBboxChanged)
        self.SpinBoxUrcrnrlon.valueChanged.connect(self.onBboxChanged)
        self.BtnUpdateBbox.clicked.connect(self.onBboxUpdateClick)
        #self.txtFileIndex.setText(str(0))
        self.modelList.itemClicked.connect(self.onModelSelected)
        self.dataDir = 'data/test'

        methods = ['bilinear', 'nearest']
        self.InterpMethodsBox.addItems(methods)
        self.InterpMethodsBox.setCurrentIndex(0)

        #self.InputImg.setFocus()
        #self.setCentralWidget(self.InputImg)
        self.data = None
        self.net = None
        self.bbox = (5, 17, 42, 49)

        self.inputCPU = None
        self.inputNet = None
        self.inputNetHR = None
        self.targetCPU = None
        self.helperGrids = None

        self._loadDataset()
        self.loadModelList()
        self._loadData()

        if not self.net:
            self.PredictBtn.setEnabled(False)

    def _loadDataset(self):
        dataOpt = dl.dataSettings('input', 'input_orography', configOptions['data']['gridsInput'],
                                  'target', 'target_orography', configOptions['data']['gridsTarget'])

        trainingOpt = dl.trainingSettings(configOptions['preprocess']['monthsTest'],
                                          None,
                                          configOptions['data']['useHROrography'],
                                          configOptions['preprocess']['testRegions'])

        print('=> Load dataset from <{}>'.format(self.dataDir))
        # opt = dl.vecFieldOptions('input', 'input_orography', ['windU', 'windV', 'z', 'seaMask'],
        #                          'target', 'target_orography', ['windU', 'windV'],
        #                          2, True, False,
        #                          (10, 12), (0, 0), False)

        scalerFile = torch.load('saved_models/Server/UNet_area1_area2_2017_2018/scaling.pth')
        self.scalerUV = scalerFile['scalingsInput'][0][0]


        # quantizerUV = Quantizer(num_bins=100)

        scaleOpt = dl.scalingSettings(scalerFile['scalingsInput'],
                                      # [(quantizerUV, ['windU', 'windV'])])
                                      scalerFile['scalingsTarget'],
                                      scalerFile['scalingsHR'])

        dataOpt = dl.dataSettings('input', 'input_orography', configOptions['data']['gridsInput'],
                                  'target', 'target_orography', configOptions['data']['gridsTarget'])

        dataLoader = dl.DataLoader(configOptions['preprocess']['pathTraining'],
                                   configOptions['data']['maxNumFiles'],
                                   dataOpt, trainingOpt, scaleOpt)
        dataTraining, _ = dataLoader.loadDataForTraining()
        #dataTraining.normalize()

        self.FileSpinBox.setMinimum(0)
        self.FileSpinBox.setMaximum(dataTraining.numFields - 1)
        self.FileSpinBox.setSingleStep(1)
        self.data = dataTraining


    def loadModelList(self):
        print('=> Load model list from <{}>'.format(self.modelDir))
        modelFiles = sorted(os.listdir(self.modelDir))
        self.modelList.addItems(modelFiles)

        # for modelFile in modelFiles:
        #     modelItem = QListWidgetItem
        #     self.ModelList.addItem()

    def loadModel(self, filename):
        print('=> Load model {}'.format(filename))
        fileDir = os.path.join(self.modelDir, filename)
        modelFile = torch.load(fileDir)
        self.net = modelFile['model']
        self.net.load_state_dict(modelFile['model'].state_dict())
        self.net.eval()
        self.PredictBtn.setEnabled(True)

        # test for minimum
        minLoss = 100000.0
        avgLoss = 0.0
        minIndex = -1
        print("\t => Computing min loss for all files")
        #for i, filename in enumerate(self.data.inputFilenames):
        for i in range(len(self.data.timeAreaMapping)):
            timestep, region = self.data.timeAreaMapping[i].split("+")
            curData = self.data[i]

            gridLR, gridHR, maskLR, maskHR = curData[2], curData[3], curData[4], curData[5]
            inputCPU = np.copy(curData[0])
            inputNet = torch.tensor([inputCPU]).to(device)
            inputNetHR = torch.tensor([self.data.hrOroGrid[region]]).to(device)
            targetCPU = np.copy(curData[1])

            #inputCPU[[0, 1]] = scalerUV.transform_back(inputCPU[[0, 1]])

            pred = self.net(inputNet, inputNetHR)
            pred_cpu = pred.data.cpu().numpy()[0]

            lossReal = computeBatchLoss([pred_cpu], [targetCPU],
                                        [i], [0, 1], self.data, normalize=False)

            if lossReal < minLoss:
                minIndex = i
                minLoss = lossReal

            avgLoss += lossReal

            print("\t Loss({}): {}".format(i, lossReal))
        avgLoss /= len(self.data.timeAreaMapping)
        print("\t => Min loss {} (avg: {}) for input file index {}:".format(minLoss, avgLoss, minIndex))


    def _loadData(self):
        fileIndex = self.FileSpinBox.value()
        print('=> Load vector field {}'.format(fileIndex))
        date, region = self.data.timeAreaMapping[fileIndex].split("+")
        print('=> Date {}'.format(date))

        #date = self.data.inputFilenames[fileIndex]
        #date = date.replace('input_', '')
        #date = date.replace('.npz', '')

        curData = self.data[fileIndex]

        gridLR, gridHR, maskLR, maskHR = curData[2], curData[3], curData[4], curData[5]
        self.helperGrids = gridLR, gridHR, maskLR, maskHR

        self.bbox = (gridLR[0].min(), gridLR[0].max(), gridLR[1].min(), gridLR[1].max())

        self.inputCPU = np.copy(curData[0])
        self.inputNet = torch.tensor([self.inputCPU]).to(device)
        self.inputNetHR = torch.tensor([self.data.hrOroGrid[region]]).to(device)
        self.targetCPU = np.copy(curData[1])

        print('\t => Denormalize input and outputs')
        # np.savez_compressed('input_{}.npz'.format(date), pred=self.inputCPU, maskHR=maskLR,
        #                     lonHR=gridLR[0], latHR=gridLR[1])

        self.inputCPU[[0, 1]] = self.scalerUV.transform_back(self.inputCPU[[0, 1]])
        # self.data.denormalizeOutput(self.inputCPU, fileIndex, False)
        # self.data.denormalizeOutput(self.targetCPU, fileIndex)
        # np.savez_compressed('input_{}.npz'.format(date), pred=self.inputCPU, maskHR=maskLR,
        #                     lonHR=gridLR[0], latHR=gridLR[1])

        print('\t => Remove paddings from data')
        gridsInput = unpadArrays([self.inputCPU[0], self.inputCPU[1], gridLR[0], gridLR[1]], maskLR)
        gridsTarget = unpadArrays([self.targetCPU[0], self.targetCPU[1], gridHR[0], gridHR[1]], maskHR)

        print('\t => Create plots')
        # self.inputCanvas.createPlot(self.inputCPU[0], self.inputCPU[1], gridLR[0], gridLR[1], self.bbox, 5)
        # self.targetCanvas.createPlot(self.targetCPU[0], self.targetCPU[1], gridHR[0], gridHR[1], self.bbox, 12)
        self.inputCanvas.createPlot(gridsInput[0], gridsInput[1], gridsInput[2], gridsInput[3], self.bbox, scaling,
                                    'images/input_{}.pdf'.format(date))
        self.targetCanvas.createPlot(gridsTarget[0], gridsTarget[1], gridsTarget[2], gridsTarget[3], self.bbox, scaling,
                                     'images/target_{}.pdf'.format(date))

        # np.savez_compressed('target_{}.npz'.format(date), pred=self.targetCPU, maskHR=maskHR,
        #                                                   lonHR=gridHR[0], latHR=gridHR[1])

        #self.predCanvas.clear()


    def _predictData(self):
        print('=> Apply model to data')
        fileIndex = self.FileSpinBox.value()
        gridLR, gridHR, maskLR, maskHR = self.helperGrids

        date, region = self.data.timeAreaMapping[fileIndex].split("+")
        #print('=> Date {}'.format(date))

        # date = self.data.inputFilenames[fileIndex]
        # date = date.replace('input_', '')
        # date = date.replace('.npz', '')

        pred = self.net(self.inputNet, self.inputNetHR)
        pred_cpu = pred.data.cpu().numpy()[0]

        # np.savez_compressed('net_prediction_{}.npz'.format(date), pred=pred_cpu, maskHR=maskHR,
        #                     lonHR=gridHR[0], latHR=gridHR[1])

        lossReal = computeBatchLoss([pred_cpu], [self.targetCPU],
                                    [fileIndex], [0, 1], self.data, normalize=False)
        print("\t => Loss for prediction: ", lossReal)

        # denormalize data
        #print('\t => Denormalize input and outputs')
        #self.data.denormalizeOutput(pred_cpu, fileIndex)

        print('\t => Remove paddings from data')
        gridsPred = unpadArrays([pred_cpu[0], pred_cpu[1], gridHR[0], gridHR[1]], maskHR)

        print('\t => Create plots')
        #self.predCanvas.clear()
        #self.predCanvas.createPlot(pred_cpu[0], pred_cpu[1], gridHR[0], gridHR[1], self.bbox, 12)
        self.predCanvas.createPlot(gridsPred[0], gridsPred[1], gridsPred[2], gridsPred[3], self.bbox, scaling,
                                   'images/prediction_{}.pdf'.format(date))

    def _upsampleData(self):
        print('=> Apply extrapolation to data')
        fileIndex = self.FileSpinBox.value()
        mode = self.InterpMethodsBox.currentText()

        date, region = self.data.timeAreaMapping[fileIndex].split("+")
        # date = self.data.inputFilenames[fileIndex]
        # date = date.replace('input_', '')
        # date = date.replace('.npz', '')

        gridLR, gridHR, maskLR, maskHR = self.helperGrids
        input = torch.tensor([self.inputCPU]).to(device)
        targetSize = self.targetCPU.shape[1:3]
        upsampled = F.interpolate(input[:, [0, 1]], size=targetSize, mode=mode)

        pred_cpu = upsampled.data.cpu().numpy()[0]

        # np.savez_compressed('bilinear_upsampling_{}.npz'.format(date), pred=pred_cpu, maskHR=maskHR,
        #                     lonHR=gridHR[0], latHR=gridHR[1])

        gridsPred = unpadArrays([pred_cpu[0], pred_cpu[1], gridHR[0], gridHR[1]], maskHR)

        #self.predCanvas.clear()
        self.predCanvas.createPlot(gridsPred[0], gridsPred[1], gridsPred[2], gridsPred[3], self.bbox, scaling,
                                   'images/downscaling_{}_{}.pdf'.format(mode, date))

    #self.predCanvas.createPlot(gridsPred[0], gridsPred[1], gridsPred[2], gridsPred[3], self.bbox, 12)

    @pyqtSlot()
    def onFileChanged(self):
        #self.txtFileIndex.setText(str(self.FileSpinBox.value()))
        self._loadData()

    @pyqtSlot()
    def onClick(self):
        print("Update figures")
        self._predictData()

    @pyqtSlot()
    def onInterpolateClicked(self):
        print("Use bi-linear upsampling")
        self._upsampleData()

    @pyqtSlot()
    def onModelSelected(self):
        file = self.modelList.selectedItems()[0].text()
        self.loadModel(file)

    @pyqtSlot()
    def onBboxChanged(self):
        llcrnrlon = self.SpinBoxLlcrnrlon.value()
        llcrnrlat = self.SpinBoxLlcrnrlat.value()
        urcrnrlon = self.SpinBoxUrcrnrlon.value()
        urcrnrlat = self.SpinBoxUrcrnrlat.value()
        self.bbox = llcrnrlon, urcrnrlon, llcrnrlat, urcrnrlat

    @pyqtSlot()
    def onBboxUpdateClick(self):
        self.inputCanvas.updatePlot(self.bbox)
        self.targetCanvas.updatePlot(self.bbox)
        self.predCanvas.updatePlot(self.bbox)



        # self.dc.update_figure()


    # def addPlot(self, fig):
    #     self.canvas = FigureCanvas(fig)
    #     self.LayoutImg.addWidget(self.canvas)
    #     self.canvas.draw()

def main():
    # fig1 = Figure()
    # ax1f1 = fig1.add_subplot(111)
    # ax1f1.plot(np.random.rand(5))

    app = QApplication(sys.argv)
    window = MainWindow()
    # window.addPlot(fig1)
    window.show()
    sys.exit(app.exec())


if __name__ == '__main__':
    main()
