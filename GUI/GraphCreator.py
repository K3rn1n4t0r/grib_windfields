import os
# workaround for missing proj4 library
os.environ['PROJ_LIB'] = '/home/hoehlein/anaconda3/pkgs/proj4-5.2.0-he6710b0_1/share/proj'

from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
#from matplotlib.mlab import griddata
from matplotlib import colors as mcolors
colors = dict(mcolors.BASE_COLORS, **mcolors.CSS4_COLORS)

from PyQt5 import QtWidgets


class QtCanvas(FigureCanvas):
    def __init__(self, parent=None, width=8, height=8, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.ax = fig.add_subplot(111)
        FigureCanvas.__init__(self, fig)

        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                                   QtWidgets.QSizePolicy.Expanding,
                                   QtWidgets.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

    def clear(self):
        self.ax.cla()
        self.draw()


from matplotlib.colors import LinearSegmentedColormap
cmapRYG = LinearSegmentedColormap.from_list('cmap_ryg', [(0.0, '#fed976'),
                                                         (0.25, '#fd8d3c'),
                                                         (1.0, '#e31a1c')], N=256)


def mapMagnitude(value, minMagn, maxMagn):
    dist = maxMagn - minMagn
    return min(1.0, max(0.0, (value - minMagn) / dist))

def mapMagnitudeArray(values, minMagn, maxMagn):
    #dist = maxMagn - minMagn
    val = np.array([mapMagnitude(value, minMagn, maxMagn) for value in values])
    return val


class QuiverPlot(QtCanvas):
    def __init__(self, parent=None, basemap=True, labels=False, normalize=True, *args, **kwargs):
        super(QuiverPlot, self).__init__(parent=parent)
        self.drawBasemap = basemap
        self.labels = labels
        self.normalize = normalize
        self.map = None
        self.boundaries = None
        self.data = None
        self.quiv = None

    def clear(self):
        self.ax.cla()

        self.map = None
        self.boundaries = None
        self.data = None

        self.draw()

    def updatePlot(self, bbox):
        if self.data is not None:
            u, v, lons, lats, scale = self.data
            self.createPlot(u, v, lons, lats, bbox, scale)

    def createPlot(self, u, v, lons, lats, bbox=(5, 17, 42, 49), scale=5, filename='test.pdf', streamline=False):
        #self.ax.cla()
        #self.figure.set_size_inches((12.0, 10.0))
        # bbox = (5, 17, 42, 49)

        streamline = False

        if self.quiv is None:
            self.ax.cla()

            self.boundaries = bbox
            self.data = u, v, lons, lats, scale

            # self.ax.plot([0, 1, 2, 3], [1, 2, 0, 4], 'r')
            self.map = None
            if self.drawBasemap:
                self.map = Basemap(projection='stere', lat_0=(bbox[2] + bbox[3]) / 2, lon_0=(bbox[0] + bbox[1]) / 2,
                                   llcrnrlon=bbox[0], urcrnrlon=bbox[1],
                                   llcrnrlat=bbox[2], urcrnrlat=bbox[3],
                                   resolution='l', ax=self.ax)

                self.map.drawcoastlines()
                self.map.drawcountries()

                parallels = np.arange(40., 52, 2.)
                labelsParallel = [1, 0, 0, 0] if self.labels else [0, 0, 0, 0]
                self.map.drawparallels(parallels, labels=labelsParallel, fontsize=10)

                meridians = np.arange(0., 24., 4.)
                labelsMeridians = [0, 0, 0, 1] if self.labels else [0, 0, 0, 0]
                self.map.drawmeridians(meridians, labels=labelsMeridians, fontsize=10)

                self.map.drawmapboundary(fill_color='#226699', zorder=0)  # #4499cc
                self.map.fillcontinents(color='#669999', lake_color='#99ffff', zorder=1)  # 669999

            # remove points outside the bounding box
            magnitudes = np.sqrt(u * u + v * v)
            minMagn = 0.0
            maxMagn = 30.0
            C = np.array([mapMagnitude(magn, minMagn, maxMagn) for magn in magnitudes])

            # u = u / magnitudes
            # v = v / magnitudes

            maxMagn = 7
            u /= maxMagn
            v /= maxMagn

            # lons, lats = np.meshgrid(lons, lats)
            x, y = self.map(lons, lats)

            # map.scatter(x,y,marker='o',color='r', zorder=2)
            self.quiv = self.map.quiver(x, y, u, v, color=colors['gold'], zorder=2, units='xy', pivot='middle',
                                   scale_units='inches', scale=scale, linewidth=0.1, edgecolors='k')# headwidth=3.5, headlength=4, headaxislength=4)
            # quiv = self.map.quiver(x, y, u, v, C, cmap=cmapRYG, zorder=2, units='xy', pivot='middle',
            #                        scale_units='inches', scale=scale)

            self.figure.tight_layout(pad=0, w_pad=0, h_pad=0)
            self.figure.savefig(filename, format='pdf')
            self.draw()

        else:
            maxMagn = 7
            u /= maxMagn
            v /= maxMagn

            self.quiv.set_UVC(u, v)
            self.figure.canvas.draw()

        # self.ax.cla()
        #
        # self.boundaries = bbox
        # self.data = u, v, lons, lats, scale
        #
        # # self.ax.plot([0, 1, 2, 3], [1, 2, 0, 4], 'r')
        # self.map = None
        # if self.drawBasemap:
        #     self.map = Basemap(projection='stere', lat_0=45., lon_0=10.,
        #                       llcrnrlon=bbox[0], urcrnrlon=bbox[1],
        #                        llcrnrlat=bbox[2], urcrnrlat=bbox[3],
        #                       resolution='l', ax=self.ax)
        #
        #     self.map.drawcoastlines()
        #     self.map.drawcountries()
        #
        #     parallels = np.arange(40., 52, 2.)
        #     labelsParallel = [1, 0, 0, 0] if self.labels else [0, 0, 0, 0]
        #     self.map.drawparallels(parallels, labels=labelsParallel, fontsize=10)
        #
        #     meridians = np.arange(0., 24., 4.)
        #     labelsMeridians = [0, 0, 0, 1] if self.labels else [0, 0, 0, 0]
        #     self.map.drawmeridians(meridians, labels=labelsMeridians, fontsize=10)
        #
        #     self.map.drawmapboundary(fill_color='#226699', zorder=0) # #4499cc
        #     self.map.fillcontinents(color='#669999', lake_color='#99ffff', zorder=1) #669999
        #
        # # remove points outside the bounding box
        #
        # magnitudes = np.sqrt(u * u + v * v).astype(np.float32)
        # minMagn = 0.0
        # maxMagn = 30.0
        # C = np.array([mapMagnitudeArray(magn, minMagn, maxMagn) for magn in magnitudes])
        #
        # if self.normalize:
        #    u = u / magnitudes
        #    v = v / magnitudes
        #
        # x, y = self.map(lons, lats)
        #
        # # create a new grid
        # #yi = np.arange(40., 52, 2.)
        # #xi = np.arange(0., 24., 4.)
        #
        # #zi = griddata(x, y, )
        #
        #
        #
        # xx, yy = self.map.makegrid(u.shape[1], u.shape[0], returnxy=True)[2:4]
        #
        # midX = lons[0][int(u.shape[0] / 2)]
        # midY = lats[0][int(u.shape[1] / 2)]
        # seedsX = np.arange(0. , 24., 1.)
        # seedsY = np.arange(40., 52., 1.)
        #
        # seed_points = [[],[]]#np.array(np.mesh(seedsX, seedsY))
        # #seed_points = np.array([seedsX, seedsY])
        #
        # for sY in np.arange(2, xx.shape[1] - 2, 2):
        #     for sX in np.arange(2, xx.shape[0] - 2, 2):
        #         seed_points[0] += [xx[sX][sY]]
        #         seed_points[1] += [yy[sX][sY]]
        #
        #         #seed_points[0] += [(xx[sX][sY] + xx[sX + 1][sY]) / 2]
        #         #seed_points[1] += [(yy[sX][sY] + yy[sX + 1][sY]) / 2]
        #
        # #x, y = np.meshgrid(x, y)
        # #u, v = np.meshgrid(u, v)
        # seed_points = np.array(seed_points)
        # # seed_points = np.array([[yy[xx.shape[0] // 2][xx.shape[1] // 2]],
        # #                         [xx[xx.shape[0] // 2][xx.shape[1] // 2]]])
        #
        # if self.drawBasemap:
        #     # self.map.quiver(x, y, u, v, C, cmap=cmapRYG, zorder=2, units='xy', pivot='middle',
        #     #                scale_units='inches', scale=scale)
        #     self.map.streamplot(xx, yy, u, v, color=C, linewidth=2, cmap=cmapRYG, density=(12, 6), start_points=seed_points.T)
        #     #self.map.plot(seed_points[0], seed_points[1], 'bo')
        #
        # else:
        #     self.ax.quiver(x, y, u, v, C, cmap=cmapRYG, zorder=2, units='xy', pivot='middle',
        #                    scale_units='inches')
        #
        # #self.ax.show()
        # self.figure.tight_layout(pad=0, w_pad=0, h_pad=0)
        # self.figure.savefig('test.pdf', format='pdf')
        # self.draw()


def createVectorGraph(u, v, lons, lats, scale=3, filename=None):


    # basemap = Basemap(projection='stere', lat_0=45., lon_0=10.,
    #               llcrnrlon=0, urcrnrlon=23, llcrnrlat=39, urcrnrlat=51,
    #               resolution='l')
    basemap = Basemap(projection='stere', lat_0=45., lon_0=10.,
                      llcrnrlon=5, urcrnrlon=17, llcrnrlat=42, urcrnrlat=49,
                      resolution='l')
    # map.bluemarble()
    # map.shadedrelief()
    # map.etopo()

    basemap.drawcoastlines()
    # map.drawstates()

    basemap.drawcountries()

    parallels = np.arange(40., 52, 2.)
    basemap.drawparallels(parallels, labels=[0, 0, 0, 0], fontsize=10)

    meridians = np.arange(0., 24., 4.)
    basemap.drawmeridians(meridians, labels=[0, 0, 0, 0], fontsize=10)

    basemap.drawmapboundary(fill_color='#4499cc', zorder=0)
    # basemap.fillcontinents(color='#669966', lake_color='#99ffff', zorder=1)
    basemap.fillcontinents(color='#669999', lake_color='#99ffff', zorder=1)
    #basemap.fillcontinents(color='#cccccc', lake_color='#99ffff', zorder=1)

    magnitudes = np.sqrt(u * u + v * v)
    minMagn = 0.0
    maxMagn = 30.0
    C = np.array([mapMagnitude(magn, minMagn, maxMagn) for magn in magnitudes])

    u = u / magnitudes
    v = v / magnitudes

    # lons, lats = np.meshgrid(lons, lats)
    x, y = basemap(lons, lats)

    # map.scatter(x,y,marker='o',color='r', zorder=2)
    quiv = basemap.quiver(x, y, u, v, C, cmap=cmapRYG, zorder=2, units='xy', pivot='middle',
                          scale_units='inches', scale=scale)

    # plt.show()
    if not filename:
        pass#return fig
    else:
        if filename.endswith('.pdf'):
            plt.savefig(filename, format='pdf', bbox_inches='tight', pad_inches=0)
        else:
            plt.savefig(filename)
        plt.close()
        #return None
