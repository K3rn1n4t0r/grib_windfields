import numpy as np
import matplotlib.pyplot as plt

n = 50

c1 = np.random.randn(n, 2)
c1[:,1] -= 4

c2 = np.random.randn(2 * n, 2)
c2[:,0] -= 4

c3 = np.random.randn(3 *n, 2)
c3[:,1] *= 2
a = np.pi / 4.
c3 = np.dot(c3, np.array([[np.cos(a), np.sin(a)], [-np.sin(a), np.cos(a)]]))
c3[:,1] += 4

fig, ax = plt.subplots(2, 2)
ax[1,1].hist(np.concatenate([c1[:,1], c2[:,1], c3[:,1]]), orientation='horizontal')
ax[0,0].hist(np.concatenate([c1[:,0], c2[:,0], c3[:,0]]))
ax[1,0].scatter(c1[:,0], c1[:,1])
ax[1,0].scatter(c2[:,0], c2[:,1])
ax[1,0].scatter(c3[:,0], c3[:,1])


plt.show()


