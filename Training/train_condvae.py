# region Imports
import os
import sys
# change working directory
os.chdir(os.getcwd())
# add working directory to python environment to allow calls from subdirectories
sys.path.insert(0, os.getcwd())
from DataPreprocessing import DataLoader as dl
from Networks.CondVAE import *

from Utils import computeBatchLoss
from Training.utils import *
from Training.Parser import ArgParser
from LossFunctions.KLAnnealer import KLAnnealer
from Utils.ProgressBar import ProgressBar

import numpy as np
import random
import json
##############################################
# PyTorch specific libraries
import torch
import torch.nn as nn
from torch.nn import functional as F

# Gradient optimizer
import torch.optim as optim
# loading data
from torch.utils.data import DataLoader
# auto-gradient
from torch.autograd import Variable
##############################################
from tqdm import tqdm

# summary with tensorboard
from tensorboardX import SummaryWriter
# endregion

##########################

# region Settings

# set maximum number of threads to not fully occupy all threads on server
torch.set_num_threads(4)
# parse arguments
parser = ArgParser()
parser.parse(sys.argv[1:])
configOptions = parser.options

netName = configOptions['training']['network']
print(netName)
encName, decName = netName.split('+')
print(configOptions['debugging'])
logDir = configOptions['debugging']['recordDir']
directories = {'results': os.path.join(logDir, netName),
               'records': os.path.join(logDir, netName, 'records'),
               'models':  os.path.join(logDir, netName, 'records', 'models'),
               'data': {'training': configOptions['preprocess']['pathTraining'],
                        'test':     configOptions['preprocess']['pathTest'],
                        'raw':      configOptions['preprocess']['pathRaw']}
               }

if not os.path.exists(directories['records']):
    os.makedirs(directories['records'])

runNumber = findRunNumber(directories['records'])

gpuID = configOptions['training']['gpu']
print("[INFO]: Current working directory: <{}>".format(os.getcwd()))

# set up environment to use gpu with device ID gpuID only
os.environ["CUDA_VISIBLE_DEVICES"] = str(gpuID)

cudaAvailable = torch.cuda.is_available()
device = torch.device("cuda:0" if cudaAvailable else "cpu")
if cudaAvailable:    
    print("[INFO]: Using graphics card number <{}>".format(gpuID))
else:
    print("[WARNING]: No available CUDA device found. Training is performed on CPU!")

if sys.gettrace():
    print("[WARNING]: Programs runs in DEBUG mode")
    runName = 'run_debug'
else:
    runName = 'run_%05d' % runNumber
directories.update({'currentRun': os.path.join(directories['records'], runName)})
directories.update({'models': os.path.join(directories['currentRun'], 'models')})

if not os.path.exists(directories['models']):
    os.makedirs(directories['models'])

if configOptions['debugging']['printConfig']:
    print("[INFO]: Config options:\n===================")
    printConfigs(configOptions)
    print("===================")

if configOptions['debugging']['printDirectories']:
    print("[INFO]: Directories::\n===================")
    printConfigs(directories)
    print("===================")

print("[INFO]: Training run number: <{}>".format(runNumber))

# initialize summary writer
summaryWriter = SummaryWriter(directories['currentRun'], purge_step=1)
printConfigToSummary(configOptions, summaryWriter)

# endregion

###########################

# region 0) Random seeds

seed = random.randint(0, 2**32 - 1)
#print("Random seed: {}".format(seed))
random.seed(seed)
np.random.seed(seed)
torch.manual_seed(seed)
torch.cuda.manual_seed_all(seed)

# endregion

###########################

# region 1) Generate data

# run DataGeneration/generate.py

# endregion

###########################

# region 2) Load data

from DataPreprocessing.DataScaler import DataScaler
from DataPreprocessing.Quantizer import Quantizer

dataOpt = dl.dataSettings('input', 'input_orography', configOptions['data']['gridsInput'],
                          'target', 'target_orography', configOptions['data']['gridsTarget'])

valiOpt = dl.validationSettings(configOptions['data']['validationMode'],
                                configOptions['data']['validationInput'],
                                configOptions['data']['validationRegions'])

patchMode = configOptions['preprocess']['patchSize'] if configOptions['preprocess']['patching'] else None
trainingOpt = dl.trainingSettings(configOptions['preprocess']['monthsTraining'],
                                  patchMode, configOptions['data']['useHROrography'],
                                  configOptions['preprocess']['trainingRegions'])

# set up scaling for variable transformation
scalerUV = DataScaler(mode='normalize_neg')
scalerLSM = DataScaler(mode='normalize_neg')
scalerBLH = DataScaler(mode='normalize_neg')
scalerFSR = DataScaler(mode='normalize_neg')
scalerZ = DataScaler(mode='normalize_neg')
scalerZOro = DataScaler(mode='normalize_neg')
scalerLSMOro = DataScaler(mode='normalize_neg')
# quantizerUV = Quantizer(num_bins=100)

scaleOpt = dl.scalingSettings([(scalerUV, ['windU', 'windV']),
                               (scalerLSM, ['seaMask']),
                               (scalerZ, ['z']),
                               (scalerBLH, ['blh']),
                               (scalerFSR, ['fsr'])],
                              #[(quantizerUV, ['windU', 'windV'])])
                              [],
                              [(scalerZOro, ['z']), (scalerLSMOro, ['seaMask'])])

dataLoader = dl.DataLoader(directories['data']['training'],
                           configOptions['data']['maxNumFiles'],
                           dataOpt, trainingOpt, scaleOpt, valiOpt)
dataTraining, dataValidation = dataLoader.loadDataForTraining()

# create loaders for PyTorch with pre-defined batch size
trainLoader = DataLoader(dataTraining, batch_size=configOptions['training']['batchSize'],
                         shuffle=True, drop_last=True, num_workers=0)
validationLoader = DataLoader(dataValidation, batch_size=configOptions['training']['batchSize'],
                              shuffle=False, drop_last=True, num_workers=0)

####
# Save current config in json file
with open(os.path.join(directories['currentRun'], 'config.json'), 'w') as f:
    json.dump(configOptions, f, indent=4)

saveScalings(scaleOpt, directories['models'])

# endregion

###########################

# region 3) Setup networks

if configOptions['data']['useHROrography']:
    if encName == 'CE_VGG':
        encoder = CE_VGG_HR()
    else:
        raise NotImplementedError("Network architecture <{}> not implemented".format(encName))
    if decName == 'CD_UNet':
        decoder = CD_UNet_HR()
    else:
        raise NotImplementedError("Network architecture <{}> not implemented".format(decName))
else:
    if encName == 'CE_VGG':
        encoder = CE_VGG(
            data_channels_lr=len(configOptions['data']['gridsInput']),
            data_channels_hr=len(configOptions['data']['gridsTarget']),
            in_channels=configOptions['training']['inputChannels'],
            latent_channels=configOptions['training']['latentChannels'],
            compression=[2, 3, 2, 2],
            leaky_slope=0.05,
            dropout=configOptions['training']['dropout_encoder'],
            padding_mode='replication'
        )
    elif encName == 'CE_VGG_lin':
        encoder = CE_VGG_lin(
            data_channels_lr=len(configOptions['data']['gridsInput']),
            data_channels_hr=len(configOptions['data']['gridsTarget']),
            in_channels=configOptions['training']['inputChannels'],
            latent_channels=configOptions['training']['latentChannels'],
            latent_dim=256,
            compression=[2, 3, 2, 2],
            leaky_slope=0.05,
            dropout=configOptions['training']['dropout_encoder'],
            padding_mode='replication'
        )
    else:
        raise NotImplementedError("Network architecture <{}> not implemented".format(encName))
    if decName == 'CD_UNet':
        decoder = CD_UNet(
            data_channels=len(configOptions['data']['gridsInput']),
            in_channels=configOptions['training']['inputChannels'],
            out_channels=len(configOptions['data']['gridsTarget']),
            latent_channels=configOptions['training']['latentChannels'],
            compression=[1, 3, 2, 2],
            leaky_slope=0.05,
            dropout=configOptions['training']['dropout_decoder'],
            use_oa=False,
            use_cc=True,
            padding_mode='replication',
            logVar=configOptions['training']['logVar']
        )
    elif decName == 'CD_UNet_Small':
        decoder = CD_UNet_Small(
            data_channels=len(configOptions['data']['gridsInput']),
            in_channels=configOptions['training']['inputChannels'],
            out_channels=len(configOptions['data']['gridsTarget']),
            latent_channels=configOptions['training']['latentChannels'],
            compression=[1, 3, 2, 2],
            leaky_slope=0.05,
            dropout=configOptions['training']['dropout_decoder'],
            use_oa=False,
            use_cc=True,
            padding_mode='replication',
            logVar=configOptions['training']['logVar']
        )
    elif decName == 'CD_UNet_SmallSeq':
        decoder = CD_UNet_SmallSeq(
            data_channels=len(configOptions['data']['gridsInput']),
            in_channels=configOptions['training']['inputChannels'],
            out_channels=len(configOptions['data']['gridsTarget']),
            latent_channels=configOptions['training']['latentChannels'],
            compression=[1, 3, 2, 2],
            leaky_slope=0.05,
            dropout=configOptions['training']['dropout_decoder'],
            use_oa=False,
            use_cc=True,
            padding_mode='replication',
            logVar=configOptions['training']['logVar']
        )
    elif decName == 'CD_UNet_lin':
        decoder = CD_UNet_lin(
            data_channels=len(configOptions['data']['gridsInput']),
            in_channels=configOptions['training']['inputChannels'],
            out_channels=len(configOptions['data']['gridsTarget']),
            latent_channels=configOptions['training']['latentChannels'],
            latent_dim=256,
            compression=[1, 3, 2, 2],
            leaky_slope=0.05,
            dropout=configOptions['training']['dropout_decoder'],
            use_oa=False,
            use_cc=True,
            padding_mode='replication',
            logVar=configOptions['training']['logVar']
        )
    else:
        raise NotImplementedError("Network architecture <{}> not implemented".format(decName))

print("[INFO]: Used network architecture: {} + {}".format(encoder.__class__.__name__, decoder.__class__.__name__))

net = CondVAE(encoder, decoder)

# randomize weights for initial step
initWeights(net, configOptions['training']['initMethod'])
if configOptions['debugging']['printNetworkDetails']:
    print(net)
# transform to cuda
print("[INFO]: Upload network to GPU ...")
net.cuda()

# track learnable parameters
modelParameters = filter(lambda p: p.requires_grad, net.parameters())
params = sum([np.prod(p.size()) for p in modelParameters])
print("[INFO]: Number of learnable parameters: {}".format(params))

# endregion

###########################

# region 4) Setup optimizer, scheduler and annealer

# create optimizer ADAM with learning rate
optimizer = optim.Adam(net.parameters(), lr=configOptions['optimizer']['learningRate'],
                       betas=configOptions['optimizer']['betas'], weight_decay=0.0)

# create scheduler to reduce learning rate on-the-fly, lr decay
scheduler = None
schedulerType = configOptions['scheduler']['type']
if schedulerType == 'step':
    scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=configOptions['scheduler']['steps'],
                                          gamma=configOptions['scheduler']['gamma'])
elif schedulerType == 'plateau':
    scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.717, patience=20,
                                                     verbose=False, threshold=1.e-2, threshold_mode='rel',
                                                     cooldown=0, min_lr=0, eps=1e-08)
else:
    raise NotImplementedError('Scheduler {} is not implemented'.format(schedulerType))

annealer = KLAnnealer(
    eps_start=configOptions['annealer']['eps_start'],
    eps_end=configOptions['annealer']['eps_end'],
    epochs=configOptions['annealer']['epochs'],
    offset=configOptions['annealer']['offset']
)

# endregion

###########################

# region 4) Setup loss function for comparison

lossFunction = nn.L1Loss()
lossFunction.cuda()
print("[INFO]: Used loss function for training: {}".format(lossFunction.__class__.__name__))

# endregion

###########################

# region 5) Setup training

#epochs = int(iterations / len(trainLoader) + 0.5)

from timeit import default_timer as timer

# region 5.1) Training function


def train(epoch, epochs):

    net.train()

    trainLoss_mix = 0
    trainLoss_rec = 0
    trainLossReal = 0
    trainLossUpsampled = 0
    trainLoss_dKL = 0
    trainLoss_L1 = 0
    trainLoss_L2 = 0

    numLoadings = len(trainLoader)

    progressBar = ProgressBar(numLoadings, displaySumCount=True)

    for i, trainData in enumerate(trainLoader):
        inputs_cpu, targets_cpu, gridLR, gridHR, maskLR, maskHR, hrOro, idx = trainData
        targets, inputs, hrOro = targets_cpu.to(device), inputs_cpu.to(device), hrOro.to(device)
        if epoch == 0 and i==0:
            print('Shape of inputs: {}'.format(inputs_cpu.size()))
        # convert GPU / tensor data to numpy arrays
        inputs_cpu = inputs_cpu.data.numpy()
        targets_cpu = targets_cpu.data.numpy()

        conditions = (inputs, hrOro) if configOptions['data']['useHROrography'] else inputs

        # zero out the parameters of the gradients
        optimizer.zero_grad()

        eps = annealer.get_eps()

        neg_elbo, log_posterior, dKL_zx_z, reconstruction, dev = net.estimate_elbo(targets, inputs, eps=eps, return_deviation=True, mode=configOptions['training']['loss'])
        # backpropagate loss function
        neg_elbo.backward()
        # advance weights towards minimum
        optimizer.step()
        # update training losses
        trainLoss_mix += neg_elbo.item()
        trainLoss_rec += log_posterior.item()
        trainLoss_dKL += dKL_zx_z.item()
        trainLoss_L1 += dev.size(1) * dev.mean().item()
        trainLoss_L2 += dev.pow(2).sum(dim=1).sqrt().mean().item()

        if configOptions['debugging']['printRealLoss']:
            reconstruction_cpu = reconstruction[0].data.cpu().numpy()
            trainLossReal += computeBatchLoss(reconstruction_cpu, targets_cpu,
                                              idx, [0, 1], dataTraining)

        if (configOptions['debugging']['printUpsamplingLoss'] or (configOptions['debugging']['printImages'] and i < 1)):
            # upsample images bilinear
            targetSize = targets_cpu.shape[2:4]
            inputs_denom = scalerUV.transform_back(inputs_cpu[:, [0, 1]])
            inputs_temp = torch.tensor(inputs_denom).to(device)
            upsamplesPred = F.interpolate(inputs_temp, size=targetSize, mode='bilinear')

        if configOptions['debugging']['printUpsamplingLoss']:
            targets_temp = torch.tensor(targets_cpu).to(device)
            lossUpsampledL1 = lossFunction(upsamplesPred, targets_temp).item()
            trainLossUpsampled += targets_temp.size(1) * lossUpsampledL1

        if configOptions['debugging']['printImages'] and i < 1:
            gridsLR = gridLR.data.numpy()
            gridsHR = gridHR.data.numpy()
            masksLR = maskLR.data.numpy()
            masksHR = maskHR.data.numpy()

            reconstruction_cpu = reconstruction[0].data.cpu().numpy()

            printVectorImages(epoch, i, inputs_denom[i], targets_cpu[i], reconstruction_cpu[i],
                              upsamplesPred.data.cpu().numpy()[0],
                              gridsLR[i], masksLR[i],
                              gridsHR[i], masksHR[i], 'train', summaryWriter)

        # progress bar
        progressBar.proceed(i + 1)

    # update records per train step
    trainLoss_mix /= numLoadings
    trainLoss_rec /= numLoadings
    trainLoss_dKL /= numLoadings
    trainLoss_L1 /= numLoadings
    trainLoss_L2 /= numLoadings

    summaryWriter.add_scalar('train/loss_mix', trainLoss_mix, epoch)
    summaryWriter.add_scalar('train/loss_rec', trainLoss_rec, epoch)
    summaryWriter.add_scalar('train/loss_dKL', trainLoss_dKL, epoch)
    summaryWriter.add_scalar('train/loss_L1', trainLoss_L1, epoch)
    summaryWriter.add_scalar('train/loss_L2', trainLoss_L2, epoch)
    summaryWriter.add_scalar('train/eps', eps, epoch)

    currentLR = 0.0
    if schedulerType == 'step':
        currentLR = scheduler.get_lr()[0]
    elif schedulerType == 'plateau':
        currentLR = optimizer.param_groups[0]['lr']
    summaryWriter.add_scalar('train/lr', currentLR, epoch)

    print("Training: neg. ELBO {}, LRDecay {}".format(trainLoss_mix, currentLR))

    if configOptions['debugging']['printRealLoss']:
        trainLossReal /= numLoadings
        summaryWriter.add_scalar('train/lossRealL1', trainLossReal, epoch)
        print("Training: L1 Real {},".format(trainLossReal))

    if configOptions['debugging']['printUpsamplingLoss']:
        trainLossUpsampled /= numLoadings
        summaryWriter.add_scalar('train/lossUpsampledL1', trainLossUpsampled, epoch)
        print("Training: L1 Bilinear {},".format(trainLossUpsampled))

    if epoch % configOptions['training']['saveEveryNthEpoch'] == 0:
        saveCheckpoint(epoch, net, optimizer, scheduler, scaleOpt, directories['models'])

    if schedulerType == 'step':
        scheduler.step()
    elif schedulerType == 'plateau':
        scheduler.step(trainLoss_mix)

    annealer.step()

    summaryWriter.flush()

# endregion

# region 5.2) Validation function


def validate(epoch, epochs):
    net.eval()

    validationLoss_mix = 0
    validationLoss_rec = 0
    validationLoss_dKL = 0
    validationLoss_L1 = 0
    validationLoss_L2 = 0
    validationLossReal = 0
    validationLossUpsampled = 0

    logVar_mean = []
    logVar_var = []

    numLoadings = len(validationLoader)

    mi_x_z = []
    dKL_zx_z = []
    dKL_z_z = []

    progressBar = ProgressBar(numLoadings, displaySumCount=True)
    for i, validationData in enumerate(validationLoader):
        inputs_cpu, targets_cpu, gridLR, gridHR, maskLR, maskHR, hrOro, idx = validationData
        targets, inputs, hrOro = targets_cpu.to(device), inputs_cpu.to(device), hrOro.to(device)

        inputs_cpu = inputs_cpu.data.numpy()
        targets_cpu = targets_cpu.data.numpy()

        conditions = (inputs, hrOro) if configOptions['data']['useHROrography'] else inputs

        eps = annealer.get_eps(mode='validation')

        neg_elbo, log_posterior, _, reconstruction, dev = net.estimate_elbo(targets, conditions, eps=eps, return_deviation=True, mode=configOptions['training']['loss'])
        mi_x_z_torch, dKL_zx_z_torch, dKL_z_z_torch = net.estimate_information(targets, conditions)

        validationLoss_mix += neg_elbo.item()
        validationLoss_rec += log_posterior.item()
        validationLoss_dKL += dKL_zx_z_torch.item()
        validationLoss_L1 += dev.size(1) * dev.mean().item()
        validationLoss_L2 += dev.pow(2).sum(dim=1).sqrt().mean().item()

        mi_x_z.append(mi_x_z_torch.item())
        dKL_zx_z.append(dKL_zx_z_torch.item())
        dKL_z_z.append(dKL_z_z_torch.item())

        logVar_mean.append(reconstruction[1].mean().item())
        logVar_var.append(reconstruction[1].var().item())

        if configOptions['debugging']['printRealLoss']:
            reconstruction_cpu = reconstruction[0].data.cpu().numpy()
            validationLossReal += computeBatchLoss(reconstruction_cpu, targets_cpu,
                                              idx, [0, 1], dataTraining)

        if configOptions['debugging']['printUpsamplingLoss'] or (configOptions['debugging']['printImages'] and i < 1):
            targetSize = targets_cpu.shape[2:4]
            # upsamplesPred = F.interpolate(inputs[:, [0, 1]], targetSize)
            inputs_cpu[:, [0, 1]] = scalerUV.transform_back(inputs_cpu[:, [0, 1]])
            inputs_temp = torch.tensor(inputs_cpu).to(device)
            upsamplesPred = F.interpolate(inputs_temp[:, [0, 1]], size=targetSize, mode='bilinear')

        if configOptions['debugging']['printUpsamplingLoss']:
            targets_temp = torch.tensor(targets_cpu).to(device)
            lossUpsampledL1 = lossFunction(upsamplesPred, targets_temp).item()
            validationLossUpsampled += targets_temp.size(1) * lossUpsampledL1

        if configOptions['debugging']['printImages'] and i < 1:
            gridsLR = gridLR.data.numpy()
            gridsHR = gridHR.data.numpy()
            masksLR = maskLR.data.numpy()
            masksHR = maskHR.data.numpy()

            reconstruction_cpu = reconstruction[0].data.cpu().numpy()

            printVectorImages(epoch, i, inputs_cpu[i], targets_cpu[i], reconstruction_cpu[i],
                              upsamplesPred.data.cpu().numpy()[0],
                              gridsLR[i], masksLR[i],
                              gridsHR[i], masksHR[i], 'validation', summaryWriter) \
 \
                # progress bar
        progressBar.proceed(i + 1)

    mi_x_z = np.array(mi_x_z)
    dKL_zx_z = np.array(dKL_zx_z)
    dKL_z_z = np.array(dKL_z_z)
    logVar_mean = np.array(logVar_mean)
    logVar_var = np.array(logVar_var)

    validationLoss_mix /= numLoadings
    validationLoss_rec /= numLoadings
    validationLoss_dKL /= numLoadings
    validationLoss_L1 /= numLoadings
    validationLoss_L2 /= numLoadings

    summaryWriter.add_scalar('validation/loss_mix', validationLoss_mix, epoch)
    summaryWriter.add_scalar('validation/loss_rec', validationLoss_rec, epoch)
    summaryWriter.add_scalar('validation/loss_dKL', validationLoss_dKL, epoch)
    summaryWriter.add_scalar('validation/loss_L1', validationLoss_L1, epoch)
    summaryWriter.add_scalar('validation/loss_L2', validationLoss_L2, epoch)
    print("Validation: neg. ELBO {}".format(validationLoss_mix))

    if configOptions['debugging']['printRealLoss']:
        validationLossReal /= numLoadings
        summaryWriter.add_scalar('validation/lossRealL1', validationLossReal, epoch)
        print("Validation: L1 Real {},".format(validationLossReal))

    if configOptions['debugging']['printUpsamplingLoss']:
        validationLossUpsampled /= numLoadings
        summaryWriter.add_scalar('validation/lossUpsampledL1', validationLossUpsampled, epoch)
        print("Validation: L1 Bilinear {},".format(validationLossUpsampled))

    summaryWriter.add_scalar('validation/mi_x_z_mean', mi_x_z.mean(), epoch)
    summaryWriter.add_scalar('validation/mi_x_z_var', mi_x_z.var(), epoch)

    summaryWriter.add_scalar('validation/dKL_zx_z_mean', dKL_zx_z.mean(), epoch)
    summaryWriter.add_scalar('validation/dKL_zx_z_var', dKL_zx_z.var(), epoch)

    summaryWriter.add_scalar('validation/dKL_z_z_mean', dKL_z_z.mean(), epoch)
    summaryWriter.add_scalar('validation/dKL_z_z_var', dKL_z_z.var(), epoch)

    if isinstance(reconstruction, tuple):
        summaryWriter.add_scalar('validation/logVar_mean_mean', logVar_mean.mean(), epoch)
        summaryWriter.add_scalar('validation/logVar_mean_var', logVar_mean.var(), epoch)
        summaryWriter.add_scalar('validation/logVar_var_mean', logVar_var.mean(), epoch)
        summaryWriter.add_scalar('validation/logVar_var_var', logVar_var.var(), epoch)

    summaryWriter.flush()

    print('Average mutual information score: {:.6f}+-{:.6f}'.format(
        np.array(mi_x_z).mean(), np.array(mi_x_z).std())
    )
    print('Average KL divergence posterior/prior: {:.6f}+-{:.6f}'.format(
        np.array(dKL_zx_z).mean(), np.array(dKL_zx_z).std())
    )

# endregion

# region fitting wrapper

def fit(self, dataset_train,
        optimizer=None,
        scheduler=None,
        optimizer_enc=None,
        optimizer_dec=None,
        scheduler_enc=None,
        scheduler_dec=None,
        aggressive=False,
        eps=1.,
        annealer_dKL=None,
        epochs=1, batch_size=None, device='cpu',
        shuffle=True, drop_last=True,
        path_checkpoints=None, step_checkpoints=10,
        dataset_test=None, batch_size_test=None,
        path_summary=None):
    global testloader
    if aggressive:
        assert optimizer_enc is not None
        assert optimizer_dec is not None
    else:
        assert optimizer is not None

    if path_summary:
        self.summary = SummaryWriter(path_summary)

    trainloader = DataLoader(dataset_train, batch_size=batch_size, shuffle=shuffle, drop_last=drop_last)

    if dataset_test is not None:
        if batch_size_test is None:
            batch_size_test = batch_size
        testloader = DataLoader(dataset_test, batch_size=batch_size_test, shuffle=shuffle, drop_last=drop_last)
    else:
        if aggressive:
            print('Aggressive training requested but validation data is missing.\n',
                  'Proceeding with standard training.')
            aggressive = False

    epochs_start = self.epochs_trained

    for epoch in range(epochs):
        self.train()

        if annealer_dKL:
            eps = annealer_dKL.get_eps()

        if aggressive:
            if dataset_test is not None:
                aggressive = self._fit_epoch_aggressive(
                    trainloader, testloader,
                    optimizer_enc=optimizer_enc, optimizer_dec=optimizer_dec,
                    scheduler_enc=scheduler_enc, scheduler_dec=scheduler_dec,
                    eps=eps, device=device,
                    epochs=epochs_start + epochs,
                )
        else:
            self._fit_epoch(
                trainloader,
                optimizer=optimizer,
                scheduler=scheduler,
                eps=eps, device=device,
                epochs=epochs_start + epochs,
            )

        if path_checkpoints and (epoch + 1) % step_checkpoints == 0:
            self.save_checkpoint(
                path_checkpoints,
                optimizer=optimizer, optimizer_enc=optimizer_enc, optimizer_dec=optimizer_dec,
                scheduler=scheduler, scheduler_enc=scheduler_enc, scheduler_dec=scheduler_dec,
            )

        if dataset_test is not None:
            self.eval()
            self._validate_epoch(
                testloader,
                eps=eps, device=device,
                epochs=epochs_start + epochs,
            )

        if annealer_dKL:
            annealer_dKL.step()

# endregion

# region aggressive training

# def _fit_epoch_aggressive(self,
#                           trainloader, testloader,
#                           optimizer_enc, optimizer_dec,
#                           scheduler_enc, scheduler_dec,
#                           eps=1, device='cpu',
#                           epochs=1, max_iter=100
#                           ):
#     elbo_current_cum = 0
#     elbo_current_old = 1.e8
#     loss_mix_cum = 0
#     loss_rec_cum = 0
#     loss_dKL_cum = 0
#     mi_best = 0
#     mi_not_improved = 0
#
#     aggressive = True
#     num_batches = len(trainloader)
#     pbar = ProgressBar(num_batches)
#
#     for batch, data in enumerate(trainloader):
#         target, conditions = self._transfer_data(data, device)
#
#         if aggressive:
#             iteration = 0
#             while iteration < max_iter:
#                 optimizer_enc.zero_grad()
#                 optimizer_dec.zero_grad()
#                 neg_elbo, _, _, _ = self.estimate_elbo(target, conditions, eps=eps)
#                 neg_elbo.backward()
#                 optimizer_enc.step()
#                 elbo_current_cum += neg_elbo.item()
#
#                 if (iteration + 1) % 10 == 0:
#                     elbo_current_cum /= 10
#                     if elbo_current_old > elbo_current_cum:
#                         elbo_current_old = elbo_current_cum
#                         elbo_current_cum = 0
#                     else:
#                         break
#
#                 iteration += 1
#
#         optimizer_enc.zero_grad()
#         optimizer_dec.zero_grad()
#         neg_elbo, log_posterior, dKL_zx_z, _ = self.estimate_elbo(target, conditions, eps=eps)
#         neg_elbo.backward()
#         if not aggressive:
#             optimizer_enc.step()
#         optimizer_dec.step()
#
#         loss_mix_cum += neg_elbo.item()
#         loss_rec_cum += log_posterior.item()
#         loss_dKL_cum += dKL_zx_z.item()
#
#         if aggressive and batch == (num_batches - 1):
#             self.eval()
#             mi_current, _, _ = self.estimate_mi_x_z(testloader, device=device)
#             self.train()
#             if mi_current < mi_best:
#                 mi_not_improved += 1
#                 if mi_not_improved == 5:
#                     aggressive = False
#             else:
#                 mi_best = mi_current
#                 mi_not_improved = 0
#
#         pbar.update(self.epochs_trained + 1, epochs, 'Aggressive')
#
#     if scheduler_enc is not None:
#         scheduler_enc.step(loss_mix_cum)
#     if scheduler_dec is not None:
#         scheduler_dec.step(loss_mix_cum)
#
#     self.epochs_trained += 1
#
#     pbar.close()
#
#     print('Average loss per pixel: {}'.format(
#         -2. * loss_rec_cum / (num_batches * np.prod(self.args['shape_target'])) - np.log(2. * np.pi)
#     ))
#
#     if self.summary is not None:
#         self.summary.add_scalar('training/loss_mix', loss_mix_cum / num_batches, self.epochs_trained)
#         self.summary.add_scalar('training/loss_rec', loss_rec_cum / num_batches, self.epochs_trained)
#         self.summary.add_scalar('training/loss_dkl', loss_dKL_cum / num_batches, self.epochs_trained)
#         self.summary.add_scalar('training/eps', eps, self.epochs_trained)
#         self.summary.flush()
#
#     return aggressive

# endregion

# endregion

###########################

# region 6) Start training

print("____________________________________________________________")
numEpochs = configOptions['training']['epochs']

for epoch in range(numEpochs):
    print("Epoch {} / {}".format(epoch, numEpochs))
    train(epoch, numEpochs)
    validate(epoch, numEpochs)
    print("____________________________________________________________")


summaryWriter.close()

# endregion

