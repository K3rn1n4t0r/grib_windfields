import os
import sys
import torch
# change working directory
os.chdir(os.getcwd())
# add working directory to python environment to allow calls from subdirectories
sys.path.insert(0, os.getcwd())
from Training.Parser import CoherenceModelParser
from Training.TrainingSetup import DataSetting, CoherenceModelSetting

torch.set_num_threads(4)

# parse arguments
parser = CoherenceModelParser()
parser.parse(sys.argv[1:])
configOpts = parser.options
print(configOpts.keys())

dataSetting = DataSetting(configOpts)
modelSetting = CoherenceModelSetting(configOpts)

modelSetting.run_training(dataSetting)

