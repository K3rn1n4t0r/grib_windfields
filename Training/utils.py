# adapted from Pix2Pix repository
# https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix/blob/master/models/networks.py

import os
import matplotlib.pyplot as plt
import torch
from torch.nn import init
# from GUI.GraphCreator import createVectorGraph
from DataGeneration.GridPadder import unpadArrays
import json
import numpy as np


# Adapted from Alex and Sebastian
def findRunNumber(folder):
    files = os.listdir(folder)
    files = sorted([f for f in files if f.startswith('run')])
    return len(files)


def printConfigs(d, indent=0):
    for key, value in d.items():
        # print('\t' * indent + str(key))
        if isinstance(value, dict):
            print('\t' * indent + str(key))
            print('\t' * (indent) + "{")
            printConfigs(value, indent + 1)
            print('\t' * (indent) + "}")
        else:
            print('\t' * (indent) + '{} -> {}'.format(key, value))
            #print('\t' * (indent + 1) + str(value))

class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(NpEncoder, self).default(obj)

def printConfigToSummary(d, writer):
    txt = json.dumps(d, cls=NpEncoder)
    writer.add_text("config", txt)


# Initialize the weights for the currently used network (net)
# Types for initializations are: normal | xavier | kaiming | orthogonal
def initWeights(net, initType='normal', initGain=0.02):

    def initFunc(m):
        classname = m.__class__.__name__
        if hasattr(m, 'weight') and (classname.find('Conv') != -1 or classname.find('Linear') != -1):
            if initType == 'normal':
                init.normal_(m.weight.data, 0.0, initGain)
            elif initType == 'xavier':
                init.xavier_normal_(m.weight.data, gain=initGain)
            elif initType == 'kaiming':
                init.kaiming_normal_(m.weight.data, a=0, mode='fan_in')
            elif initType == 'orthogonal':
                init.orthogonal_(m.weight.data, gain=initGain)
            else:
                raise NotImplementedError('Weight initialization method [{}] is not implemented.'.format(initType))

            if hasattr(m, 'bias') and m.bias is not None:
                init.constant_(m.bias.data, 0.0)

        elif classname.find('BatchNorm') != -1:
            init.normal_(m.weight.data, 1.0, initGain)
            init.constant_(m.bias.data, 0.0)

    print("[INFO]: Initialize network with method {}.".format(initType))
    net.apply(initFunc)


def saveCheckpoint(epoch, net, optimizer, scheduler, scaleOpt, outDir):
    modelName = net.__class__.__name__
    modelOutDir = os.path.join(outDir, '{}_epoch_{}.pth'.format(modelName, epoch))

    state = {'epoch': epoch, 'model': net}
    state.update({'optimizer': optimizer, 'scheduler': scheduler})
    state.update({'scalingsInput': scaleOpt.input, 'scalingsTarget': scaleOpt.target,
                  'scalingsHR': scaleOpt.hr})

    torch.save(state, modelOutDir)
    print("Saved checkpoint for epoch {} to file {}".format(epoch, modelOutDir))


def saveScalings(scaleOpt, outDir):
    scalingsName = "scaling.pth"
    fileOutDir = os.path.join(outDir, scalingsName)
    state = {'scalingsInput': scaleOpt.input, 'scalingsTarget': scaleOpt.target,
             'scalingsHR': scaleOpt.hr}

    torch.save(state, fileOutDir)
    print("Saved scalings to file {}".format(fileOutDir))


# B) Debugging information during training
# def printVectorImages(epoch, index, inputInfo, targetInfo, predInfo, upsamplingInfo,
#                       gridLR, maskLR, gridHR, maskHR, imagePrefix, summaryWriter):
#     fig = plt.figure(figsize=(21.6, 21.6))
#     # fig = plt.figure(figsize=(25.6, 14.4))
#     # fig = plt.figure(figsize=(16.8, 10.5))
#     ax = fig.add_subplot(1, 1, 1)
#     ax.set_title("Input", fontsize=40)
#     grids = unpadArrays([inputInfo[0], inputInfo[1], gridLR[0], gridLR[1]], maskLR)
#     createVectorGraph(grids[0], grids[1], grids[2], grids[3])  # , scale=3)
#     plt.tight_layout()
#     #plt.savefig("input_{}.pdf".format(index), format='pdf')
#     summaryWriter.add_figure('{}/figInput'.format(imagePrefix, epoch), fig, epoch)
#     # summaryWriter.add_figure('train/figInput'.format(epoch), inputFig, epoch)
#     # plt.close()
#
#     # fig = plt.figure(figsize=(16.8, 10.5))
#     fig = plt.figure(figsize=(43.2, 21.6))
#     # fig = plt.figure(figsize=(25.6, 14.4))
#     gridCur = unpadArrays([gridHR[0], gridHR[1]], maskHR)
#
#     predUV = unpadArrays([predInfo[0], predInfo[1]], maskHR)
#     ax = fig.add_subplot(1, 2, 1)
#     ax.set_title("Prediction", fontsize=40)
#     createVectorGraph(predUV[0], predUV[1], gridCur[0], gridCur[1])
#     # summaryWriter.add_figure('train/figPredict'.format(epoch), targetFig, epoch)
#     # plt.close()
#
#     grids = unpadArrays([targetInfo[0], targetInfo[1]], maskHR)
#     ax = fig.add_subplot(1, 2, 2)
#     ax.set_title("Target", fontsize=40)
#     createVectorGraph(grids[0], grids[1], gridCur[0], gridCur[1])
#     plt.tight_layout(pad=0, w_pad=0, h_pad=0)
#     #plt.savefig("prediction_{}.pdf".format(index), format='pdf')
#     summaryWriter.add_figure('{}/figPredTarget'.format(imagePrefix, epoch), fig, epoch)
#     # summaryWriter.add_figure('train/figTarget'.format(epoch), targetFig, epoch)
#     # plt.close()
#
#     # grids = unpadArrays([target0[0], target0[1]], maskHR)
#     fig = plt.figure(figsize=(43.2, 21.6))
#     ax = fig.add_subplot(1, 2, 2)
#     ax.set_title("Target", fontsize=40)
#     createVectorGraph(grids[0], grids[1], gridCur[0], gridCur[1])
#     # summaryWriter.add_figure('train/figTarget'.format(epoch), targetFig, epoch)
#     # plt.close()
#
#     predUpUV = unpadArrays([upsamplingInfo[0], upsamplingInfo[1]], maskHR)
#     ax = fig.add_subplot(1, 2, 1)
#     ax.set_title("Upsampling (Bilinear)", fontsize=40)
#     createVectorGraph(predUpUV[0], predUpUV[1], gridCur[0], gridCur[1])
#     # summaryWriter.add_figure('train/figUpBilinear'.format(epoch), targetFig, epoch)
#     # plt.close()
#     plt.tight_layout(pad=0, w_pad=0, h_pad=0)
#     #plt.savefig("target_{}.pdf".format(index), format='pdf')
#     summaryWriter.add_figure('{}/figUpTarget'.format(imagePrefix, epoch), fig, epoch)

