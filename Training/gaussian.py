import numpy as np
import math
import torch
import gpytorch

class GPModel(gpytorch.models.ExactGP):
    def __init__(self, train_x, train_y, likelihood):
        super(GPModel, self).__init__(train_x, train_y, likelihood)

        self.mean_module = gpytorch.means.ConstantMean()
        self.covar_module = gpytorch.kernels.ScaleKernel(gpytorch.kernels.RBFKernel(ard_num_dims=3))

    def forward(self, x):
        mean_x = self.mean_module(x)
        covar_x = self.covar_module(x)
        return gpytorch.distributions.MultivariateNormal(mean_x, covar_x)

likelihood = gpytorch.likelihoods.GaussianLikelihood()

lons = np.linspace(0., 20., 60)
lats = np.linspace(40., 50., 36)

grid_ll = torch.tensor([lons, lats])

print(grid_ll.shape)
print(lons, lats)
train_x = gpytorch.utils.grid.create_data_from_grid(grid_ll)

print(train_x)

model = GPModel(train_x, train_y, likelihood)

