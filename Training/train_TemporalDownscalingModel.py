import os
import sys
import json
import torch
# change working directory
os.chdir(os.getcwd())
# add working directory to python environment to allow calls from subdirectories
sys.path.insert(0, os.getcwd())

from Training.Parser import TemporalDownscalingParser
from Training.TrainingSetup import DataSetting, TemporalDownscalingSetting

torch.set_num_threads(4)

# parse arguments
parser = TemporalDownscalingParser()
parser.parse(sys.argv[1:])
configOpts = parser.options

dataSetting = DataSetting(configOpts)
modelSetting = TemporalDownscalingSetting(configOpts)

modelSetting.run_training(dataSetting)

