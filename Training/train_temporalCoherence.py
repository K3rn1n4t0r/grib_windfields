import os
import sys
# change working directory
os.chdir(os.getcwd())
# add working directory to python environment to allow calls from subdirectories
sys.path.insert(0, os.getcwd())
from DataPreprocessing import DataLoader as dl
from Networks.UNet import UNet, UNetHROro
from Networks.ResUNet import ResUNet, ResUNetHROro
from Networks.EnhanceNet import EnhanceNet
from Networks.UNet_D2S import UNet_D2S
from Utils import computeBatchLoss
from Training.utils import *
from Training.Parser import ArgParser
from Utils.ProgressBar import ProgressBar

import numpy as np
import random
import json
##############################################
# PyTorch specific libraries
import torch
import torch.nn as nn
from torch.nn import functional as F

# Gradient optimizer
import torch.optim as optim
# loading data
from torch.utils.data import DataLoader
##############################################
from tqdm import tqdm

# summary with tensorboard
from tensorboardX import SummaryWriter

##########################
# Settings

# set maximum number of threads to not fully occupy all threads on server
torch.set_num_threads(4)
# parse arguments
parser = ArgParser()
parser.parse(sys.argv[1:])
configOptions = parser.options

netName = configOptions['training']['network']

logDir = configOptions['debugging']['recordDir']
directories = {'results': os.path.join(logDir, netName),
               'records': os.path.join(logDir, netName, 'records'),
               'models':  os.path.join(logDir, netName, 'records', 'models'),
               'data': {'training': configOptions['preprocess']['pathTraining'],
                        'test':     configOptions['preprocess']['pathTest'],
                        'raw':      configOptions['preprocess']['pathRaw']}
               }

if not os.path.exists(directories['records']):
    os.makedirs(directories['records'])

runNumber = findRunNumber(directories['records'])

gpuID = configOptions['training']['gpu']
print("[INFO]: Current working directory: <{}>".format(os.getcwd()))

# set up environment to use gpu with device ID gpuID only
os.environ["CUDA_VISIBLE_DEVICES"] = str(gpuID)

cudaAvailable = torch.cuda.is_available()
device = torch.device("cuda:0" if cudaAvailable else "cpu")
if cudaAvailable:    
    print("[INFO]: Using graphics card number <{}>".format(gpuID))
else:
    print("[WARNING]: No available CUDA device found. Training is performed on CPU!")

if sys.gettrace():
    print("[WARNING]: Programs runs in DEBUG mode")
    runName = 'run_debug'
else:
    runName = 'run_%05d' % runNumber
directories.update({'currentRun': os.path.join(directories['records'], runName)})
directories.update({'models': os.path.join(directories['currentRun'], 'models')})

if not os.path.exists(directories['models']):
    os.makedirs(directories['models'])

if configOptions['debugging']['printConfig']:
    print("[INFO]: Config options:\n===================")
    printConfigs(configOptions)
    print("===================")

if configOptions['debugging']['printDirectories']:
    print("[INFO]: Directories::\n===================")
    printConfigs(directories)
    print("===================")

print("[INFO]: Training run number: <{}>".format(runNumber))

# initialize summary writer
summaryWriter = SummaryWriter(directories['currentRun'], purge_step=1)
printConfigToSummary(configOptions, summaryWriter)

###########################
# 0) Random seeds
seed = random.randint(0, 2**32 - 1)
#print("Random seed: {}".format(seed))
random.seed(seed)
np.random.seed(seed)
torch.manual_seed(seed)
torch.cuda.manual_seed_all(seed)

###########################
# 1) Generate data

# run DataGeneration/generate.py

###########################
# 2) Load data
from DataPreprocessing.DataScaler import DataScaler
from DataPreprocessing.Quantizer import Quantizer

dataOpt = dl.dataSettings('input', 'input_orography', configOptions['data']['gridsInput'],
                          'target', 'target_orography', configOptions['data']['gridsTarget'])

valiOpt = dl.validationSettings(configOptions['data']['validationMode'],
                                configOptions['data']['validationInput'],
                                configOptions['data']['validationRegions'])

patchMode = configOptions['preprocess']['patchSize'] if configOptions['preprocess']['patching'] else None
trainingOpt = dl.trainingSettings(configOptions['preprocess']['monthsTraining'],
                                  patchMode, configOptions['data']['useHROrography'],
                                  configOptions['preprocess']['trainingRegions'])

# set up scaling for variable transformation
scalerUV = DataScaler(mode='normalize_neg')
scalerLSM = DataScaler(mode='normalize_neg')
scalerBLH = DataScaler(mode='normalize_neg')
scalerFSR = DataScaler(mode='normalize_neg')
scalerZ = DataScaler(mode='normalize_neg')
scalerZOro = DataScaler(mode='normalize_neg')
scalerLSMOro = DataScaler(mode='normalize_neg')
# quantizerUV = Quantizer(num_bins=100)

scalerUVHR = DataScaler(mode='normalize_neg')

scaleOpt = dl.scalingSettings([(scalerUV, ['windU', 'windV']),
                               (scalerLSM, ['seaMask']),
                               (scalerZ, ['z']),
                               (scalerBLH, ['blh']),
                               (scalerFSR, ['fsr'])],
                              #[(quantizerUV, ['windU', 'windV'])])
                              [(scalerUVHR, ['windU', 'windV'])],
                              [(scalerZOro, ['z']), (scalerLSMOro, ['seaMask'])])

tempOpt = dl.temporalSettings(configOptions['data']['numStepsPast'], 1, configOptions['data']['numStepsFuture'], 1)

dataLoader = dl.DataLoader(directories['data']['training'],
                           configOptions['data']['maxNumFiles'],
                           dataOpt, trainingOpt, scaleOpt, valiOpt, tempOpt)
dataTraining, dataValidation = dataLoader.loadDataForTraining()

# create loaders for PyTorch with pre-defined batch size
batchSize = configOptions['training']['batchSize']
trainLoader = DataLoader(dataTraining, batch_size=batchSize,
                         shuffle=True, drop_last=True, num_workers=0)
validationLoader = DataLoader(dataValidation, batch_size=batchSize,
                              shuffle=False, drop_last=True, num_workers=0)
numStepsPast = configOptions['data']['numStepsPast']
numStepsFuture = configOptions['data']['numStepsFuture']
numSteps = numStepsPast + numStepsFuture + 1
idxPresent = numStepsPast
####
# Save current config in json file
with open(os.path.join(directories['currentRun'], 'config.json'), 'w') as f:
    json.dump(configOptions, f, indent=4)

saveScalings(scaleOpt, directories['models'])

###########################
# 3) Setup networks, optimizer, and scheduler

# build network
# netName = configOptions['training']['network']

if configOptions['data']['useHROrography']:

    if netName == 'UNet':
        net = UNetHROro(dataTraining.inputs.shape[1], dataTraining.hrChannels, dataTraining.targets.shape[1],
                        configOptions['training']['inputChannels'],
                        num_hr_layers=configOptions['training']['hrConvLayers'],
                        hr_kernel_size=configOptions['training']['hrKernelSize'])
    elif netName == 'ResUNet':
        net = ResUNetHROro(data_channels=dataTraining.inputs.shape[1], in_channels=configOptions['training']['inputChannels'],
                           out_channels=dataTraining.targets.shape[1], hr_channels=dataTraining.hrChannels,
                           resblocks_per_stage=1, layers_per_block=2, use_oa=False,
                           num_hr_layers=configOptions['training']['hrConvLayers'],
                           hr_kernel_size=configOptions['training']['hrKernelSize'])
    else:
        raise NotImplemented("Network architecture <{}> not implemented".format(netName))
else:
    if netName == 'UNet':
        net = UNet(dataTraining.inputs.shape[1], dataTraining.targets.shape[1],
                   configOptions['training']['inputChannels'])
    elif netName == 'EnhanceNet':
        net = EnhanceNet(data_channels=dataTraining.inputs.shape[1],
                      in_channels=configOptions['training']['inputChannels'],
                      out_channels=dataTraining.targets.shape[1],
                      resblocks=10, layers_per_block=2)

    elif netName == 'ResUNet':
        net = ResUNet(data_channels=dataTraining.inputs.shape[1],
                      in_channels=configOptions['training']['inputChannels'],
                      out_channels=dataTraining.targets.shape[1],
                      resblocks_per_stage=1, layers_per_block=2, use_oa=False)
    elif netName == 'UNet_D2S':
        net = UNet_D2S(data_channels=dataTraining.inputs.shape[1],
                       in_channels=configOptions['training']['inputChannels'],
                       out_channels=dataTraining.targets.shape[1],
                       use_oa=False, dropout = 0.1)
    else:
        raise NotImplementedError("Network architecture <{}> not implemented".format(netName))

print("[INFO]: Used network architecture: {}".format(net.__class__.__name__))

# randomize weights for initial step
initWeights(net, configOptions['training']['initMethod'])
if configOptions['debugging']['printNetworkDetails']:
    print(net)
# transform to cuda
print("[INFO]: Upload downscaling network to GPU ...")
net.cuda()
# track learnable parameters
modelParameters = filter(lambda p: p.requires_grad, net.parameters())
params = sum([np.prod(p.size()) for p in modelParameters])
print("[INFO]: Number of learnable parameters: {}".format(params))

lossNetPath = os.path.join('results', 'MinimalNet', 'records',
                           'run_00072', 'models', 'MinimalNet_Temporal_epoch_100.pth')
modelDict = torch.load(lossNetPath)
lossNet = modelDict['model']
del modelDict
print("[INFO]: Upload loss network to GPU ...")
lossNet.cuda()
lossNet.eval()

# create optimizer ADAM with learning rate
optimizer = optim.Adam(net.parameters(), lr=configOptions['optimizer']['learningRate'],
                       betas=configOptions['optimizer']['betas'], weight_decay=0.0)
# create scheduler to reduce learning rate on-the-fly, lr decay
scheduler = None
schedulerType = configOptions['scheduler']['type']
if schedulerType == 'step':
    scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=configOptions['scheduler']['steps'],
                                          gamma=configOptions['scheduler']['gamma'])
elif schedulerType == 'plateau':
    scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.717, patience=20,
                                                     verbose=False, threshold=1.e-2, threshold_mode='rel',
                                                     cooldown=0, min_lr=0, eps=1e-08)
else:
    raise NotImplementedError('Scheduler {} is not implemented'.format(schedulerType))

###########################
# 4) Setup loss function
lossFunction = nn.L1Loss()
lossFunction.cuda()
print("[INFO]: Used loss function for training: {}".format(lossFunction.__class__.__name__))

###########################
# 5) Setup training
#epochs = int(iterations / len(trainLoader) + 0.5)

shape_inputs = dataTraining.inputs.shape[1:4]
shape_targets = dataTraining.targets.shape[1:4]




def train(epoch, epochs):
    # set mode of network to train (parameters will be affected / updated)
    net.train()
    # advance scheduler to perform lr decay after certain steps
    if schedulerType == 'step':
        scheduler.step()

    # loss for this epoch
    trainLoss = 0
    trainLossL1 = 0
    trainLossTemp = 0
    trainLossL1Real = 0
    trainLossUpsampled = 0

    # for p in net.parameters():
    #     p.requires_grad = True

    # iterate over all batches
    numLoadings = len(trainLoader)

    progressBar = ProgressBar(numLoadings, displaySumCount=True)

    for i, trainData in enumerate(trainLoader, 0):
        inputs_cpu, targets_cpu, gridLR, gridHR, maskLR, maskHR, hrOro, idx = trainData
        inputs = torch.stack(inputs_cpu, dim=0).to(device)
        targets = torch.stack(targets_cpu, dim=0).to(device)

        optimizer.zero_grad()

        if configOptions['data']['useHROrography']:
            hrOro = hrOro.to(device)
            hrOro = torch.repeat(hrOro, numSteps, 1, 1, 1)
            predictions = net(inputs.view(numSteps * batchSize, *shape_inputs), hrOro)
        else:
            predictions = net(inputs.view(numSteps * batchSize, *shape_inputs))
        predictions = predictions.view(numSteps, batchSize, *shape_targets)

        # compute loss to current targets
        lossL1 = lossFunction(predictions, targets)

        # compute real loss
        if configOptions['debugging']['printRealLoss']:
            lossL1Real = lossFunction(scalerUVHR.transform_back(predictions), scalerUVHR.transform_back(targets))
            trainLossL1Real += lossL1Real.item()

        # compute temporal loss
        section = list(range(numSteps))
        section.pop(idxPresent)
        lossTemp = lossFunction(
            predictions[idxPresent],
            lossNet(
                predictions[section].transpose(0, 1).view(
                    batchSize, (numSteps - 1) * shape_targets[0], *shape_targets[1:]
                )
            )
        )

        # compute total loss
        if configOptions['training']['useTemporalLoss']:
            loss = lossL1 + lossTemp
        else:
            loss = lossL1
        loss.backward()

        # advance weights towards minimum
        optimizer.step()

        # update training loss
        trainLoss += loss.item()
        trainLossL1 += lossL1.item()
        trainLossTemp += lossTemp.item()

        # progress bar
        progressBar.proceed(i + 1)

    # update records per train step
    trainLoss /= numLoadings
    trainLossL1 /= numLoadings
    trainLossTemp /= numLoadings
    summaryWriter.add_scalar('train/loss', trainLoss, epoch)
    summaryWriter.add_scalar('train/lossL1', trainLossL1, epoch)
    summaryWriter.add_scalar('train/lossTemp', trainLossTemp, epoch)

    currentLR = 0.0
    if schedulerType == 'step':
        currentLR = scheduler.get_lr()[0]
    elif schedulerType == 'plateau':
        currentLR = optimizer.param_groups[0]['lr']
    summaryWriter.add_scalar('train/lr', currentLR, epoch)

    print("Training: L1 {}, LRDecay {}".format(trainLossL1, currentLR))
    print("Training: TC {}".format(trainLossTemp))

    if configOptions['debugging']['printRealLoss']:
        trainLossL1Real /= numLoadings
        summaryWriter.add_scalar('train/lossL1Real', trainLossL1Real, epoch)
        print("Training: L1 Real {},".format(trainLossL1Real))

    if epoch % configOptions['training']['saveEveryNthEpoch'] == 0:
        saveCheckpoint(epoch, net, optimizer, scheduler, scaleOpt, directories['models'])

    if schedulerType == 'plateau':
        scheduler.step(trainLoss)

    summaryWriter.flush()


def validate(epoch, epochs):
    # set mode of network to evaluation to not update any parameters
    net.eval()

    validationLoss = 0
    validationLossL1 = 0
    validationLossTemp = 0
    validationLossL1Real = 0
    validationLossUpsampled = 0

    numLoadings = len(validationLoader)

    progressBar = ProgressBar(numLoadings, displaySumCount=True)
    with torch.no_grad():
        for i, validationData in enumerate(validationLoader, 0):
            inputs_cpu, targets_cpu, gridLR, gridHR, maskLR, maskHR, hrOro, idx = validationData
            inputs = torch.stack(inputs_cpu, dim=0).to(device)
            targets = torch.stack(targets_cpu, dim=0).to(device)

            if configOptions['data']['useHROrography']:
                hrOro = hrOro.to(device)
                hrOro = torch.repeat(hrOro, numSteps, 1, 1, 1)
                predictions = net(inputs.view(numSteps * batchSize, *shape_inputs), hrOro)
            else:
                predictions = net(inputs.view(numSteps * batchSize, *shape_inputs))
            predictions = predictions.view(numSteps, batchSize, *shape_targets)

            # compute loss to current targets
            lossL1 = lossFunction(predictions, targets)

            # compute real loss
            if configOptions['debugging']['printRealLoss']:
                lossL1Real = lossFunction(scalerUVHR.transform_back(predictions), scalerUVHR.transform_back(targets))
                validationLossL1Real += lossL1Real.item()

            # compute temporal loss
            section = list(range(numSteps))
            section.pop(idxPresent)
            lossTemp = lossFunction(
                predictions[idxPresent],
                lossNet(
                    predictions[section].transpose(0, 1).view(
                        batchSize, (numSteps - 1) * shape_targets[0], *shape_targets[1:]
                    )
                )
            )

            # compute total loss
            if configOptions['training']['useTemporalLoss']:
                loss = lossL1 + lossTemp
            else:
                loss = lossL1

            validationLoss += loss.item()
            validationLossL1 += lossL1.item()
            validationLossTemp += lossTemp.item()

            # progress bar
            progressBar.proceed(i + 1)

    # update records per validation step
    validationLoss /= numLoadings
    validationLossL1 /= numLoadings
    validationLossTemp /= numLoadings
    summaryWriter.add_scalar('validation/loss', validationLoss, epoch)
    summaryWriter.add_scalar('validation/lossL1', validationLossL1, epoch)
    summaryWriter.add_scalar('validation/lossTemp', validationLossTemp, epoch)

    print("Validation: L1 {}".format(validationLossL1))
    print("Validation: TC {}".format(validationLossTemp))

    if configOptions['debugging']['printRealLoss']:
        validationLossL1Real /= numLoadings
        summaryWriter.add_scalar('validation/lossRealL1', validationLossL1Real, epoch)
        print("Validation: L1 Real {},".format(validationLossL1Real))

    # if configOptions['debugging']['printUpsamplingLoss']:
    #     validationLossUpsampled /= numLoadings
    #     summaryWriter.add_scalar('validation/lossUpsampledL1', validationLossUpsampled, epoch)
    #     print("Validation: L1 Bilinear {},".format(validationLossUpsampled))

    summaryWriter.flush()

###########################
# 6) Start training




# summaryWriter.close()



