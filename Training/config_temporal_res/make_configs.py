import json
import os

output_dir = 'open'
if not os.path.isdir(output_dir):
    os.makedirs(output_dir)

counter = 0
config = {}

with open('temporal_base.json', 'r') as f:
    config.update(json.load(f))

config['network'].update({'name': 'ResUNet_Temporal'})

for idx_HROro, config_HROro in enumerate([[0, 7], [1, 5]]):
    for idx_num_steps, num_steps in enumerate([[0, 0], [1, 0], [2, 0], [4, 0], [1, 1], [2, 2]]):
        config['preprocess'].update({
            'patching': True,
            'pathTraining': '/home/hoehlein/data/processed'
        })
        config['data'].update({
            'useHROrography': True if config_HROro else False,
        })
        config['training'].update({
            'loss': {
                'name': 'L1',
                'useMask': True,
            },
            'gpu': 0,
        })
        config['network'].update({
            'inputChannels': 64,
            'hrConvLayers': config_HROro[0] if config_HROro else None,
            'hrKernelSize': config_HROro[1] if config_HROro else None
        })
        config['debugging'].update({
            'recordDir': 'results/temporal',
        })
        config['coherence'].update({
            'numStepsPast': num_steps[0],
            'numStepsFuture': num_steps[1],
        })
        file_name = 'config_{}{}.json'.format(
            idx_HROro, idx_num_steps
        )
        with open(os.path.join(output_dir, file_name), 'w') as f:
            json.dump(config, f, indent=4)
        counter += 1
