from Training.utils import printConfigs
from Training.TrainingSetup import SingleTimeDataSetting, SingleTimeDownscalingModelSetting
import numpy as np
from itertools import combinations, product
from Networks.UNet import UNet
from Networks.ResUNet import ResUNet
from Networks.EnhanceNet import EnhanceNet
from Networks.HROrographyNet import *


class GridPermutationGenerator:
    def __init__(self, grids):
        self.grids = np.array(grids)
        self.num_grids = len(grids)
        self.idx_selection = np.arange(0, self.num_grids, 1)

    def __iter__(self):
        # permutation length (size of tuples for combination)
        self.lp = 1
        # types of combinations
        self.p = []
        self.n = 0
        return self

    def __next__(self):
        if self.lp > self.num_grids:
            raise StopIteration
        else:
            if self.n >= len(self.p):
                self.p = list(combinations(self.idx_selection, self.lp))
                self.lp += 1
                self.n = 0

            permutation = list(self.p[self.n])
            result = self.grids[permutation]
            self.n += 1
            return result


class ModelParamSettings:
    def __init__(self, model='UNet', loss='L1', loss_use_mask=False, init_method='orthogonal', num_channels=64):
        self.model = model
        self.loss = loss
        self.loss_use_mask = loss_use_mask
        self.init_method = init_method
        self.num_channels = num_channels

    def update_config(self, cfg):
        cfg['training']['loss'].update({'name': self.loss})
        cfg['training']['loss'].update({'useMask': self.loss_use_mask})
        cfg['network'].update({'name': self.model,
                               'initMethod': self.init_method,
                               'inputChannels': self.num_channels})


class CrossValidationSettings:
    def __init__(self, training_domain='alps', validation_domain='alps', validation_year=2016):
        self.training_domain = training_domain
        self.validation_domain = validation_domain
        self.validation_year = validation_year

    def update_config(self, cfg):
        cfg['preprocess']['monthsTraining'] = [[(2016, 1), (2020, 1)]]
        cfg['preprocess']['trainingRegions'] = [self.training_domain]
        cfg['data']['validationInput'] = [[(self.validation_year, 1), (self.validation_year + 1, 1)]]
        cfg['data']['validationRegions'] = [self.validation_domain]


class ModelHRSettings:
    def __init__(self, num_conv_layer=0, kernel_size=3):
        self.num_conv_layer = num_conv_layer
        self.kernel_size = kernel_size

    def update_config(self, cfg):
        cfg['network'].update({'hrConvLayers': self.num_conv_layer,
                               'hrKernelSize': self.kernel_size})


class OptimizerSettings:
    def __init__(self, beta=0.9, learning_rate=0.001):
        self.beta = beta
        self.learning_rate = learning_rate

    def update_config(self, cfg):
        cfg['optimizer'].update({'learningRate': self.learning_rate,
                                 'betas': [self.beta, 0.999]})


class HRTopographySetting:
    def __init__(self, enable=False):
        self.enable = enable

    def update_config(self, cfg):
        cfg['data'].update({'useHROrography': self.enable})


class PatchModeSettings:
    def __init__(self, enable=False, size=(36, 36)):
        self.enable = enable
        self.size = size

    def update_config(self, cfg):
        cfg['preprocess'].update({'patching': self.enable,
                                  'patchSize': self.size})


class ParameterIterator:
    def __init__(self, cfg):
        model_set_combos = list(product(cfg['models'], cfg['losses'], cfg['lossUseMask'], cfg['init_methods']))
        self.model_setting_pool = [ModelParamSettings(*setting) for setting in model_set_combos]

        # model_hr_set_combos = list(product(cfg['hr_num_conv_layers'], cfg['hr_kernel_sizes']))
        # self.model_hr_setting_pool = [ModelHRSettings(*setting) for setting in model_hr_set_combos]

        optimizer_set_combos = list(product(cfg['betas'], cfg['learning_rates']))
        self.optimizer_setting_pool = [OptimizerSettings(*setting) for setting in optimizer_set_combos]

        hr_set_combos = list(product(cfg['use_hr']))
        self.hr_setting_pool = [HRTopographySetting(*setting) for setting in hr_set_combos]

        patch_set_combos = cfg['patch_params']
        self.patch_mode_pool = [PatchModeSettings(setting[0], setting[1]) for setting in patch_set_combos]

        cross_valid_combos = list(product(cfg['training_domain'], cfg['validation_domain'], cfg['validation_year']))
        self.cross_valid_pool = [CrossValidationSettings(*setting) for setting in cross_valid_combos]

        self.settingCombos = list(product(self.model_setting_pool,
                                          self.optimizer_setting_pool))

    def __iter__(self):
        self.n = 0
        return self

    def __next__(self):
        if self.n >= len(self.settingCombos):
            raise StopIteration
        else:
            result = self.settingCombos[self.n]
            self.n += 1
            return result


class ParameterSettingTrainer:
    def __init__(self, num_epochs, input_grids, config_opts, param_opts,
                 train_grids=False, train_network_params=False, num_repeats=1,
                 train_padding_modes=False, train_cosine_norm=False,
                 default_grids=['windU', 'windV']):
        # static parameters
        # config_opts['training']['loss']['useMask'] = False
        config_opts['training']['epochs'] = num_epochs
        config_opts['training']['coherenceModel'] = None

        self.train_padding_modes = train_padding_modes
        self.train_cosine_norm = train_cosine_norm

        self.cfg_opts = config_opts

        self.param_pool = ParameterIterator(param_opts)
        self.train_grids = train_grids
        if train_grids:
            self.grid_permutator = GridPermutationGenerator(input_grids)
        else:
            self.grid_permutator = [input_grids]
        self.static_input_grids = default_grids
        self.train_network_params = train_network_params
        # number of repetitions per setting
        self.num_repeats = num_repeats

    def run(self):
        # start data settings and load data first
        data_setting = SingleTimeDataSetting(self.cfg_opts, prepare=False)

        # for each input grid --> recreate torch dataset
        for grids in self.grid_permutator:
            input_grids = self.static_input_grids + grids.tolist()
            self.cfg_opts['data'].update({'gridsInput': input_grids})

            for cross_valid in self.param_pool.cross_valid_pool:
                cross_valid.update_config(self.cfg_opts)

                # for each patch_enabled setting --> recreate torch dataset
                for patch_setting in self.param_pool.patch_mode_pool:
                    patch_setting.update_config(self.cfg_opts)

                    for hr_setting in self.param_pool.hr_setting_pool:
                        hr_setting.update_config(self.cfg_opts)

                        # update settings for data
                        # --> activate pre-processing
                        data_setting.update_data(self.cfg_opts)

                        for settings in self.param_pool:
                            for setting in settings:
                                # update configs
                                setting.update_config(self.cfg_opts)

                            if self.train_network_params:
                                use_hr_oro = self.cfg_opts['data']['useHROrography']
                                network_param_pool = None

                                if use_hr_oro:
                                    input_module_type = InputModuleType( self.cfg_opts['network']['HROro']['mode'])
                                    if input_module_type == InputModuleType.CONV:
                                        # network_param_pool = InputConvBlocksParameterSetting.create_parameter_pool()
                                        network_param_pool = UNet.create_parameter_pool()
                                    elif input_module_type == InputModuleType.RESNET:
                                        network_param_pool = InputResNetBlocksParameterSetting.create_parameter_pool()
                                    elif input_module_type == InputModuleType.INCEPTION:
                                        network_param_pool = InputInceptionBlocksParameterSetting.create_parameter_pool()
                                else:
                                    if self.cfg_opts['network']['name'] == 'UNet':
                                        network_param_pool = UNet.create_parameter_pool()
                                    elif self.cfg_opts['network']['name'] == 'ResUNet':
                                        network_param_pool = ResUNet.create_parameter_pool()
                                    elif self.cfg_opts['network']['name'] == 'EnhanceNet':
                                        network_param_pool = EnhanceNet.create_parameter_pool()

                                for network_param in network_param_pool:
                                    network_param.update_config(self.cfg_opts)

                                    self._start_training(data_setting)

                            elif self.train_padding_modes:
                                padding_modes = ['zero', 'replication', 'reflection']
                                for mode in padding_modes:
                                    self.cfg_opts['network']['paddingMode'] = mode

                                    self._start_training(data_setting)
                            elif self.train_cosine_norm:
                                cosine_w = np.arange(0.5, 1.1, 0.1)
                                norm_w = np.arange(0.5, 1.1, 0.1)

                                for cw in cosine_w:
                                    for nw in norm_w:
                                        self.cfg_opts['training']['loss']['cosineNorm']['weight_cosine'] = cw
                                        self.cfg_opts['training']['loss']['cosineNorm']['weight_norm'] = nw

                                        self._start_training(data_setting)
                            else:
                                self._start_training(data_setting)

    def _start_training(self, data_setting):
        for i in range(self.num_repeats):
            printConfigs(self.cfg_opts)
            model_setting = SingleTimeDownscalingModelSetting(self.cfg_opts, data_setting.scalings)
            model_setting.run_training(data_setting)
