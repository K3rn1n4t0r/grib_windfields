import os
import sys
# change working directory
os.chdir(os.getcwd())
# add working directory to python environment to allow calls from subdirectories
sys.path.insert(0, os.getcwd())
from DataPreprocessing import DataLoader as dl
# from Networks.UNet import UNet, UNetHROro
# from Networks.ResUNet import ResUNet, ResUNetHROro
from Networks.EnhanceNet import EnhanceNet
from Networks.UNet_D2S import UNet_D2S
from Utils import computeBatchLoss
from Training.utils import *
from Training.Parser import ArgParser
from Utils.ProgressBar import ProgressBar

import numpy as np
import random
import json
##############################################
# PyTorch specific libraries
import torch
import torch.nn as nn
from torch.nn import functional as F

# Gradient optimizer
import torch.optim as optim
# loading data
from torch.utils.data import DataLoader
# auto-gradient
from torch.autograd import Variable
##############################################
from tqdm import tqdm

# summary with tensorboard
from tensorboardX import SummaryWriter

##########################
# Settings

# set maximum number of threads to not fully occupy all threads on server
torch.set_num_threads(4)
# parse arguments
parser = ArgParser()
parser.parse(sys.argv[1:])
configOptions = parser.options

netName = configOptions['training']['network']

logDir = configOptions['debugging']['recordDir']
directories = {'results': os.path.join(logDir, netName),
               'records': os.path.join(logDir, netName, 'records'),
               'models':  os.path.join(logDir, netName, 'records', 'models'),
               'data': {'training': configOptions['preprocess']['pathTraining'],
                        'test':     configOptions['preprocess']['pathTest'],
                        'raw':      configOptions['preprocess']['pathRaw']}
               }

if not os.path.exists(directories['records']):
    os.makedirs(directories['records'])

runNumber = findRunNumber(directories['records'])

gpuID = configOptions['training']['gpu']
print("[INFO]: Current working directory: <{}>".format(os.getcwd()))

# set up environment to use gpu with device ID gpuID only
os.environ["CUDA_VISIBLE_DEVICES"] = str(gpuID)

cudaAvailable = torch.cuda.is_available()
device = torch.device("cuda:0" if cudaAvailable else "cpu")
if cudaAvailable:    
    print("[INFO]: Using graphics card number <{}>".format(gpuID))
else:
    print("[WARNING]: No available CUDA device found. Training is performed on CPU!")

if sys.gettrace():
    print("[WARNING]: Programs runs in DEBUG mode")
    runName = 'run_debug'
else:
    runName = 'run_%05d' % runNumber
directories.update({'currentRun': os.path.join(directories['records'], runName)})
directories.update({'models': os.path.join(directories['currentRun'], 'models')})

if not os.path.exists(directories['models']):
    os.makedirs(directories['models'])

if configOptions['debugging']['printConfig']:
    print("[INFO]: Config options:\n===================")
    printConfigs(configOptions)
    print("===================")

if configOptions['debugging']['printDirectories']:
    print("[INFO]: Directories::\n===================")
    printConfigs(directories)
    print("===================")

print("[INFO]: Training run number: <{}>".format(runNumber))

# initialize summary writer
summaryWriter = SummaryWriter(directories['currentRun'], purge_step=1)
printConfigToSummary(configOptions, summaryWriter)

###########################
# 0) Random seeds
seed = random.randint(0, 2**32 - 1)
#print("Random seed: {}".format(seed))
random.seed(seed)
np.random.seed(seed)
torch.manual_seed(seed)
torch.cuda.manual_seed_all(seed)

###########################
# 1) Generate data

# run DataGeneration/generate.py

###########################
# 2) Load data
from DataPreprocessing.DataScaler import DataScaler
from DataPreprocessing.Quantizer import Quantizer

dataOpt = dl.dataSettings('input', 'input_orography', configOptions['data']['gridsInput'],
                          'target', 'target_orography', configOptions['data']['gridsTarget'])

valiOpt = dl.validationSettings(configOptions['data']['validationMode'],
                                configOptions['data']['validationInput'],
                                configOptions['data']['validationRegions'])

patchMode = configOptions['preprocess']['patchSize'] if configOptions['preprocess']['patching'] else None
trainingOpt = dl.trainingSettings(configOptions['preprocess']['monthsTraining'],
                                  patchMode, configOptions['data']['useHROrography'],
                                  configOptions['preprocess']['trainingRegions'])

# set up scaling for variable transformation
scalerUV = DataScaler(mode='normalize_neg')
scalerLSM = DataScaler(mode='normalize_neg')
scalerBLH = DataScaler(mode='normalize_neg')
scalerFSR = DataScaler(mode='normalize_neg')
scalerZ = DataScaler(mode='normalize_neg')
scalerZOro = DataScaler(mode='normalize_neg')
scalerLSMOro = DataScaler(mode='normalize_neg')
# quantizerUV = Quantizer(num_bins=100)

scaleOpt = dl.scalingSettings([(scalerUV, ['windU', 'windV']),
                               (scalerLSM, ['seaMask']),
                               (scalerZ, ['zOro']),
                               (scalerBLH, ['blh']),
                               (scalerFSR, ['fsr'])],
                              #[(quantizerUV, ['windU', 'windV'])])
                              [],
                              [(scalerZOro, ['zOro']), (scalerLSMOro, ['seaMask'])])

dataLoader = dl.DataLoader(directories['data']['training'],
                           configOptions['data']['maxNumFiles'],
                           dataOpt, trainingOpt, scaleOpt, valiOpt)
dataTraining, dataValidation = dataLoader.loadDataForTraining()

# create loaders for PyTorch with pre-defined batch size
trainLoader = DataLoader(dataTraining, batch_size=configOptions['training']['batchSize'],
                         shuffle=True, drop_last=True, num_workers=0)
validationLoader = DataLoader(dataValidation, batch_size=configOptions['training']['batchSize'],
                              shuffle=False, drop_last=True, num_workers=0)

####
# Save current config in json file
with open(os.path.join(directories['currentRun'], 'config.json'), 'w') as f:
    json.dump(configOptions, f, indent=4)

saveScalings(scaleOpt, directories['models'])

###########################
# 3) Setup networks, optimizer, and scheduler

# build network
# netName = configOptions['training']['network']

if configOptions['data']['useHROrography']:

    if netName == 'UNet':
        net = UNetHROro(dataTraining.inputs.shape[1], dataTraining.hrChannels, dataTraining.targets.shape[1],
                        configOptions['training']['inputChannels'],
                        num_hr_layers=configOptions['training']['hrConvLayers'],
                        hr_kernel_size=configOptions['training']['hrKernelSize'])
    elif netName == 'ResUNet':
        net = ResUNetHROro(data_channels=dataTraining.inputs.shape[1], in_channels=configOptions['training']['inputChannels'],
                           out_channels=dataTraining.targets.shape[1], hr_channels=dataTraining.hrChannels,
                           resblocks_per_stage=1, layers_per_block=2, use_oa=False,
                           num_hr_layers=configOptions['training']['hrConvLayers'],
                           hr_kernel_size=configOptions['training']['hrKernelSize'])
    else:
        raise NotImplemented("Network architecture <{}> not implemented".format(netName))
else:
    if netName == 'UNet':
        net = UNet(dataTraining.inputs.shape[1], dataTraining.targets.shape[1],
                   configOptions['training']['inputChannels'])
    elif netName == 'EnhanceNet':
        net = EnhanceNet(data_channels=dataTraining.inputs.shape[1],
                      in_channels=configOptions['training']['inputChannels'],
                      out_channels=dataTraining.targets.shape[1],
                      resblocks=10, layers_per_block=2)

    elif netName == 'ResUNet':
        net = ResUNet(data_channels=dataTraining.inputs.shape[1],
                      in_channels=configOptions['training']['inputChannels'],
                      out_channels=dataTraining.targets.shape[1],
                      resblocks_per_stage=1, layers_per_block=2, use_oa=False)
    elif netName == 'UNet_D2S':
        net = UNet_D2S(data_channels=dataTraining.inputs.shape[1],
                       in_channels=configOptions['training']['inputChannels'],
                       out_channels=dataTraining.targets.shape[1],
                       use_oa=False, dropout = 0.1)
    else:
        raise NotImplementedError("Network architecture <{}> not implemented".format(netName))

print("[INFO]: Used network architecture: {}".format(net.__class__.__name__))

# randomize weights for initial step
initWeights(net, configOptions['training']['initMethod'])
if configOptions['debugging']['printNetworkDetails']:
    print(net)
# transform to cuda
print("[INFO]: Upload network to GPU ...")
net.cuda()

# track learnable parameters
modelParameters = filter(lambda p: p.requires_grad, net.parameters())
params = sum([np.prod(p.size()) for p in modelParameters])
print("[INFO]: Number of learnable parameters: {}".format(params))

# create optimizer ADAM with learning rate
optimizer = optim.Adam(net.parameters(), lr=configOptions['optimizer']['learningRate'],
                       betas=configOptions['optimizer']['betas'], weight_decay=0.0)
# create scheduler to reduce learning rate on-the-fly, lr decay
scheduler = None
schedulerType = configOptions['scheduler']['type']
if schedulerType == 'step':
    scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=configOptions['scheduler']['steps'],
                                          gamma=configOptions['scheduler']['gamma'])
elif schedulerType == 'plateau':
    scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.717, patience=20,
                                                     verbose=False, threshold=1.e-2, threshold_mode='rel',
                                                     cooldown=0, min_lr=0, eps=1e-08)
else:
    raise NotImplementedError('Scheduler {} is not implemented'.format(schedulerType))

###########################
# 4) Setup loss function
lossFunction = nn.L1Loss()
lossFunction.cuda()
print("[INFO]: Used loss function for training: {}".format(lossFunction.__class__.__name__))

###########################
# 5) Setup training
#epochs = int(iterations / len(trainLoader) + 0.5)

inputs = Variable(torch.FloatTensor(configOptions['training']['batchSize'], *dataTraining.inputs.shape[1:4]))
targets = Variable(torch.FloatTensor(configOptions['training']['batchSize'], *dataTraining.targets.shape[1:4]))

inputs = inputs.cuda()
targets = targets.cuda()

from timeit import default_timer as timer

def train(epoch, epochs):
    # set mode of network to train (parameters will be affected / updated)
    net.train()
    # advance scheduler to perform lr decay after certain steps
    if schedulerType == 'step':
        scheduler.step()

    # loss for this epoch
    trainLoss = 0
    trainLossReal = 0
    trainLossUpsampled = 0

    # for p in net.parameters():
    #     p.requires_grad = True

    # iterate over all batches
    numLoadings = len(trainLoader)
    #barBins = 20
    #barBinCount = numLoadings // min(numLoadings, 20)

    progressBar = ProgressBar(numLoadings, displaySumCount=True)

    for i, trainData in enumerate(trainLoader, 0):
        inputs_cpu, targets_cpu, gridLR, gridHR, maskLR, maskHR, hrOro, idx = trainData
        targets, inputs, hrOro = targets_cpu.to(device), inputs_cpu.to(device), hrOro.to(device)

        # convert GPU / tensor data to numpy arrays
        inputs_cpu = inputs_cpu.data.numpy()
        targets_cpu = targets_cpu.data.numpy()

        # zero out the parameters of the gradients
        optimizer.zero_grad()

        if configOptions['data']['useHROrography']:
            predictions = net(inputs, hrOro)
        else:
            predictions = net(inputs)

        lossL1 = lossFunction(predictions, targets)
        # backpropagate loss function
        lossL1.backward()
        # advance weights towards minimum
        optimizer.step()
        # update training loss
        trainLoss += lossL1.item()

        if configOptions['debugging']['printRealLoss']:
            predictions_cpu = predictions.data.cpu().numpy()
            trainLossReal += computeBatchLoss(predictions_cpu, targets_cpu,
                                               idx, [0, 1], dataTraining)

        # upsample images bilinear
        targetSize = targets_cpu.shape[2:4]

        inputs_denom = scalerUV.transform_back(inputs_cpu[:, [0, 1]])
        inputs_temp = torch.tensor(inputs_denom).to(device)
        upsamplesPred = F.interpolate(inputs_temp, size=targetSize, mode='bilinear')
        targets_temp = torch.tensor(targets_cpu).to(device)
        lossUpsampledL1 = lossFunction(upsamplesPred, targets_temp).item()

        if configOptions['debugging']['printUpsamplingLoss']:
            trainLossUpsampled += lossUpsampledL1

        if configOptions['debugging']['printImages'] and i < 1:
            gridsLR = gridLR.data.numpy()
            gridsHR = gridHR.data.numpy()
            masksLR = maskLR.data.numpy()
            masksHR = maskHR.data.numpy()

            predictions_cpu = predictions.data.cpu().numpy()

            printVectorImages(epoch, i, inputs_denom[i], targets_cpu[i], predictions_cpu[i],
                              upsamplesPred.data.cpu().numpy()[0],
                              gridsLR[i], masksLR[i],
                              gridsHR[i], masksHR[i], 'train', summaryWriter)

        # progress bar
        progressBar.proceed(i + 1)

    # update records per train step
    trainLoss /= numLoadings
    summaryWriter.add_scalar('train/lossL1', trainLoss, epoch)

    currentLR = 0.0
    if schedulerType == 'step':
        currentLR = scheduler.get_lr()[0]
    elif schedulerType == 'plateau':
        currentLR = optimizer.param_groups[0]['lr']

    print("Training: L1 {}, LRDecay {}".format(trainLoss, currentLR))

    if configOptions['debugging']['printRealLoss']:
        trainLossReal /= numLoadings
        summaryWriter.add_scalar('train/lossRealL1', trainLossReal, epoch)
        print("Training: L1 Real {},".format(trainLossReal))

    if configOptions['debugging']['printUpsamplingLoss']:
        trainLossUpsampled /= numLoadings
        summaryWriter.add_scalar('train/lossUpsampledL1', trainLossUpsampled, epoch)
        print("Training: L1 Bilinear {},".format(trainLossUpsampled))

    summaryWriter.add_scalar('train/lr', currentLR, epoch)

    if epoch % configOptions['training']['saveEveryNthEpoch'] == 0:
        saveCheckpoint(epoch, net, optimizer, scheduler, scaleOpt, directories['models'])

    if schedulerType == 'plateau':
        scheduler.step(trainLoss)

    summaryWriter.flush()


def validate(epoch, epochs):
    # set mode of network to evaluation to not update any parameters
    net.eval()
    validationLoss = 0
    validationLossReal = 0
    validationLossUpsampled = 0

    numLoadings = len(validationLoader)

    progressBar = ProgressBar(numLoadings, displaySumCount=True)

    for i, validationData in enumerate(validationLoader, 0):
        inputs_cpu, targets_cpu, gridLR, gridHR, maskLR, maskHR, hrOro, idx = validationData

        targets, inputs, hrOro = targets_cpu.to(device), inputs_cpu.to(device), hrOro.to(device)

        inputs_cpu = inputs_cpu.data.numpy()
        targets_cpu = targets_cpu.data.numpy()

        # predict validation data
        if configOptions['data']['useHROrography']:
            predictions = net(inputs, hrOro)
        else:
            predictions = net(inputs)
        # compute loss to current targets
        lossL1 = lossFunction(predictions, targets)
        # update loss
        validationLoss += lossL1.item()

        # compute real loss
        if configOptions['debugging']['printRealLoss']:
            predictions_cpu = predictions.data.cpu().numpy()
            validationLossReal += computeBatchLoss(predictions_cpu, targets_cpu,
                                                   idx, [0, 1], dataValidation)

        targetSize = targets_cpu.shape[2:4]
        # upsamplesPred = F.interpolate(inputs[:, [0, 1]], targetSize)

        inputs_cpu[:, [0, 1]] = scalerUV.transform_back(inputs_cpu[:, [0, 1]])
        inputs_temp = torch.tensor(inputs_cpu).to(device)
        upsamplesPred = F.interpolate(inputs_temp[:, [0, 1]], size=targetSize, mode='bilinear')
        targets_temp = torch.tensor(targets_cpu).to(device)
        lossUpsampledL1 = lossFunction(upsamplesPred, targets_temp).item()
        if configOptions['debugging']['printUpsamplingLoss']:
            validationLossUpsampled += lossUpsampledL1

        if configOptions['debugging']['printImages'] and i < 1:
            gridsLR = gridLR.data.numpy()
            gridsHR = gridHR.data.numpy()
            masksLR = maskLR.data.numpy()
            masksHR = maskHR.data.numpy()

            predictions_cpu = predictions.data.cpu().numpy()

            printVectorImages(epoch, i, inputs_cpu[i], targets_cpu[i], predictions_cpu[i],
                              upsamplesPred.data.cpu().numpy()[0],
                              gridsLR[i], masksLR[i],
                              gridsHR[i], masksHR[i], 'validation', summaryWriter)\

        # progress bar
        progressBar.proceed(i + 1)


    validationLoss /= numLoadings
    summaryWriter.add_scalar('validation/lossL1', validationLoss, epoch)
    print("Validation: L1 {}".format(validationLoss))

    if configOptions['debugging']['printRealLoss']:
        validationLossReal /= numLoadings
        summaryWriter.add_scalar('validation/lossRealL1', validationLossReal, epoch)
        print("Validation: L1 Real {},".format(validationLossReal))

    if configOptions['debugging']['printUpsamplingLoss']:
        validationLossUpsampled /= numLoadings
        summaryWriter.add_scalar('validation/lossUpsampledL1', validationLossUpsampled, epoch)
        print("Validation: L1 Bilinear {},".format(validationLossUpsampled))

    summaryWriter.flush()

###########################
# 6) Start training

print("____________________________________________________________")
numEpochs = configOptions['training']['epochs']

for epoch in range(numEpochs):
    print("Epoch {} / {}".format(epoch, numEpochs))
    train(epoch, numEpochs)
    validate(epoch, numEpochs)
    print("____________________________________________________________")


# summaryWriter.close()



