import os
import sys
import torch
# change working directory
os.chdir(os.getcwd())
# add working directory to python environment to allow calls from subdirectories
sys.path.insert(0, os.getcwd())
from Training.Parser import DownscalingParser
from Training.TrainingSetup import DataSetting, ModelSetting

torch.set_num_threads(4)

# parse arguments
parser = DownscalingParser()
parser.parse(sys.argv[1:])
configOpts = parser.options
print(configOpts.keys())

dataSetting = DataSetting(configOpts)
modelSetting = ModelSetting(configOpts)

modelSetting.run_training(dataSetting)

