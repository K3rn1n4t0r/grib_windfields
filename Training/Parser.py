import argparse
import json
import os


"Parse arguments either by terminal input (arguments) or by parsing json config file"


class ArgParser:
    def __init__(self):
        # parser
        self.parser = argparse.ArgumentParser(description='Parse arguments for training')
        self._add_arguments()

        # default options
        self.options = {}
        self._set_defaults()

    def parse(self, args):
        arguments = self.parser.parse_args(args)
        if arguments.config is not None:
            self._use_config(arguments)
        else:
            self._use_arguments(arguments)

    def _add_arguments(self):
        self.parser.add_argument('--config', type=str, help='specify location to a json config file')
        self._add_arguments_preprocessing()
        self._add_arguments_data()
        self._add_arguments_training()
        self._add_arguments_network()
        self._add_arguments_optimizer()
        self._add_arguments_debugging()

    # region Adding arguments
    def _add_arguments_preprocessing(self):
        self.parser.add_argument('--patch_data', action='store_true')
        self.parser.add_argument('--dataset', type=str)

    def _add_arguments_data(self):
        self.parser.add_argument('--input_vars', nargs='+', type=str)
        self.parser.add_argument('--use_hr', action='store_true')

    def _add_arguments_training(self):
        self.parser.add_argument('--epochs', type=int, help='number of training iterations')
        self.parser.add_argument('--batch', type=int, help='batch size for training')
        self.parser.add_argument('--checkpoint', type=int, help='save model state after every n-th iteration')
        self.parser.add_argument('--gpu', type=int, help='GPU ID used for training')

    def _add_arguments_network(self):
        self.parser.add_argument('--net', type=str, help='type of network')
        self.parser.add_argument('--init', type=str, choices=['normal', 'xavier', 'kaiming', 'orthogonal'],
                                 help='initialization method of network weights')

    def _add_arguments_optimizer(self):
        self.parser.add_argument('--lr', type=float, help='learning rate for optimizer')
        self.parser.add_argument('--betas', type=float, nargs=2, help='beta values for ADAM optimizer')

    def _add_arguments_debugging(self):
        self.parser.add_argument('--logdir', type=str)
    # endregion

    def _set_defaults(self):
        self._set_default_preprocessing()
        self._set_default_data()
        self._set_default_training()
        self._set_default_network()
        self._set_default_optimizer()
        self._set_default_scheduler()
        self._set_default_debugging()

    # region Setting defaults
    def _set_default_preprocessing(self):
        self.options.update({
            "preprocess": {
                "gridMapInput": {
                "windU": "100u",
                "windV": "100v",
                "blh": "blh",
                "fsr": "fsr",
                },
            "gridMapTarget": {
                "windU": "100u",
                "windV": "100v",
                },
            "gridMapSfc": {
                "z": "z",
                "seaMask": "lsm"
                },
            "exportGridsInput": ["windU", "windV", "blh", "fsr"],
            "exportGridsTarget": ["windU", "windV"],
            "exportGridsSfc": ["z", "seaMask"],
            "monthsTraining": [[(2017, 1), (2018, 12)], [(2017, 1), (2018, 12)]],
            "trainingRegions": ["alps", "canada"],
            # "monthsTraining": [[(2017, 1), (2018, 12)]],
            # "trainingRegions": ["alps"],
            "monthsTest": [[(2018, 1), (2018, 12)]],
            "testRegions": ["area1"],
            "padding": True,
            "patching": True,
            "patchSize": [24, 24],
            "pathTraining": "data/training_h5_new",
            "pathTest": "data/test",
            "pathRaw": "data/raw"
            },
        })

    def _set_default_data(self):
        self.options.update({
            "data": {
                "maxNumFiles": 10000,
                "gridsInput": ["windU", "windV", "blh", "fsr", "seaMask", "z"],
                "gridsTarget": ["windU", "windV"],
                "validationMode": "monthly",
                "validationInput": [[(2018, 1), (2018, 12)], [(2018, 1), (2018, 12)]],
                "validationRegions": ["alps", "canada"],
                # "validationInput": [[(2018, 1), (2018, 12)]],
                # "validationRegions": ["alps"],
                "useHROrography": False,
            }
        })

    def _set_default_training(self):
        self.options.update({
            "training": {
                "loss": {
                    "name": "L1",
                    "useMask": True,
                    "cosineNorm": {
                        "weight_cosine": 1.,
                        "weight_norm": 1.
                    },
                },
                "epochs": 200,
                "batchSize": 40,
                "saveEveryNthEpoch": 20,
                "seed": 42,
                "gpu": 0,
                "coherenceModel": {
                    "name": "PolynomialInterpolator",
                    "loss": {
                        "name": "L1",
                        "useMask": True,
                    },
                    "weight": 1.,
                    "run": -1,
                    "epoch": -1,
                }
            }
        })

    def _set_default_network(self):
        self.options.update({
            "network": {
                "name": "UNet",
                "initMethod": "orthogonal",
                "inputChannels": 32,
                "hrConvLayers": 0,
                "hrKernelSize": 7,
                "paddingMode": "replication",
                "UNet": {
                    "featureChannels": 32,
                    "kernelSizeEncoding": 5,
                    "kernelSizeDecoding": 5,
                    "numConvLayers": 2,
                    "useBatchNorm": True,
                    "useDropout": False,
                    "dropoutRate": 0.0
                },
                "HROro": {
                    "mode": 0,
                    "numConvLayers": 0,
                    "kernelSizeHR": 3,
                    "kernelSizeLR": 3,
                    "numInceptionBlocks": 1,
                    "numResBlocks": 2,
                    "numConvPerResBlocks": 2
                },
                "ResUNet": {
                    "featureChannels": 64,
                    "kernelSize": 3,
                    "resblocksPerStage": 1,
                    "convLayersPerBlock": 2,
                    "dropoutRate": 0.2,
                    "useDropout": False
                },
                "EnhanceNet": {
                    "featureChannels": 64,
                    "numResBlocks": 8,
                    "layersPerBlock": 4
                },
                "LinearRegressor": {
                    "featureChannels": 32,
                },
                "LinearCNN": {
                    "featureChannels": 32,
                    "numConvLayers": 0,
                    "kernelSize": 5
                }
            }
        })

    def _set_default_optimizer(self):
        self.options.update({
            "optimizer": {
                "learningRate": 0.001,
                "betas": [0.9, 0.999],
                "decayLearningRate": True,
                "LRExpFactor": -1
            },
        })

    def _set_default_scheduler(self):
        self.options.update({
            "scheduler": {
                "type": "plateau",
                "steps": 5,
                "gamma": 0.1
            },
        })

    def _set_default_debugging(self):
        self.options.update({
            "debugging": {
                "recordDir": "results",
                "printConfig": False,
                "printDirectories": False,
                "printNetworkDetails": True,
                "printImages": False,
                "printRealLoss": False,
                "printUpsamplingLoss": True
            }
        })
    # endregion

    def _use_config(self, arguments):
        file = open(arguments.config, 'r')
        self.options.update(json.load(file))

    def _use_arguments(self, arguments):
        self._parse_preprocessing(arguments)
        self._parse_data(arguments)
        self._parse_training(arguments)
        self._parse_network(arguments)
        self._parse_optimizer(arguments)
        self._parse_debugging(arguments)

    # region Parsing arguments
    def _parse_preprocessing(self, arguments):
        if arguments.patch_data is not None:
            self.options['preprocess'].update({'patching': arguments.patch_data})
        if arguments.dataset is not None:
            if not os.path.exists(arguments.dataset):
                raise Exception("Dataset path does not exist")
            self.options['preprocess'].update({'pathTraining': arguments.dataset})

    def _parse_data(self, arguments):
        if arguments.input_vars is not None:
            self.options['data'].update({'gridsInput': arguments.gpu})
        if arguments.use_hr is not None:
            self.options['data'].update({'useHROrography': arguments.use_hr})

    def _parse_training(self, arguments):
        if arguments.epochs is not None:
            self.options['training'].update({'epochs': arguments.epochs})
        if arguments.batch is not None:
            self.options['training'].update({'batchSize': arguments.batch})
        if arguments.checkpoint is not None:
            self.options['training'].update({'saveEveryNthEpoch': arguments.checkpoint})
        if arguments.gpu is not None:
            self.options['training'].update({'gpu': arguments.gpu})

    def _parse_network(self, arguments):
        if arguments.net is not None:
            self.options['network'].update({'name': arguments.net})
        if arguments.init is not None:
            self.options['network'].update({'initMethod': arguments.init})

    def _parse_optimizer(self, arguments):
        if arguments.lr is not None:
            self.options['optimizer'].update({'learningRate': arguments.lr})
        if arguments.betas is not None:
            self.options['optimizer'].update({'betas': arguments.betas})

    def _parse_debugging(self, arguments):
        if arguments.logdir is not None:
            if not os.path.exists(arguments.dataset):
                os.makedirs(arguments.dataset)
            self.options['debugging'].update({'recordDir': arguments.logdir})
    # endregion


class CoherenceModelParser(ArgParser):
    def __init__(self):
        super(CoherenceModelParser, self).__init__()

    def _set_defaults(self):
        super(CoherenceModelParser, self)._set_defaults()
        self._set_default_coherence()

    # region Setting defaults
    def _set_default_training(self):
        self.options.update({
            "training": {
                "loss": {
                    "name": "L1",
                    "useMask": True,
                },
                "epochs": 120,
                "batchSize": 100,
                "saveEveryNthEpoch": 10,
                "seed": 42,
                "gpu": 0,
                "coherenceModel": None
            }
        })

    def _set_default_coherence(self):
        self.options.update({
            "coherence": {
                "numStepsPast": 2,
                "numStepsFuture": 2,
                "stepSizePast": 1,
                "stepSizeFuture": 1,
            }
        })

    def _set_default_network(self):
        self.options.update({
            "network": {
                "name": "MinimalNet",
                "initMethod": "orthogonal",
                "inputChannels": 32,
                "kernelSize": 5,
                "dilation": 1,
            }
        })
    # endregion


class DownscalingParser(ArgParser):
    def __init__(self):
        super(DownscalingParser, self).__init__()

    def _set_defaults(self):
        super(DownscalingParser, self)._set_defaults()
        self._set_default_coherence()

    def _set_default_coherence(self):
        self.options.update({
            "coherence": {
                "numStepsPast": 1,
                "numStepsFuture": 1,
                "stepSizePast": 1,
                "stepSizeFuture": 1,
            }
        })


class TemporalDownscalingParser(DownscalingParser):
    def __init__(self):
        super(TemporalDownscalingParser, self).__init__()

    def _set_default_training(self):
        self.options.update({
            "training": {
                "loss": {
                    "name": "L1",
                    "useMask": True,
                },
                "epochs": 120,
                "batchSize": 40,
                "saveEveryNthEpoch": 10,
                "seed": 42,
                "gpu": 0,
                "coherenceModel": None,
            }
        })

    def _set_default_network(self):
        self.options.update({
            "network": {
                "name": "UNet_Temporal",
                "initMethod": "orthogonal",
                "inputChannels": 64,
                "hrConvLayers": 0,
                "hrKernelSize": 7,
            }
        })


class CVAEParser(ArgParser):
    def __init__(self):
        super(CVAEParser, self).__init__()

    # region Adding arguments
    def _add_arguments(self):
        super(CVAEParser)._add_arguments()
        self._add_arguments_annealer()

    def _add_arguments_network(self):
        super(CVAEParser, self)._add_arguments_network()
        self.parser.add_argument('--output_logVar', type=str, choices=['diag', 'unit'],
                                 help='type of covariance matrix for use in posterior distribution')
        self.parser.add_argument('--loss', type=str, choices=['gaussian', 'exponential'],
                                 help='type of loss function for measuring deviations')

    def _add_arguments_annealer(self):
        self.parser.add_argument('--eps_kl', type=float, nargs=2, help='eps range for KL annealer ')
        self.parser.add_argument('--epochs_kl', type=int, help='duration of KL annealing period')
        self.parser.add_argument('--offset_kl', type=int, nargs=2, help='epochs before beginning KL annealing')
    # endregion

    def _set_defaults(self):
        super(CVAEParser, self)._set_defaults()
        self._set_default_annealer()

    # region Setting defaults
    def _set_default_network(self):
        self.options.update({
            "network": {
                "name": "CD_...+CE_...",
                "initMethod": "orthogonal",
                "epochs": 201,
                "inputChannels": 32,
                "latentChannels": 16,
                "hrConvLayers": 0,
                "hrKernelSize": 7,
                "logVar": "diag",
                "loss": {
                    "name": "exponential",
                    "useMask": True,
                },
                "dropout_encoder": 0.1,
                "dropout_decoder": 0.1,
            },
        })

    def _set_default_annealer(self):
        self.options.update({
            "annealer": {
                "eps_start": 0.,
                "eps_end": 1.,
                "epochs": 10,
                "offset": 1,
            },
        })
    # endregion

    def _use_arguments(self, arguments):
        super(CVAEParser, self)._use_arguments(arguments)
        self._parse_annealer(arguments)

    # region Parsing arguments
    def _parse_network(self, arguments):
        super(CVAEParser, self)._parse_network(arguments)
        if arguments.output_logVar is not None:
            self.options['training'].update({'logVar': arguments.output_logVar})

    def _parse_annealer(self, arguments):
        if arguments.eps_kl is not None:
            self.options['annealer'].update({'eps_start': arguments.eps_kl[0],
                                             'eps_end': arguments.eps_kl[1]})
        if arguments.epochs_kl is not None:
            self.options['annealer'].update({'epochs': arguments.epochs_kl})
        if arguments.offset_kl is not None:
            self.options['annealer'].update({'offset': arguments.offset_kl})
    # endregion
