import os
import sys
import torch
# change working directory
os.chdir(os.getcwd())
# add working directory to python environment to allow calls from subdirectories
sys.path.insert(0, os.getcwd())
from Training.Parser import DownscalingParser
from Training.ParameterTrainer import ParameterSettingTrainer

import numpy as np


torch.set_num_threads(4)

# parse arguments
# load default arguments
parser = DownscalingParser()
parser.parse(sys.argv[1:])
configOpts = parser.options
print(configOpts.keys())

paramInitConfig = {'models': ['UNet', 'ResUNet', 'EnhanceNet'],
                   'losses': ['L1', 'L2'],
                   'init_methods': ['normal', 'orthogonal', 'xavier', 'kaiming'],
                   'patch_params': [[False, (0, 0)], [True, (36, 36)], [True, (36, 24)], [True, (24, 36)], [True, (24, 24)]],
                   'use_hr': [False, True],
                   'learning_rates': np.arange(0.001, 0.01 + 0.001, 0.001),
                   'betas': np.arange(0.6, 1.0, 0.1)
                   }

paramInitConfig2 = {'models': ['ResUNet'],#, 'EnhanceNet'],
                    'losses': ['L1'], #, 'L2', 'CosineNorm'],  #, 'SmoothL1', 'CosineDistance', 'CosineNorm'],
                    'lossUseMask': [True],
                    'init_methods': ['orthogonal'],
                    'training_domain': ['alps'],
                    'validation_domain': ['alps'],
                    'validation_year': [2017],  #[2016, 2017, 2018, 2019],
                    'patch_params': [[False, (36, 24)]],
                    'use_hr': [True],
                    'learning_rates': [0.01],
                    'betas': [0.9]
                    }


gridsInputSelection = np.array(["blh", "fsr", "lons", "lats", "z"])  #, "lons", "lats"])  #, "zOro"])
num_epochs = 101
num_repeats = 1

paramTrainer = ParameterSettingTrainer(num_epochs=num_epochs,
                                       input_grids=gridsInputSelection,
                                       config_opts=configOpts,
                                       param_opts=paramInitConfig2,
                                       train_network_params=False,
                                       train_grids=False,
                                       train_padding_modes=False,
                                       train_cosine_norm=False,
                                       num_repeats=num_repeats)
                                       #default_grids=[])
paramTrainer.run()
