import os
import sys
# change working directory
os.chdir(os.getcwd())
# add working directory to python environment to allow calls from subdirectories
sys.path.insert(0, os.getcwd())
from Training.Parser import ArgParser
import numpy as np
import torch
import random
from Training.TrainingSetup import TrainingSetup

torch.set_num_threads(4)

# parse arguments
parser = ArgParser()
parser.parse(sys.argv[1:])
configOpts = parser.options



# seed random number generators
seed = random.randint(0, 2**32 - 1)
print("Random seed: {}".format(seed))
random.seed(seed)
np.random.seed(seed)
torch.manual_seed(seed)
torch.cuda.manual_seed_all(seed)

setup = TrainingSetup(configOpts)

print("____________________________________________________________")
numEpochs = configOpts['training']['epochs']

for epoch in range(numEpochs):
    print("Epoch {} / {}".format(epoch, numEpochs))
    setup.train_epoch()
    setup.validate_epoch()
    print("____________________________________________________________")

setup.summary.close()
