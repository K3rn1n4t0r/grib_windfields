import os
import sys
import numpy as np
import json
import random
# change working directory
os.chdir(os.getcwd())
# add working directory to python environment to allow calls from subdirectories
sys.path.insert(0, os.getcwd())
from DataPreprocessing import DataLoader as dl
from DataPreprocessing.DataScaler import DataScaler
from Training.utils import initWeights, saveCheckpoint, saveScalings, findRunNumber, printConfigs, printConfigToSummary
from Networks.DownscalingModelSelector import ModelSelector
from LossFunctions.LossSelector import LossSelector
from Utils.ProgressBar import ProgressBar
from Training.utils import NpEncoder
from Networks.DownscalingNet import DownscalingNet
from LossFunctions.MaskedLosses import MaskedCosineDistanceLoss, MaskedNormLoss
##############################################
# PyTorch specific libraries
import torch
import torch.optim as optim
from torch.utils.data import DataLoader
##############################################
# summary with tensorboard
from tensorboardX import SummaryWriter


class DataSetting(object):
    def __init__(self, configOpts, prepare=True):
        self.config = configOpts
        self.directories = self._get_directories()
        self.fileLoader = dl.H5Importer(self.directories['training'])
                                        #self.config['preprocess']['trainingRegions'])
        if prepare:
            self.data, self.scalings = self._prepare_data()
        self.loaders = None

    def prepare_loaders(self, configOpts):
            loaderTrain = DataLoader(
                self.data['training'], batch_size=configOpts['training']['batchSize'],
                shuffle=True, drop_last=True, num_workers=0
            )
            if self.data['validation'] is not None:
                loaderVal = DataLoader(
                    self.data['validation'], batch_size=configOpts['training']['batchSize'],
                    shuffle=False, drop_last=True, num_workers=0
                )
            else:
                loaderVal = None
            self.loaders = {
                'training': loaderTrain,
                'validation': loaderVal,
            }
            return loaderTrain, loaderVal

    def update_data(self, configOpts):
        print("[INFO] Update training and validation config settings")

        # get differences between new and old config
        diff_cfg = set(configOpts) ^ set(self.config)

        if len(diff_cfg) > 0:
            print("[INFO] Updated parameters:")
            print(diff_cfg)

        self.config = configOpts
        self.data, self.scalings = self._prepare_data()

    def shapes(self):
        return {
            'shapeInputs': self.config['data']['shapeInputs'],
            'shapeTargets': self.config['data']['shapeTargets'],
            'channelsHR': self.config['data']['channelsHR']
        }

    def _get_directories(self):
        return {
            'training': self.config['preprocess']['pathTraining'],
            'test': self.config['preprocess']['pathTest'],
            'raw': self.config['preprocess']['pathRaw']
        }

    def _prepare_data(self):
        dataOpt = self._get_data_options()
        trainOpt = self._get_training_options()
        scaleOpt = self._get_scaler_options()
        valiOpt = self._get_validation_options()
        tempOpt = self._get_temporal_options()
        dataLoader = dl.DataLoader(
            #self.directories['training'],
            self.fileLoader,
            self.config['data']['maxNumFiles'],
            dataOpt, trainOpt, scaleOpt, valiOpt, tempOpt
        )
        dataTrain, dataVal = dataLoader.loadDataForTraining()
        channels_input = len(self.config['data']['gridsInput'])
        # print(type(channels_input))
        channels_target = len(self.config['data']['gridsTarget'])
        # print(type(channels_target))
        if self.config['preprocess']['patching']:
            shape_input = np.array(self.config['preprocess']['patchSize'], dtype=int)
            shape_target = np.array([4, 3], dtype=int) * shape_input
        else:
            shape_input = dataTrain.inputs.shape[2:]
            shape_target = dataTrain.targets.shape[2:]
        self.config['data'].update({
            'shapeInputs': [channels_input, shape_input[0], shape_input[1]],
            'shapeTargets': [channels_target, shape_target[0], shape_target[1]],
            'channelsHR': dataTrain.hrChannels
        })
        return {'training': dataTrain, 'validation': dataVal}, scaleOpt

    def _get_data_options(self):
        opts = dl.dataSettings(
            'input', 'input_orography', self.config['data']['gridsInput'],
            'target', 'target_orography', self.config['data']['gridsTarget']
        )
        return opts

    def _get_validation_options(self):
        opts = dl.validationSettings(
            self.config['data']['validationMode'],
            self.config['data']['validationInput'],
            self.config['data']['validationRegions']
        )
        return opts

    def _get_training_options(self):
        patchMode = self.config['preprocess']['patchSize'] if self.config['preprocess']['patching'] else None
        opts = dl.trainingSettings(
            self.config['preprocess']['monthsTraining'],
            patchMode, self.config['data']['useHROrography'],
            self.config['preprocess']['trainingRegions']
        )
        return opts

    @staticmethod
    def _get_scaler_options():
        # set up scaling for variable transformation
        scalerUV = DataScaler(mode='normalize_neg')
        scalerBLH = DataScaler(mode='normalize_neg')
        scalerFSR = DataScaler(mode='normalize_neg')
        scalerLON = DataScaler(mode='normalize_neg')
        scalerLAT = DataScaler(mode='normalize_neg')
        scalerZ = DataScaler(mode='normalize_neg')
        scalerZORO_LR = DataScaler(mode='normalize_neg')
        scalerZORO_HR = DataScaler(mode='normalize_neg')
        scalerLSM_HR = DataScaler(mode='normalize_neg')
        scalerLSM_LR = DataScaler(mode='normalize_neg')
        # quantizerUV = Quantizer(num_bins=100)
        scalerUVHR = DataScaler(mode='normalize_neg')
        opts = dl.scalingSettings(
            [(scalerUV, ['windU', 'windV']),
             (scalerLSM_LR, ['seaMask']),
             (scalerZORO_LR, ['zOro']),
             (scalerBLH, ['blh']),
             (scalerFSR, ['fsr']),
             (scalerLON, ['lons']),
             (scalerLAT, ['lats']),
             (scalerZ, ['z'])
             ],
                # [(quantizerUV, ['windU', 'windV'])])
            # [(scalerUVHR, ['windU', 'windV'])],
            [],
            [(scalerZORO_HR, ['zOro']),
             (scalerLSM_HR, ['seaMask'])]
        )
        return opts

    def _get_temporal_options(self):
        opts = dl.temporalSettings(
            self.config['coherence']['numStepsPast'], 1,
            self.config['coherence']['numStepsFuture'], 1
        )
        return opts


class SingleTimeDataSetting(DataSetting):
    def __init__(self, configOpts, prepare=True):
        super(SingleTimeDataSetting, self).__init__(configOpts, prepare)

    def _get_temporal_options(self):
        return None


class _TrainingEpoch(object):
    def __init__(self, model_setting, data_setting, loss_names):
        self.modelSetting = model_setting
        self.dataSetting = data_setting
        self.trainLoader, self.validationLoader = data_setting.prepare_loaders(model_setting.config)
        self.numLoadings = None
        self.schedulerType = self.modelSetting.config['scheduler']['type']
        self.loss_names = loss_names
        if self.modelSetting.config['debugging']['printRealLoss']:
            self.loss_names.append('deviation_real')

    def _initialize_losses(self):
        self.cumulative_losses = {loss_name: 0 for loss_name in self.loss_names}

    def train(self):
        # set cumulative losses to zero
        self._initialize_losses()
        # set mode of network to train (parameters will be affected / updated)
        self.modelSetting.model.train()
        # set progress bar
        self.numLoadings = len(self.trainLoader)
        progressBar = ProgressBar(self.numLoadings, displaySumCount=True)
        # iterate over data loader
        for i, batch in enumerate(self.trainLoader, 0):
            # load data to device
            inputs_device, targets_device, hrOro_device, mask_device = self._prepare_data(batch)
            # zero out optimizer weights
            self.modelSetting.optimizer.zero_grad()
            # apply model
            predictions_device = self._apply_model(inputs_device, hrOro_device)
            # compute loss to current targets
            loss = self._update_loss(predictions_device, targets_device, mask_device)
            # backpropagate loss
            loss.backward()
            # advance weights towards minimum
            self.modelSetting.optimizer.step()
            # update progress bar
            progressBar.proceed(i + 1)
        # update epoch count
        self._update_summary('training')
        # save epoch state
        self._save_state()
        # update scheduler
        self._update_scheduler()

    def validate(self):
        # set cumulative losses to 0
        self._initialize_losses()
        # set mode of network to train (parameters will be affected / updated)
        self.modelSetting.model.eval()
        # set progress bar
        self.numLoadings = len(self.validationLoader)
        progressBar = ProgressBar(self.numLoadings, displaySumCount=True)
        # iterate over data loader
        with torch.no_grad():
            for i, batch in enumerate(self.validationLoader, 0):
                # load data to device
                inputs_device, targets_device, hrOro_device, mask_device = self._prepare_data(batch)
                # apply model
                predictions_device = self._apply_model(inputs_device, hrOro_device)
                # compute loss to current targets
                self._update_loss(predictions_device, targets_device, mask_device)
                # update progress bar
                progressBar.proceed(i + 1)
        # update epoch count
        self._update_summary('validation')

    def get_current_learning_rate(self):
        if self.schedulerType == 'step':
            return self.modelSetting.scheduler.get_lr()[0]
        elif self.schedulerType == 'plateau':
            return self.modelSetting.optimizer.param_groups[0]['lr']

    def _prepare_data(self, batch):
        raise NotImplementedError()

    def _apply_model(self, inputs_device, hrOro_device=None):
        raise NotImplementedError()

    def _update_loss(self, predictions_device, targets_device, mask_device=None):
        raise NotImplementedError()

    def _update_summary(self, mode='training'):
        # update epoch counter
        if mode == 'training':
            self.modelSetting.epochs_trained += 1
            currentLR = 0.0
            if self.schedulerType == 'step':
                currentLR = self.modelSetting.scheduler.get_lr()[0]
            elif self.schedulerType == 'plateau':
                currentLR = self.modelSetting.optimizer.param_groups[0]['lr']
            self.modelSetting.summary.add_scalar(
                mode + '/params/lr',
                currentLR,
                self.modelSetting.epochs_trained
            )
            print(
                '{}: Loss {}, LR {}'.format(
                    mode.capitalize(),
                    self.modelSetting.config['training']['loss']['name'],
                    currentLR
                )
            )
        elif mode == 'validation':
            print(
                '{}: Loss {}'.format(
                    mode.capitalize(),
                    self.modelSetting.config['training']['loss']['name']
                )
            )
        else:
            pass
        # update records per train step
        for loss_name in self.loss_names:
            self.cumulative_losses.update(
                {loss_name: self.cumulative_losses[loss_name] / self.numLoadings}
            )
            self.modelSetting.summary.add_scalar(
                '{}/loss/{}'.format(mode, loss_name),
                self.cumulative_losses[loss_name],
                self.modelSetting.epochs_trained,
            )
            print('{}: {}'.format(loss_name, self.cumulative_losses[loss_name]))
        self.modelSetting.summary.flush()

    def _save_state(self):
        # do not save any network states if saveEveryNthEpoch is set to -1 (or any negative value)
        saveEveryNthEpoch = self.modelSetting.config['training']['saveEveryNthEpoch']
        if saveEveryNthEpoch >= 0 and self.modelSetting.epochs_trained % saveEveryNthEpoch == 0:
            saveCheckpoint(
                self.modelSetting.epochs_trained,
                self.modelSetting.model, self.modelSetting.optimizer, self.modelSetting.scheduler,
                self.dataSetting.scalings,
                self.modelSetting.directories['models']
            )

    def _update_scheduler(self):
        if self.schedulerType == 'plateau':
            self.modelSetting.scheduler.step(self.cumulative_losses[self.loss_names[0]])
        if self.schedulerType == 'step':
            self.modelSetting.scheduler.step()


class _TemporalTrainingEpoch(_TrainingEpoch):
    def __init__(self, model_setting, data_setting, loss_names):
        super(_TemporalTrainingEpoch, self).__init__(
            model_setting, data_setting, loss_names
        )
        batchSize = self.modelSetting.config['training']['batchSize']
        shapeInputs = self.modelSetting.config['data']['shapeInputs']
        shapeTargets = self.modelSetting.config['data']['shapeTargets']
        numStepsPast = self.modelSetting.config['coherence']['numStepsPast']
        numStepsFuture = self.modelSetting.config['coherence']['numStepsFuture']
        self.numSteps = numStepsPast + numStepsFuture + 1
        self.selection = list(np.arange(self.numSteps))
        self.selection.pop(numStepsPast)
        self.idxPresent = numStepsPast
        self.shapeInputs_expanded = (self.numSteps, batchSize, *shapeInputs)
        self.shapeTargets_expanded = (self.numSteps, batchSize, *shapeTargets)
        self.shapeInputs_compressed = (self.numSteps * batchSize, *shapeInputs)
        self.shapeTargets_compressed = (self.numSteps * batchSize, *shapeTargets)

    def _prepare_data(self, batch):
        raise NotImplementedError()

    def _apply_model(self, inputs_device, hrOro_device=None):
        raise NotImplementedError()

    def _update_loss(self, predictions_device, targets_device, mask_device=None):
        raise NotImplementedError()


class _ModelSetting(object):
    def __init__(self, configOpts, scalings):
        self.config = configOpts
        self._seed_random()
        self.scaling_opts = scalings
        self.device = self._prepare_device()
        self.model = self._prepare_network()
        self.directories = self._prepare_directories()
        self.summary = self._prepare_summary()
        self.optimizer = self._prepare_optimizer()
        self.scheduler = self._prepare_scheduler()
        self.loss = self._prepare_loss_function()
        self.epochs_trained = 0
        self._save_configuration()

    def _seed_random(self, seed=None):
        # seed random number generators
        if seed is None:
            seed = random.randint(0, 2 ** 32 - 1)
        print("Random seed: {}".format(seed))
        random.seed(seed)
        np.random.seed(seed)
        torch.manual_seed(seed)
        torch.cuda.manual_seed_all(seed)

    def _prepare_directories(self):
        if isinstance(self.model, DownscalingNet):
            netName = self.model.name
        else:
            netName = self.config['network']['name']
        logDir = self.config['debugging']['recordDir']
        directories = {
            'results': os.path.join(logDir, netName),
            'records': os.path.join(logDir, netName, 'records'),
            'models':  os.path.join(logDir, netName, 'records', 'models'),
            'data':   {'training': self.config['preprocess']['pathTraining'],
                       'test':     self.config['preprocess']['pathTest'],
                       'raw':      self.config['preprocess']['pathRaw']}
        }
        if not os.path.exists(directories['records']):
            os.makedirs(directories['records'])
        runNumber = findRunNumber(directories['records'])
        if sys.gettrace():
            print("[WARNING]: Programs runs in DEBUG mode")
            runName = 'run_debug'
        else:
            runName = 'run_{:05d}'.format(runNumber)
        directories.update({'currentRun': os.path.join(directories['records'], runName)})
        directories.update({'models': os.path.join(directories['currentRun'], 'models')})
        if not os.path.exists(directories['models']):
            os.makedirs(directories['models'])
        if self.config['debugging']['printConfig']:
            print("[INFO]: Config options:\n===================")
            printConfigs(self.config)
            print("===================")
        if self.config['debugging']['printDirectories']:
            print("[INFO]: Directories::\n===================")
            printConfigs(directories)
            print("===================")

        print("[INFO]: Training run number: <{}>".format(runNumber))
        return directories
    
    def _prepare_summary(self):
        summary = SummaryWriter(self.directories['currentRun'], purge_step=1)
        printConfigToSummary(self.config, summary)
        return summary
        
    def _prepare_device(self):
        gpuID = self.config['training']['gpu']
        print("[INFO]: Current working directory: <{}>".format(os.getcwd()))

        # set up environment to use gpu with device ID gpuID only
        os.environ["CUDA_VISIBLE_DEVICES"] = str(gpuID)

        if torch.cuda.is_available():
            print("[INFO]: Using graphics card number <{}>".format(gpuID))
            device = torch.device('cuda:0')
        else:
            print("[WARNING]: No available CUDA device found. Training is performed on CPU!")
            device = torch.device('cpu')
        return device
    
    def _prepare_network(self):
        net = ModelSelector(self.config, self.scaling_opts).select()
        # track learnable parameters
        modelParameters = filter(lambda p: p.requires_grad, net.parameters())
        params = sum([np.prod(p.size()) for p in modelParameters])
        print('[INFO]: Number of learnable parameters: {}'.format(params))
        print('[INFO]: Uploading downscaling network to GPU.')
        net = net.to(self.device)
        initWeights(net, self.config['network']['initMethod'])
        if self.config['debugging']['printNetworkDetails']:
            print(net)
        return net
    
    def _prepare_optimizer(self):
        optimizer = optim.Adam(
            self.model.parameters(),
            lr=self.config['optimizer']['learningRate'],
            betas=self.config['optimizer']['betas'], 
            weight_decay=0.0
        )
        return optimizer
    
    def _prepare_scheduler(self):
        schedulerType = self.config['scheduler']['type']
        if schedulerType is None:
            scheduler = None
        elif schedulerType == 'step':
            scheduler = optim.lr_scheduler.StepLR(
                self.optimizer,
                step_size=self.config['scheduler']['steps'],
                gamma=self.config['scheduler']['gamma']
            )
        elif schedulerType == 'plateau':
            scheduler = optim.lr_scheduler.ReduceLROnPlateau(
                self.optimizer, 
                mode='min', factor=self.config['scheduler']['gamma'],
                patience=self.config['scheduler']['steps'],
                verbose=False, threshold=1.e-2, threshold_mode='rel',
                cooldown=0, min_lr=0, eps=1e-08
            )
        else:
            raise NotImplementedError('Scheduler type <{}> is not implemented'.format(schedulerType))
        return scheduler
    
    def _prepare_loss_function(self):
        lossFunction = LossSelector(self.config).select().to(self.device)
        print("[INFO]: Used loss function for training: {}".format(lossFunction.__class__.__name__))
        return lossFunction
    
    def _save_configuration(self):
        with open(os.path.join(self.directories['currentRun'], 'config.json'), 'w') as f:
            json.dump(self.config, f, indent=4, cls=NpEncoder)

    def run_training(self, data_setting):
        print("____________________________________________________________")
        numEpochs = self.config['training']['epochs']
        numIterLR = 0

        for n in range(numEpochs):
            print("Epoch {} / {}".format(n + 1, numEpochs))
            epoch = self.TrainingEpoch(self, data_setting)
            epoch.train()
            epoch.validate()
            print("____________________________________________________________")

            # early termination
            cur_lr = epoch.get_current_learning_rate()
            if cur_lr <= 1.2e-06:
                numIterLR += 1

                if numIterLR > 5:
                    return

    class TrainingEpoch(_TrainingEpoch):
        def __init__(self, model_setting, data_setting):
            super(_ModelSetting.TrainingEpoch, self).__init__(model_setting, data_setting, ['deviation'])

        def _prepare_data(self, batch):
            raise NotImplementedError()

        def _apply_model(self, inputs_device, hrOro_device=None):
            raise NotImplementedError()

        def _update_loss(self, predictions_device, targets_device, mask_device=None):
            raise NotImplementedError()


class DownscalingModelSetting(_ModelSetting):
    def __init__(self, configOpts):
        super(DownscalingModelSetting, self).__init__(configOpts)

    class TrainingEpoch(_TrainingEpoch):
        def __init__(self, model_setting, data_setting):
            super(DownscalingModelSetting.TrainingEpoch, self).__init__(
                model_setting, data_setting,
                loss_names=['deviation']
            )

        def _prepare_data(self, batch):
            inputs_cpu, targets_cpu, gridLR, gridHR, maskLR, maskHR, hrOro, idx = batch
            inputs_device = inputs_cpu.to(self.modelSetting.device)
            targets_device = targets_cpu.to(self.modelSetting.device)
            if self.modelSetting.config['data']['useHROrography']:
                hrOro_device = hrOro.to(self.modelSetting.device)
            else:
                hrOro_device = None
            if self.modelSetting.config['training']['loss']['useMask']:
                mask_device = 1 - maskHR.to(self.modelSetting.device)
            else:
                mask_device = None
            return inputs_device, targets_device, hrOro_device, mask_device

        def _apply_model(self, inputs_device, hrOro_device=None):
            if self.modelSetting.config['data']['useHROrography']:
                predictions_device = self.modelSetting.model(inputs_device, hrOro_device)
            else:
                predictions_device = self.modelSetting.model(inputs_device)
            return predictions_device

        def _update_loss(self, predictions_device, targets_device, mask_device=None):
            loss = self.modelSetting.loss(predictions_device, targets_device, mask_device)
            self.cumulative_losses.update({
                'deviation': self.cumulative_losses['deviation'] + loss.item()
            })
            # compute real loss
            if self.modelSetting.config['debugging']['printRealLoss']:
                scaler = self.dataSetting.scalings.target[0][0]
                loss_real = self.modelSetting.loss(
                    scaler.transform_back(predictions_device), scaler.transform_back(targets_device), mask_device
                )
                self.cumulative_losses.update({
                    'deviation_real': self.cumulative_losses['deviation_real'] + loss_real.item()
                })
            return loss


class TemporalDownscalingModelSetting(_ModelSetting):
    def __init__(self, configOpts):
        super(TemporalDownscalingModelSetting, self).__init__(configOpts)

    class TrainingEpoch(_TemporalTrainingEpoch):
        def __init__(self, model_setting, data_setting):
            super(TemporalDownscalingModelSetting.TrainingEpoch, self).__init__(
                model_setting, data_setting,
                loss_names=['deviation']
            )

        def _prepare_data(self, batch):
            inputs_cpu, targets_cpu, gridLR, gridHR, maskLR, maskHR, hrOro, idx = batch
            inputs_device = torch.cat(inputs_cpu, dim=1).to(self.modelSetting.device)
            targets_device = targets_cpu[self.idxPresent].to(self.modelSetting.device)
            if self.modelSetting.config['data']['useHROrography']:
                hrOro_device = hrOro.to(self.modelSetting.device)
            else:
                hrOro_device = None
            if self.modelSetting.config['training']['loss']['useMask']:
                mask_device = 1 - maskHR.to(self.modelSetting.device)
            else:
                mask_device = None
            return inputs_device, targets_device, hrOro_device, mask_device

        def _apply_model(self, inputs_device, hrOro_device=None):
            if self.modelSetting.config['data']['useHROrography']:
                predictions_device = self.modelSetting.model(inputs_device, hrOro_device)
            else:
                predictions_device = self.modelSetting.model(inputs_device)
            return predictions_device

        def _update_loss(self, predictions_device, targets_device, mask_device=None):
            loss = self.modelSetting.loss(predictions_device, targets_device, mask_device)
            self.cumulative_losses.update({
                'deviation': self.cumulative_losses['deviation'] + loss.item()
            })
            # compute real loss
            if self.modelSetting.config['debugging']['printRealLoss']:
                scaler = self.dataSetting.scalings.target[0][0]
                loss_real = self.modelSetting.loss(
                    scaler.transform_back(predictions_device), scaler.transform_back(targets_device), mask_device
                )
                self.cumulative_losses.update({
                    'deviation_real': self.cumulative_losses['deviation_real'] + loss_real.item()
                })
            return loss


class CoherentDownscalingModelSetting(_ModelSetting):
    def __init__(self, configOpts):
        super(CoherentDownscalingModelSetting, self).__init__(configOpts)

    class TrainingEpoch(_TemporalTrainingEpoch):
        def __init__(self, model_setting, data_setting):
            super(CoherentDownscalingModelSetting.TrainingEpoch, self).__init__(
                model_setting, data_setting,
                loss_names=['total', 'deviation', 'coherence']
            )

        def _prepare_data(self, batch):
            inputs_cpu, targets_cpu, gridLR, gridHR, maskLR, maskHR, hrOro, idx = batch
            inputs_device = torch.stack(inputs_cpu, dim=1).to(self.modelSetting.device)
            targets_device = torch.stack(targets_cpu, dim=1).to(self.modelSetting.device)
            print('Inputs: ', inputs_device.size(), 'Targets:', targets_device.size())
            if self.modelSetting.config['data']['useHROrography']:
                hrOro_device = torch.repeat(hrOro.to(self.modelSetting.device), self.numSteps, 1, 1, 1)
            else:
                hrOro_device = None
            if self.modelSetting.config['training']['loss']['useMask']:
                mask_device = 1 - maskHR.to(self.modelSetting.device)
            else:
                mask_device = None
            return inputs_device, targets_device, hrOro_device, mask_device

        def _apply_model(self, inputs_device, hrOro_device=None):
            if self.modelSetting.config['data']['useHROrography']:
                predictions_device = self.modelSetting.model(inputs_device.view(*self.shapeInputs_compressed),
                                                             hrOro_device)
            else:
                predictions_device = self.modelSetting.model(inputs_device.view(*self.shapeInputs_compressed))
            return predictions_device.view(*self.shapeTargets_expanded)

        def _update_loss(self, predictions_device, targets_device, mask_device=None):
            loss_dev = self.modelSetting.loss.deviation(predictions_device, targets_device, mask_device)
            # compute real loss
            if self.modelSetting.config['debugging']['printRealLoss']:
                scaler = self.dataSetting.scalings.target[0][0]
                loss_dev_real = self.modelSetting.loss.deviation(
                    scaler.transform_back(predictions_device), scaler.transform_back(targets_device), mask_device
                )
                self.cumulative_losses.update({
                    'deviation_real': self.cumulative_losses['deviation_real'] + loss_dev_real.item()
                })
            # compute temporal coherence loss
            loss_coh = self.modelSetting.loss.coherence(predictions_device)
            # compute total loss
            if self.modelSetting.loss.coherenceWeight != 0:
                loss = loss_dev + self.modelSetting.loss.coherenceWeight * loss_coh
            else:
                loss = loss_dev
            # update cumulative loss
            self.cumulative_losses.update({
                'total': self.cumulative_losses['total'] + loss.item(),
                'deviation': self.cumulative_losses['deviation'] + loss_dev.item(),
                'coherence': self.cumulative_losses['coherence'] + loss_coh.item()
            })
            return loss


class CoherenceModelSetting(_ModelSetting):
    def __init__(self, configOpts):
        super(CoherenceModelSetting, self).__init__(configOpts)

    class TrainingEpoch(_TemporalTrainingEpoch):
        def __init__(self, model_setting, data_setting):
            super(CoherenceModelSetting.TrainingEpoch, self).__init__(
                model_setting, data_setting,
                ['deviation']
            )

        def _prepare_data(self, batch):
            inputs_cpu, targets_cpu, gridLR, gridHR, maskLR, maskHR, hrOro, idx = batch
            inputs_device = torch.stack(targets_cpu, dim=0).to(self.modelSetting.device)
            targets_device = inputs_device[self.idxPresent]
            if self.modelSetting.config['training']['loss']['useMask']:
                mask_device = 1 - maskHR.to(self.modelSetting.device)
            else:
                mask_device = None
            return inputs_device, targets_device, None, mask_device

        def _apply_model(self, inputs_device, hrOro_device=None):
            return self.modelSetting.model(inputs_device)

        def _update_loss(self, predictions_device, targets_device, mask_device=None):
            loss = self.modelSetting.loss(predictions_device, targets_device, mask_device)
            # compute real loss
            if self.modelSetting.config['debugging']['printRealLoss']:
                scaler = self.dataSetting.scalings.target[0][0]
                loss_real = self.modelSetting.loss(
                    scaler.transform_back(predictions_device), scaler.transform_back(targets_device), mask_device
                )
                self.cumulative_losses.update({
                    'deviation_real': self.cumulative_losses['deviation_real'] + loss_real.item()
                })
            # update cumulative loss
            self.cumulative_losses.update({
                'deviation': self.cumulative_losses['deviation'] + loss.item()
            })
            return loss


class SingleTimeDownscalingModelSetting(_ModelSetting):
    def __init__(self, configOpts, scalings):
        super(SingleTimeDownscalingModelSetting, self).__init__(configOpts, scalings)

        # prepare loss functions for evaluation of network on different characteristics of the wind field
        self.cosine_loss = LossSelector(self.config).select_loss_function('CosineDistance', 'avg_cosine_distance').to(self.device)
        self.norm_loss = LossSelector(self.config).select_loss_function('Norm', 'avg_norm_distance').to(self.device)
        self.l2_loss = LossSelector(self.config).select_loss_function('RMSE', 'RMSE').to(self.device)

    class TrainingEpoch(_TrainingEpoch):
        def __init__(self, model_setting, data_setting):
            super(SingleTimeDownscalingModelSetting.TrainingEpoch, self).__init__(
                model_setting, data_setting,
                loss_names=['deviation', 'RMSE', 'avg_cosine_distance', 'avg_norm_distance']
            )

        def _prepare_data(self, batch):
            inputs_cpu, targets_cpu, gridLR, gridHR, maskLR, maskHR, hrOro, idx = batch
            inputs_device = inputs_cpu.to(self.modelSetting.device)
            targets_device = targets_cpu.to(self.modelSetting.device)
            if self.modelSetting.config['data']['useHROrography']:
                hrOro_device = hrOro.to(self.modelSetting.device)
            else:
                hrOro_device = None

            mask_device = 1 - maskHR.to(self.modelSetting.device)
            # if self.modelSetting.config['training']['loss']['useMask']:
            #     mask_device = 1 - maskHR.to(self.modelSetting.device)
            # else:
            #     mask_device = None
            return inputs_device, targets_device, hrOro_device, mask_device

        def _apply_model(self, inputs_device, hrOro_device=None):
            if self.modelSetting.config['data']['useHROrography']:
                predictions_device = self.modelSetting.model(inputs_device, hrOro_device)
            else:
                predictions_device = self.modelSetting.model(inputs_device)
            return predictions_device

        def _update_loss(self, predictions_device, targets_device, mask_device=None):
            if self.modelSetting.config['training']['loss']['useMask']:
                loss = self.modelSetting.loss(predictions_device, targets_device, mask_device)
            else:
                loss = self.modelSetting.loss(predictions_device, targets_device, None)

            # get scaler for targets
            if len(self.dataSetting.scalings.target) > 0:
                scaler = self.dataSetting.scalings.target[0][0]
                predictions_real_device = scaler.transform_back(predictions_device)
                targets_real_device = scaler.transform_back(targets_device)
            else:
                predictions_real_device = predictions_device
                targets_real_device = targets_device

            l2_loss = self.modelSetting.l2_loss(predictions_real_device, targets_real_device, mask_device)
            loss_mask = mask_device
            # if self.modelSetting.config['training']['loss']['useMask']:
            #     loss_mask = None

            cosine_loss = self.modelSetting.cosine_loss(predictions_real_device, targets_real_device, loss_mask)
            norm_loss = self.modelSetting.norm_loss(predictions_real_device, targets_real_device, loss_mask)

            self.cumulative_losses.update({
                'deviation': self.cumulative_losses['deviation'] + loss.item(),
                'RMSE': self.cumulative_losses['RMSE'] + l2_loss.item(),
                'avg_cosine_distance': self.cumulative_losses['avg_cosine_distance'] + cosine_loss.item(),
                'avg_norm_distance': self.cumulative_losses['avg_norm_distance'] + norm_loss.item()
            })

            # compute real deviation loss
            if self.modelSetting.config['debugging']['printRealLoss']:
                loss_real = self.modelSetting.loss(
                    predictions_real_device, targets_real_device, mask_device
                )
                self.cumulative_losses.update({
                    'deviation_real': self.cumulative_losses['deviation_real'] + loss_real.item()
                })
            return loss
