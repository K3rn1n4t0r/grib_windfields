import os
import sys
import json
import torch
# change working directory
os.chdir(os.getcwd())
# add working directory to python environment to allow calls from subdirectories
sys.path.insert(0, os.getcwd())

from Training.TrainingSetup import DataSetting, TemporalDownscalingModelSetting

torch.set_num_threads(4)

# parse arguments
# parser = TemporalDownscalingParser()
# parser.parse(sys.argv[1:])

print(os.getcwd())
folder = os.path.join('Training', 'config_linear')
folder_open = os.path.join(folder, 'open')
folder_finished = os.path.join(folder, 'finished')
if not os.path.isdir(folder_finished):
    os.makedirs(folder_finished)

id = 'config'

config_files = [file_name for file_name in os.listdir(folder_open) if file_name.startswith(id)]

for i, file_name in enumerate(config_files):
    print('[INFO] Processing config file <{}>: {} of {}'.format(file_name, i, len(config_files)))
    configOpts = {}
    with open(os.path.join(folder_open, file_name)) as file:
        configOpts.update(json.load(file))

    dataSetting = DataSetting(configOpts)
    modelSetting = TemporalDownscalingModelSetting(configOpts)
    modelSetting.run_training(dataSetting)

    os.rename(os.path.join(folder_open, file_name), os.path.join(folder_finished, file_name))
