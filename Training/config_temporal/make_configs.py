import json
import os

output_dir = 'open'
if not os.path.isdir(output_dir):
    os.makedirs(output_dir)

counter = 0
config = {}

with open('temporal_base.json', 'r') as f:
    config.update(json.load(f))

config['network'].update({'name': 'UNet_Temporal'})


for idx_in_channels, in_channels in enumerate([16, 32, 64]):
    for idx_mask, use_mask in enumerate([True, False]):
        for idx_loss_name, loss_name in enumerate(['L1', 'MSE']):
            for idx_patching, use_patching in enumerate([True, False]):
                for idx_HROro, use_HROro in enumerate([True, False]):
                    for idx_num_steps, num_steps in enumerate([[1, 0], [2, 0], [4, 0], [1, 1], [2, 2]]):
                        gpu = 2 if counter%2 == 0 else 5
                        config['preprocess'].update({
                            'patching': use_patching,
                        })
                        config['data'].update({
                            'useHROrography': use_HROro,
                        })
                        config['training'].update({
                            'loss': {
                                'name': loss_name,
                                'useMask': use_mask,
                            },
                            'gpu': gpu,
                        })
                        config['network'].update({
                            'inputChannels': in_channels,
                        })
                        config['debugging'].update({
                            'recordDir': '/mnt/nfs/netdiskcg/datasets/Kevin/results_gpu{}'.format(gpu),
                        })
                        config['coherence'].update({
                            'numStepsPast': num_steps[0],
                            'numStepsFuture': num_steps[0],
                        })
                        file_name = 'gpu{}_{}{}{}{}{}{}.json'.format(
                            gpu,
                            idx_in_channels, idx_mask, idx_loss_name, idx_patching, idx_HROro, idx_num_steps
                        )
                        with open(os.path.join(output_dir, file_name), 'w') as f:
                            json.dump(config, f, indent=4)
                        counter += 1

