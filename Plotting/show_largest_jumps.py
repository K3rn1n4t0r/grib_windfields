import numpy as np
import os
import matplotlib.pyplot as plt
import pandas as pd

data_dir = os.path.join('results', 'LinearModel_Temporal', 'plots')
parameters = pd.read_csv(os.path.join(data_dir, 'parameters_LinearModel_Temporal.csv'), index_col=0, header=0)
results = pd.read_csv(os.path.join(data_dir, 'training_results_LinearModel_Temporal.csv'), index_col=0, header=0)

plt.figure()
plt.scatter(np.log10(results['training_loss_final']), results['training_loss_max_jump'])
plt.show()