import numpy as np
import os
import pandas as pd
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import plotly.express as px

model_name = 'UNet_Temporal_Server'
results_dir = os.path.join('results', model_name)

parameters = pd.read_csv(os.path.join(results_dir, 'plots', 'parameters_{}.csv'.format(model_name)), index_col=0, header=0)
results = pd.read_csv(os.path.join(results_dir, 'plots', 'training_results_{}.csv'.format(model_name)), index_col=0, header=0)

valid_l1 = (parameters['loss_name'] == 'L1') & (results['training_loss_final'] <= 0.1)
valid_mse = (parameters['loss_name'] == 'MSE') & (results['training_loss_final'] <= 0.01)

column_names = list(parameters.columns)
column_names.pop(column_names.index('run_names'))
column_names.pop(column_names.index('hrOro'))

pars_l1_hr = parameters.loc[valid_l1 & (parameters.hrOro == True), column_names]
pars_mse_hr = parameters.loc[valid_mse & (parameters.hrOro == True), column_names]
pars_l1_lr = parameters.loc[valid_l1 & (parameters.hrOro == False), column_names]
pars_mse_lr = parameters.loc[valid_mse & (parameters.hrOro == False), column_names]

data_l1_hr = results.loc[valid_l1 & (parameters.hrOro == True)]
data_mse_hr = results.loc[valid_mse & (parameters.hrOro == True)]
data_l1_lr = results.loc[valid_l1 & (parameters.hrOro == False)]
data_mse_lr = results.loc[valid_mse & (parameters.hrOro == False)]

print('Searching pairs...')
pairs_l1 = []
for i in range(len(pars_l1_hr)):
    for j in range(len(pars_l1_lr)):
        if np.all(pars_l1_hr.iloc[i,:] == pars_l1_lr.iloc[j,:]):
            pairs_l1.append([i, j])
pairs_l1 = np.array(pairs_l1).transpose()

pairs_mse = []
for i in range(len(pars_mse_hr)):
    for j in range(len(pars_mse_lr)):
        if np.all(pars_mse_hr.iloc[i,:] == pars_mse_lr.iloc[j,:]):
            pairs_mse.append([i, j])
pairs_mse = np.array(pairs_mse).transpose()


print('Plotting...')
fig = make_subplots(1, 2, subplot_titles=['L1', 'MSE'])
border_colors = ['black' if mask else 'grey' for mask in pars_l1_hr['loss_mask'].iloc[pairs_l1[0]].values]
fig.add_trace(
    go.Scatter(
        x=np.log10(data_l1_hr['training_loss_final'].iloc[pairs_l1[0]]),
        y=np.log10(data_l1_lr['training_loss_final'].iloc[pairs_l1[1]]),
        mode='markers',
        name='L1',
        marker={
            'size': pars_l1_hr['in_channels'].iloc[pairs_l1[0]].values,
            'color': pars_l1_hr['steps_past'].iloc[pairs_l1[0]].values,
            'showscale': True,
            'line': {'color': border_colors, 'width': 4},
        },
    ),
    row=1,
    col=1,
)
border_colors = ['black' if mask else 'grey' for mask in pars_mse_hr['loss_mask'].iloc[pairs_mse[0]].values]
fig.add_trace(
    go.Scatter(
        x=np.log10(data_mse_hr['training_loss_final'].iloc[pairs_mse[0]]),
        y=np.log10(data_mse_lr['training_loss_final'].iloc[pairs_mse[1]]),
        mode='markers',
        name='MSE',
        marker={
            'size': pars_mse_hr['in_channels'].iloc[pairs_mse[0]].values,
            'color': pars_mse_hr['steps_past'].iloc[pairs_mse[0]].values,
            'showscale': True,
            'line': {'color': border_colors, 'width': 4},
        },
    ),
    row=1,
    col=2,
)
fig.add_trace(
    go.Scatter(
        x = [-1.5, -1.3],
        y = [-1.5, -1.3],
        mode='lines',
        name='Equality',
        line={'color': 'black', 'dash': 'dot'},
    ),
    row=1,
    col=1,
)
fig.add_trace(
    go.Scatter(
        x = [-2.7, -2],
        y = [-2.7, -2],
        mode='lines',
        name='Equality',
        line={'color': 'black', 'dash': 'dot'},
    ),
    row=1,
    col=2,
)
for col in [1,2]:
    fig.update_xaxes(title_text='with high-res orography', row=1, col=col)
    fig.update_yaxes(title_text='without high-res orography', row=1, col=col)

fig.show()