import plotly.graph_objects as go
import pandas as pd
import numpy as np

model_name = 'LinearModel_Temporal'
pars = pd.read_csv('parameters_{}.csv'.format(model_name), index_col=0, header=0)
res_training = pd.read_csv('training_results_{}.csv'.format(model_name), index_col=0, header=0)
data = pd.merge(pars, res_training, on='run_names')
selection = (data.loss_name == 'L1')
data = data.loc[selection]

fig = go.Figure(
    data = go.Parcoords(
        line_color = 'blue',
        dimensions = list([
            {
                'values': data['in_channels'],
                'label': 'input channels'
            },
            {
                'values': data['hr_layers'],
                'label': 'high-res layers'
            },
            {
                'values': data['steps_past'],
                'label': 'past steps',
                'range': [0,4],
            },
            {
                'values': data['hrOro'],
                'label': 'high-res orography',
                'range': [0, 1],
            },
            {
                'values': data['loss_mask'],
                'label': 'loss mask',
                'range': [0, 1],
            },
            {
                'values': data['training_time'],
                'label': 'training time'
            },
            {
                'values': np.log10(data['training_loss_final']),
                'label': 'training loss at end of training (log10)'
            },
            # {
            #     'values': np.log10(data['training_loss_real_final']),
            #     'label': 'training loss real at end of training (log10)'
            # },
            {
                'values': data['training_slope_final'],
                'label': 'loss slope at end of training'
            }
        ])
    )
)
fig.show()