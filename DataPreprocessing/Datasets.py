from torch.utils.data import Dataset
from .Utils import *
import numpy as np

# Standardization vs Normalization
# Scikit-learn provides StandardScalar, MinMaxScalar
# https://machinelearningmastery.com/how-to-improve-neural-network-stability-and-modeling-performance-with-data-scaling/


class VectorFieldData(Dataset):
    def __init__(self, lowRes, highRes, helperGridsLow, helperGridsHigh, timeAreaMapping,
                 vali=False, patches=False, hrOro={}):
        # be careful if input is different
        # for now, we assume that input shape is
        # (s, c, w, h), where s = number of samples,
        # c = channel, w = width, h = height
        self.inputChannels = lowRes.shape[1]
        self.outputChannels = highRes.shape[1]
        # data sets (input, target)
        self.inputs = lowRes
        self.targets = highRes
        # grid points
        self.static_variables_input = helperGridsLow
        self.static_variables_target = helperGridsHigh
        # self.lonLatLR = lonLatLR
        # self.lonLatHR = lonLatHR
        # self.mask = mask
        # resolution of data sets
        self.inputShape = lowRes.shape[2:4]
        self.outputShape = highRes.shape[2:4]
        self.hrOroGrid = hrOro
        if len(hrOro) != 0:
            self.hrChannels = hrOro[list(hrOro.keys())[0]].shape[0]
        else:
            self.hrChannels = 0

        self.numFields = lowRes.shape[0]

        self.vali = vali
        self.patches = patches
        self.pairSampleIDs = []

        self.timeAreaMapping = timeAreaMapping

        assert(lowRes.shape[0] == highRes.shape[0])

    def __getitem__(self, index):
        # train padded data
        # input = np.copy(self.inputs[index])
        # target = np.copy(self.targets[index])
        # gridLR = np.copy(self.lonLatLR)
        # gridHR = np.copy(self.lonLatHR)
        # mask = np.copy(self.mask)

        input = self._get_steps(index, self.inputs)
        target = self._get_steps(index, self.targets)
        timestep, region = self.timeAreaMapping[index]

        lonLR, latLR, maskLR = self.static_variables_input[region]
        lonHR, latHR, maskHR = self.static_variables_target[region]
        gridLR = np.array([lonLR, latLR])
        gridHR = np.array([lonHR, latHR])
        mask = [
            np.expand_dims(np.array(maskLR, dtype=np.float32), axis=0),
            np.expand_dims(np.array(maskHR, dtype=np.float32), axis=0)
        ]
        if self.hrChannels != 0:
            hrOro = self.hrOroGrid[region]
        else:
            hrOro = np.array([])

        if self.patches is None:
            return input, target, gridLR, gridHR, mask[0], mask[1], hrOro, index
#            return input, target, gridLR, gridHR, mask[0], mask[1], 1, index
        else:
            patchSize = (self.patches[0], self.patches[1])

            if not self.vali:
                if len(self.pairSampleIDs) == 0:
                    indexRangeX, indexRangeY = self._get_index_ranges(input, patchSize)
                    randRangeX = np.arange(indexRangeX)
                    randRangeY = np.arange(indexRangeY)

                    for yy in range(len(randRangeY)):
                        for xx in range(len(randRangeX)):
                            self.pairSampleIDs += [(randRangeY[yy], randRangeX[xx])]

                idx = np.random.randint(len(self.pairSampleIDs))
                pair = self.pairSampleIDs[idx]
                self.pairSampleIDs.remove(pair)

                startIDX = pair[1]
                startIDY = pair[0]
                endIDX = startIDX + patchSize[1]
                endIDY = startIDY + patchSize[0]

                inputPatch  = self._select_patch(input,  xBounds=(startIDX, endIDX), yBounds=(startIDY, endIDY))
                targetPatch = self._select_patch(target, xBounds=(3*startIDX, 3*endIDX), yBounds=(4*startIDY, 4*endIDY))
                gridLRPatch = gridLR[:, startIDY:endIDY, startIDX:endIDX]
                gridHRPatch = gridHR[:, startIDY * 4:endIDY * 4, startIDX * 3:endIDX * 3]
                maskLRPatch = mask[0][:, startIDY:endIDY, startIDX:endIDX]
                maskHRPatch = mask[1][:, startIDY * 4:endIDY * 4, startIDX * 3:endIDX * 3]

                # inputPatch = np.copy(input[:, startIDY:endIDY, startIDX:endIDX])
                # targetPatch = np.copy(target[:, startIDY * 4:endIDY * 4, startIDX * 3:endIDX * 3])
                # gridLRPatch = np.copy(gridLR[:, startIDY:endIDY, startIDX:endIDX])
                # gridHRPatch = np.copy(gridHR[:, startIDY * 4:endIDY * 4, startIDX * 3:endIDX * 3])
                # maskLRPatch = np.copy(mask[0][startIDY:endIDY, startIDX:endIDX])
                # maskHRPatch = np.copy(mask[1][startIDY * 4:endIDY * 4, startIDX * 3:endIDX * 3])

                hrOroPatch = np.array([])
                if len(hrOro) != 0:
                    hrOroPatch = hrOro[:, startIDY * 4:endIDY * 4, startIDX * 3:endIDX * 3]
                    # hrOroPatch = np.copy(hrOro[:, startIDY * 4:endIDY * 4, startIDX * 3:endIDX * 3])

                return inputPatch, targetPatch, \
                       gridLRPatch, gridHRPatch, \
                       maskLRPatch, maskHRPatch, \
                       hrOroPatch, index

    def __len__(self):
        return self.numFields

    def _get_steps(self, index, data):
        return data[index]

    @staticmethod
    def _get_index_ranges(input, patchSize):
        indexRangeX = input.shape[2] - patchSize[1] + 1
        indexRangeY = input.shape[1] - patchSize[0] + 1
        return indexRangeX, indexRangeY

    @staticmethod
    def _select_patch(data, xBounds=None, yBounds=None):
        return data[:, yBounds[0]:yBounds[1], xBounds[0]:xBounds[1]]


class TemporalVectorFieldData(VectorFieldData):
    def __init__(self, lowRes, highRes, helperGridsLow, helperGridsHigh, timeAreaMapping,
                 vali=False, patches=False, hrOro={},
                 numStepsPast=1, numStepsFuture=1, dtPast=1, dtFuture=1):
        super(TemporalVectorFieldData, self).__init__(
            lowRes, highRes, helperGridsLow, helperGridsHigh, timeAreaMapping,
            vali=vali, patches=patches, hrOro=hrOro
        )
        self.numStepsPast = numStepsPast
        self.numStepsFuture = numStepsFuture
        self.dtPast = dtPast
        self.dtFuture = dtFuture

    def _get_steps(self, index, data):
        indexPresent = index + (self.dtPast * self.numStepsPast)
        indexLast = indexPresent + (self.dtFuture * self.numStepsFuture)
        past = [data[i] for i in range(index, indexPresent, self.dtPast)]
        present = [data[indexPresent]]
        future = [data[i] for i in range(indexPresent + self.dtFuture, indexLast + 1, self.dtFuture)]
        return tuple(past + present + future)

    @staticmethod
    def _get_index_ranges(input, patchSize):
        indexRangeX = input[0].shape[2] - patchSize[1] + 1
        indexRangeY = input[0].shape[1] - patchSize[0] + 1
        return indexRangeX, indexRangeY

    @staticmethod
    def _select_patch(data, xBounds=None, yBounds=None):
        return tuple([datum[:, yBounds[0]:yBounds[1], xBounds[0]:xBounds[1]] for datum in data])

    def __len__(self):
        return self.numFields - (self.dtPast * self.numStepsPast) - (self.dtFuture * self.numStepsFuture)