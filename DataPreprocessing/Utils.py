import numpy as np

# def findGlobalMaxNorm(fields, channels):
#     maxNorm = 0
#
#     for field in fields:
#         norm = np.sqrt(np.max(np.abs(field[channels[0], :, :])) ** 2 +
#                        np.max(np.abs(field[channels[1], :, :])) * 2)
#
#         maxNorm = max(maxNorm, norm)
#
#     return maxNorm


def findGlobalMinMax(fields, channels):
    minVal = 1E10
    maxVal = -1E10

    for field in fields:
        for channel in channels:
            maxV = np.max(field[channel, :, :])
            minV = np.min(field[channel, :, :])

            maxVal = max(maxVal, maxV)
            minVal = min(minVal, minV)

    return minVal, maxVal


def findGlobalMaxNorm(fields, channels):
    maxNorm = 0

    for field in fields:
        u = field[channels[0]]
        v = field[channels[1]]

        norms = np.sqrt(u ** 2 + v ** 2)
        maxNormCur = np.max(norms)
        maxNorm = max(maxNorm, maxNormCur)

    return maxNorm


def findGlobalMax(fields, channel):
    maxVal = -1E10

    for field in fields:
        maxV = np.max(np.abs(field[channel, :, :]))
        maxVal = max(maxVal, maxV)

    return maxVal


def normalizeValues2(fields, channel, minMax):
    for field in fields:
        value = field[channel, :, :]

        field[channel, :, :] = 2 * (value - minMax[0]) / (minMax[1] - minMax[0]) - 1

def normalizeValues(fields, channel, minMax):
    for field in fields:
        value = field[channel, :, :]

        field[channel, :, :] = (value - minMax[0]) / (minMax[1] - minMax[0])


def normalizeFieldChannels(field, channels):
    minVal = np.min(field[channels])
    maxVal = np.max(field[channels])

    values = field[channels]
    field[channels] = (values - minVal) / (maxVal - minVal)

    return minVal, maxVal


def denormalizeFieldChannels(field, channels, minVal, maxVal):
    values = field[channels]
    field[channels] = (values) * (maxVal - minVal) + minVal


def denormalizeField(field, channels, norm, minValue, maxValue):
    # denormalize data
    denormalizeFieldChannels(field, channels, minValue, maxValue)

    # reconstruct dimensions
    field[channels] *= norm


def findMaxNorm(field, channels):
    return np.sqrt(np.max(np.abs(field[channels[0], :, :])) ** 2 +
                   np.max(np.abs(field[channels[1], :, :])) ** 2)


def scaleToChannels(field, blockSize=(2, 2)):
    field = np.asarray(field)
    N, C, height, width = field.shape

    heightReduced = height // blockSize[0]
    widthReduced = width // blockSize[1]

    #print(field)
    # y = field.reshape(N, C, heightReduced, blockSize[0], widthReduced, blockSize[1])
    y = field.reshape(N, C, blockSize[0], heightReduced, blockSize[1], widthReduced)
    #print(y)
    #z = np.swapaxes(y, 4, 5)
    z = np.swapaxes(y, 3, 4)
    #z = np.swapaxes(y, 4, 5)
    #print(z)
    k = z.reshape(N, -1, heightReduced, widthReduced)

    return k
