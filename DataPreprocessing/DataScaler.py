import numpy as np
from DataPreprocessing.Utils import findGlobalMaxNorm
import pickle

class DataScaler(object):
    def __init__(self, mode, min_max=None, option=''):
        self.mode = mode
        # for normalization
        self.min_max = min_max
        self.extent = 0
        # for standardization
        self.mu = 0
        self.std = 0
        # options
        self.option = option
        self.max_norm = 0
        # for denormalization
        self.funcList = []

    def fit(self, data):
        if 'dimensionless' in self.option:
            self.max_norm = findGlobalMaxNorm(data, np.arange(len(data)))
            data /= self.max_norm

        if 'normalize' in self.mode:
            if self.min_max is None:
                self.min_max = (np.min(data), np.max(data))

            self.extent = self.min_max[1] - self.min_max[0]

        if 'standardize' in self.mode:
            self.mu = np.mean(data)
            self.std = np.var(data)

    def transform(self, data):
        self.funcList = []
        #tData = np.copy(data)

        if self.option == 'dimensionless':
            data = self.__make_dimensionless(data)
            self.funcList = [3] + self.funcList

        if self.mode == 'normalize':
            data = self.__normalize(data)
            self.funcList = [0] + self.funcList
        elif self.mode == 'normalize_neg':
            data = self.__normalize_neg(data)
            self.funcList = [1] + self.funcList
        elif self.mode == 'standardize':
            data = self.__standardize(data)
            self.funcList = [2] + self.funcList

        return data

    def transform_back(self, data):
        #tData = np.copy(data)

        for func in self.funcList:
            if func == 0:
                data = self.__normalize(data, True)
            elif func == 1:
                data = self.__normalize_neg(data, True)
            elif func == 2:
                data = self.__standardize(data, True)
            elif func == 3:
                data = self.__make_dimensionless(data, True)
            else:
                raise NotImplementedError("Function has not been implemented")

        return data

    # FuncID 0
    def __normalize(self, data, revert=False):
        if revert:
            data = data * self.extent + self.min_max[0]
        else:
            data = (data - self.min_max[0]) / self.extent

        return data

    # FuncID 1
    def __normalize_neg(self, data, revert=False):
        if revert:
            data = (data + 1) / 2 * self.extent + self.min_max[0]
        else:
            data = 2 * (data - self.min_max[0]) / self.extent - 1

        return data

    # FuncID 2
    def __standardize(self, data, revert=False):
        if revert:
            data = data * self.std + self.mu
        else:
            data = (data - self.mu) / self.std

        return data

    # FuncID 3
    def __make_dimensionless(self, data, revert=False):
        if revert:
            data *= self.max_norm
        else:
            data /= self.max_norm

        return data

    def save(self, filename):
        f = open(filename, 'wb')
        pickle.dump(self, f)
        f.close()

    def load(self, filename):
        f = open(filename, 'rb')
        obj = pickle.load(f)
        f.close()
        return obj