import numpy as np


class Quantizer(object):
    def __init__(self, num_bins, min_max=None):
        self.num_bins = num_bins
        self.min_max = min_max
        self.delta = 0

    def fit(self, data):
        if self.min_max is None:
            self.min_max = (np.floor(np.min(data)), np.ceil(np.max(data)))

        self.delta = (self.min_max[1] - self.min_max[0]) / self.num_bins

    def transform(self, data):
        #tData = np.copy(data)

        data = self.__quantize(data)
        return data

    def __quantize(self, data):
        return self.delta * np.floor(data / self.delta + 0.5)
