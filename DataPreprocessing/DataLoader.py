import numpy as np
import os
import re
from DataPreprocessing.Datasets import VectorFieldData, TemporalVectorFieldData
from DataPreprocessing.Utils import scaleToChannels
import datetime
from DataPreprocessing.DataScaler import DataScaler
from DataPreprocessing.Quantizer import Quantizer
import random
import math
import sys
import glob
import re
import time
from calendar import monthrange
from Utils import checkTimesteps
from Utils.ProgressBar import ProgressBar
import h5py
import pandas

from collections import namedtuple, OrderedDict
dataSettings = namedtuple('DataSettings', ['lrDataName', 'lrOroName', 'lrGrids',
                                           'hrDataName', 'hrOroName', 'hrGrids'])

trainingSettings = namedtuple('TrainingSettings', ['timeRange', 'patches', 'useHROrography', 'regions'])

# hourly --> settings should be integer only: 5
# monthly --> settings should be tuple or array of integers: (8, 10, ...)
validationSettings = namedtuple('ValidationSettings', ['mode', 'settings', 'regions'])

scalingSettings = namedtuple('ScalingSettings', ['input', 'target', 'hr'])

temporalSettings = namedtuple('TemporalSettings', ['numStepsPast','dtPast',
                                                   'numStepsFuture', 'dtFuture'])


class HDF5File:
    def __init__(self, filename, time_format='%Y-%m-%d %H'):
        print('[INFO]: Load information from H5 file <{}>'.format(filename))

        self._file = h5py.File(filename, "r")
        self._version = self._file.attrs['version']
        self.mode = self._file.attrs['mode']
        self.area = self._file.attrs['area']

        self._time_format = time_format

        print('[INFO]: Convert timesteps <{}>'.format(filename))
        self._valid_times = pandas.to_datetime(self._file['valid_times'], format=self._time_format).values

        self._dynamic_datasets = self._file['dynamic_variables']
        self._static_datasets = self._file['static_variables']

    def _compute_time_range(self):
        # take timesteps and compute min / max date range
        pass

    def is_timestep_in_data(self, timestep):
        #assert(isinstance(time, datetime.datetime))
        return timestep in self._valid_times

    def get_version(self):
        return self._version

    def get_static_datasets(self, datasets):
        if not isinstance(datasets, list):
            datasets = [datasets]

        result = []

        grids_contained = [d for d in datasets if d in self._static_datasets.keys()]
        if len(grids_contained) > 0:
            progress = ProgressBar(len(grids_contained))
            print("[INFO]: Load static grids <{}> for <{}> ...".format(grids_contained, self.area))
            progress.proceed(0)

        for g, grid in enumerate(grids_contained):
            if grid == 'seaMask':
                data = self._static_datasets[grid]
                lsm = (np.array(data) > 0.5).astype(np.float32)
                result += [lsm]
            else:
                result += [self._static_datasets[grid]]

            progress.proceed(g + 1)

        return np.array(result)

    def get_dynamic_datasets_at_index(self, datasets, index):
        if not isinstance(datasets, list):
            datasets = [datasets]

        result = []

        for grid in datasets:
            if grid in self._dynamic_datasets.keys():

                result += [self._dynamic_datasets[grid][index]]

        return np.array(result)

    def get_dynamic_datasets_at_timestep(self, datasets, timestep):
        idx = np.argwhere(self._valid_times == timestep).flatten()
        if len(idx) == 0:
            raise Exception()
        return self.get_dynamic_datasets_at_index(datasets, idx)

    def get_dynamic_datasets_in_timerange(self, datasets, time_min, time_max):
        assert(isinstance(time_min, np.datetime64))
        assert(isinstance(time_max, np.datetime64))

        mask = np.full(len(self._valid_times), True, dtype=bool)
        #valid_times = list(self._valid_times)
        for i, valid_time in enumerate(self._valid_times):
            #valid_time = valid_time.decode('utf-8')
            # time = datetime.datetime.strptime(valid_time, self._time_format)
            mask[i] = (time_min <= valid_time <= time_max)

        idxs = list(np.argwhere(mask).flatten().astype(np.int32))
        result = []

        # find all grids contained in dynamic variables
        grids_contained = [d for d in datasets if d in self._dynamic_datasets.keys()]
        if len(grids_contained) > 0:
            progress = ProgressBar(len(grids_contained))

            print("[INFO]: Load dynamic grids <{}> for <{}> in time range ({} - {}) ..."
                  .format(grids_contained, self.area, str(time_min), str(time_max)))

        progress.proceed(0)

        for g, grid in enumerate(grids_contained):
            # if grid in self._dynamic_datasets.keys():
            selected_grids = self._dynamic_datasets[grid]
            result += [selected_grids[idxs]]
            progress.proceed(g + 1)

        return np.array(result), self._valid_times[idxs]

    def get_valid_times(self):
        return self._valid_times

    def get_dynamic_dataset_names(self):
        return self._dynamic_datasets.keys()

    def get_static_dataset_names(self):
        return self._static_datasets.keys()

    def close(self):
        self._file.close()


class Importer:
    def __init__(self, directory):
        self.directory = directory
        self.preloaded_files_input = []
        self.preloaded_files_target = []

        self._load_files()

    def _load_files(self):
        raise NotImplementedError()


class H5Importer(Importer):
    def __init__(self, directory, low_res_filter_name='input', high_res_filter_name='target'):
        self.low_res_filter_name = low_res_filter_name
        self.high_res_filter_name = high_res_filter_name
        super(H5Importer, self).__init__(directory)

    def _load_files(self):
        all_files = sorted(glob.glob(self.directory + "/**/*.hdf5", recursive=True))

        if len(all_files) == 0:
            raise Exception("Dataset directory <{}> is empty.".format(self.directory))

        # divide into input and target files (ERA5 = low-res, HRES = high-res)
        input_files = list(filter(lambda x: os.path.basename(x).startswith(self.low_res_filter_name), all_files))
        target_files = list(filter(lambda x: os.path.basename(x).startswith(self.high_res_filter_name), all_files))

        assert (len(input_files) == len(target_files))

        # load all input HDF5 files
        for filename in input_files:
            file = HDF5File(filename)
            # print('[INFO]: Open input HDF5 file <{}>'.format(filename))
            cur_area = file.area

            self.preloaded_files_input += [(file, cur_area)]

        # load all target HDF5 files
        for filename in target_files:
            file = HDF5File(filename)
            # print('[INFO]: Open target HDF5 file <{}>'.format(filename))
            cur_area = file.area

            self.preloaded_files_target += [(file, cur_area)]

    def get_dynamic_grids_training_pair(self, grids_input, grids_target, time_min, time_max, area):
        input_grids, input_timesteps = self.get_input_dynamic_grids(grids_input, time_min, time_max, area)
        target_grids, target_timesteps = self.get_target_dynamic_grids(grids_target, time_min, time_max, area)

        assert(len(input_timesteps) == len(target_timesteps))

        return [input_grids, input_timesteps], [target_grids, target_timesteps]

    def get_static_grids_training_pair(self, grids_input, grids_target, area):
        input_grids = self.get_input_static_grids(grids_input, area)
        target_grids = self.get_target_static_grids(grids_target, area)

        # assert (len(input_grids) == len(target_grids))

        return input_grids, target_grids

    def get_input_dynamic_grids(self, grids, time_min, time_max, area):
        return self._dynamic_grids(self.preloaded_files_input, time_min, time_max, area, grids)

    def get_input_static_grids(self, grids, area):
        return self._static_grids(self.preloaded_files_input, area, grids)

    def get_target_dynamic_grids(self, grids, time_min, time_max, area):
        return self._dynamic_grids(self.preloaded_files_target, time_min, time_max, area, grids)

    def get_target_static_grids(self, grids, area):
        return self._static_grids(self.preloaded_files_target, area, grids)

    def _dynamic_grids(self, files, time_min, time_max, area, grids):
        dynamic_grids = []
        all_timesteps = []

        for file, file_area in files:
            if file_area != area:
                continue

            grids, timesteps = file.get_dynamic_datasets_in_timerange(grids, time_min, time_max)
            dynamic_grids += [grids]
            all_timesteps += [timesteps]

        return np.concatenate(dynamic_grids, axis=1), np.concatenate(all_timesteps, axis=0)

    def _static_grids(self, files, area, grids):
        static_grids = []

        for file, file_area in files:
            if file_area != area:
                continue

            static_grids += [file.get_static_datasets(grids)]

        return np.concatenate(static_grids, axis=0)

    # def get_training_pairs(self, time_min, time_max, area, grids_input, grids_target):
    #     pass


class NPZImporter(Importer):
    def __init__(self, directory, regions):
        self.trainingRegions = regions
        super(NPZImporter, self).__init__(directory)

        self._load_files()

    def _load_files(self):
        # for now we assume we cover the same area / region / domain
        allFiles = sorted(glob.glob(self.directory + "/**/*.npz", recursive=True))

        if len(allFiles) == 0:
            raise Exception("Dataset directory <{}> is empty.".format(self.directory))

        # divide into input and target files (ERA5 = low-res, HRES = high-res)
        allInputFiles = list(filter(lambda x: os.path.basename(x).startswith('ERA5'), allFiles))
        allTargetFiles = list(filter(lambda x: os.path.basename(x).startswith('HRES'), allFiles))

        assert (len(allInputFiles) == len(allTargetFiles))

        if len(self.preloaded_files_input) == 0:
            for filename in allInputFiles:
                curArea = re.search(r"(area[0-9])", filename).group(0)

                if curArea not in self.trainingRegions:
                    continue

                file = np.load(filename)
                print('[INFO]: Load data from file <{}>'.format(filename))

                fileContent = {}
                for key in file.files:
                    fileContent[key] = file[key]

                self.preloaded_files_input += [(fileContent, curArea)]
                file.close()

        if len(self.preloaded_files_target) == 0:
            for filename in allTargetFiles:
                curArea = re.search(r"(area[0-9])", filename).group(0)

                if curArea not in self.trainingRegions:
                    continue

                file = np.load(filename)
                print('[INFO]: Load data from file <{}>'.format(filename))

                fileContent = {}
                for key in file.files:
                    fileContent[key] = file[key]

                self.preloaded_files_target += [(fileContent, curArea)]
                file.close()


class DataLoader(object):
    def __init__(self, loader, maxNumFiles, dataOpts, trainingOpts, scaleOpts, valiOpts=None, tempOpts=None):
        self.loader = loader
        self.maxNumFiles = maxNumFiles

        self.lrName = dataOpts.lrDataName
        self.lrOroName = dataOpts.lrOroName
        self.hrName = dataOpts.hrDataName
        self.hrOroName = dataOpts.hrOroName

        self.input_grids = dataOpts.lrGrids
        self.target_grids = dataOpts.hrGrids
        self.input_hr_grids = None

        self.inTestMode = valiOpts is None

        if valiOpts:
            self.valiMode = valiOpts.mode
            self.valiSettings = valiOpts.settings
            self.valiRegions = valiOpts.regions

        self.inTemporalMode = tempOpts is not None

        if tempOpts:
            self.numStepsPast = tempOpts.numStepsPast
            self.numStepsFuture = tempOpts.numStepsFuture
            self.dtPast = tempOpts.dtPast
            self.dtFuture = tempOpts.dtFuture

        # self.fileGridsMapLR = OrderedDict()
        # self.fileGridsMapHR = OrderedDict()
        # self.inputs = None
        # self.targets = None
        # self.additionalGridsLR = []
        # self.additionalGridsHR = []
        self.hrInfo = []
        self.hrOroGrids = []

        self.inputScalers = scaleOpts.input
        self.targetScalers = scaleOpts.target
        self.hrScaler = scaleOpts.hr
        self.patches = trainingOpts.patches
        self.useHROrography = trainingOpts.useHROrography
        self.timeRange = trainingOpts.timeRange
        self.trainingRegions = trainingOpts.regions

    def loadDataForTraining(self):
        # 1) Parse directory, get all files and load grids
        self.__prepareData()
        # 2) Normalize data
        self.__normalize()
        # 3) Divide data into training and validation
        return self.__createData()

    def fillInputs(self, i, grids):
        self.inputs[i] = grids

    def __prepareData(self):
        self.input_targets = {'times_areas': None, 'inputs_hr': {},
                              'input_static_variables': {}, 'target_static_variables': {},
                              'inputs': [], 'targets': []}
        # {'valid_times', 'region', 'data' }

        # prepare input and target pairs for training
        # print("[INFO]: prepare input and target files.")
        # progress = ProgressBar(len(self.timeRange))
        print(self.timeRange)

        # check whether valdiation is run on different time ranges and regions from the
        # training options --> in this case, also load these data
        missing_regions = []
        missing_ranges = []

        for v, time_range_valid in enumerate(self.valiSettings):
            region_valid = self.valiRegions[v]

            min_time_range = time_range_valid[0]
            max_time_range = time_range_valid[1]

            min_date_valid = datetime.datetime.combine(datetime.date(*min_time_range, 1), datetime.time.min)
            max_date_valid = datetime.datetime.combine(datetime.date(*max_time_range, monthrange(*max_time_range)[1]),
                                                 datetime.time.max)

            is_already_contained = False
            temp_missing_region = None
            temp_missing_range = None

            for r, time_range in enumerate(self.timeRange):
                region = self.trainingRegions[r]

                if region != region_valid:
                    temp_missing_region = [region_valid]
                    temp_missing_range = [time_range_valid]
                    continue

                min_time_range = time_range[0]
                max_time_range = time_range[1]

                min_date = datetime.datetime.combine(datetime.date(*min_time_range, 1), datetime.time.min)
                max_date = datetime.datetime.combine(datetime.date(*max_time_range, monthrange(*max_time_range)[1]),
                                                     datetime.time.max)

                if min_date_valid >= min_date and max_date_valid <= max_date:
                    is_already_contained = True
                    continue

                elif min_date_valid < min_date:
                    temp_missing_range += [(min_date_valid.year, min_date_valid.month),
                                           (min_date.year, min_date.month - 1)]
                    temp_missing_region += [region_valid]

                elif max_date_valid > max_date:
                    temp_missing_range += [(max_date.year, max_date.month + 1),
                                           (max_date_valid.year, max_date_valid.month)]
                    temp_missing_region += [region_valid]

            if not is_already_contained:
                missing_regions += temp_missing_region
                missing_ranges += temp_missing_range


        time_ranges_for_loading = self.timeRange + missing_ranges
        regions_for_loading = self.trainingRegions + missing_regions


        # iterate over regions and time ranges
        for r, time_range in enumerate(time_ranges_for_loading):
            region = regions_for_loading[r]
            min_time_range = time_range[0]
            max_time_range = time_range[1]

            min_date = datetime.datetime.combine(datetime.date(*min_time_range, 1), datetime.time.min)
            max_date = datetime.datetime.combine(datetime.date(*max_time_range, monthrange(*max_time_range)[1]), datetime.time.max)

            min_date = np.datetime64(min_date)
            max_date = np.datetime64(max_date)

            dynamic_grids_training = self.loader.get_dynamic_grids_training_pair(self.input_grids, self.target_grids, min_date, max_date, region)

            # self.input_hr_grids = None
            if self.useHROrography:
                self.input_hr_grids = ['zOro', 'seaMask']

            static_grids_training = self.loader.get_static_grids_training_pair(self.input_grids, self.target_grids, region)

            static_grid_selection = list(OrderedDict.fromkeys(['lons', 'lats', 'padding_mask']))
            static_grids_common = self.loader.get_static_grids_training_pair(static_grid_selection, static_grid_selection, region)
            self.input_targets['input_static_variables'][region] = static_grids_common[0]
            self.input_targets['target_static_variables'][region] = static_grids_common[1]

            static_grids_input_hr = []
            if self.useHROrography:
                static_grids_input_hr = self.loader.get_target_static_grids(['zOro', 'seaMask'], region)

            # generate training data (input and target pairs)
            training_input_target = []

            for i, (dynamic_grids, timesteps) in enumerate(dynamic_grids_training):
                num_timesteps = len(timesteps)
                if i == 0:
                    times_area = np.array(list(zip(timesteps, np.repeat([region], num_timesteps))))

                    if self.input_targets['times_areas'] is None:
                        self.input_targets['times_areas'] = times_area
                    else:
                        self.input_targets['times_areas'] = \
                            np.concatenate([self.input_targets['times_areas'], times_area], axis=0)

                used_grids = [dynamic_grids]

                for grid in static_grids_training[i]:
                    used_grids += [np.expand_dims(np.repeat([grid], num_timesteps, axis=0), 0)]

                final_grids = np.concatenate(used_grids, axis=0)
                training_input_target += [np.swapaxes(final_grids, 0, 1)]

            # store hr information for training in input_targets dictionary
            if region not in self.input_targets['inputs_hr'] and self.useHROrography:
                self.input_targets['inputs_hr'][region] = static_grids_input_hr

            # store all input and targets in dictionary for further processing
            # Each element in inputs and targets can be referenced by times_areas (with timestep and area)
            self.input_targets['inputs'] += [training_input_target[0]]
            self.input_targets['targets'] += [training_input_target[1]]
            # if 'inputs' not in self.input_targets:
            #     self.input_targets['inputs'] = training_input_target[0]
            #     self.input_targets['targets'] = training_input_target[1]
            # else:
            #     self.input_targets['inputs'] = np.concatenate([self.input_targets['inputs'],
            #                                                    training_input_target[0]], axis=0)
            #     self.input_targets['targets'] = np.concatenate([self.input_targets['targets'],
            #                                                     training_input_target[1]], axis=0)

        self.input_targets['inputs'] = np.vstack(self.input_targets['inputs'])
        self.input_targets['targets'] = np.vstack(self.input_targets['targets'])

            # progress.proceed(r + 1)

    def __findScalerGridMapping(self, grids, gridPool):
        channels = []

        for grid in grids:
            # find channels of grids
            # channels += [self.lrGrids.index(grid)]
            channels += [i for i, x in enumerate(gridPool) if x == grid]
            if len(channels) == 0:
                # except ValueError:
                #print("Could not find grid {} in input fields.".format(grid))
                break
                #exit(-2)

        return channels

    def __normalize(self):
        print("[INFO]: Normalize input and output channels")
        # scale input files
        for scaler, grids in self.inputScalers:
            if scaler is None:
                continue
            # obtain scaler = any transformation class containing a fully implemented fit and transform method
            # and grids = the grids the scaler has to be applied to, indices are obtained from the input grid list
            channels = self.__findScalerGridMapping(grids, self.input_grids)
            if len(channels) == 0:
                print("[WARNING]: Could not scale input grid {}.".format(str(grids)))
                continue

            if not self.inTestMode:
                print("[INFO]: Re-scale input grids {} stored in channels {}.".format(str(grids), str(channels)))
                scaler.fit(self.input_targets['inputs'][:, channels])
            self.input_targets['inputs'][:, channels] = scaler.transform(self.input_targets['inputs'][:, channels])

        # scale target files
        for scaler, grids in self.targetScalers:
            if scaler is None:
                continue
            # obtain scaler = any transformation class containing a fully implemented fit and transform method
            # and grids = the grids the scaler has to be applied to, indices are obtained from the target grid list
            channels = self.__findScalerGridMapping(grids, self.target_grids)
            if len(channels) == 0:
                print("[WARNING]: Could not target scale grid {}.".format(str(grids)))
                continue
            if self.inTestMode == False:
                print("[INFO]: Re-scale target grids {} stored in channels {}.".format(str(grids), str(channels)))
                scaler.fit(self.input_targets['targets'][:, channels])
            self.input_targets['targets'][:, channels] = scaler.transform(self.input_targets['targets'][:, channels])

        # scale optional hr orography
        if self.useHROrography:
            for scaler, grids in self.hrScaler:
                if scaler is None:
                    continue
                # obtain scaler = any transformation class containing a fully implemented fit and transform method
                # and grids = the grids the scaler has to be applied to, indices are obtained from the target grid list
                channels = self.__findScalerGridMapping(grids, self.input_hr_grids)
                if len(channels) == 0:
                    print("[WARNING]: Could not scale high-res grid {}.".format(str(grids)))
                    continue
                if self.inTestMode == False:
                    hrFields = []
                    for area in self.input_targets['inputs_hr']:
                        hrFields += [self.input_targets['inputs_hr'][area]]
                    hrFields = np.array(hrFields)
                    scaler.fit(hrFields[:, channels])

                for area in self.input_targets['inputs_hr']:
                    print("[INFO]: Re-scale high-res grids {} stored in channels {} in area {}.".format(str(grids),
                                                                                                        str(channels), str(area)))
                    self.input_targets['inputs_hr'][area][channels] = scaler.transform(self.input_targets['inputs_hr'][area][channels])

    def __mask_times_per_area(self, mode, time_filter, regions):
        mask = np.full(len(self.input_targets['times_areas']), False, dtype=bool)

        if mode == 'hourly':
            hour = time_filter
            mask = list(map(lambda x: x[0].hour % hour, self.input_targets['times_areas']))

        elif mode == 'monthly':
            for r, range in enumerate(time_filter):
                region_filter = regions[r]

                min_date = datetime.datetime.combine(datetime.date(*range[0], 1), datetime.time.min)
                max_date = datetime.datetime.combine(datetime.date(*range[1], monthrange(*range[1])[1]),
                                                     datetime.time.max)

                min_date = np.datetime64(min_date)
                max_date = np.datetime64(max_date)

                mask_cur = list(map(lambda x: min_date <= x[0] <= max_date and x[1] == region_filter,
                                    self.input_targets['times_areas']))

                # combine masks
                mask = [x | y for x, y in zip(mask, mask_cur)]

        return mask

    def __createData(self):
        print("[INFO]: Create training and validation data")
        valiInputIDX, valiTargetIDX = [], []
        trainingInputIDX, trainingTargetIDX = [], []

        training_idxs = np.arange(0, len(self.input_targets['times_areas']), 1)
        validation_idxs = []

        if not self.inTestMode:
            valid_mask = self.__mask_times_per_area(self.valiMode, self.valiSettings, self.valiRegions)
            validation_idxs = np.argwhere(valid_mask).flatten()
            training_idxs = np.argwhere(np.invert(valid_mask)).flatten()

        training_times_areas = self.input_targets['times_areas'][training_idxs]
        validation_times_areas = self.input_targets['times_areas'][validation_idxs]

        training_data_input = self.input_targets['inputs'][training_idxs]
        training_data_target = self.input_targets['targets'][training_idxs]

        validation_data_input = self.input_targets['inputs'][validation_idxs]
        validation_data_target = self.input_targets['targets'][validation_idxs]

        if not self.inTestMode:
            print("[INFO]: Number of training files: {}".format(len(training_times_areas)))
            print("[INFO]: Number of validation files: {}".format(len(validation_times_areas)))
        else:
            print("[INFO]: Number of test files: {}".format(len(training_times_areas)))

        # Create the data set class required by the PyTorch framework for training
        if self.inTemporalMode:
            trainingData = TemporalVectorFieldData(
                training_data_input, training_data_target,
                self.input_targets['input_static_variables'], self.input_targets['target_static_variables'],
                training_times_areas,
                patches=self.patches, hrOro=self.input_targets['inputs_hr'],
                numStepsPast=self.numStepsPast, numStepsFuture=self.numStepsFuture,
                dtPast=self.dtPast, dtFuture=self.dtFuture
            )
            validationData = None
            if not self.inTestMode:
                # Create the data set class required by the PyTorch framework for validation
                validationData = TemporalVectorFieldData(
                    validation_data_input, validation_data_target,
                    self.input_targets['input_static_variables'], self.input_targets['target_static_variables'],
                    validation_times_areas,
                    patches=None, hrOro=self.input_targets['inputs_hr'],
                    numStepsPast = self.numStepsPast, numStepsFuture = self.numStepsFuture,
                    dtPast = self.dtPast, dtFuture = self.dtFuture
                )
        else:
            trainingData = VectorFieldData(
                training_data_input, training_data_target,
                self.input_targets['input_static_variables'], self.input_targets['target_static_variables'],
                training_times_areas,
                patches=self.patches, hrOro=self.input_targets['inputs_hr']
            )
            validationData = None
            if not self.inTestMode:
                # Create the data set class required by the PyTorch framework for validation
                validationData = VectorFieldData(
                    validation_data_input, validation_data_target,
                    self.input_targets['input_static_variables'], self.input_targets['target_static_variables'],
                    validation_times_areas,
                    patches=None, hrOro=self.input_targets['inputs_hr'],
                )

        return trainingData, validationData
