#!/bin/bash
serverPort=$1
localPort=1$serverPort
ssh -N -f -L localhost:$localPort:localhost:$serverPort kern@gpusrv01.cg.in.tum.de;
chromium-browser "http://localhost:$localPort"
