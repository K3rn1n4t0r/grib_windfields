import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from .Buildingblocks import CompressionNet, ExpansionNet, InputLR, ConvLayer
from Utils import depth_to_space


class UNet_D2S(nn.Module):
    def __init__(self, data_channels=4, in_channels=64, out_channels=2,
                 compression = [1, 3, 2, 2],
                 use_bn=True, use_cc=True, leaky_slope=0.05, dropout=0.1, use_oa=False,
                 learn_residual=True,
                 padding_mode='reflection'):
        super(UNet_D2S, self).__init__()

        self.learn_residual = learn_residual
        self.in_channels = in_channels

        self.inputLR = InputLR(
            data_channels=data_channels, in_channels=in_channels, out_channels=in_channels,
            dropout=dropout, leaky_slope=leaky_slope,
            use_bn=use_bn, use_oa=True,
            padding_mode=padding_mode)

        depth_channels = self._get_channels(compression)

        self.compression = CompressionNet(
            in_channels=in_channels,
            depth_channels=depth_channels,
            dropout=dropout, leaky_slope=leaky_slope,
            use_bn=use_bn, use_cc=use_cc, use_oa=True,
            padding_mode=padding_mode)

        self.expansion = ExpansionNet(
            in_channels=self.compression.out_channels(),
            out_channels=in_channels,
            depth_channels=depth_channels,
            dropout=dropout, leaky_slope=leaky_slope,
            use_bn=use_bn, use_cc=use_cc, use_oa=True,
            padding_mode=padding_mode
        )

        self.output = nn.Sequential(
            ConvLayer(
                in_channels=in_channels, out_channels=in_channels,
                kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=0.
            ),
            ConvLayer(
                in_channels=in_channels, out_channels=in_channels,
                kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
            ),
            ConvLayer(
                in_channels=in_channels, out_channels = out_channels * 4 * 3,
                kernel_size=(1, 1), stride=(1, 1), padding=(0, 0), padding_mode=padding_mode,
                use_bn=use_bn, use_oa=use_oa, leaky_slope=leaky_slope, dropout=0
            ),
        )

    def forward(self, x):
        features = self.inputLR(x)
        features = self.compression(features)
        features = self.expansion(features)
        features = depth_to_space(self.output(features), (4, 3))
        if self.learn_residual:
            features += F.interpolate(x[:,0:2,:,:], size=features.size()[-2:], mode='bilinear')
        return features

    def _get_channels(self, compression):
        assert isinstance(compression, (list, np.array)) and len(compression) == 4
        compression = np.cumprod(np.array(compression))
        dof = 60 * 36 * self.in_channels / compression
        pixels = 3 * 5 * np.array([9 * 4**2, 4**2, 4, 1])
        channels = dof / pixels
        return np.ceil(channels).astype(int)
