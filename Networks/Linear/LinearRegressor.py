import torch
import torch.nn as nn
import numpy as np


# Simple linear regression by using the nn.Linear module only
# Should demonstrate the potential of linear models
class LinearRegressor(nn.Module):
    def __init__(self, input_shape=(36, 60), output_shape=(144, 180)):
        super(LinearRegressor, self).__init__()
        self.input_shape = input_shape
        self.output_shape = output_shape
        self.linear = torch.nn.Linear(np.prod(input_shape), np.prod(output_shape))

    def forward(self, x):
        batches = x.shape[0]
        # x_copy = x.clone()
        output = x.reshape((batches,) + (np.prod(self.input_shape),))
        output = self.linear(output)
        output = output.reshape((batches,) + tuple(self.output_shape))
        return output
