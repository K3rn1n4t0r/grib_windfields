import torch
import torch.nn as nn
import numpy as np
from Networks.CustomModules import ConvBlock
from Networks.Buildingblocks import Resampling2D


# Simple convolutional neural network without any non-linear activation functions
class LinearCNN(nn.Module):
    def __init__(self, num_input_channels, num_output_channels,
                 num_feature_channels=32, kernel_size=5, num_additional_conv_layers=0,
                 padding_mode='replication', use_batch_norm=True):
        super(LinearCNN, self).__init__()
        # input block linear
        blocks = [ConvBlock(num_input_channels, num_feature_channels, kernel_size=kernel_size,
                            leaky_slope=0.0, use_batch_norm=use_batch_norm, padding_mode=padding_mode)]

        for _ in range(num_additional_conv_layers):
            blocks += [ConvBlock(num_feature_channels, num_feature_channels, kernel_size=kernel_size,
                                 leaky_slope=0.0, use_batch_norm=use_batch_norm, padding_mode=padding_mode)]

        # super-sampling block linear
        blocks += [Resampling2D(scale_factor=(4, 3))]
        blocks += [ConvBlock(num_feature_channels, num_feature_channels, kernel_size=kernel_size,
                             leaky_slope=0.0, use_batch_norm=use_batch_norm, padding_mode=padding_mode)]

        for _ in range(num_additional_conv_layers):
            blocks += [ConvBlock(num_feature_channels, num_feature_channels, kernel_size=kernel_size,
                                 leaky_slope=0.0, use_batch_norm=use_batch_norm, padding_mode=padding_mode)]

        # output block linear
        blocks += [ConvBlock(num_feature_channels, num_output_channels, kernel_size=kernel_size,
                             leaky_slope=0.0, use_batch_norm=use_batch_norm, padding_mode=padding_mode)]

        self.model = nn.Sequential(*blocks)

    def forward(self, x):
        output = self.model(x)
        return output

