import torch
import torch.nn as nn
from ..InformationMeasures import estimate_dKL_zx_z, estimate_dKL_z_z


class CondEncoder(nn.Module):
    def __init__(self, args=None):
        super(CondEncoder, self).__init__()
        if args is None:
            args = {}
        if 'use_nf' in args.keys():
            self.use_nf = args['use_nf']
        else:
            self.use_nf = False

    def forward(self, target, conditions, samples=1):
        mu, logVar = self.encode(target, conditions)
        z = self.reparametrize(mu, logVar, samples=samples)
        if self.use_nf:
            z = self.normalizing_flow(z)
        return mu, logVar, z

    def encode(self, target, conditions, samples=1):
        raise NotImplementedError

    @staticmethod
    def reparametrize(mu, logVar, samples=1):
        batch_size, *shape = mu.size()
        std = torch.exp(logVar / 2.)
        mu_expd = mu.unsqueeze(1).expand(-1, samples, *[-1 for _ in shape])
        eps = torch.randn_like(mu_expd)
        z = mu.unsqueeze(1) + eps * std.unsqueeze(1)
        return z

    def estimate_information(self, target, conditions, samples=1, reduction='mean'):
        mu, logVar = self.encode(target, conditions)
        dKL_zx_z = self.estimate_dKL_zx_z(mu, logVar, reduction)
        z = self.reparametrize(mu, logVar, samples)
        dKL_z_z = self.estimate_dKL_z_z(z, mu, logVar, reduction)
        diff = dKL_zx_z - dKL_z_z
        mi_x_z = torch.max(diff, torch.zeros_like(diff))
        return mi_x_z, dKL_zx_z, dKL_z_z

    @staticmethod
    def estimate_dKL_zx_z(mu, logVar, reduction='mean'):
        return estimate_dKL_zx_z(mu, logVar, reduction)

    @staticmethod
    def estimate_dKL_z_z(z, mu, logVar, reduction='mean'):
        return estimate_dKL_z_z(z, mu, logVar, reduction)

    def normalizing_flow(self, z):
        return z
