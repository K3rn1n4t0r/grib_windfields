import torch
import torch.nn as nn
from .CondEncoder import CondEncoder
from Networks.Buildingblocks import CompressionNet, InputLR, InputHR, ConvLayer
import numpy as np


class CE_VGG(CondEncoder):
    def __init__(self, data_channels_lr=4, data_channels_hr=2, in_channels=64, latent_channels=16,
                 compression = [2, 3, 2, 2],
                 use_bn=True, leaky_slope=0.05, dropout=0.1,
                 padding_mode='reflection'):
        super(CE_VGG, self).__init__()

        self.data_channels_lr = data_channels_lr
        self.data_channels_hr = data_channels_hr
        self.in_channels = in_channels

        self.latent_channels = latent_channels

        depth_channels = self._get_channels(compression)

        self.inputLR = InputLR(
            data_channels=data_channels_lr, in_channels=in_channels, out_channels=in_channels,
            dropout=dropout, leaky_slope=leaky_slope,
            use_bn=use_bn, use_oa=True,
            padding_mode='reflection')

        self.inputHR = InputHR(
            data_channels=data_channels_hr, in_channels=in_channels, out_channels=in_channels,
            dropout=dropout, leaky_slope=leaky_slope,
            use_bn=use_bn, use_oa=True,
            padding_mode='reflection')

        self.compression = CompressionNet(
            in_channels=2*in_channels,
            depth_channels=depth_channels,
            dropout=dropout, leaky_slope=leaky_slope,
            use_bn=use_bn, use_cc=False, use_oa=True,
            padding_mode=padding_mode)

        self.output_mu = nn.Conv2d(
            in_channels=depth_channels[3], out_channels=self.latent_channels,
            kernel_size=(1, 1), stride=(1, 1), padding=(0, 0)
        )

        self.output_logVar = nn.Conv2d(
            in_channels=depth_channels[3], out_channels=self.latent_channels,
            kernel_size=(1, 1), stride=(1, 1), padding=(0, 0)
        )

    def encode(self, target, conditions, samples=1):
        features = torch.cat([self.inputLR(conditions), self.inputHR(target)], 1)
        features = self.compression(features)
        return self.output_mu(features), self.output_logVar(features)

    def _get_channels(self, compression):
        assert isinstance(compression, (list, np.array)) and len(compression) == 4
        compression = np.cumprod(np.array(compression))
        dof = 60 * 36 * self.in_channels / compression
        pixels = 3 * 5 * np.array([9 * 4**2, 4**2, 4, 1])
        channels = dof / pixels
        return np.ceil(channels).astype(int)





class CE_VGG_HR(nn.Module):
    def __init__(self):
        super(CE_VGG_HR, self).__init__()