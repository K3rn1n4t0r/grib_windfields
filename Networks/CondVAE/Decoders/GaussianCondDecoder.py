import torch
import numpy as np
from .CondDecoder import CondDecoder


class GaussianCondDecoder(CondDecoder):
    def __init__(self, args=None):
        super(GaussianCondDecoder, self).__init__(args)
        if args is None:
            args = {}
        if 'logVar' in args.keys():
            self.type_logVar = args['logVar']
        else:
            self.type_logVar = 'unit'

    def decode(self, z, conditions):
        raise NotImplementedError

    def sample(self, conditions, samples=1):
        raise NotImplementedError

    def estimate_log_posterior(self, target, reconstruction, reduction='mean', return_deviation=False, mode='gaussian'):
        dev = (target - reconstruction[0]).abs()
        if mode == 'gaussian':
            log_posterior = - (torch.exp(-reconstruction[1]) * dev.pow(2) + reconstruction[1] + np.log(2.*np.pi).astype(np.float32))
        elif mode == 'exponential':
            logLambda = -reconstruction[1]/2
            log_posterior = - torch.exp(logLambda) * dev + logLambda - np.log(2.).astype(np.float32)
        else:
            raise NotImplementedError('[ERROR] Requested posterior mode not implemented yet.')
        log_posterior = log_posterior.flatten(start_dim=1).sum(dim=-1) / 2
        reduced_posterior = log_posterior.mean() if reduction == 'mean' else log_posterior.sum()
        return reduced_posterior, dev if return_deviation else reduced_posterior

