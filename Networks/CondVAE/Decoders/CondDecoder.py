import torch
import torch.nn as nn


class CondDecoder(nn.Module):
    def __init__(self, args=None):
        super(CondDecoder, self).__init__()
        if args is None:
            args = {}
        if 'use_conditions' in args.keys():
            self.use_conditions = args['use_conditions']
        else:
            self.use_conditions = True

    def forward(self, z, conditions):
        if self.use_conditions:
            reconstruction = self.decode(z, conditions)
        else:
            reconstruction = self.decode(z, torch.zeros_like(conditions))
        return reconstruction

    def decode(self, z, conditions):
        raise NotImplementedError

    def sample(self, conditions, samples=1):
        raise NotImplementedError

    def estimate_log_posterior(self, target, reconstruction, reduction='mean', mode='gaussian'):
        raise NotImplementedError
