import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from .GaussianCondDecoder import GaussianCondDecoder
from Networks.Buildingblocks import CompressionNet, CompressionNetSmall, CompressionNetSmallSeq
from Networks.Buildingblocks import ProbExpansionNet, ProbExpansionNetSmall, ProbExpansionNetSmallSeq
from Networks.Buildingblocks import InputLR, InputLRSeq, InputHR
from Networks.Buildingblocks import UpresNet, UpresNetSmall, UpresNetSmallSeq
from Networks.Buildingblocks import ConvLayer
from Networks.Buildingblocks import UnitLogVar


class ProbUNet(nn.Module):
    def __init__(self, in_channels=64, out_channels=64, latent_channels=16,
                 compression = [1, 3, 2, 2], dropout= 0.25,
                 use_bn=True, use_cc=True, leaky_slope=0.05,
                 output_activation=None,
                 padding_mode='reflection'):

        super(ProbUNet, self).__init__()

        self.in_channels = in_channels
        self.out_channels = out_channels
        self.latent_channels = latent_channels

        depth_channels = self._get_channels(compression)

        self.use_cc = use_cc

        self.compression = CompressionNet(
            in_channels=in_channels,
            depth_channels=depth_channels,
            dropout=dropout, leaky_slope=leaky_slope,
            use_bn=use_bn, use_cc=use_cc, use_oa=True,
            padding_mode=padding_mode)

        self.expansion = ProbExpansionNet(
            in_channels=self.compression.out_channels(),
            latent_channels=self.latent_channels,
            out_channels=self.out_channels,
            depth_channels=depth_channels,
            dropout=dropout, leaky_slope=leaky_slope,
            use_bn=use_bn, use_cc=use_cc, use_oa=False,
            padding_mode=padding_mode
        )

        if output_activation is not None:
            self.output = output_activation
        else:
            self.output = nn.Identity()

    def forward(self, z, x):
        features = self.compression(x)
        z, features = self.reshape(z, features)
        features = self.expansion(z, features)
        return self.output(features)

    def reshape(self, z, features):
        sz = z.size()
        if sz[1] > 1:
            if self.use_cc:
                for i in range(len(features)):
                    features[i] = features[i].repeat_interleave(sz[1], dim=0)
            else:
                features = features.repeat_interleave(sz[1], dim=0)
        return z.view(sz[0] * sz[1], *sz[2:]), features

    def _get_channels(self, compression):
        assert isinstance(compression, (list, np.array)) and len(compression) == 4
        compression = np.cumprod(np.array(compression))
        dof = 60 * 36 * self.in_channels / compression
        pixels = 3 * 5 * np.array([9 * 4**2, 4**2, 4, 1])
        channels = dof / pixels
        return np.ceil(channels).astype(int)


class ProbUNetSmall(ProbUNet):
    def __init__(self, in_channels=64, out_channels=64, latent_channels=16,
                 compression = [1, 3, 2, 2], dropout= 0.25,
                 use_bn=True, use_cc=True, leaky_slope=0.05,
                 output_activation=None,
                 padding_mode='reflection'):

        super(ProbUNetSmall, self).__init__(
            in_channels=in_channels, out_channels=out_channels, latent_channels=latent_channels,
            compression=compression, dropout=dropout,
            use_bn=use_bn, use_cc=use_cc, leaky_slope=leaky_slope,
            output_activation=output_activation,
            padding_mode=padding_mode
        )

        depth_channels = self._get_channels(compression)

        self.compression = CompressionNetSmall(
            in_channels=in_channels,
            depth_channels=depth_channels,
            dropout=dropout, leaky_slope=leaky_slope,
            use_bn=use_bn, use_cc=use_cc, use_oa=True,
            padding_mode=padding_mode)

        self.expansion = ProbExpansionNetSmall(
            in_channels=self.compression.out_channels(),
            latent_channels=self.latent_channels,
            out_channels=self.out_channels,
            depth_channels=depth_channels,
            dropout=dropout, leaky_slope=leaky_slope,
            use_bn=use_bn, use_cc=use_cc, use_oa=False,
            padding_mode=padding_mode
        )


class ProbUNetSmallSeq(ProbUNet):
    def __init__(self, in_channels=64, out_channels=64, latent_channels=16,
                 compression = [1, 3, 2, 2], dropout= 0.25,
                 use_bn=True, use_cc=True, leaky_slope=0.05,
                 output_activation=None,
                 padding_mode='reflection'):

        super(ProbUNetSmallSeq, self).__init__(
            in_channels=in_channels, out_channels=out_channels, latent_channels=latent_channels,
            compression=compression, dropout=dropout,
            use_bn=use_bn, use_cc=use_cc, leaky_slope=leaky_slope,
            output_activation=output_activation,
            padding_mode=padding_mode
        )

        depth_channels = self._get_channels(compression)

        self.compression = CompressionNetSmallSeq(
            in_channels=in_channels,
            depth_channels=depth_channels,
            dropout=dropout, leaky_slope=leaky_slope,
            use_bn=use_bn, use_cc=use_cc, use_oa=True,
            padding_mode=padding_mode)

        self.expansion = ProbExpansionNetSmallSeq(
            in_channels=self.compression.out_channels(),
            latent_channels=self.latent_channels,
            out_channels=self.out_channels,
            depth_channels=depth_channels,
            dropout=dropout, leaky_slope=leaky_slope,
            use_bn=use_bn, use_cc=use_cc, use_oa=False,
            padding_mode=padding_mode
        )


class CD_UNet(GaussianCondDecoder):
    def __init__(self, data_channels=4, in_channels=64, out_channels=2, latent_channels=16,
                 compression = [1, 3, 2, 2],
                 use_bn=True, use_cc=True, leaky_slope=0.05, dropout=0.1, use_oa=False,
                 learn_residual=True,
                 padding_mode='reflection', logVar='unit'):
        super(CD_UNet, self).__init__()

        self.data_channels_lr = data_channels
        self.in_channels = in_channels
        self.latent_channels = latent_channels
        self.out_channels = out_channels

        self.use_cc = use_cc
        self.use_oa = use_oa
        self.learn_residual = learn_residual

        self.type_logVar = logVar

        self.inputLR = InputLR(
            data_channels=data_channels,in_channels=in_channels, out_channels=in_channels,
            dropout=dropout, leaky_slope=leaky_slope,
            use_bn=use_bn, use_oa=True,
            padding_mode=padding_mode)

        self.probUNet = ProbUNet(
            in_channels=self.in_channels, out_channels=self.in_channels, latent_channels=self.latent_channels,
            compression=compression, dropout=dropout,
            use_bn=use_bn, use_cc=self.use_cc, leaky_slope=0.05,
            output_activation=nn.LeakyReLU(negative_slope=leaky_slope, inplace=True)
        )

        self.upres = UpresNet(
            in_channels=self.in_channels, out_channels=self.in_channels,
            dropout=dropout, leaky_slope=leaky_slope,
            use_bn=use_bn, use_oa=True,
            padding_mode=padding_mode
        )

        self.output_mu = nn.Conv2d(
            in_channels=self.in_channels, out_channels=self.out_channels,
            kernel_size=(1, 1), stride=(1, 1), padding=(0, 0),
        )

        if self.type_logVar == 'diag':
            self.output_logVar = nn.Conv2d(
                in_channels=self.in_channels, out_channels=self.out_channels,
                kernel_size=(1, 1), stride=(1, 1), padding=(0, 0),
            )
        elif self.type_logVar == 'unit':
            self.output_logVar = UnitLogVar(out_channels=self.out_channels)
        else:
            raise NotImplementedError('Variance estimator <{}> not implemented yet.'.format(self.type_logVar))

    def decode(self, z, conditions):
        features = self.inputLR(conditions)
        features = self.probUNet(z, features)
        features = self.upres(features)
        output_mu = self.output_mu(features)
        if self.learn_residual:
            output_mu += F.interpolate(conditions[:,0:2,:,:], size=output_mu.size()[-2:], mode='bilinear')
        return output_mu, self.output_logVar(features)

    def sample(self, conditions, samples=1):
        sz = conditions.size()
        z = torch.randn(sz[0], samples, self.latent_channels, 3, 5, device=conditions.device)
        rec_mu, rec_logVar = self.decode(z, conditions)
        sz_rec = rec_mu.size()
        sz_out = (sz[0], samples) + sz_rec[1:]
        return rec_mu.view(*sz_out), rec_logVar.view(*sz_out), z


class CD_UNet_Small(CD_UNet):
    def __init__(self, data_channels=4, in_channels=64, out_channels=2, latent_channels=16,
                 compression = [1, 3, 2, 2],
                 use_bn=True, use_cc=True, leaky_slope=0.05, dropout=0.1, use_oa=False,
                 learn_residual=True,
                 padding_mode='reflection', logVar='unit'):
        super(CD_UNet_Small, self).__init__(
            data_channels=data_channels, in_channels=in_channels, out_channels=out_channels, latent_channels=latent_channels,
            compression=compression,
            use_bn=use_bn, use_cc=use_cc, leaky_slope=leaky_slope, dropout=dropout, use_oa=use_oa,
            learn_residual=learn_residual,
            padding_mode=padding_mode, logVar=logVar
        )

        self.probUNet = ProbUNetSmall(
            in_channels=self.in_channels, out_channels=self.in_channels, latent_channels=self.latent_channels,
            compression=compression, dropout=dropout,
            use_bn=use_bn, use_cc=self.use_cc, leaky_slope=0.05,
            output_activation=nn.LeakyReLU(negative_slope=leaky_slope, inplace=True)
        )

        self.upres = UpresNetSmall(
            in_channels=self.in_channels, out_channels=self.in_channels,
            dropout=dropout, leaky_slope=leaky_slope,
            use_bn=use_bn, use_oa=True,
            padding_mode=padding_mode
        )


class CD_UNet_SmallSeq(CD_UNet):
    def __init__(self, data_channels=4, in_channels=64, out_channels=2, latent_channels=16,
                 compression = [1, 3, 2, 2],
                 use_bn=True, use_cc=True, leaky_slope=0.05, dropout=0.1, use_oa=False,
                 learn_residual=True,
                 padding_mode='reflection', logVar='unit'):
        super(CD_UNet_SmallSeq, self).__init__(
            data_channels=data_channels, in_channels=in_channels, out_channels=out_channels, latent_channels=latent_channels,
            compression=compression,
            use_bn=use_bn, use_cc=use_cc, leaky_slope=leaky_slope, dropout=dropout, use_oa=use_oa,
            learn_residual=learn_residual,
            padding_mode=padding_mode, logVar=logVar
        )

        self.inputLR = InputLRSeq(
            data_channels=data_channels,in_channels=in_channels, out_channels=in_channels,
            dropout=dropout, leaky_slope=leaky_slope,
            use_bn=use_bn, use_oa=True,
            padding_mode=padding_mode)

        self.probUNet = ProbUNetSmallSeq(
            in_channels=self.in_channels, out_channels=self.in_channels, latent_channels=self.latent_channels,
            compression=compression, dropout=dropout,
            use_bn=use_bn, use_cc=self.use_cc, leaky_slope=0.05,
            output_activation=nn.LeakyReLU(negative_slope=leaky_slope, inplace=True)
        )

        self.upres = UpresNetSmallSeq(
            in_channels=self.in_channels, out_channels=self.in_channels,
            dropout=dropout, leaky_slope=leaky_slope,
            use_bn=use_bn, use_oa=True,
            padding_mode=padding_mode
        )


class CD_UNet_HR(CD_UNet):
    def __init__(self, data_channels_lr=4, data_channels_hr=4, in_channels=64, out_channels=2, latent_channels=16,
                 compression = [2, 3, 2, 2],
                 use_bn=True, use_cc=True, leaky_slope=0.05, dropout=0.1, use_oa=False,
                 padding_mode='reflection', learn_residual = True, logVar='unit'):

        super(CD_UNet_HR, self).__init__(
            data_channels=data_channels_lr, in_channels=in_channels, out_channels=out_channels, latent_channels=latent_channels,
            compression=compression,
            use_bn=use_bn, use_cc=use_cc, leaky_slope=leaky_slope, dropout=dropout, use_oa=use_oa,
            learn_residual=learn_residual,
            padding_mode=padding_mode, logVar=logVar
        )

        self.data_channels_hr = data_channels_hr

        self.inputHR = InputHR(
            data_channels=data_channels_hr, in_channels=in_channels, out_channels=in_channels,
            dropout=dropout, leaky_slope=leaky_slope,
            use_bn=use_bn, use_oa=True,
            padding_mode='reflection')

        self.probUNet = ProbUNet(
            in_channels = 2 * self.in_channels, out_channels=self.in_channels, latent_channels=self.latent_channels,
            compression=compression, dropout=dropout,
            use_bn=use_bn, use_cc=self.use_cc, leaky_slope=0.05,
            output_activation=nn.LeakyReLU(negative_slope=leaky_slope, inplace=True)
        )

    def decode(self, z, conditions):
        features = torch.cat(self.inputLR(conditions[0]), self.inputHR(conditions[1]))
        features = self.probUNet(z, features)
        features = self.upres(features)
        return self.output_mu(features), self.output_logVar(features)