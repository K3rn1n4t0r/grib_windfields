import torch
import torch.nn as nn
import numpy as np
from .GaussianCondDecoder import GaussianCondDecoder
from Networks.Buildingblocks import InputLR, InputHR, UpresNet, ConvLayer, UnitLogVar
from .CD_UNet import ProbUNet


class CD_UNet_lin(GaussianCondDecoder):
    def __init__(self, data_channels=4, in_channels=64, out_channels=2, latent_channels=64, latent_dim=256,
                 compression = [1, 3, 2, 2],
                 use_bn=True, use_cc=True, leaky_slope=0.05, dropout=0.1, use_oa=False,
                 padding_mode='reflection', logVar='unit'):
        super(CD_UNet_lin, self).__init__()

        self.data_channels = data_channels
        self.in_channels = in_channels
        self.latent_dim = latent_dim
        self.latent_channels = latent_channels
        self.out_channels = out_channels

        self.use_cc = use_cc
        self.use_oa = use_oa
        self.type_logVar = logVar

        self.inputLR = InputLR(
            data_channels=data_channels,in_channels=in_channels, out_channels=in_channels,
            dropout=dropout, leaky_slope=leaky_slope,
            use_bn=use_bn, use_oa=True,
            padding_mode='reflection')

        self.latent_input = nn.Sequential(
            nn.Linear(
                in_features=self.latent_dim,
                out_features=3 * 5 * self.latent_channels
            ),
            nn.LeakyReLU(negative_slope=leaky_slope, inplace=True)
        )

        self.probUNet = ProbUNet(
            in_channels=self.in_channels, out_channels=self.in_channels, latent_channels=self.latent_channels,
            compression=compression, dropout=dropout,
            use_bn=use_bn, use_cc=self.use_cc, leaky_slope=0.05,
            output_activation=nn.LeakyReLU(negative_slope=leaky_slope, inplace=True)
        )

        self.upres = UpresNet(
            in_channels=self.in_channels, out_channels=self.in_channels,
            dropout=dropout, leaky_slope=leaky_slope,
            use_bn=use_bn, use_oa=False,
            padding_mode=padding_mode
        )

        self.output_mu = ConvLayer(
            in_channels=self.in_channels, out_channels=self.out_channels,
            kernel_size=(1, 1), stride=(1, 1), padding=(0, 0),
            use_bn=use_bn, use_oa=use_oa, leaky_slope=leaky_slope, dropout=0.
        )

        if self.type_logVar == 'diag':
            self.output_logVar = ConvLayer(
                in_channels=self.in_channels, out_channels=self.out_channels,
                kernel_size=(1, 1), stride=(1, 1), padding=(0, 0),
                use_bn=use_bn, use_oa=use_oa, leaky_slope=leaky_slope, dropout=0.
            )
        elif self.type_logVar == 'unit':
            self.output_logVar = UnitLogVar(out_channels=self.out_channels)
        else:
            raise NotImplementedError('Variance estimator <{}> not implemented yet.'.format(self.type_logVar))

    def decode(self, z, conditions):
        features = self.inputLR(conditions)
        latent = self.latent_input(z).view(-1, 1, self.latent_channels, 3, 5)
        features = self.probUNet(latent, features)
        features = self.upres(features)
        return self.output_mu(features), self.output_logVar(features)


class CD_UNet_lin_HR(GaussianCondDecoder):
    def __init__(self, data_channels_lr=4, data_channels_hr=4, in_channels=64, out_channels=2, latent_channels=64, latent_dim=256,
                 compression = [2, 3, 2, 2],
                 use_bn=True, use_cc=True, leaky_slope=0.05, dropout=0.1, use_oa=False,
                 padding_mode='reflection'):

        super(CD_UNet_lin_HR, self).__init__()

        self.data_channels_lr = data_channels_lr
        self.data_channels_hr = data_channels_hr
        self.in_channels = in_channels
        self.latent_dim = latent_dim
        self.latent_channels = latent_channels
        self.out_channels = out_channels

        self.use_cc = use_cc
        self.use_oa = use_oa

        self.inputLR = InputLR(
            data_channels=data_channels_lr, in_channels=in_channels, out_channels=in_channels,
            dropout=dropout, leaky_slope=leaky_slope,
            use_bn=use_bn, use_oa=True,
            padding_mode='reflection')

        self.latent_input = nn.Sequential(
            nn.Linear(
                in_features=self.latent_dim,
                out_features=3 * 5 * self.latent_channels
            ),
            nn.LeakyReLU(negative_slope=leaky_slope, inplace=True)
        )
        self.inputHR = InputHR(
            data_channels=data_channels_hr, in_channels=in_channels, out_channels=in_channels,
            dropout=dropout, leaky_slope=leaky_slope,
            use_bn=use_bn, use_oa=True,
            padding_mode='reflection')

        self.probUNet = ProbUNet(
            in_channels = 2 * self.in_channels, out_channels=self.in_channels, latent_channels=self.latent_channels,
            compression=compression, dropout=dropout,
            use_bn=use_bn, use_cc=self.use_cc, leaky_slope=0.05,
            output_activation=nn.LeakyReLU(negative_slope=leaky_slope, inplace=True)
        )

        self.upres = UpresNet(
            in_channels=self.in_channels, out_channels=self.in_channels,
            dropout=dropout, leaky_slope=leaky_slope,
            use_bn=use_bn, use_oa=False,
            padding_mode=padding_mode
        )

        self.output_mu = ConvLayer(
            in_channels=self.in_channels, out_channels=self.out_channels,
            kernel_size=(1, 1), stride=(1, 1), padding=(0, 0),
            use_bn=use_bn, use_oa=use_oa, leaky_slope=leaky_slope, dropout=0.
        )

        if self.type_logVar == 'diag':
            self.output_logVar = ConvLayer(
                in_channels=self.in_channels, out_channels=self.out_channels,
                kernel_size=(1, 1), stride=(1, 1), padding=(0, 0),
                use_bn=use_bn, use_oa=use_oa, leaky_slope=leaky_slope, dropout=0.
            )
        elif self.type_logVar == 'unit':
            self.output_logVar = lambda features: torch.ones([1], device=features.device)
        else:
            raise NotImplementedError('Variance estimator <{}> not implemented yet.'.format(self.type_logVar))

    def decode(self, z, conditions):
        features = torch.cat(self.inputLR(conditions[0]), self.inputHR(conditions[1]))
        latent = self.latent_input(z).view(-1, 1, self.latent_channels, 3, 5)
        features = self.probUNet(latent, features)
        features = self.upres(features)
        return self.output_mu(features), self.output_logVar(features)