import numpy as np
import torch
import torch.nn as nn
from torch.utils.data import DataLoader
from time import time
import os
import sys
from tensorboardX import SummaryWriter
from .Encoders.CondEncoder import CondEncoder
from .Decoders.CondDecoder import CondDecoder
from .InformationMeasures import estimate_dKL_zx_z
from Utils.ProgressBar import ProgressBar


class CondVAE(nn.Module):
    def __init__(self, encoder: CondEncoder, decoder: CondDecoder, args: dict={}):
        super(CondVAE, self).__init__()

        self.encoder = encoder
        self.decoder = decoder

    def forward(self, target, conditions):
        mu, logVar, z = self.encoder(target, conditions, samples=1)
        reconstruction = self.decoder(z, conditions)
        return reconstruction, mu, logVar, z

    def estimate_elbo(self, target, conditions, eps=1, neg_elbo=True, return_deviation=False, reduction='mean', mode='gaussian'):
        reconstruction, mu, logVar, z = self.forward(target, conditions)
        dKL_zx_z = self.encoder.estimate_dKL_zx_z(mu, logVar, reduction=reduction)
        if return_deviation:
            log_posterior, dev = self.decoder.estimate_log_posterior(target, reconstruction, return_deviation=True, reduction=reduction, mode=mode)
        else:
            log_posterior = self.decoder.estimate_log_posterior(target, reconstruction,return_deviation=False, reduction=reduction, mode=mode)
        elbo = log_posterior.clone()
        if eps != 0:
            elbo -= eps * dKL_zx_z
        if return_deviation:
            return (- elbo) if neg_elbo else elbo, log_posterior, dKL_zx_z, reconstruction, dev
        else:
            return (- elbo) if neg_elbo else elbo, log_posterior, dKL_zx_z, reconstruction

    def estimate_information(self, target, conditions, samples=1, reduction='mean'):
        return self.encoder.estimate_information(target, conditions, samples, reduction)


    # def estimate_mi_x_z(self, loader, device='cpu', reduction='mean'):
    #     mi_x_z = []
    #     dKL_zx_z = []
    #     dKL_z_z = []
    #     for batch, data in enumerate(loader):
    #         target, conditions = self._transfer_data(data, device)
    #         mi_x_z_torch, dKL_zx_z_torch, dKL_z_z_torch = self._estimate_mi_x_z_batch(target, conditions, reduction=reduction)
    #         mi_x_z.append(mi_x_z_torch.item())
    #         dKL_zx_z.append(dKL_zx_z_torch.item())
    #         dKL_z_z.append(dKL_z_z_torch.item())
    #     mi_x_z = np.array(mi_x_z)
    #     dKL_zx_z = np.array(dKL_zx_z)
    #     dKL_z_z = np.array(dKL_z_z)
    #     if reduction == 'mean':
    #         return mi_x_z.mean(), dKL_zx_z.mean(), dKL_z_z.mean()
    #     else:
    #         return mi_x_z.sum(), dKL_zx_z.sum(), dKL_z_z.sum()
    #
    #
    #
    # def _transfer_data(self, data, device):
    #     target_cpu, *conditions_cpu = data
    #     target = target_cpu.to(device)
    #     if self.args['num_conditions'] == 1:
    #         conditions = conditions_cpu[0].to(device)
    #     else:
    #         conditions = [condition.to(device) for condition in conditions_cpu]
    #     return target, conditions