import torch
import numpy as np


def estimate_dKL_zx_z(mu, logVar, reduction='mean'):
    dKL_zx_z = torch.flatten(logVar.exp() + mu.pow(2) - logVar - 1, start_dim=1).sum(1) / 2
    return dKL_zx_z.mean() if reduction == 'mean' else dKL_zx_z.sum()


def estimate_dKL_z_z(z, mu, logVar, reduction='mean'):
    z_flat = z.flatten(start_dim=2)
    mu_flat = mu.flatten(start_dim=1)
    logVar_flat = logVar.flatten(start_dim=1)
    batch_size, samples, latent = z_flat.size()
    if samples > 1:
        z_flat = z_flat.reshape((batch_size * samples, 1, latent))
    dev = z_flat - mu_flat.unsqueeze(0)
    log_q_zx = -(dev.pow(2) / logVar_flat.exp().unsqueeze(0)).sum(dim=2) / 2
    log_q_zx = log_q_zx - (torch.sum(logVar_flat, axis=1) + latent * np.log(2 * np.pi)) / 2
    max_log_q_zx, _ = torch.max(log_q_zx, dim=1, keepdim=True)
    log_q_z = (log_q_zx - max_log_q_zx).exp().sum(dim=1).log() + max_log_q_zx - np.log(batch_size)
    log_p_z = -(z_flat.squeeze_(dim=1).pow(2).sum(dim=1) + latent * np.log(2 * np.pi)) / 2
    return (log_q_z - log_p_z).mean() if reduction == 'mean' else (log_q_z - log_p_z).sum()
