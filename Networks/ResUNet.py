import torch
import torch.nn as nn
from torch.nn import functional as F
import numpy as np
from itertools import product
from collections import ChainMap
from DataPreprocessing.DataScaler import DataScaler
from .CustomModules import *
from .BuildUtils import Resampling2D


class ResUNetParameterSetting:
    def __init__(self, feature_channels=32, kernel_size=3,
                 num_resblocks_per_stage=1, num_conv_layers_per_block=2,
                 dropout_rate=0.1):
        self.num_feature_channels = feature_channels
        self.kernel_size = kernel_size
        self.num_resblocks_per_stage = num_resblocks_per_stage
        self.num_conv_layers_per_block = num_conv_layers_per_block
        self.dropout_rate = dropout_rate
        self.use_dropout = dropout_rate > 0.0

    def update_config(self, cfg):
        cfg['network']['ResUNet'].update({
            "featureChannels": self.num_feature_channels,
            "kernelSize": self.kernel_size,
            "resblocksPerStage": self.num_resblocks_per_stage,
            "convLayersPerBlock": self.num_conv_layers_per_block,
            "dropoutRate": self.dropout_rate,
            "useDropout": self.use_dropout
        })


class EncodingResnetBlock(nn.Module):
    def __init__(self, num_input_channels, num_output_channels, downscaling_factor,
                 kernel_size=5, num_conv_layers_per_resnet=2, padding_mode='replication',
                 num_resnet_blocks=1,
                 use_batch_norm=True, leaky_slope=0.2, dropout_rate=0.0):
        super(EncodingResnetBlock, self).__init__()

        blocks = []

        if dropout_rate > 0.0:
            blocks += [nn.Dropout2d(dropout_rate)]

        blocks += [ResnetMultiBlock(num_input_channels, num_input_channels,
                                    num_resnet_blocks=num_resnet_blocks,
                                    num_conv_layers=num_conv_layers_per_resnet,
                                    kernel_size=kernel_size, use_batch_norm=use_batch_norm,
                                    leaky_slope=leaky_slope,
                                    padding_mode=padding_mode)]

        blocks += [ConvBlock(num_input_channels, num_output_channels, stride=downscaling_factor,
                             kernel_size=kernel_size, use_batch_norm=use_batch_norm,
                             leaky_slope=leaky_slope,
                             padding_mode=padding_mode)]

        self.model = nn.Sequential(*blocks)

    def forward(self, x):
        output = self.model(x)
        return output


class DecodingResnetBlock(nn.Module):
    def __init__(self, num_channels,
                 upscaling_factor, interpolation_mode='nearest',
                 kernel_size=5, num_conv_layers_per_resnet=2, padding_mode='replication',
                 num_resnet_blocks=1,
                 use_batch_norm=True, leaky_slope=0.2, dropout_rate=0.0):
        super(DecodingResnetBlock, self).__init__()
        self.upscaling_factor = upscaling_factor
        self.interpolation_mode = interpolation_mode

        blocks = []

        if dropout_rate > 0.0:
            blocks += [nn.Dropout2d(dropout_rate)]

        blocks += [ResnetMultiBlock(num_channels, num_channels,
                                    num_resnet_blocks=num_resnet_blocks,
                                    num_conv_layers=num_conv_layers_per_resnet,
                                    kernel_size=kernel_size, use_batch_norm=use_batch_norm,
                                    leaky_slope=leaky_slope,
                                    padding_mode=padding_mode)]

        self.model = nn.Sequential(*blocks)

    def forward(self, x):
        output = self.model(x)
        output = F.interpolate(output, scale_factor=self.upscaling_factor, mode=self.interpolation_mode)
        return output


class ResnetSkipConnectionBlock(nn.Module):
    def __init__(self, channels,
                 encode_decode_factor, inner_block=None, outermost=False,
                 kernel_size=5,
                 num_conv_layers_per_resnet=2, padding_mode='replication',
                 num_resnet_blocks=1,
                 use_batch_norm=True, leaky_slope=0.2, dropout_rate=0.0):
        super(ResnetSkipConnectionBlock, self).__init__()

        assert (isinstance(encode_decode_factor, (tuple, list)))

        out_channels = channels * 3 if outermost else channels * 2
        encoding_block = EncodingResnetBlock(channels, out_channels, downscaling_factor=encode_decode_factor,
                                             kernel_size=kernel_size,
                                             num_conv_layers_per_resnet=num_conv_layers_per_resnet,
                                             num_resnet_blocks=num_resnet_blocks, use_batch_norm=use_batch_norm,
                                             leaky_slope=leaky_slope, dropout_rate=dropout_rate,
                                             padding_mode=padding_mode)

        decoding_block = DecodingResnetBlock(out_channels, upscaling_factor=encode_decode_factor,
                                             kernel_size=kernel_size,
                                             num_conv_layers_per_resnet=num_conv_layers_per_resnet,
                                             num_resnet_blocks=num_resnet_blocks, use_batch_norm=use_batch_norm,
                                             leaky_slope=leaky_slope, dropout_rate=dropout_rate,
                                             padding_mode=padding_mode)

        if inner_block is None:
            blocks = [encoding_block, decoding_block]
        else:
            blocks = [encoding_block, inner_block, decoding_block]

        self.model = nn.Sequential(*blocks)

        out_block_channels = channels * 4 if outermost else channels * 3
        out_blocks = [ConvBlock(out_block_channels, channels,
                                kernel_size=kernel_size, use_batch_norm=use_batch_norm,
                                leaky_slope=leaky_slope,
                                padding_mode=padding_mode)]

        if outermost:
            out_blocks += [ResnetMultiBlock(channels, channels,
                                            num_resnet_blocks=num_resnet_blocks,
                                            num_conv_layers=num_conv_layers_per_resnet,
                                            kernel_size=kernel_size, use_batch_norm=use_batch_norm,
                                            leaky_slope=leaky_slope,
                                            padding_mode=padding_mode)]

        self.out_block = nn.Sequential(*out_blocks)

    def forward(self, x):
        output = self.model(x)
        output = torch.cat([x, output], dim=1)
        output = self.out_block(output)

        return output


class SupersamplingResnetBlock(nn.Module):
    def __init__(self, num_channels,
                 upscaling_factor, interpolation_mode='nearest',
                 kernel_size=5, num_conv_layers_per_resnet=2, padding_mode='replication',
                 num_resnet_blocks=1,
                 use_batch_norm=True, leaky_slope=0.2, dropout_rate=0.0):
        super(SupersamplingResnetBlock, self).__init__()

        self.upscaling_factor = upscaling_factor
        self.interpolation_mode = interpolation_mode

        blocks = [nn.Upsample(scale_factor=upscaling_factor, mode=interpolation_mode)]

        blocks += [ResnetMultiBlock(num_channels, num_channels,
                                    num_resnet_blocks=num_resnet_blocks,
                                    num_conv_layers=num_conv_layers_per_resnet,
                                    kernel_size=kernel_size, use_batch_norm=use_batch_norm,
                                    leaky_slope=leaky_slope,
                                    padding_mode=padding_mode)]

        self.model = nn.Sequential(*blocks)

    def forward(self, x):
        #output = F.interpolate(x, scale_factor=self.upscaling_factor, mode=self.interpolation_mode)
        output = self.model(x)
        return output


class ResUNet(nn.Module):
    def __init__(self, kernel_conv=3,
                 data_channels=4, in_channels=64, out_channels=2,
                 resblocks_per_stage=1, layers_per_block=2, dropout=0.1,
                 use_bn=True, use_cc=True, leaky_slope=0.2, use_oa=True,
                 input_scaler_uv=None, target_scaler_uv=None, padding_mode='replication'):
        super(ResUNet, self).__init__()

        self.data_channels = data_channels
        self.in_channels = in_channels
        self.out_channels = out_channels

        self.kernel_conv = kernel_conv
        self.padding_mode = padding_mode
        # self.padding_conv = self._get_padding()
        self.leaky_slope = leaky_slope

        self.resblocks_per_stage = resblocks_per_stage
        self.layers_per_block = layers_per_block

        if input_scaler_uv is not None:
            assert(isinstance(input_scaler_uv, DataScaler))
        if target_scaler_uv is not None:
            assert(isinstance(target_scaler_uv, DataScaler))
        self.input_scaler_uv = input_scaler_uv
        self.target_scaler_uv = target_scaler_uv

        self.use_bn = use_bn
        self.use_cc = use_cc
        self.use_oa = use_oa
        layers = []
        layers += [self._build_block_in()]

        dropout_rate = dropout if dropout > 0.0 else 0.0

        skipconnection_blocks = None
        skipconnection_blocks = ResnetSkipConnectionBlock(6 * self.in_channels, (2, 2), skipconnection_blocks,
                                                          kernel_size=self.kernel_conv,
                                                          num_conv_layers_per_resnet=self.layers_per_block,
                                                          num_resnet_blocks=self.resblocks_per_stage,
                                                          use_batch_norm=self.use_bn,
                                                          leaky_slope=self.leaky_slope,
                                                          dropout_rate=dropout_rate,
                                                          padding_mode=self.padding_mode)
        skipconnection_blocks = ResnetSkipConnectionBlock(3 * self.in_channels, (2, 2), skipconnection_blocks,
                                                          kernel_size=self.kernel_conv,
                                                          num_conv_layers_per_resnet=self.layers_per_block,
                                                          num_resnet_blocks=self.resblocks_per_stage,
                                                          use_batch_norm=self.use_bn,
                                                          leaky_slope=self.leaky_slope,
                                                          dropout_rate=dropout_rate,
                                                          padding_mode=self.padding_mode)
        skipconnection_blocks = ResnetSkipConnectionBlock(self.in_channels, (3, 3), skipconnection_blocks,
                                                          kernel_size=self.kernel_conv, outermost=True,
                                                          num_conv_layers_per_resnet=self.layers_per_block,
                                                          num_resnet_blocks=self.resblocks_per_stage,
                                                          use_batch_norm=self.use_bn,
                                                          leaky_slope=self.leaky_slope,
                                                          dropout_rate=dropout_rate,
                                                          padding_mode=self.padding_mode)

        layers += [skipconnection_blocks]

        layers += [SupersamplingResnetBlock(self.in_channels, (2, 3),
                                            kernel_size=self.kernel_conv,
                                            num_conv_layers_per_resnet=self.layers_per_block,
                                            num_resnet_blocks=self.resblocks_per_stage,
                                            use_batch_norm=self.use_bn,
                                            leaky_slope=self.leaky_slope,
                                            dropout_rate=dropout_rate,
                                            padding_mode=self.padding_mode)]
        layers += [SupersamplingResnetBlock(self.in_channels, (2, 1),
                                            kernel_size=self.kernel_conv,
                                            num_conv_layers_per_resnet=self.layers_per_block,
                                            num_resnet_blocks=self.resblocks_per_stage,
                                            use_batch_norm=self.use_bn,
                                            leaky_slope=self.leaky_slope,
                                            dropout_rate=dropout_rate,
                                            padding_mode=self.padding_mode)]

        layers += [self._build_block_out()]

        self.model = nn.Sequential(*layers)

    def forward(self, x, x_orig=None):
        features = self.model(x)

        if x_orig is None:
            return self._interpolate_add_block(x, features)
        else:
            return self._interpolate_add_block(x_orig, features)

    def _interpolate_add_block(self, input_lr, output):
        # transform low-res input to original domain [uv_min, uv_max]
        if self.input_scaler_uv is not None:
            input_lr = self.input_scaler_uv.transform_back(input_lr[:, :self.out_channels])
        # upsample low-res input to target shape
        input_lr_to_hr = F.interpolate(input_lr[:, :self.out_channels],
                                       scale_factor=(4, 3), mode='bicubic')

        # transform output to original target value domain [uv_min, uv_max]
        # if self.target_scaler_uv is not None:
        #     output = self.target_scaler_uv.transform_back(output[:, :self.out_channels])
        # add transformed network values to upsampled low-res input
        output = input_lr_to_hr + output
        # transform back to target domain, in example [-1, 1] or keep original domain [uv_min, uv_max]
        if self.target_scaler_uv is not None:
            return self.target_scaler_uv.transform(output[:, :self.out_channels])
        else:
            return output[:, : self.out_channels]

    def _build_block_in(self):
        block = ConvBlock(self.data_channels, self.in_channels, kernel_size=self.kernel_conv,
                          use_batch_norm=self.use_bn, leaky_slope=self.leaky_slope,
                          padding_mode=self.padding_mode)
        return block

    def _build_block_out(self):
        return nn.Conv2d(self.in_channels, self.out_channels, (1, 1))

    @staticmethod
    def create_parameter_pool():
        feature_channels = [64]  # 2 ** np.arange(5, 7, 1)
        kernel_sizes = np.arange(3, 6, 2)  # [3]  # np.arange(1, 8, 2)
        resblocks_per_stage = [1, 2, 3]  # np.arange(1, 4, 1)
        conv_layers_per_block = [1, 2, 3]  # np.arange(1, 4, 1)
        dropout_rate = np.arange(0.0, 0.5, 0.1)

        param_combos = list(product(feature_channels, kernel_sizes, resblocks_per_stage,
                                    conv_layers_per_block, dropout_rate))
        print("[INFO]: Number of learnable parameters in network model {}: {}".format(ResUNet.__name__, len(param_combos)))
        return [ResUNetParameterSetting(*setting) for setting in param_combos]
