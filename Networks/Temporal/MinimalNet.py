import torch.nn as nn
# from ..Buildingblocks import ConvLayer, CompressionNetSmall, ExpansionNetSmall


class MinimalNet(nn.Module):
    def __init__(self, data_channels=4, out_channels=2, kernel_size = 5, dilation=1):
        super(MinimalNet, self).__init__()
        if isinstance(kernel_size, int):
            kernel_size = (kernel_size, kernel_size)
        if isinstance(dilation, int):
            dilation = (dilation, dilation)
        padding = (dilation[0] * (kernel_size[0]//2), dilation[1] * (kernel_size[1]//2))

        self.conv = nn.Conv2d(data_channels, out_channels, kernel_size=kernel_size, padding=padding, dilation=dilation)

    def forward(self, x):
        return self.conv(x)

        # self.net = nn.Sequential(
        #     ConvLayer(
        #         in_channels=data_channels, out_channels=in_channels,
        #         kernel_size=(5, 5), stride=(1, 1), padding=(2, 2), padding_mode=padding_mode,
        #         use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
        #     ),
        #     ConvLayer(
        #         in_channels=in_channels, out_channels=out_channels,
        #         kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
        #         use_bn=False, use_oa=use_oa, leaky_slope=leaky_slope, dropout=0
        #     )
        # )
    #
    #     self.net = nn.Sequential(
    #         nn.ReplicationPad2d((1, 1, 1, 1)),
    #         nn.Conv2d(in_channels=data_channels, out_channels=in_channels, kernel_size=(3, 3),
    #                   stride=(1, 1), padding=(0, 0)),
    #         nn.BatchNorm2d(in_channels),
    #         nn.LeakyReLU(leaky_slope),
    #         nn.Dropout2d(dropout),
    #         nn.ReplicationPad2d((1, 1, 1, 1)),
    #         nn.Conv2d(in_channels=in_channels, out_channels=in_channels, kernel_size=(3, 3),
    #                   stride=(1, 1), padding=(0, 0)),
    #         nn.BatchNorm2d(in_channels),
    #         nn.LeakyReLU(leaky_slope),
    #         nn.Dropout2d(dropout),
    #         nn.ReplicationPad2d((1, 1, 1, 1)),
    #         nn.Conv2d(in_channels=in_channels, out_channels=in_channels, kernel_size=(3, 3),
    #                   stride=(1, 1), padding=(0, 0)),
    #         nn.BatchNorm2d(in_channels),
    #         nn.LeakyReLU(leaky_slope),
    #         nn.Dropout2d(dropout),
    #         nn.ReplicationPad2d((1, 1, 1, 1)),
    #         nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=(3, 3),
    #                   stride=(1, 1), padding=(0, 0)),
    #     )
    #
    # def forward(self, x):
    #     if self.learn_residual:
    #         return x[:, 0:self.out_channels, :, :] + self.net(x)
    #     else:
    #         return self.net(x)

        # self.input = nn.Sequential(
        #     nn.ReplicationPad2d((1, 1, 1, 1)),
        #     nn.Conv2d(in_channels=data_channels, out_channels=in_channels, kernel_size=(3, 3),
        #               stride=(1, 1), padding=(0, 0)),
        #     nn.BatchNorm2d(in_channels),
        #     nn.LeakyReLU(leaky_slope),
        #     nn.Dropout2d(dropout),
        # )
        # self.blocks = nn.ModuleList([
        #     nn.Sequential(
        #         nn.ReplicationPad2d((1, 1, 1, 1)),
        #         nn.Conv2d(in_channels=in_channels, out_channels=2*in_channels, kernel_size=(3, 3),
        #                   stride=(2, 2), padding=(0, 0)),
        #         nn.BatchNorm2d(2*in_channels),
        #         nn.LeakyReLU(leaky_slope),
        #         nn.Dropout2d(dropout),
        #         nn.ReplicationPad2d((1, 1, 1, 1)),
        #         nn.Conv2d(in_channels=2*in_channels, out_channels=in_channels, kernel_size=(3, 3),
        #                   stride = (1, 1), padding = (0, 0)),
        #         nn.BatchNorm2d(in_channels),
        #         nn.UpsamplingNearest2d(scale_factor=2)
        #     )
        #     for i in range(4)
        # ])
        # self.output = nn.Sequential(
        #     nn.LeakyReLU(leaky_slope),
        #     nn.Dropout2d(dropout),
        #     nn.ReplicationPad2d((1, 1, 1, 1)),
        #     nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=(3, 3),
        #               stride=(1, 1), padding=(0, 0)),
        # )

    # def forward(self, x):
    #     features = self.input(x)
    #     for block in self.blocks:
    #         features = features + block(features)
    #     if self.learn_residual:
    #         return x[:, 0:self.out_channels, :, :] + self.output(features)
    #     else:
    #         return self.output(features)