import torch
import torch.nn as nn

class RecursiveNet(nn.Module):
    def __init__(self, data_channels=2, memory_channels = 16, out_channels=2, kernel_size=5, dilation = 2, iterations=2):
        super(RecursiveNet, self).__init__()
        self.data_channels = data_channels
        self.memory_channels = memory_channels
        self.out_channels = out_channels
        if isinstance(kernel_size, tuple):
            assert len(kernel_size) == 2, '[ERROR] <kernel_size> must be tuple of length 2 or int.'
            self.kernel_size = kernel_size
        else:
            assert isinstance(kernel_size, int), '[ERROR] <kernel_size> must be tuple of length 2 or int.'
            self.kernel_size = (kernel_size, kernel_size)
        if isinstance(dilation, tuple):
            assert len(kernel_size) == 2, '[ERROR] <dilation> must be tuple of length 2 or int.'
            self.dilation = dilation
        else:
            assert isinstance(dilation, int), '[ERROR] <dilation> must be tuple of length 2 or int.'
            self.dilation = (dilation, dilation)
        padding_size = (
            (self.kernel_size[1] // 2) * self.dilation[1], (self.kernel_size[1] // 2) * self.dilation[1],
            (self.kernel_size[0] // 2) * self.dilation[0], (self.kernel_size[0] // 2) * self.dilation[0]
        )
        self.padding = nn.ReflectionPad2d(padding_size)
        self.iterations = iterations

        self.initialize_memory = nn.Conv2d(
            data_channels, memory_channels,
            kernel_size=self.kernel_size, dilation=self.dilation,
            padding=0
        )
        self.read_state_memory = nn.Conv2d(
            data_channels, memory_channels,
            kernel_size=self.kernel_size, dilation=self.dilation,
            padding=0, bias=False
        )
        self.read_state_gates = nn.Conv2d(
            data_channels, memory_channels,
            kernel_size=self.kernel_size, dilation=self.dilation,
            padding=0, bias=False
        )
        self.read_memory_memory = nn.Conv2d(
            memory_channels, memory_channels,
            kernel_size=self.kernel_size, dilation=self.dilation,
            padding=0
        )
        self.read_memory_gates = nn.Conv2d(
            memory_channels, memory_channels,
            kernel_size=self.kernel_size, dilation=self.dilation,
            padding=0
        )
        self.update_state = nn.Conv2d(
            memory_channels, out_channels,
            kernel_size=self.kernel_size, dilation=self.dilation,
            padding=0
        )

    def forward(self, x):
        if not isinstance(x, tuple):
            x = (x,)
        memory = torch.tanh(self.initialize_memory(self.padding(x[0])))
        for t in range(len(x)):
            state_memory = self.read_state_memory(self.padding(x[t]))
            state_gates  = self.read_state_gates(self.padding(x[t]))
            for i in range(self.iterations):
                memory_padded = self.padding(memory)
                memory_new = torch.tanh(
                    state_memory + self.read_memory_memory(memory_padded)
                )
                gates = torch.sigmoid(
                    state_gates + self.read_memory_gates(memory_padded)
                )
                memory = gates * memory + (1. - gates) * memory_new
        output = x[-1][:,:self.out_channels] + self.update_state(self.padding(self.arcTanh(memory)))
        return output

    @staticmethod
    def arcTanh(x):
        return torch.log1p(x) - torch.log1p(-x)
