import torch.nn as nn
from ..Buildingblocks import ConvLayer, CompressionNetSmall, ExpansionNetSmall


class UpDownNet_Temporal(nn.Module):
    def __init__(self, data_channels=4, in_channels=64, out_channels=2,
                 use_bn=True, leaky_slope=0.05, dropout=0.1, use_oa=False,
                 learn_residual=True,
                 padding_mode='reflection', num_blocks=4):
        super(UpDownNet_Temporal, self).__init__()

        self.learn_residual = learn_residual
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.num_blocks = num_blocks

        # self.net = nn.Sequential(
        #     ConvLayer(
        #         in_channels=data_channels, out_channels=in_channels,
        #         kernel_size=(5, 5), stride=(1, 1), padding=(2, 2), padding_mode=padding_mode,
        #         use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
        #     ),
        #     ConvLayer(
        #         in_channels=in_channels, out_channels=out_channels,
        #         kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
        #         use_bn=False, use_oa=use_oa, leaky_slope=leaky_slope, dropout=0
        #     )
        # )
    #
    #     self.net = nn.Sequential(
    #         nn.ReplicationPad2d((1, 1, 1, 1)),
    #         nn.Conv2d(in_channels=data_channels, out_channels=in_channels, kernel_size=(3, 3),
    #                   stride=(1, 1), padding=(0, 0)),
    #         nn.BatchNorm2d(in_channels),
    #         nn.LeakyReLU(leaky_slope),
    #         nn.Dropout2d(dropout),
    #         nn.ReplicationPad2d((1, 1, 1, 1)),
    #         nn.Conv2d(in_channels=in_channels, out_channels=in_channels, kernel_size=(3, 3),
    #                   stride=(1, 1), padding=(0, 0)),
    #         nn.BatchNorm2d(in_channels),
    #         nn.LeakyReLU(leaky_slope),
    #         nn.Dropout2d(dropout),
    #         nn.ReplicationPad2d((1, 1, 1, 1)),
    #         nn.Conv2d(in_channels=in_channels, out_channels=in_channels, kernel_size=(3, 3),
    #                   stride=(1, 1), padding=(0, 0)),
    #         nn.BatchNorm2d(in_channels),
    #         nn.LeakyReLU(leaky_slope),
    #         nn.Dropout2d(dropout),
    #         nn.ReplicationPad2d((1, 1, 1, 1)),
    #         nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=(3, 3),
    #                   stride=(1, 1), padding=(0, 0)),
    #     )
    #
    # def forward(self, x):
    #     if self.learn_residual:
    #         return x[:, 0:self.out_channels, :, :] + self.net(x)
    #     else:
    #         return self.net(x)

        self.input = nn.Sequential(
            nn.ReplicationPad2d((1, 1, 1, 1)),
            nn.Conv2d(in_channels=data_channels, out_channels=in_channels, kernel_size=(3, 3),
                      stride=(1, 1), padding=(0, 0)),
            nn.BatchNorm2d(in_channels),
            nn.LeakyReLU(leaky_slope),
            nn.Dropout2d(dropout),
        )
        self.downBlocks = nn.ModuleList([
            nn.Sequential(
                nn.ReplicationPad2d((1, 1, 1, 1)),
                nn.Conv2d(in_channels=in_channels, out_channels=2*in_channels, kernel_size=(3, 3),
                          stride=(2, 2), padding=(0, 0)),
                nn.BatchNorm2d(2*in_channels),
                nn.LeakyReLU(leaky_slope),
                nn.Dropout2d(dropout),
            )
            for i in range(self.num_blocks)
        ])
        self.upBlocks = nn.ModuleList([
            nn.Sequential(
                nn.ReplicationPad2d((1, 1, 1, 1)),
                nn.Conv2d(in_channels=2*in_channels, out_channels=in_channels, kernel_size=(3, 3),
                          stride = (1, 1), padding = (0, 0)),
                nn.BatchNorm2d(in_channels),
                nn.UpsamplingNearest2d(scale_factor=2)
            )
            for i in range(num_blocks)
        ])
        self.output = nn.Sequential(
            nn.LeakyReLU(leaky_slope),
            nn.Dropout2d(dropout),
            nn.ReplicationPad2d((1, 1, 1, 1)),
            nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=(3, 3),
                      stride=(1, 1), padding=(0, 0)),
        )

    def forward(self, x):
        features_up = self.input(x)
        for i in range(self.num_blocks):
            if i == 0:
                features_down = self.downBlocks[i](features_up)
            else:
                features_down = features_down + self.downBlocks[i](features_up)
            features_up = features_up + self.upBlocks[i](features_down)
        if self.learn_residual:
            return x[:, 0:self.out_channels, :, :] + self.output(features_up)
        else:
            return self.output(features_up)