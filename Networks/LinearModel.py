import numpy as np
import torch
import torch.nn as nn
from Utils import depth_to_space


class LinearModel(nn.Module):
    def __init__(self, data_channels=4, in_channels=64, out_channels=2,
                 kernel_lres=5, kernel_hres=5, dilation_lres=1, dilation_hres=1, num_layers_lres=1, num_layers_hres=1,
                 padding_mode='reflection', scale_factor=(4,3)):
        super(LinearModel, self).__init__()
        if isinstance(kernel_lres, int):
            kernel_lres = (kernel_lres, kernel_lres)
        if isinstance(kernel_hres, int):
            kernel_hres = (kernel_hres, kernel_hres)
        if isinstance(dilation_lres, int):
            dilation_lres = (dilation_lres, dilation_lres)
        if isinstance(dilation_hres, int):
            dilation_hres = (dilation_hres, dilation_hres)
        self.data_channels = data_channels
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.kernel_lres = kernel_lres
        self.kernel_hres = kernel_hres
        self.dilation_lres = dilation_lres
        self.dilation_hres = dilation_hres
        self.num_layers_lres = num_layers_lres
        self.num_layers_hres = num_layers_hres
        self.padding_mode = padding_mode
        self.scale_factor = scale_factor
        self._build_model()

    def _build_model(self):
        mult = np.prod(self.scale_factor)
        layers_lres = [self._padded_conv(self.data_channels, mult * self.in_channels, self.kernel_lres, self.dilation_lres)]
        layers_lres += [
            self._padded_conv(mult * self.in_channels, mult * self.in_channels, self.kernel_lres, self.dilation_lres)
            for _ in range(self.num_layers_lres - 1)
        ]
        self.conv_lres = nn.Sequential(*layers_lres) if len(layers_lres) > 1 else layers_lres[0]
        layers_hres = [
            self._padded_conv(self.in_channels, self.in_channels, self.kernel_hres, self.dilation_hres)
            for _ in range(self.num_layers_hres - 1)
        ]
        layers_hres += [self._padded_conv(self.in_channels, self.out_channels, self.kernel_hres, self.dilation_hres)]
        self.conv_hres = nn.Sequential(*layers_hres) if len(layers_hres) > 1 else layers_hres[0]

    def forward(self, x):
        return self.conv_hres(depth_to_space(self.conv_lres(x), self.scale_factor))

    def _padded_conv(self, in_channels, out_channels, kernel_size, dilation, bias=True):
        padding = (dilation[0] * (kernel_size[0] // 2), dilation[1] * (kernel_size[1] // 2))
        layers = []
        if not (padding == 0 or padding == (0, 0)):
            if isinstance(padding, tuple) and len(padding) == 2:
                padding = (padding[1], padding[1], padding[0], padding[0])
            if self.padding_mode == 'reflection':
                layers.append(nn.ReflectionPad2d(padding))
            elif self.padding_mode == 'replication':
                layers.append(nn.ReplicationPad2d(padding))
            elif self.padding_mode == 'zero':
                layers.append(nn.ZeroPad2d(padding))
            else:
                raise NotImplementedError('Padding mode <{}> not implemented'.format(self.padding_mode))
        layers.append(nn.Conv2d(in_channels, out_channels, kernel_size, dilation=dilation, padding=0, bias=bias))
        return nn.Sequential(*layers) if len(layers) > 1 else layers[0]


class LinearModelHROro(LinearModel):
    def __init__(self, data_channels=4, in_channels=64, out_channels=2, hres_channels=2,
                 kernel_lres=5, kernel_hres=5, dilation_lres=1, dilation_hres=1, num_layers_lres=1, num_layers_hres=1,
                 padding_mode='reflection', scale_factor=(4,3)):
        super(LinearModelHROro, self).__init__(data_channels, in_channels, out_channels,
                 kernel_lres, kernel_hres, dilation_lres, dilation_hres, num_layers_lres, num_layers_hres,
                 padding_mode, scale_factor)
        self.hres_channels = hres_channels
        self._build_hres()

    def _build_hres(self):
        if self.num_layers_hres == 1:
            layers_hres = [self._padded_conv(self.in_channels + self.hres_channels, self.out_channels, self.kernel_hres, self.dilation_hres)]
        else:
            layers_hres = [self._padded_conv(self.in_channels + self.hres_channels, self.in_channels, self.kernel_hres, self.dilation_hres)]
            layers_hres += [
                self._padded_conv(self.in_channels, self.in_channels, self.kernel_hres, self.dilation_hres)
                for _ in range(self.num_layers_hres - 2)
            ]
            layers_hres += [self._padded_conv(self.in_channels, self.out_channels, self.kernel_hres, self.dilation_hres)]
        self.conv_hres = nn.Sequential(*layers_hres) if len(layers_hres) > 1 else layers_hres[0]

    def forward(self, x, hres):
        return self.conv_hres(torch.cat((depth_to_space(self.conv_lres(x), self.scale_factor), hres), dim=1))