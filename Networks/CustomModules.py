import torch
import torch.nn as nn
import torch.nn.functional as F


class ConvBlock(nn.Module):
    def __init__(self, num_input_channels, num_output_channels,
                 padding='keep_dimensions', padding_mode='replication',
                 stride=(1, 1),  # can be used for downscaling
                 kernel_size=5,
                 use_batch_norm=True, leaky_slope=0.2, dropout_rate=0.0):
        super(ConvBlock, self).__init__()
        assert(isinstance(stride, (tuple, list)))

        if padding == 'keep_dimensions':
            # compute padding to keep same dimension
            kernel_size_half = kernel_size // 2
            padding = kernel_size_half
        else:
            assert(isinstance(padding, (int, tuple, list)))

        conv_block = []

        p = padding

        if not padding == 0 or padding == (0, 0):
            if isinstance(padding, tuple) and len(padding) == 2:
                # handle asymmetric case
                padding = (padding[1], padding[1], padding[0], padding[0])

            p = 0
            if padding_mode == 'reflection':
                conv_block += [nn.ReflectionPad2d(padding)]
            elif padding_mode == 'replication':
                conv_block += [nn.ReplicationPad2d(padding)]
            elif padding_mode == 'zero':
                # conv_block += [nn.ZeroPad2d(padding)]
                p = padding
            else:
                raise NotImplementedError('Padding mode <{}> not implemented'.format(padding_mode))

        conv_block += [nn.Conv2d(num_input_channels, num_output_channels,
                                 kernel_size=kernel_size, stride=stride, padding=p)]
        # Batch normalization
        if use_batch_norm:
            conv_block += [nn.BatchNorm2d(num_output_channels)]
        # LeakyReLU
        if leaky_slope > 0.0:
            conv_block += [nn.LeakyReLU(leaky_slope, inplace=True)]
        # Dropout layer
        if dropout_rate > 0.0:
            conv_block += [nn.Dropout2d(dropout_rate, inplace=False)]

        self.conv_block = nn.Sequential(*conv_block)

    def forward(self, x):
        output = self.conv_block(x)
        return output


class ResnetBlock(nn.Module):
    def __init__(self, num_input_channels, num_output_channels, num_conv_layers=2,
                 kernel_size=5,
                 use_batch_norm=True, leaky_slope=0.2, padding_mode='replication'):
        super(ResnetBlock, self).__init__()

        leaky_negative_slope = leaky_slope

        block = []
        for i in range(num_conv_layers):
            if i >= num_conv_layers - 1:
                leaky_negative_slope = 0.0
            block += [ConvBlock(num_input_channels, num_output_channels,
                                kernel_size=kernel_size, padding_mode=padding_mode,
                                use_batch_norm=use_batch_norm, leaky_slope=leaky_negative_slope)]

        self.model = nn.Sequential(*block)

    def forward(self, x):
        output = x + self.model(x)
        return output


class ResnetMultiBlock(nn.Module):
    def __init__(self, num_input_channels, num_output_channels, num_resnet_blocks=1,
                 num_conv_layers=2, kernel_size=5, padding_mode='replication',
                 use_batch_norm=True, leaky_slope=0.2):
        super(ResnetMultiBlock, self).__init__()

        modules = []

        for i in range(num_resnet_blocks):
            modules += [ResnetBlock(num_input_channels, num_output_channels, num_conv_layers,
                                    kernel_size, use_batch_norm, leaky_slope,
                                    padding_mode=padding_mode)]

        self.model = nn.ModuleList(modules)

    def forward(self, x):
        output = x
        for module in self.model:
            output = module(output)

        return output


class MaxPoolBlock(nn.Module):
    def __init__(self, num_channels, kernel_size=3, leaky_slope=0.2, use_batch_norm=True):
        super(MaxPoolBlock, self).__init__()

        # compute padding to keep same dimension
        kernel_size_half = kernel_size // 2
        kernel = (kernel_size, kernel_size)
        padding = (kernel_size_half, kernel_size_half)

        blocks = []
        blocks += [nn.MaxPool2d(kernel_size=kernel, stride=(1, 1), padding=padding)]
        if use_batch_norm:
            blocks += [nn.BatchNorm2d(num_channels)]
        if leaky_slope > 0.0:
            blocks += [nn.LeakyReLU(leaky_slope, inplace=True)]

        self.model = nn.Sequential(*blocks)

    def forward(self, x):
        output = self.model(x)
        return output


class InceptionBlock(nn.Module):
    def __init__(self, num_input_channels, num_output_channels, leaky_slope=0.2, use_batch_norm=True):
        super(InceptionBlock, self).__init__()

        # https://www.analyticsvidhya.com/blog/2018/10/understanding-inception-network-from-scratch/
        # compute the number of channels for each conv layer --> number of channels should sum up
        # to num_output_channels after concatenation
        num_block_output_channels = num_output_channels // 4

        block_1x1 = [ConvBlock(num_input_channels, num_block_output_channels, kernel_size=1,
                               leaky_slope=leaky_slope, use_batch_norm=use_batch_norm)]
        num_output_channels -= num_block_output_channels

        block_3x3 = [ConvBlock(num_input_channels, num_block_output_channels, kernel_size=1,
                               leaky_slope=leaky_slope, use_batch_norm=use_batch_norm)]
        block_3x3 += [ConvBlock(num_block_output_channels, num_block_output_channels, kernel_size=3,
                                leaky_slope=leaky_slope, use_batch_norm=use_batch_norm)]
        num_output_channels -= num_block_output_channels

        block_5x5 = [ConvBlock(num_input_channels, num_block_output_channels, kernel_size=1,
                               leaky_slope=leaky_slope, use_batch_norm=use_batch_norm)]
        block_5x5 += [ConvBlock(num_block_output_channels, num_block_output_channels, kernel_size=5,
                                leaky_slope=leaky_slope, use_batch_norm=use_batch_norm)]
        num_output_channels -= num_block_output_channels

        # (N, C, H, W) -> (N, C, H_out, W_out) for max pooling
        block_max_pool = [MaxPoolBlock(num_input_channels, kernel_size=3, leaky_slope=leaky_slope,
                                       use_batch_norm=use_batch_norm)]
        block_max_pool += [ConvBlock(num_input_channels, num_output_channels, kernel_size=1,
                                     leaky_slope=leaky_slope, use_batch_norm=use_batch_norm)]

        self.model = nn.ModuleList([nn.Sequential(*block_1x1),
                                    nn.Sequential(*block_3x3),
                                    nn.Sequential(*block_5x5),
                                    nn.Sequential(*block_max_pool)])

    def forward(self, x):
        outputs = []
        for module in self.model:
            outputs += [module(x)]

        output = torch.cat(outputs, dim=1)
        return output
