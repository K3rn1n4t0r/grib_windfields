import torch
import torch.nn as nn
from Networks.HROrographyNet import LRHRInputBlock


class DownscalingNet(nn.Module):
    def __init__(self, input_modules, net_module, super_res_module):
        super(DownscalingNet, self).__init__()

        module_names = []
        # prepare modules (Input, Network, SuperRes)
        self.input_module = None
        self._prepare_input_module(input_modules)

        self.net_module = net_module
        self.super_res_module = super_res_module

        if self.input_module is not None:
            module_names += [self.input_module.__class__.__name__]
        module_names += [self.net_module.__class__.__name__]
        if self.super_res_module is not None:
            module_names += [self.super_res_module.__class__.__name__]

        self.name = str.join('_', module_names)

    def _prepare_input_module(self, input_modules):
        if input_modules is not None:
            if isinstance(input_modules, (list, tuple)):
                if len(input_modules) > 2:
                    raise NotImplementedError()

                self.input_module = LRHRInputBlock(input_modules[0], input_modules[1])
            else:
                self.input_module = input_modules

    def forward(self, *x):
        output = x[0]
        # 1) Run input processing blocks
        if self.input_module is not None:
            if isinstance(self.input_module, LRHRInputBlock):
                if len(x) != 2:
                    raise AttributeError()

                output = self.input_module(x[0], x[1])
            else:
                output = self.input_module(x[0])

        # 2) Run main net
        output = self.net_module(output)

        # 3) Run super resolution blocks
        if self.super_res_module is not None:
            output = self.super_res_module(output)

        return output

