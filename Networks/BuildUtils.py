from .Buildingblocks import Resampling2D
import torch.nn as nn
from .CustomModules import ConvBlock


def build_conv_block(name, num_input_channels, num_output_channels,
                     kernel=(5, 5), stride=(1, 1), padding=(1, 1), norm=True, block=None,
                     leaky_slope=0.2, dropout_rate=0.0):
    if block is None:
        block = nn.Sequential()

    block.add_module('{}_{}'.format(name, 'Conv2D'), nn.Conv2d(num_input_channels, num_output_channels,
                                                 kernel_size=kernel, stride=stride,
                                                 padding=padding, bias=True))
    if norm:
        block.add_module('{}_{}'.format(name, 'BatchNorm2D'), nn.BatchNorm2d(num_output_channels))
    block.add_module('{}_{}'.format(name, 'LeakyReLu'), nn.LeakyReLU(leaky_slope, inplace=False))
    if dropout_rate > 0.0:
        block.add_module('{}_{}'.format(name, 'Dropout2D'), nn.Dropout2d(dropout_rate, inplace=False))

    return block


def build_encoding_block(name, num_input_channels, num_output_channels,
                         downsampling_factor_2d, kernel_size=3,
                         norm=True, num_conv_layers=0, leaky_slope=0.2, dropout_rate=0.0):
    kernel_size_half = kernel_size // 2
    padding = (kernel_size_half, kernel_size_half)
    kernel = (kernel_size, kernel_size)
    stride = downsampling_factor_2d

    block = nn.Sequential()
    block.add_module('{}_{}'.format(name, 'Conv2D'), nn.Conv2d(num_input_channels,
                                                               num_output_channels,
                                                               kernel_size=kernel, stride=stride,
                                                               padding=padding, bias=True))
    if norm:
        block.add_module('{}_{}'.format(name, 'BatchNorm2D'), nn.BatchNorm2d(num_output_channels))
    block.add_module('{}_{}'.format(name, 'LeakyReLu'), nn.LeakyReLU(leaky_slope, inplace=False))
    if dropout_rate > 0.0:
        block.add_module('{}_{}'.format(name, 'Dropout2D'), nn.Dropout2d(dropout_rate, inplace=False))

    for i in range(num_conv_layers):
        build_conv_block('{}_conv_{}'.format(name, i), num_output_channels, num_output_channels,
                         kernel=kernel, padding=padding, block=block, leaky_slope=leaky_slope,
                         norm=norm, dropout_rate=dropout_rate)

    return block


def build_decoding_block(name, num_input_channels, num_output_channels,
                         kernel_size=3, upsampling_factor_2d=(2, 2), norm=True, num_conv_layers=0,
                         leaky_slope=0.2, dropout_rate=0.0):
    kernel_size_half = kernel_size // 2
    padding = (kernel_size_half, kernel_size_half)
    kernel = (kernel_size, kernel_size)

    block = nn.Sequential()
    block.add_module('{}_{}'.format(name, 'Upsampling'), nn.Upsample(scale_factor=upsampling_factor_2d))

    block.add_module('{}_{}'.format(name, 'Conv2D'), nn.Conv2d(num_input_channels, num_output_channels,
                                                               kernel_size=kernel, stride=(1, 1),
                                                               padding=padding, bias=True))
    if norm:
        block.add_module('{}_{}'.format(name, 'BatchNorm2d'), nn.BatchNorm2d(num_output_channels))
    block.add_module('{}_{}'.format(name, 'LeakyReLu'), nn.LeakyReLU(leaky_slope, inplace=False))
    if dropout_rate > 0.0:
        block.add_module('{}_{}'.format(name, 'Dropout2D'), nn.Dropout2d(dropout_rate, inplace=False))

    for i in range(num_conv_layers):
        build_conv_block('{}_conv_{}'.format(name, i), num_output_channels, num_output_channels,
                         kernel=kernel, padding=padding, block=block, leaky_slope=leaky_slope,
                         norm=norm, dropout_rate=dropout_rate)

    return block


def build_supersampling_block(name, num_input_channels, num_output_channels, scale_factor=(2, 2),
                              kernel_size=3, norm=True, num_conv_layers=0, leaky_slope=0.2, dropout_rate=0.0):
    kernel_size_half = kernel_size // 2
    padding = (kernel_size_half, kernel_size_half)
    kernel = (kernel_size, kernel_size)

    block = nn.Sequential()
    block.add_module('{}_{}'.format(name, 'Upsampling Bilinear'), Resampling2D(scale_factor=scale_factor))
    block.add_module('{}_{}'.format(name, 'Conv2D'), nn.Conv2d(num_input_channels, num_output_channels,
                                                               kernel_size=kernel, stride=(1, 1),
                                                               padding=padding, bias=True))
    if norm:
        block.add_module('{}_{}'.format(name, 'BatchNorm2d'), nn.BatchNorm2d(num_output_channels))
    if dropout_rate > 0.0:
        block.add_module('{}_{}'.format(name, 'Dropout2D'), nn.Dropout2d(dropout_rate, inplace=False))
    block.add_module('{}_{}'.format(name, 'LeakyReLu'), nn.LeakyReLU(leaky_slope, inplace=False))  # nn.ReLU(True))

    for i in range(num_conv_layers):
        build_conv_block('{}_conv_{}'.format(name, i), num_output_channels, num_output_channels,
                         kernel=kernel, padding=padding, block=block, leaky_slope=leaky_slope,
                         norm=norm, dropout_rate=dropout_rate)

    return block


# def build_inception_block(num_input_channels, num_output_channels, leaky_slope=0.2, batch_norm=True):
#     # https://www.analyticsvidhya.com/blog/2018/10/understanding-inception-network-from-scratch/
#     # compute the number of channels for each conv layer --> number of channels should sum up
#     # to num_output_channels after concatenation
#     num_concat_channels = num_output_channels // 4
#
#     block_1x1 = nn.Sequential()
#     block_1x1.add_module("{}_{}".format('inception_conv_1x1', 'Conv2D'),
#                          nn.Conv2d(num_input_channels, num_concat_channels,
#                                    kernel_size=(1, 1), stride=(1, 1), padding=(0, 0)))
#     if batch_norm:
#         block_1x1.add_module('{}_{}'.format('inception_conv_1x1', 'BatchNorm2D'),
#                              nn.BatchNorm2d(num_concat_channels))
#     block_1x1.add_module('{}_{}'.format('inception_conv_1x1', 'LeakyReLu'), nn.LeakyReLU(leaky_slope,
#                                                                                          inplace=True))
#
#     block_1x1.add_module("{}_{}".format('inception_conv_1x1_1', 'Conv2D'),
#                          nn.Conv2d(num_concat_channels, num_concat_channels,
#                                    kernel_size=(1, 1), stride=(1, 1), padding=(0, 0)))
#     if batch_norm:
#         block_1x1.add_module('{}_{}'.format('inception_conv_1x1_1', 'BatchNorm2D'),
#                              nn.BatchNorm2d(num_concat_channels))
#     block_1x1.add_module('{}_{}'.format('inception_conv_1x1_1', 'LeakyReLu'), nn.LeakyReLU(leaky_slope,
#                                                                                            inplace=True))
#
#     block_3x3 = nn.Sequential()
#     block_3x3.add_module("{}_{}".format('inception_conv_3x3', 'Conv2D'),
#                          nn.Conv2d(num_input_channels, num_concat_channels,
#                                    kernel_size=(1, 1), stride=(1, 1), padding=(0, 0)))
#     if batch_norm:
#         block_3x3.add_module('{}_{}'.format('inception_conv_3x3', 'BatchNorm2D'),
#                              nn.BatchNorm2d(num_concat_channels))
#     block_3x3.add_module('{}_{}'.format('inception_conv_3x3', 'LeakyReLu'), nn.LeakyReLU(leaky_slope,
#                                                                                          inplace=True))
#
#     block_3x3.add_module("{}_{}".format('inception_conv_3x3_1', 'Conv2D'),
#                          nn.Conv2d(num_concat_channels, num_concat_channels,
#                                    kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)))
#     if batch_norm:
#         block_3x3.add_module('{}_{}'.format('inception_conv_3x3_1', 'BatchNorm2D'),
#                          nn.BatchNorm2d(num_concat_channels))
#     block_3x3.add_module('{}_{}'.format('inception_conv_3x3_1', 'LeakyReLu'), nn.LeakyReLU(leaky_slope,
#                                                                                            inplace=True))
#
#     block_5x5 = nn.Sequential()
#     block_5x5.add_module("{}_{}".format('inception_conv_5x5', 'Conv2D'),
#                          nn.Conv2d(num_input_channels, num_concat_channels,
#                                    kernel_size=(1, 1), stride=(1, 1), padding=(0, 0)))
#     if batch_norm:
#         block_5x5.add_module('{}_{}'.format('inception_conv_5x5', 'BatchNorm2D'),
#                              nn.BatchNorm2d(num_concat_channels))
#     block_5x5.add_module('{}_{}'.format('inception_conv_5x5', 'LeakyReLu'), nn.LeakyReLU(leaky_slope,
#                                                                                          inplace=True))
#
#     block_5x5.add_module("{}_{}".format('inception_conv_5x5_1', 'Conv2D'),
#                          nn.Conv2d(num_concat_channels, num_concat_channels,
#                                    kernel_size=(5, 5), stride=(1, 1), padding=(2, 2)))
#     if batch_norm:
#         block_5x5.add_module('{}_{}'.format('inception_conv_5x5_1', 'BatchNorm2D'),
#                             nn.BatchNorm2d(num_concat_channels))
#     block_5x5.add_module('{}_{}'.format('inception_conv_5x5_1', 'LeakyReLu'), nn.LeakyReLU(leaky_slope,
#                                                                                            inplace=True))
#
#     block_maxpool = nn.Sequential()
#     # (N, C, H, W) -> (N, C, H_out, W_out) for max pooling
#     block_maxpool.add_module("{}_{}".format('inception_max_pool', 'MaxPool2D'),
#                              nn.MaxPool2d(kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)))
#     if batch_norm:
#         block_maxpool.add_module('{}_{}'.format('inception_max_pool', 'BatchNorm2D'),
#                                  nn.BatchNorm2d(num_input_channels))
#     block_maxpool.add_module('{}_{}'.format('inception_max_pool', 'LeakyReLu'), nn.LeakyReLU(leaky_slope,
#                                                                                              inplace=True))
#
#     block_maxpool.add_module("{}_{}".format('inception_max_pool_conv_1x1', 'Conv2D'),
#                              nn.Conv2d(num_input_channels, num_output_channels - (3 * num_concat_channels),
#                                        kernel_size=(1, 1), stride=(1, 1), padding=(0, 0)))
#     if batch_norm:
#         block_maxpool.add_module('{}_{}'.format('inception_max_pool_conv_1x1', 'BatchNorm2D'),
#                                  nn.BatchNorm2d(num_output_channels - (3 * num_concat_channels)))
#     block_maxpool.add_module('{}_{}'.format('inception_max_pool_conv_1x1', 'LeakyReLu'), nn.LeakyReLU(leaky_slope,
#                                                                                                       inplace=True))
#
#     block_sequence = [block_1x1, block_3x3, block_5x5, block_maxpool]
#
#     return block_sequence
