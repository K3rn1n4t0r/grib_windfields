from .CustomModules import *
import numpy as np
from itertools import product
from enum import Enum
from Networks.Linear.LinearRegressor import LinearRegressor


class InputModuleType(Enum):
    CONV = 0
    RESNET = 1
    INCEPTION = 2


class LRHRInputBlock(nn.Module):
    def __init__(self, submodule_lr, submodule_hr):
        super(LRHRInputBlock, self).__init__()
        # self.submodule_lr = submodule_lr
        # self.submodule_hr = submodule_hr

        self.model = nn.ModuleList([submodule_lr, submodule_hr])
        # self.out_channels = submodule_lr.out_channels + submodule_lr.out_channels

    def forward(self, input_lr, input_hr):
        # output_lr = self.submodule_lr(input_lr)
        # output_hr = self.submodule_hr(input_hr)
        output_lr = self.model[0](input_lr)
        output_hr = self.model[1](input_hr)
        output = torch.cat([output_lr, output_hr], dim=1)
        return output


class InputBlock(nn.Module):
    def __init__(self, num_input_channels, num_output_channels):
        super(InputBlock, self).__init__()
        self.in_channels = num_input_channels
        self.out_channels = num_output_channels
        self.model = None

    def forward(self, x):
        output = self.model(x)
        return output


class InputBlockIdentity(nn.Module):
    def __init__(self):
        super(InputBlockIdentity, self).__init__()

    def forward(self, x):
        return x


########################################################################################################################
class InputLinearRegressorHiRes(InputBlock):
    def __init__(self, input_shape=(144, 180), output_shape=(36, 60)):
        super(InputLinearRegressorHiRes, self).__init__(0, 0)
        self.model = LinearRegressor(input_shape=input_shape, output_shape=output_shape)


########################################################################################################################
class InputConvBlocksHiRes(InputBlock):
    def __init__(self, num_input_channels, num_output_channels,
                 num_feature_channels,
                 num_conv_blocks=3, kernel_size=5, padding_mode='replication',
                 use_batch_norm=True, leaky_slope=0.2, dropout_rate=0.0):
        super(InputConvBlocksHiRes, self).__init__(num_input_channels, num_output_channels)
        self.num_conv_blocks = num_conv_blocks
        self.kernel_size = kernel_size
        self.total_num_hidden_conv_blocks = 0

        input_block = []

        # build input convolution to produce n feature channels
        input_block += [ConvBlock(self.in_channels, num_feature_channels, kernel_size=self.kernel_size,
                                  padding_mode=padding_mode, use_batch_norm=use_batch_norm,
                                  leaky_slope=leaky_slope, dropout_rate=dropout_rate)]

        conv_in_channels = num_feature_channels
        conv_out_channels = num_feature_channels

        # build conv blocks in between
        for i in range(num_conv_blocks):
            input_block += [ConvBlock(conv_in_channels, conv_out_channels, kernel_size=self.kernel_size,
                                      padding_mode=padding_mode, use_batch_norm=use_batch_norm,
                                      leaky_slope=leaky_slope, dropout_rate=dropout_rate)]
            self.total_num_hidden_conv_blocks += 1

        # build downscaling block / (2, 3)
        input_block += [ConvBlock(conv_in_channels, conv_out_channels, kernel_size=self.kernel_size,
                                  stride=(2, 3), padding_mode=padding_mode, use_batch_norm=use_batch_norm,
                                  leaky_slope=leaky_slope, dropout_rate=dropout_rate)]
        self.total_num_hidden_conv_blocks += 1

        # build conv blocks in between
        for i in range(num_conv_blocks):
            input_block += [ConvBlock(conv_in_channels, conv_out_channels, kernel_size=self.kernel_size,
                                      padding_mode=padding_mode, use_batch_norm=use_batch_norm,
                                      leaky_slope=leaky_slope, dropout_rate=dropout_rate)]
            self.total_num_hidden_conv_blocks += 1

        # build downscaling block / (2, 1)
        input_block += [ConvBlock(conv_in_channels, conv_out_channels, kernel_size=self.kernel_size,
                                  stride=(2, 1), padding_mode=padding_mode, use_batch_norm=use_batch_norm,
                                  leaky_slope=leaky_slope, dropout_rate=dropout_rate)]
        self.total_num_hidden_conv_blocks += 1

        # build output convolution to produce n target channels
        input_block += [ConvBlock(conv_out_channels, self.out_channels, kernel_size=self.kernel_size,
                                  padding_mode=padding_mode, use_batch_norm=use_batch_norm,
                                  leaky_slope=leaky_slope, dropout_rate=dropout_rate)]

        self.model = nn.Sequential(*input_block)


class InputConvBlocksLowRes(InputBlock):
    def __init__(self, num_input_channels, num_output_channels,
                 num_feature_channels,
                 num_conv_blocks=3, kernel_size=5, padding_mode='replication',
                 use_batch_norm=True, leaky_slope=0.2, dropout_rate=0.0):
        super(InputConvBlocksLowRes, self).__init__(num_input_channels, num_output_channels)
        self.num_conv_blocks = num_conv_blocks
        self.kernel_size = kernel_size
        input_block = []

        # build input convolution to produce n feature channels
        input_block += [ConvBlock(self.in_channels, num_feature_channels, kernel_size=self.kernel_size,
                                  padding_mode=padding_mode, use_batch_norm=use_batch_norm,
                                  leaky_slope=leaky_slope, dropout_rate=dropout_rate)]

        conv_in_channels = num_feature_channels
        conv_out_channels = num_feature_channels

        # build conv blocks in between
        for i in range(num_conv_blocks):
            input_block += [ConvBlock(conv_in_channels, conv_out_channels, kernel_size=self.kernel_size,
                                      padding_mode=padding_mode, use_batch_norm=use_batch_norm,
                                      leaky_slope=leaky_slope, dropout_rate=dropout_rate)]

        # build output convolution to produce n target channels
        input_block += [ConvBlock(conv_out_channels, self.out_channels, kernel_size=self.kernel_size,
                                  padding_mode=padding_mode, use_batch_norm=use_batch_norm,
                                  leaky_slope=leaky_slope, dropout_rate=dropout_rate)]

        self.model = nn.Sequential(*input_block)


class InputConvBlocksParameterSetting:
    def __init__(self, num_conv_blocks=3,
                 kernel_size_lr=5, kernel_size_hr=5):
        self.num_conv_blocks = num_conv_blocks
        self.kernel_size_lr = kernel_size_lr
        self.kernel_size_hr = kernel_size_hr

    def update_config(self, cfg):
        cfg['network']['HROro'].update({
            "numConvLayers": self.num_conv_blocks,
            "kernelSizeLR": self.kernel_size_lr,
            "kernelSizeHR": self.kernel_size_hr,
        })

    @staticmethod
    def create_parameter_pool():
        num_conv_layers = np.arange(0, 4, 1)
        kernel_sizes_lr = np.arange(1, 9, 2)
        kernel_sizes_hr = np.arange(1, 9, 2)

        param_combos = list(product(num_conv_layers, kernel_sizes_lr, kernel_sizes_hr))
        print("[INFO]: Number of learnable parameters in network model {}: {}".format("InputConvParams",
                                                                                      len(param_combos)))
        return [InputConvBlocksParameterSetting(*setting) for setting in param_combos]


########################################################################################################################
class InputResNetBlocksHiRes(InputBlock):
    def __init__(self, num_input_channels, num_output_channels,
                 num_feature_channels,
                 num_resnet_blocks=3,
                 num_conv_layers_per_resnet=2, kernel_size=5):
        super(InputResNetBlocksHiRes, self).__init__(num_input_channels, num_output_channels)
        self.num_conv_layers_per_resnet = num_conv_layers_per_resnet
        self.num_resnet_blocks = num_resnet_blocks
        self.kernel_size = kernel_size
        input_block = []

        # build input convolution to produce n feature channels
        input_block += [ConvBlock(self.in_channels, num_feature_channels, kernel_size=self.kernel_size)]

        conv_in_channels = num_feature_channels
        conv_out_channels = num_feature_channels

        # build resnet blocks in between
        for i in range(num_resnet_blocks):
            input_block += [ResnetMultiBlock(conv_in_channels, conv_out_channels,
                                             num_resnet_blocks=self.num_resnet_blocks,
                                             num_conv_layers=self.num_conv_layers_per_resnet,
                                             kernel_size=self.kernel_size)]

        # build downscaling block / (2, 3)
        input_block += [ConvBlock(conv_in_channels, conv_out_channels, kernel_size=self.kernel_size,
                                  stride=(2, 3))]

        # build resnet blocks in between
        for i in range(num_resnet_blocks):
            input_block += [ResnetMultiBlock(conv_in_channels, conv_out_channels,
                                             num_resnet_blocks=self.num_resnet_blocks,
                                             num_conv_layers=self.num_conv_layers_per_resnet,
                                             kernel_size=self.kernel_size)]

        # build downscaling block / (2, 1)
        input_block += [ConvBlock(conv_in_channels, conv_out_channels, kernel_size=self.kernel_size,
                                  stride=(2, 1))]

        # build output convolution to produce n target channels
        input_block += [ConvBlock(conv_out_channels, self.out_channels, kernel_size=self.kernel_size)]

        self.model = nn.Sequential(*input_block)


class InputResNetBlocksLowRes(InputBlock):
    def __init__(self, num_input_channels, num_output_channels,
                 num_feature_channels,
                 num_resnet_blocks=3,
                 num_conv_layers_per_resnet=2, kernel_size=5):
        super(InputResNetBlocksLowRes, self).__init__(num_input_channels, num_output_channels)
        self.num_conv_layers_per_resnet = num_conv_layers_per_resnet
        self.num_resnet_blocks = num_resnet_blocks
        self.kernel_size = kernel_size
        input_block = []

        # build input convolution to produce n feature channels
        input_block += [ConvBlock(self.in_channels, num_feature_channels, kernel_size=self.kernel_size)]

        conv_in_channels = num_feature_channels
        conv_out_channels = num_feature_channels

        # build resnet blocks in between
        for i in range(num_resnet_blocks):
            input_block += [ResnetMultiBlock(conv_in_channels, conv_out_channels,
                                             num_resnet_blocks=self.num_resnet_blocks,
                                             num_conv_layers=self.num_conv_layers_per_resnet,
                                             kernel_size=self.kernel_size)]

        # build output convolution to produce n target channels
        input_block += [ConvBlock(conv_out_channels, self.out_channels, kernel_size=self.kernel_size)]

        self.model = nn.Sequential(*input_block)


class InputResNetBlocksParameterSetting:
    def __init__(self, num_resnet_blocks=3,
                 num_conv_layers_per_resnet=2,
                 kernel_size_lr=5, kernel_size_hr=5):
        self.num_resnet_blocks = num_resnet_blocks
        self.num_conv_layers_per_resnet = num_conv_layers_per_resnet
        self.kernel_size_lr = kernel_size_lr
        self.kernel_size_hr = kernel_size_hr

    def update_config(self, cfg):
        cfg['network']['HROro'].update({
            "kernelSizeLR": self.kernel_size_lr,
            "kernelSizeHR": self.kernel_size_hr,
            "numResBlocks": self.num_resnet_blocks,
            "numConvPerResBlocks": self.num_conv_layers_per_resnet
        })

    @staticmethod
    def create_parameter_pool():
        num_resnet_blocks = np.arange(0, 4, 1)
        num_conv_layers_per_resnet = np.arange(1, 4, 1)
        kernel_sizes_lr = np.arange(1, 9, 2)
        kernel_sizes_hr = np.arange(1, 9, 2)

        param_combos = list(product(num_resnet_blocks, num_conv_layers_per_resnet, kernel_sizes_lr, kernel_sizes_hr))
        print("[INFO]: Number of learnable parameters in network model {}: {}".format("InputConvParams",
                                                                                      len(param_combos)))
        return [InputResNetBlocksParameterSetting(*setting) for setting in param_combos]


########################################################################################################################
class InputInceptionBlocksLowRes(InputBlock):
    def __init__(self, num_input_channels, num_output_channels,
                 num_feature_channels,
                 num_inception_blocks=3,
                 kernel_size=5):
        super(InputInceptionBlocksLowRes, self).__init__(num_input_channels, num_output_channels)
        self.num_inception_blocks = num_inception_blocks
        self.kernel_size = kernel_size
        input_block = []

        # build input convolution to produce n feature channels
        input_block += [ConvBlock(self.in_channels, num_feature_channels, kernel_size=self.kernel_size)]

        conv_in_channels = num_feature_channels
        conv_out_channels = num_feature_channels

        # build resnet blocks in between
        for i in range(num_inception_blocks):
            input_block += [InceptionBlock(conv_in_channels, conv_out_channels)]

        # build output convolution to produce n target channels
        input_block += [ConvBlock(conv_out_channels, self.out_channels, kernel_size=self.kernel_size)]

        self.model = nn.Sequential(*input_block)


class InputInceptionBlocksHiRes(InputBlock):
    def __init__(self, num_input_channels, num_output_channels,
                 num_feature_channels,
                 num_inception_blocks=3,
                 kernel_size=5):
        super(InputInceptionBlocksHiRes, self).__init__(num_input_channels, num_output_channels)
        self.num_inception_blocks = num_inception_blocks
        self.kernel_size = kernel_size
        self.total_num_inception_blocks = 0
        input_block = []

        # build input convolution to produce n feature channels
        input_block += [ConvBlock(self.in_channels, num_feature_channels, kernel_size=self.kernel_size)]

        conv_in_channels = num_feature_channels
        conv_out_channels = num_feature_channels

        # build resnet blocks in between
        for i in range(num_inception_blocks):
            input_block += [InceptionBlock(conv_in_channels, conv_out_channels)]
            self.total_num_inception_blocks += 1

        # build downscaling block / (2, 3)
        input_block += [ConvBlock(conv_in_channels, conv_out_channels, kernel_size=self.kernel_size,
                                  stride=(2, 3))]

        # build resnet blocks in between
        for i in range(num_inception_blocks):
            input_block += [InceptionBlock(conv_in_channels, conv_out_channels)]
            self.total_num_inception_blocks += 1

        # build downscaling block / (2, 1)
        input_block += [ConvBlock(conv_in_channels, conv_out_channels, kernel_size=self.kernel_size,
                                  stride=(2, 1))]

        # build output convolution to produce n target channels
        input_block += [ConvBlock(conv_out_channels, self.out_channels, kernel_size=self.kernel_size)]

        self.model = nn.Sequential(*input_block)


class InputInceptionBlocksParameterSetting:
    def __init__(self, num_inception_blocks=2,
                 kernel_size_lr=5, kernel_size_hr=5):
        self.num_inception_blocks = num_inception_blocks
        self.kernel_size_lr = kernel_size_lr
        self.kernel_size_hr = kernel_size_hr

    def update_config(self, cfg):
        cfg['network']['HROro'].update({
            "kernelSizeLR": self.kernel_size_lr,
            "kernelSizeHR": self.kernel_size_hr,
            "numInceptionBlocks": self.num_inception_blocks,
        })

    @staticmethod
    def create_parameter_pool():
        kernel_sizes_lr = np.arange(1, 9, 2)
        kernel_sizes_hr = np.arange(1, 9, 2)
        num_inception_blocks = np.arange(0, 4, 1)

        param_combos = list(product(num_inception_blocks, kernel_sizes_lr, kernel_sizes_hr))
        print("[INFO]: Number of learnable parameters in network model {}: {}".format("InputConvParams",
                                                                                      len(param_combos)))
        return [InputInceptionBlocksParameterSetting(*setting) for setting in param_combos]
