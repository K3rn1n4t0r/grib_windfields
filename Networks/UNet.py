import torch
import torch.nn as nn
import numpy as np
from itertools import product
from .BuildUtils import *
from .CustomModules import ConvBlock

# UNet Pix2Pix
# https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix/blob/master/models/networks.py

# Super-sampling
# https://towardsdatascience.com/deep-learning-based-super-resolution-without-using-a-gan-11c9bb5b6cd5
# https://towardsdatascience.com/u-nets-with-resnet-encoders-and-cross-connections-d8ba94125a2c


class UNetParameterSetting:
    def __init__(self, feature_channels=32, kernel_size_encoding=3,
                 kernel_size_decoding=3, num_conv_layers=1,
                 use_batch_norm=True, use_dropout=False, dropout_rate=0.2):
        self.num_feature_channels = feature_channels
        self.kernel_size_encoding = kernel_size_encoding
        self.kernel_size_decoding = kernel_size_decoding
        self.num_conv_layers = num_conv_layers
        self.use_batch_norm = use_batch_norm
        self.use_dropout = use_dropout
        self.dropout_rate = dropout_rate

    def update_config(self, cfg):
        cfg['network']['UNet'].update({
            "featureChannels": self.num_feature_channels,
            "kernelSizeEncoding": self.kernel_size_encoding,
            "kernelSizeDecoding": self.kernel_size_decoding,
            "numConvLayers": self.num_conv_layers,
            "useBatchNorm": self.use_batch_norm,
            "useDropout": self.use_dropout,
            "dropoutRate": self.dropout_rate
        })


class EncodingBlock(nn.Module):
    def __init__(self, num_input_channels, num_output_channels,
                 downsampling_factor_2d, kernel_size=3, padding_mode='replication',
                 use_batch_norm=True, num_conv_layers=0, leaky_slope=0.2, dropout_rate=0.0):
        super(EncodingBlock, self).__init__()

        block = []
        block += [ConvBlock(num_input_channels, num_output_channels, kernel_size=kernel_size,
                            use_batch_norm=use_batch_norm, leaky_slope=leaky_slope, dropout_rate=dropout_rate,
                            stride=downsampling_factor_2d, padding_mode=padding_mode)]

        for i in range(num_conv_layers):
            block += [ConvBlock(num_output_channels, num_output_channels, kernel_size=kernel_size,
                                use_batch_norm=use_batch_norm, leaky_slope=leaky_slope, dropout_rate=dropout_rate,
                                padding_mode=padding_mode)]

        self.model = nn.Sequential(*block)

    def forward(self, x):
        output = self.model(x)
        return output


class DecodingBlock(nn.Module):
    def __init__(self, num_input_channels, num_output_channels,
                 upsampling_factor_2d, kernel_size=3, padding_mode='replication',
                 use_batch_norm=True, num_conv_layers=0, leaky_slope=0.2, dropout_rate=0.0):
        super(DecodingBlock, self).__init__()

        block = []
        block += [nn.Upsample(scale_factor=upsampling_factor_2d)]
        block += [ConvBlock(num_input_channels, num_output_channels, kernel_size=kernel_size,
                            use_batch_norm=use_batch_norm, leaky_slope=leaky_slope, dropout_rate=dropout_rate,
                            stride=(1, 1), padding_mode=padding_mode)]

        for i in range(num_conv_layers):
            block += [ConvBlock(num_output_channels, num_output_channels, kernel_size=kernel_size,
                                use_batch_norm=use_batch_norm, leaky_slope=leaky_slope, dropout_rate=dropout_rate,
                                padding_mode=padding_mode)]

        self.model = nn.Sequential(*block)

    def forward(self, x):
        output = self.model(x)
        return output


class SkipConnectionBlock(nn.Module):
    def __init__(self, channels,
                 encode_decode_factor, inner_block=None, kernel_size_encoding=3, kernel_size_decoding=3,
                 padding_mode='replication',
                 use_batch_norm=True, num_conv_layers=0, leaky_slope=0.2, dropout_rate=0.0):
        super(SkipConnectionBlock, self).__init__()
        # assert(isinstance(block, list))
        assert(isinstance(encode_decode_factor, (tuple, list)))

        encoding_block = EncodingBlock(channels, channels * 2, downsampling_factor_2d=encode_decode_factor,
                                       kernel_size=kernel_size_encoding, use_batch_norm=use_batch_norm,
                                       num_conv_layers=num_conv_layers, leaky_slope=leaky_slope,
                                       dropout_rate=dropout_rate, padding_mode=padding_mode)

        decoding_input = channels * 2 if inner_block is None else channels * 4
        decoding_block = DecodingBlock(decoding_input, channels, upsampling_factor_2d=encode_decode_factor,
                                       kernel_size=kernel_size_decoding, use_batch_norm=use_batch_norm,
                                       num_conv_layers=num_conv_layers, leaky_slope=leaky_slope,
                                       dropout_rate=dropout_rate, padding_mode=padding_mode)

        if inner_block is None:
            blocks = [encoding_block, decoding_block]
        else:
            blocks = [encoding_block, inner_block, decoding_block]

        self.model = nn.Sequential(*blocks)

    def forward(self, x):
        output = self.model(x)
        output = torch.cat([x, output], dim=1)
        return output


class SupersamplingBlock(nn.Module):
    def __init__(self, num_input_channels, num_output_channels, scale_factor,
                 kernel_size=3, num_conv_layers=1, padding_mode='replication',
                 use_batch_norm=True, leaky_slope=0.2, dropout_rate=0.0):
        super(SupersamplingBlock, self).__init__()

        blocks = []
        blocks += [Resampling2D(scale_factor=scale_factor)]
        blocks += [ConvBlock(num_input_channels, num_output_channels, kernel_size=kernel_size,
                             use_batch_norm=use_batch_norm, leaky_slope=leaky_slope, dropout_rate=dropout_rate,
                             padding_mode=padding_mode)]

        for i in range(num_conv_layers):
            blocks += [ConvBlock(num_output_channels, num_output_channels, kernel_size=kernel_size,
                                 use_batch_norm=use_batch_norm, leaky_slope=leaky_slope, dropout_rate=dropout_rate,
                                 padding_mode=padding_mode)]

        self.model = nn.Sequential(*blocks)

    def forward(self, x):
        output = self.model(x)
        return output


class UNet(nn.Module):
    def __init__(self, input_channels, output_channels, feature_channels=32,
                 kernel_size_encoding=3, kernel_size_decoding=3, num_conv_layers=1,
                 leaky_slope=0.2, use_batch_norm=True, use_dropout=False, dropout_rate=0.2,
                 padding_mode='replication'):
        super(UNet, self).__init__()
        # stores the encoding layers
        self.encoding_layers = []
        # stores the decoding layers
        self.decoding_layers = []
        # number of feature channels at the beginning
        self.num_feature_channels = feature_channels
        # number if input channels
        self.num_input_channels = input_channels
        # number of output channels
        self.num_output_channels = output_channels
        # kernel size for encoding and decoding part
        self.kernel_size_encoding = kernel_size_encoding
        self.kernel_size_decoding = kernel_size_decoding
        # number of conv layers per block
        self.num_conv_layers = num_conv_layers
        # negative slope for leaky RELU
        self.leaky_slope = leaky_slope
        # use batch normalization
        self.use_batch_norm = use_batch_norm
        # dropout usage
        self.use_dropout = use_dropout
        # dropout rate
        self.dropout_rate = dropout_rate
        # type of padding mode
        self.padding_mode = padding_mode
        # stores the model
        self.model = None
        # build the model
        self._build_model()

    def _build_init_block(self, num_output_channels, kernel_size=3, leaky_slope=0.2,
                          norm=True, dropout_rate=0.0):

        blocks = []
        blocks += [ConvBlock(self.num_input_channels, num_output_channels, kernel_size=kernel_size,
                             use_batch_norm=norm, leaky_slope=leaky_slope, dropout_rate=dropout_rate,
                             padding_mode=self.padding_mode)]

        for i in range(self.num_conv_layers):
            blocks += [ConvBlock(num_output_channels, num_output_channels, kernel_size=kernel_size,
                                 use_batch_norm=norm, leaky_slope=leaky_slope, dropout_rate=dropout_rate,
                                 padding_mode=self.padding_mode)]

        return nn.Sequential(*blocks)

    def _build_output_block(self, name, num_input_channels, kernel_size=3):

        blocks = []
        blocks += [ConvBlock(num_input_channels, self.num_output_channels, kernel_size=kernel_size,
                             leaky_slope=0.0, use_batch_norm=False, dropout_rate=0.0,
                             padding_mode=self.padding_mode)]

        return nn.Sequential(*blocks)

    def _build_model(self):
        channels = self.num_feature_channels
        dropout_rate = 0.0 if self.use_batch_norm or not self.use_dropout else self.dropout_rate

        layers = [self._build_init_block(channels, self.kernel_size_encoding, self.leaky_slope,
                                         self.use_batch_norm, dropout_rate=dropout_rate)]

        skip_connection_blocks = SkipConnectionBlock(channels * 4, (3, 3), None,
                                                     kernel_size_encoding=self.kernel_size_encoding,
                                                     kernel_size_decoding=self.kernel_size_decoding,
                                                     use_batch_norm=self.use_batch_norm,
                                                     num_conv_layers=self.num_conv_layers,
                                                     leaky_slope=self.leaky_slope, dropout_rate=dropout_rate,
                                                     padding_mode=self.padding_mode)

        skip_connection_blocks = SkipConnectionBlock(channels * 2, (2, 2), skip_connection_blocks,
                                                     kernel_size_encoding=self.kernel_size_encoding,
                                                     kernel_size_decoding=self.kernel_size_decoding,
                                                     use_batch_norm=self.use_batch_norm,
                                                     num_conv_layers=self.num_conv_layers,
                                                     leaky_slope=self.leaky_slope, dropout_rate=dropout_rate,
                                                     padding_mode=self.padding_mode)

        skip_connection_blocks = SkipConnectionBlock(channels, (2, 2), skip_connection_blocks,
                                                     kernel_size_encoding=self.kernel_size_encoding,
                                                     kernel_size_decoding=self.kernel_size_decoding,
                                                     use_batch_norm=self.use_batch_norm,
                                                     num_conv_layers=self.num_conv_layers,
                                                     leaky_slope=self.leaky_slope, dropout_rate=dropout_rate,
                                                     padding_mode=self.padding_mode)

        layers += [skip_connection_blocks]

        layers += [SupersamplingBlock(channels * 2, channels, scale_factor=(2, 3),
                                      kernel_size=self.kernel_size_decoding,
                                      num_conv_layers=self.num_conv_layers,
                                      use_batch_norm=self.use_batch_norm, leaky_slope=self.leaky_slope,
                                      dropout_rate=dropout_rate,
                                      padding_mode=self.padding_mode)]

        layers += [SupersamplingBlock(channels, channels // 2, scale_factor=(2, 1),
                                      kernel_size=self.kernel_size_decoding,
                                      num_conv_layers=self.num_conv_layers,
                                      use_batch_norm=self.use_batch_norm, leaky_slope=self.leaky_slope,
                                      dropout_rate=dropout_rate,
                                      padding_mode=self.padding_mode)]

        layers += [self._build_output_block('conv_layer', int(channels // 2))]

        self.model = nn.Sequential(*layers)

    def forward(self, x):
        output = self.model(x)
        return output

    @staticmethod
    def create_parameter_pool():
        feature_channels = 2 ** np.arange(5, 7, 1)
        encoding_kernel_sizes = np.arange(3, 6, 2)
        decoding_kernel_sizes = np.arange(3, 6, 2)
        num_conv_layers = np.arange(1, 4, 1)
        padding_modes = ['zero', 'replication', 'reflection']

        # param_combos = list(product(feature_channels, encoding_kernel_sizes, decoding_kernel_sizes, num_conv_layers))
        param_combos = list(product(padding_modes))
        print("[INFO]: Number of learnable parameters in network model {}: {}".format(UNet.__name__, len(param_combos)))
        return [UNetParameterSetting(*setting) for setting in param_combos]
