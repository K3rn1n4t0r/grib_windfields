# from Networks.UNet import UNet, UNetHROro, UNetHROroParameter
# from Networks.ResUNet import ResUNet, ResUNetHROro, ResUNetHROroParameter, HROroType
# from Networks.EnhanceNet import EnhanceNet, EnhanceNetHROro, EnhanceNetHROroParameter
from Networks.UNet import UNet
from Networks.ResUNet import ResUNet
from Networks.EnhanceNet import EnhanceNet
from Networks.HROrographyNet import *
from Networks.UNet_D2S import UNet_D2S
from Networks.Linear.LinearRegressor import LinearRegressor
from Networks.Linear.LinearCNN import LinearCNN
from Networks.DownscalingNet import DownscalingNet
from Networks.LinearModel import LinearModel, LinearModelHROro
from LossFunctions.CoherenceModels import MinimalNet


class ModelSelector(object):
    def __init__(self, configOpts, scaler_opts):
        self.config = configOpts
        self.scaler_opts = scaler_opts

    def select(self):
        netName = self.config['network']['name']
        try:
            netCreationRoutine = getattr(self, '_get_{}'.format(netName))
        except AttributeError:
            raise NotImplementedError('[ERROR] Network architecture <{}> not implemented.'.format(netName))
        else:
            net = netCreationRoutine()
            if isinstance(net, DownscalingNet):
                net_id = net.name
            else:
                net_id = net.__class__.__name__

            print('[INFO]: Used downscaling network: {}'.format(net_id))
        return net

    def _build_input_modules(self, feature_channels):
        if self.config['data']['useHROrography']:
            mode = InputModuleType(self.config['network']['HROro']['mode'])

            out_channels = feature_channels // 2

            num_channels_lr = self.config['data']['shapeInputs'][0]
            num_channels_hr = self.config['data']['channelsHR']
            kernel_size_lr = self.config['network']['HROro']['kernelSizeLR']
            kernel_size_hr = self.config['network']['HROro']['kernelSizeHR']

            in_linear_mode = "Linear" in self.config['network']['name']

            use_batch_norm = True if in_linear_mode else True
            leaky_slope = 0.0 if in_linear_mode else 0.2
            dropout_rate = 0.0

            if "LinearRegressor" in self.config['network']['name']:
                input_process_lr = InputBlockIdentity()
                input_shape_hr = (num_channels_hr,) + tuple(self.config['data']['shapeTargets'][1:])
                output_shape_hr = (num_channels_hr,) + tuple(self.config['data']['shapeInputs'][1:])
                input_process_hr = InputLinearRegressorHiRes(input_shape=input_shape_hr,
                                                             output_shape=output_shape_hr)

            elif mode == InputModuleType.CONV:
                num_conv_blocks = self.config['network']['HROro']['numConvLayers']

                padding_mode = self.config['network']['paddingMode']

                input_process_hr = InputConvBlocksHiRes(num_input_channels=num_channels_hr,
                                                        num_output_channels=out_channels,
                                                        num_feature_channels=feature_channels,
                                                        num_conv_blocks=num_conv_blocks,
                                                        kernel_size=kernel_size_hr,
                                                        padding_mode=padding_mode,
                                                        use_batch_norm=use_batch_norm, leaky_slope=leaky_slope,
                                                        dropout_rate=dropout_rate)

                input_process_lr = InputConvBlocksLowRes(num_input_channels=num_channels_lr,
                                                         num_output_channels=out_channels,
                                                         num_feature_channels=feature_channels,
                                                         num_conv_blocks=input_process_hr.total_num_hidden_conv_blocks,
                                                         kernel_size=kernel_size_lr,
                                                         padding_mode=padding_mode,
                                                         use_batch_norm=use_batch_norm, leaky_slope=leaky_slope,
                                                         dropout_rate=dropout_rate)

            elif mode == InputModuleType.RESNET:
                num_res_blocks = self.config['network']['HROro']['numResBlocks']
                num_conv_blocks_per_resnet = self.config['network']['HROro']['numConvPerResBlocks']

                input_process_hr = InputResNetBlocksHiRes(num_input_channels=num_channels_hr,
                                                          num_output_channels=out_channels,
                                                          num_feature_channels=feature_channels,
                                                          num_resnet_blocks=num_res_blocks,
                                                          num_conv_layers_per_resnet=num_conv_blocks_per_resnet,
                                                          kernel_size=kernel_size_hr)

                input_process_lr = InputResNetBlocksLowRes(num_input_channels=num_channels_lr,
                                                           num_output_channels=out_channels,
                                                           num_feature_channels=feature_channels,
                                                           num_resnet_blocks=num_res_blocks,
                                                           num_conv_layers_per_resnet=num_conv_blocks_per_resnet,
                                                           kernel_size=kernel_size_lr)
            elif mode == InputModuleType.INCEPTION:
                num_inception_blocks = self.config['network']['HROro']['numInceptionBlocks']

                input_process_hr = InputInceptionBlocksHiRes(num_input_channels=num_channels_hr,
                                                             num_output_channels=out_channels,
                                                             num_feature_channels=feature_channels,
                                                             num_inception_blocks=num_inception_blocks,
                                                             kernel_size=kernel_size_hr)

                input_process_lr = InputInceptionBlocksLowRes(num_input_channels=num_channels_lr,
                                                              num_output_channels=out_channels,
                                                              num_feature_channels=feature_channels,
                                                              num_inception_blocks=num_inception_blocks,
                                                              kernel_size=kernel_size_lr)
            else:
                raise NotImplementedError()

            return [input_process_lr, input_process_hr]
        else:
            out_channels = feature_channels
            return None

    def _get_UNet(self, data_channels=None):
        num_feature_channels = self.config['network']['UNet']['featureChannels']
        input_modules = self._build_input_modules(num_feature_channels)

        if data_channels is None:
            data_channels = num_feature_channels if self.config['data']['useHROrography'] \
                else self.config['data']['shapeInputs'][0]

        unet = UNet(
            input_channels=data_channels,
            output_channels=self.config['data']['shapeTargets'][0],
            feature_channels=self.config['network']['UNet']['featureChannels'],
            kernel_size_encoding=self.config['network']['UNet']['kernelSizeEncoding'],
            kernel_size_decoding=self.config['network']['UNet']['kernelSizeDecoding'],
            num_conv_layers=self.config['network']['UNet']['numConvLayers'],
            use_batch_norm=self.config['network']['UNet']['useBatchNorm'],
            use_dropout=self.config['network']['UNet']['useDropout'],
            dropout_rate=self.config['network']['UNet']['dropoutRate'],
            padding_mode=self.config['network']['paddingMode'])

        net = DownscalingNet(input_modules=input_modules,
                             net_module=unet,
                             super_res_module=None)

        # print(net)
        return net

    def _get_UNet_Temporal(self):
        data_channels = self._infere_temporal_data_channels()
        return self._get_UNet(data_channels=data_channels)

    def _get_ResUNet(self, data_channels=None):
        num_feature_channels = self.config['network']['ResUNet']['featureChannels']
        input_modules = self._build_input_modules(num_feature_channels)

        if data_channels is None:
            data_channels = num_feature_channels if self.config['data']['useHROrography'] \
                else self.config['data']['shapeInputs'][0]

        target_scaler = self.scaler_opts.target[0][0] if len(self.scaler_opts.target) > 0 else None

        resunet = ResUNet(
                data_channels=data_channels,
                in_channels=self.config['network']['ResUNet']['featureChannels'],
                out_channels=self.config['data']['shapeTargets'][0],
                kernel_conv=int(self.config['network']['ResUNet']['kernelSize']),
                resblocks_per_stage=self.config['network']['ResUNet']['resblocksPerStage'],
                layers_per_block=self.config['network']['ResUNet']['convLayersPerBlock'],
                dropout=self.config['network']['ResUNet']['dropoutRate'],
                padding_mode=self.config['network']['paddingMode'],
                use_oa=False,
                input_scaler_uv=self.scaler_opts.input[0][0],
                target_scaler_uv=target_scaler
        )

        net = DownscalingNet(input_modules=input_modules,
                             net_module=resunet,
                             super_res_module=None)

        # print(net)
        return net

    def _get_ResUNet_Temporal(self):
        data_channels = self._infere_temporal_data_channels()
        return self._get_ResUNet(data_channels=data_channels)

    def _get_EnhanceNet(self, data_channels=None):
        num_feature_channels = self.config['network']['EnhanceNet']['featureChannels']
        input_modules = self._build_input_modules(num_feature_channels)

        if data_channels is None:
            data_channels = num_feature_channels if self.config['data']['useHROrography'] \
                else self.config['data']['shapeInputs'][0]

        enhance_net = EnhanceNet(
            data_channels=data_channels,
            in_channels=self.config['network']['EnhanceNet']['featureChannels'],
            out_channels=self.config['data']['shapeTargets'][0],
            resblocks=self.config['network']['EnhanceNet']['numResBlocks'],
            layers_per_block=self.config['network']['EnhanceNet']['layersPerBlock']
        )

        net = DownscalingNet(input_modules=input_modules,
                             net_module=enhance_net,
                             super_res_module=None)

        # print(net)
        return net

    def _get_EnhanceNet_Temporal(self):
        data_channels = self._infere_temporal_data_channels()
        return self._get_EnhanceNet(data_channels=data_channels)

    def _get_LinearRegressor(self, data_channels=None):
        num_feature_channels = self.config['network']['LinearRegressor']['featureChannels']
        input_modules = self._build_input_modules(num_feature_channels)

        input_shape = self.config['data']['shapeInputs']
        target_shape = self.config['data']['shapeTargets']

        if self.config['data']['useHROrography']:
            num_channels_lr = self.config['data']['shapeInputs'][0]
            num_channels_hr = self.config['data']['channelsHR']

            input_shape[0] = num_channels_lr + num_channels_hr

        linear_regressor = LinearRegressor(input_shape=input_shape,
                                           output_shape=target_shape)

        net = DownscalingNet(input_modules=input_modules,
                             net_module=linear_regressor,
                             super_res_module=None)

        return net

    def _get_LinearCNN(self, data_channels=None):
        num_feature_channels = self.config['network']['LinearCNN']['featureChannels']
        input_modules = self._build_input_modules(num_feature_channels)

        if data_channels is None:
            data_channels = num_feature_channels if self.config['data']['useHROrography'] \
                                else self.config['data']['shapeInputs'][0]

        linear_regressor = LinearCNN(num_input_channels=data_channels,
                                     num_output_channels=self.config['data']['shapeTargets'][0],
                                     num_feature_channels=num_feature_channels,
                                     kernel_size=self.config['network']['LinearCNN']['kernelSize'],
                                     num_additional_conv_layers=self.config['network']['LinearCNN']['numConvLayers'],
                                     padding_mode=self.config['network']['paddingMode'])

        net = DownscalingNet(input_modules=input_modules,
                             net_module=linear_regressor,
                             super_res_module=None)

        return net

    def _get_UNet_D2S(self, data_channels=None):
        if data_channels is None:
            data_channels = self.config['data']['shapeInputs'][0]
        if self.config['data']['useHROrography']:
            self._set_HROro_unavailable()
        net = UNet_D2S(
            data_channels=data_channels,
            in_channels=self.config['network']['inputChannels'],
            out_channels=self.config['data']['shapeTargets'][0],
            use_oa=False, dropout=0.1
        )
        return net

    def _get_UNet_D2S_Temporal(self):
        data_channels = self._infere_temporal_data_channels()
        return self._get_UNet_D2S(data_channels=data_channels)

    def _get_MinimalNet(self):
        numStepsPast = self.config['coherence']['numStepsPast']
        numStepsFuture = self.config['coherence']['numStepsFuture']
        numSteps = numStepsPast + numStepsFuture
        dataChannels = numSteps * self.config['data']['shapeTargets'][0]
        net = MinimalNet(
            data_channels=dataChannels,
            out_channels=self.config['data']['shapeTargets'][0],
            kernel_size=self.config['network']['kernelSize'],
            dilation=self.config['network']['dilation'],
            numStepsPast=numStepsPast,
            numStepsFuture=numStepsFuture,
            stepSizePast=self.config['coherence']['stepSizePast'],
            stepSizeFuture=self.config['coherence']['stepSizeFuture']
        )
        return net

    def _get_LinearModel(self, data_channels=None):
        if data_channels is None:
            data_channels = self.config['data']['shapeInputs'][0]
        if self.config['data']['useHROrography']:
            net = LinearModelHROro(
                data_channels=data_channels,
                in_channels=self.config['network']['inputChannels'],
                out_channels=self.config['data']['shapeTargets'][0],
                hres_channels=self.config['data']['channelsHR'],
                kernel_lres=5, kernel_hres=self.config['network']['hrKernelSize'],
                num_layers_lres=1, num_layers_hres=self.config['network']['hrConvLayers'],
                dilation_hres=2,
                scale_factor=(4,3)
            )
        else:
            net = LinearModel(
                data_channels=data_channels,
                in_channels=self.config['network']['inputChannels'],
                out_channels=self.config['data']['shapeTargets'][0],
                kernel_lres=5, kernel_hres=self.config['network']['hrKernelSize'],
                num_layers_lres=2, num_layers_hres=self.config['network']['hrConvLayers'],
                dilation_hres=1,
                scale_factor=(4, 3)
            )
        return net

    def _get_LinearModel_Temporal(self):
        data_channels = self._infere_temporal_data_channels()
        return self._get_LinearModel(data_channels=data_channels)

    def _infere_temporal_data_channels(self):
        dynamics = [
            var for var in self.config['preprocess']['exportGridsInput'] if var in self.config['data']['gridsInput']
        ]
        statics = [
            var for var in self.config['preprocess']['exportGridsSfc'] if var in self.config['data']['gridsInput']
        ]
        numStepsPast = self.config['coherence']['numStepsPast']
        numStepsFuture = self.config['coherence']['numStepsFuture']
        numSteps = numStepsPast + numStepsFuture + 1
        dataChannels = (numSteps * len(dynamics)) + len(statics)
        return dataChannels

    def _set_HROro_unavailable(self):
        netName = self.config['network']['name']
        print('[WARNING] Network architecture <{}> does not support high resolution orography (HROro).'.format(netName))
        print('[INFO] Creating network without HROro support.')
        self.config['data'].update({'useHROrography': False})
