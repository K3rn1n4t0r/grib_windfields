import torch
import torch.nn as nn
import numpy as np
from torch.nn import functional as F
from itertools import product


class EnhanceNetParameterSetting:
    def __init__(self, feature_channels=32, num_resblocks=10,
                 layers_per_block=2):
        self.num_feature_channels = feature_channels
        self.num_resblocks = num_resblocks
        self.layers_per_block = layers_per_block

    def update_config(self, cfg):
        cfg['network']['EnhanceNet'].update({
            "featureChannels": self.num_feature_channels,
            "numResBlocks": self.num_resblocks,
            "layersPerBlock": self.layers_per_block,
        })


class EnhanceNet(nn.Module):
    def __init__(self,
                 data_channels=4, in_channels=64, out_channels=2,
                 resblocks=10, layers_per_block=2,
                 use_bn=True, use_cc=True, leaky_slope=0.2, use_oa=True):
        super(EnhanceNet, self).__init__()
        self.out_channels = out_channels
        self.block_conv1 = self._block_conv(data_channels, in_channels, use_bn, True)
        self.blocks_res = self._blocks_res(in_channels, resblocks, layers_per_block, use_bn)
        self.block_conv2 = self._block_conv(in_channels, in_channels, use_bn, True)
        self.block_conv3 = self._block_conv(in_channels, in_channels, use_bn, True)
        self.block_out = self._block_out(in_channels, out_channels, use_bn)
        #self._initialize_weights()

    def forward(self, x):
        features = self.block_conv1(x)
        for block in self.blocks_res:
            features = features + block(features)
        features = self.block_conv2(F.interpolate(features, scale_factor=(2, 3), mode='nearest'))
        features = self.block_conv3(F.interpolate(features, scale_factor=(2, 1), mode='nearest'))
        features = self.block_out(features)
        return F.interpolate(x[:, :self.out_channels], scale_factor=(4, 3), mode='bicubic') + features

    def _block_conv(self, data_channels, in_channels, use_bn, use_oa):
        layers = [nn.Conv2d(data_channels, in_channels, (3, 3), padding=1)]
        if use_bn:
            layers += [nn.BatchNorm2d(in_channels)]
        if use_oa:
            layers += [nn.ReLU(inplace=True)]
        return nn.Sequential(*layers)

    def _blocks_res(self, in_channels, resblocks, layers_per_block, use_bn):

        def resblock(use_oa):
            layers = []
            for l in range(layers_per_block):
                layers += [nn.Conv2d(in_channels, in_channels, (3, 3), padding=1)]
                if use_bn:
                    layers += [nn.BatchNorm2d(in_channels)]
                if l < layers_per_block - 1:
                    layers += [nn.ReLU(inplace=True)]
            if use_oa:
                layers += [nn.ReLU(inplace=True)]
            return nn.Sequential(*layers)

        blocks = nn.ModuleList()
        for _ in range(resblocks):
            blocks.append(resblock(use_oa=False))
        return blocks

    def _block_out(self, in_channels, out_channels, use_bn):
        layers = [nn.Conv2d(in_channels, in_channels, (3, 3), padding=1)]
        if use_bn:
            layers += [nn.BatchNorm2d(in_channels)]
        layers += [nn.ReLU(inplace=True)]
        layers += [nn.Conv2d(in_channels, out_channels, (1, 1))]
        return nn.Sequential(*layers)

    def _initialize_weights(self):
        def init(m):
            classname = m.__class__.__name__
            if classname.find('Conv') != -1:
                torch.nn.init.orthogonal_(m.weight, torch.nn.init.calculate_gain('relu'))

        for block in self.blocks_res:
            block.apply(init)

    @staticmethod
    def create_parameter_pool():
        feature_channels = 2 ** np.arange(5, 7, 1)
        num_res_blocks = np.arange(8, 13, 1)
        layers_per_block = np.arange(1, 5, 1)

        param_combos = list(product(feature_channels, num_res_blocks, layers_per_block))
        print("[INFO]: Number of learnable parameters in network model {}: {}".format(EnhanceNet.__name__, len(param_combos)))
        return [EnhanceNetParameterSetting(*setting) for setting in param_combos]
