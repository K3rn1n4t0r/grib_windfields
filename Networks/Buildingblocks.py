import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np


class Resampling2D(nn.Module):
    def __init__(self, size=None, scale_factor=None, mode='nearest'):
        super(Resampling2D, self).__init__()
        self.size = size
        self.scaleFactor = scale_factor
        self.mode = mode

    def forward(self, x):
        if self.size is not None:
            return F.interpolate(x, size=self.size, mode=self.mode)

        elif self.scaleFactor is not None:
            dimX = x.shape[3]
            dimY = x.shape[2]

            newSize = (dimY * self.scaleFactor[0], dimX * self.scaleFactor[1])
            return F.interpolate(x, size=newSize, mode=self.mode)
        else:
            raise Exception

    def extra_repr(self):
        if self.scaleFactor is not None:
            info = 'scale_factor=' + str(self.scaleFactor)
        else:
            info = 'size=' + str(self.size)
        info += ', mode=' + self.mode
        return info


def ConvLayer(in_channels, out_channels, kernel_size, stride=(1,1), padding=(0,0), padding_mode='zero',
              use_bn=True, use_oa=True, leaky_slope=0.1, dropout=0.):
    layers = []
    if not (padding==0 or padding==(0,0)):
        if isinstance(padding, tuple) and len(padding)==2:
            padding = (padding[1], padding[1], padding[0], padding[0])
        if padding_mode == 'reflection':
            layers += [nn.ReflectionPad2d(padding)]
        elif padding_mode == 'replication':
            layers += [nn.ReplicationPad2d(padding)]
        elif padding_mode == 'zero':
            layers += [nn.ZeroPad2d(padding)]
        else:
            raise NotImplementedError('Padding mode <{}> not implemented'.format(padding_mode))
    layers += [nn.Conv2d(in_channels, out_channels, kernel_size, stride=stride)]
    if use_bn:
        layers.append(nn.BatchNorm2d(out_channels))
    if use_oa:
        layers.append(nn.LeakyReLU(leaky_slope))
    if dropout != 0:
        layers.append(nn.Dropout2d(dropout))
    return nn.Sequential(*layers)


class ConstBlock(nn.Module):
    def __init__(self, shape):
        super(ConstBlock, self).__init__()
        self.output = torch.ones(1, *shape[1:], dtype=torch.float32)

    def forward(self, x):
        return self.output.expand(x.size(0), -1, -1, -1)


class UnitLogVar(nn.Module):
    def __init__(self, out_channels=2):
        super(UnitLogVar, self).__init__()
        self.out_channels = out_channels

    def forward(self, x):
        sz = list(x.size())
        sz[1] = self.out_channels
        return torch.zeros(sz, device=x.device)


class ResBlock(nn.Module):
    def __init__(
            self,
            channels, kernel_size=(3,3), num_layers=2,
            use_bn=True, use_oa=True, leaky_slope=0.1, dropout = 0.
    ):
        super(ResBlock, self).__init__()

        if isinstance(kernel_size, (int, float)):
            kernel_size = (kernel_size, kernel_size)
        padding = tuple([k//2 for k in kernel_size])

        layers = [
            ConvLayer(
                channels, channels, kernel_size=kernel_size, stride=(1,1), padding=padding,
                use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=0.,
            )
            for _ in range(num_layers-1)
        ]
        layers.append(
            ConvLayer(
                channels, channels, kernel_size=kernel_size, stride=(1, 1), padding=padding,
                use_bn=use_bn, use_oa=use_oa, leaky_slope=leaky_slope, dropout=dropout,
            ))

        self.convolutions = nn.Sequential(*layers)

    def forward(self, x):
        return x + self.convolutions(x)


class CompressionNet(nn.Module):
    def __init__(self, in_channels=64, latent_channels=16,
                 depth_channels=np.array([16, 16, 16, 16]),
                 dropout=0.25, leaky_slope=0.05,
                 use_bn=True, use_cc=True, use_oa=False,
                 padding_mode='reflection'):
        super(CompressionNet, self).__init__()

        self.in_channels = in_channels
        self.latent_channels = latent_channels

        self.depth_channels = depth_channels

        self.use_cc = use_cc

        self.blocks = nn.ModuleList([
            nn.Sequential(
                ConvLayer(
                    in_channels=self.in_channels, out_channels=self.depth_channels[0],
                    kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
                ),
                ConvLayer(
                    in_channels=self.depth_channels[0], out_channels=self.depth_channels[0],
                    kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=0.
                ),
            ),
            nn.Sequential(
                ConvLayer(
                    in_channels=self.depth_channels[0], out_channels=self.depth_channels[1],
                    kernel_size=(3, 3), stride=(3, 3), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
                ),
                ConvLayer(
                    in_channels=self.depth_channels[1], out_channels=self.depth_channels[1],
                    kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=0.
                ),
            ),
            nn.Sequential(
                ConvLayer(
                    in_channels=self.depth_channels[1], out_channels=self.depth_channels[2],
                    kernel_size=(3, 3), stride=(2, 2), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
                ),
                ConvLayer(
                    in_channels=self.depth_channels[2], out_channels=self.depth_channels[2],
                    kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=0.
                ),
            ),
            nn.Sequential(
                ConvLayer(
                    in_channels=self.depth_channels[2], out_channels=self.depth_channels[3],
                    kernel_size=(3, 3), stride=(2, 2), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
                ),
                ConvLayer(
                    in_channels=self.depth_channels[3], out_channels=self.depth_channels[3],
                    kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=use_oa, leaky_slope=leaky_slope, dropout=0.
                ),
            ),
        ])

    def forward(self, x):
        features = x
        if self.use_cc:
            outputs = []
            for block in self.blocks:
                features = block(features)
                outputs += [features]
        else:
            for block in self.blocks:
                features = block(features)
            outputs = features
        return outputs

    def out_channels(self):
        return self.depth_channels[3]

    def _get_channels(self, compression):
        compression = np.cumprod(np.array(compression))
        dof = 60 * 36 * self.in_channels / compression
        pixels = 3 * 5 * np.array([9 * 4 ** 2, 4 ** 2, 4, 1])
        channels = dof / pixels
        return np.ceil(channels).astype(int)


class CompressionNetSmall(CompressionNet):
    def __init__(self, in_channels=64, latent_channels=16,
                 depth_channels=np.array([16, 16, 16, 16]),
                 dropout=0.25, leaky_slope=0.05,
                 use_bn=True, use_cc=True, use_oa=False,
                 padding_mode='reflection'):
        super(CompressionNetSmall, self).__init__(
            in_channels=in_channels, latent_channels=latent_channels,
            depth_channels=depth_channels,
            dropout=dropout, leaky_slope=leaky_slope,
            use_bn=use_bn, use_cc=use_cc, use_oa=use_oa,
            padding_mode=padding_mode
        )
        self.blocks = nn.ModuleList([
            ConvLayer(
                in_channels=self.in_channels, out_channels=self.depth_channels[0],
                kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
            ),
            ConvLayer(
                in_channels=self.depth_channels[0], out_channels=self.depth_channels[1],
                kernel_size=(3, 3), stride=(3, 3), padding=(1, 1), padding_mode=padding_mode,
                use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
            ),
            ConvLayer(
                in_channels=self.depth_channels[1], out_channels=self.depth_channels[2],
                kernel_size=(3, 3), stride=(2, 2), padding=(1, 1), padding_mode=padding_mode,
                use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
            ),
            ConvLayer(
                in_channels=self.depth_channels[2], out_channels=self.depth_channels[3],
                kernel_size=(3, 3), stride=(2, 2), padding=(1, 1), padding_mode=padding_mode,
                use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
            ),
        ])


class CompressionNetSmallSeq(CompressionNet):
    def __init__(self, in_channels=64, latent_channels=16,
                 depth_channels=np.array([16, 16, 16, 16]),
                 dropout=0.25, leaky_slope=0.05,
                 use_bn=True, use_cc=True, use_oa=False,
                 padding_mode='reflection'):
        super(CompressionNetSmallSeq, self).__init__(
            in_channels=in_channels, latent_channels=latent_channels,
            depth_channels=depth_channels,
            dropout=dropout, leaky_slope=leaky_slope,
            use_bn=use_bn, use_cc=use_cc, use_oa=use_oa,
            padding_mode=padding_mode
        )
        self.blocks = nn.ModuleList([
            nn.Sequential(
                ConvLayer(
                    in_channels=self.in_channels, out_channels=self.depth_channels[0],
                    kernel_size=(3, 1), stride=(1, 1), padding=(1, 0), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
                ),
                ConvLayer(
                    in_channels=self.depth_channels[0], out_channels=self.depth_channels[0],
                    kernel_size=(1, 3), stride=(1, 1), padding=(0, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=0.
                ),
            ),
            nn.Sequential(
                ConvLayer(
                    in_channels=self.depth_channels[0], out_channels=self.depth_channels[1],
                    kernel_size=(3, 1), stride=(3, 1), padding=(1, 0), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
                ),
                ConvLayer(
                    in_channels=self.depth_channels[1], out_channels=self.depth_channels[1],
                    kernel_size=(1, 3), stride=(1, 3), padding=(0, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=0.
                ),
            ),
            nn.Sequential(
                ConvLayer(
                    in_channels=self.depth_channels[1], out_channels=self.depth_channels[2],
                    kernel_size=(3, 1), stride=(2, 1), padding=(1, 0), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
                ),
                ConvLayer(
                    in_channels=self.depth_channels[2], out_channels=self.depth_channels[2],
                    kernel_size=(1, 3), stride=(1, 2), padding=(0, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=0.
                ),
            ),
            nn.Sequential(
                ConvLayer(
                    in_channels=self.depth_channels[2], out_channels=self.depth_channels[3],
                    kernel_size=(3, 1), stride=(2, 1), padding=(1, 0), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
                ),
                ConvLayer(
                    in_channels=self.depth_channels[3], out_channels=self.depth_channels[3],
                    kernel_size=(1, 3), stride=(1, 2), padding=(0, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=0.
                ),
            ),
        ])


class ExpansionNet(nn.Module):
    def __init__(self, in_channels=64, out_channels=64,
                 depth_channels=np.array([16, 16, 16, 16]),
                 dropout=0.25, leaky_slope=0.05,
                 use_bn=True, use_cc=True, use_oa=False,
                 padding_mode='reflection'):
        super(ExpansionNet, self).__init__()

        self.in_channels = in_channels
        self.out_channels = out_channels

        self.depth_channels = depth_channels

        self.use_cc = use_cc

        factor_cc = 2 if use_cc else 1

        self.blocks = nn.ModuleList([
            nn.Sequential(
                ConvLayer(
                    in_channels=self.in_channels, out_channels=self.depth_channels[3],
                    kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=0.
                ),
                Resampling2D(scale_factor=(2, 2)),
                ConvLayer(
                    in_channels=self.depth_channels[3], out_channels=self.depth_channels[2],
                    kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
                ),
            ),
            nn.Sequential(
                ConvLayer(
                    in_channels=factor_cc * self.depth_channels[2], out_channels=self.depth_channels[2],
                    kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=0.
                ),
                Resampling2D(scale_factor=(2, 2)),
                ConvLayer(
                    in_channels=self.depth_channels[2], out_channels=self.depth_channels[1],
                    kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
                ),
            ),
            nn.Sequential(
                ConvLayer(
                    in_channels=factor_cc * self.depth_channels[1], out_channels=self.depth_channels[0],
                    kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=0.
                ),
                Resampling2D(scale_factor=(3, 3)),
                ConvLayer(
                    in_channels=self.depth_channels[0], out_channels=self.depth_channels[0],
                    kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
                ),
            ),
            nn.Sequential(
                ConvLayer(
                    in_channels=factor_cc * self.depth_channels[0], out_channels=self.depth_channels[0],
                    kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=0.
                ),
                ConvLayer(
                    in_channels=self.depth_channels[0], out_channels=self.out_channels,
                    kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=use_oa, leaky_slope=leaky_slope, dropout=0.
                ),
            )
        ])

    def forward(self, x):
        for i, block in enumerate(self.blocks):
            if i == 0:
                if self.use_cc:
                    features = block(x[-1])
                else:
                    features = block(x)
            else:
                if self.use_cc:
                    features = torch.cat([features, x[3-i]], 1)
                features = block(features)
        return features


class ExpansionNetSmall(ExpansionNet):
    def __init__(self, in_channels=64, out_channels=64,
                 depth_channels=np.array([16, 16, 16, 16]),
                 dropout=0.25, leaky_slope=0.05,
                 use_bn=True, use_cc=True, use_oa=False,
                 padding_mode='reflection'):
        super(ExpansionNetSmall, self).__init__(
            in_channels=in_channels, out_channels=out_channels,
            depth_channels=depth_channels,
            dropout=dropout, leaky_slope=leaky_slope,
            use_bn=use_bn, use_cc=use_cc, use_oa=use_oa,
            padding_mode=padding_mode
        )

        factor_cc = 2 if use_cc else 1

        self.blocks = nn.ModuleList([
            nn.Sequential(
                ConvLayer(
                    in_channels=self.in_channels, out_channels=self.depth_channels[2],
                    kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
                ),
                Resampling2D(scale_factor=(2, 2)),
            ),
            nn.Sequential(
                ConvLayer(
                    in_channels=factor_cc * self.depth_channels[2], out_channels=self.depth_channels[1],
                    kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
                ),
                Resampling2D(scale_factor=(2, 2)),
            ),
            nn.Sequential(
                ConvLayer(
                    in_channels=factor_cc * self.depth_channels[1], out_channels=self.depth_channels[0],
                    kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
                ),
                Resampling2D(scale_factor=(3, 3)),
            ),
            nn.Sequential(
                ConvLayer(
                    in_channels=factor_cc * self.depth_channels[0], out_channels=self.out_channels,
                    kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=0.
                ),
            )
        ])


class ExpansionNetSmallSeq(ExpansionNet):
    def __init__(self, in_channels=64, out_channels=64,
                 depth_channels=np.array([16, 16, 16, 16]),
                 dropout=0.25, leaky_slope=0.05,
                 use_bn=True, use_cc=True, use_oa=False,
                 padding_mode='reflection'):
        super(ExpansionNetSmallSeq, self).__init__(
            in_channels=in_channels, out_channels=out_channels,
            depth_channels=depth_channels,
            dropout=dropout, leaky_slope=leaky_slope,
            use_bn=use_bn, use_cc=use_cc, use_oa=use_oa,
            padding_mode=padding_mode
        )

        factor_cc = 2 if use_cc else 1

        self.blocks = nn.ModuleList([
            nn.Sequential(
                ConvLayer(
                    in_channels=self.in_channels, out_channels=self.depth_channels[3],
                    kernel_size=(3, 1), stride=(1, 1), padding=(1, 0), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
                ),
                ConvLayer(
                    in_channels=self.depth_channels[3], out_channels=self.depth_channels[2],
                    kernel_size=(1, 3), stride=(1, 1), padding=(0, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=0.
                ),
                Resampling2D(scale_factor=(2, 2)),
            ),
            nn.Sequential(
                ConvLayer(
                    in_channels=factor_cc * self.depth_channels[2], out_channels=self.depth_channels[2],
                    kernel_size=(3, 1), stride=(1, 1), padding=(1, 0), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
                ),
                ConvLayer(
                    in_channels=self.depth_channels[2], out_channels=self.depth_channels[1],
                    kernel_size=(1, 3), stride=(1, 1), padding=(0, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=0.
                ),
                Resampling2D(scale_factor=(2, 2)),
            ),
            nn.Sequential(
                ConvLayer(
                    in_channels=factor_cc * self.depth_channels[1], out_channels=self.depth_channels[1],
                    kernel_size=(3, 1), stride=(1, 1), padding=(1, 0), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
                ),
                ConvLayer(
                    in_channels=self.depth_channels[1], out_channels=self.depth_channels[0],
                    kernel_size=(1, 3), stride=(1, 1), padding=(0, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=0.
                ),
                Resampling2D(scale_factor=(3, 3)),
            ),
            nn.Sequential(
                ConvLayer(
                    in_channels=factor_cc * self.depth_channels[0], out_channels=self.depth_channels[0],
                    kernel_size=(3, 1), stride=(1, 1), padding=(1, 0), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
                ),
                ConvLayer(
                    in_channels=self.depth_channels[0], out_channels=self.out_channels,
                    kernel_size=(1, 3), stride=(1, 1), padding=(0, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=0.
                ),
            )
        ])


class ProbExpansionNet(nn.Module):
    def __init__(self, in_channels=64, latent_channels=16, out_channels=64,
                 depth_channels=np.array([16, 16, 16, 16]),
                 dropout=0.25, leaky_slope=0.05,
                 use_bn=True, use_cc=True, use_oa=False,
                 padding_mode='reflection'):
        super(ProbExpansionNet, self).__init__()

        self.in_channels = in_channels
        self.latent_channels = latent_channels
        self.out_channels = out_channels

        self.depth_channels = depth_channels

        self.use_cc = use_cc

        factor_cc = 2 if use_cc else 1

        self.blocks = nn.ModuleList([
            nn.Sequential(
                ConvLayer(
                    in_channels=self.in_channels+self.latent_channels, out_channels=self.depth_channels[3],
                    kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=0.
                ),
                Resampling2D(scale_factor=(2, 2)),
                ConvLayer(
                    in_channels=self.depth_channels[3], out_channels=self.depth_channels[2],
                    kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
                ),
            ),
            nn.Sequential(
                ConvLayer(
                    in_channels=factor_cc * self.depth_channels[2], out_channels=self.depth_channels[2],
                    kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=0.
                ),
                Resampling2D(scale_factor=(2, 2)),
                ConvLayer(
                    in_channels=self.depth_channels[2], out_channels=self.depth_channels[1],
                    kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
                ),
            ),
            nn.Sequential(
                ConvLayer(
                    in_channels=factor_cc * self.depth_channels[1], out_channels=self.depth_channels[0],
                    kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=0.
                ),
                Resampling2D(scale_factor=(3, 3)),
                ConvLayer(
                    in_channels=self.depth_channels[0], out_channels=self.depth_channels[0],
                    kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
                ),
            ),
            nn.Sequential(
                ConvLayer(
                    in_channels=factor_cc * self.depth_channels[0], out_channels=self.depth_channels[0],
                    kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=0.
                ),
                ConvLayer(
                    in_channels=self.depth_channels[0], out_channels=self.out_channels,
                    kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=False, leaky_slope=leaky_slope, dropout=0.
                ),
            )
        ])

    def forward(self, z, x):
        features = z
        if self.use_cc:
            for i, block in enumerate(self.blocks):
                features = torch.cat([features, x[3-i]], 1)
                features = block(features)
        else:
            features = torch.cat([features, x], 1)
            for block in self.blocks:
                features = block(features)
        return features


class ProbExpansionNetSmall(ProbExpansionNet):
    def __init__(self, in_channels=64, latent_channels=16, out_channels=64,
                 depth_channels=np.array([16, 16, 16, 16]),
                 dropout=0.25, leaky_slope=0.05,
                 use_bn=True, use_cc=True, use_oa=False,
                 padding_mode='reflection'):
        super(ProbExpansionNetSmall, self).__init__(
            in_channels=in_channels, latent_channels=latent_channels, out_channels=out_channels,
            depth_channels=depth_channels,
            dropout=dropout, leaky_slope=leaky_slope,
            use_bn=use_bn, use_cc=use_cc, use_oa=use_oa,
            padding_mode=padding_mode
        )

        factor_cc = 2 if use_cc else 1

        self.blocks = nn.ModuleList([
            nn.Sequential(
                Resampling2D(scale_factor=(2, 2)),
                ConvLayer(
                    in_channels=self.in_channels+self.latent_channels, out_channels=self.depth_channels[2],
                    kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
                ),
            ),
            nn.Sequential(
                Resampling2D(scale_factor=(2, 2)),
                ConvLayer(
                    in_channels=factor_cc * self.depth_channels[2], out_channels=self.depth_channels[1],
                    kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
                ),
            ),
            nn.Sequential(
                Resampling2D(scale_factor=(3, 3)),
                ConvLayer(
                    in_channels=factor_cc * self.depth_channels[1], out_channels=self.depth_channels[0],
                    kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
                ),
            ),
            ConvLayer(
                in_channels=factor_cc * self.depth_channels[0], out_channels=self.out_channels,
                kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
                use_bn=use_bn, use_oa=False, leaky_slope=leaky_slope, dropout=0.
            ),
        ])


class ProbExpansionNetSmallSeq(ProbExpansionNet):
    def __init__(self, in_channels=64, latent_channels=16, out_channels=64,
                 depth_channels=np.array([16, 16, 16, 16]),
                 dropout=0.25, leaky_slope=0.05,
                 use_bn=True, use_cc=True, use_oa=False,
                 padding_mode='reflection'):
        super(ProbExpansionNetSmallSeq, self).__init__(
            in_channels=in_channels, latent_channels=latent_channels, out_channels=out_channels,
            depth_channels=depth_channels,
            dropout=dropout, leaky_slope=leaky_slope,
            use_bn=use_bn, use_cc=use_cc, use_oa=use_oa,
            padding_mode=padding_mode
        )

        factor_cc = 2 if use_cc else 1

        self.blocks = nn.ModuleList([
            nn.Sequential(
                ConvLayer(
                    in_channels=self.in_channels+self.latent_channels, out_channels=self.depth_channels[3],
                    kernel_size=(3, 1), stride=(1, 1), padding=(1, 0), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
                ),
                ConvLayer(
                    in_channels=self.depth_channels[3], out_channels=self.depth_channels[2],
                    kernel_size=(1, 3), stride=(1, 1), padding=(0, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=0.
                ),
                Resampling2D(scale_factor=(2, 2)),
            ),
            nn.Sequential(
                ConvLayer(
                    in_channels=factor_cc * self.depth_channels[2], out_channels=self.depth_channels[2],
                    kernel_size=(3, 1), stride=(1, 1), padding=(1, 0), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
                ),
                ConvLayer(
                    in_channels=self.depth_channels[2], out_channels=self.depth_channels[1],
                    kernel_size=(1, 3), stride=(1, 1), padding=(0, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=0.
                ),
                Resampling2D(scale_factor=(2, 2)),
            ),
            nn.Sequential(
                ConvLayer(
                    in_channels=factor_cc * self.depth_channels[1], out_channels=self.depth_channels[1],
                    kernel_size=(3, 1), stride=(1, 1), padding=(1, 0), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
                ),
                ConvLayer(
                    in_channels=self.depth_channels[1], out_channels=self.depth_channels[0],
                    kernel_size=(1, 3), stride=(1, 1), padding=(0, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=0.
                ),
                Resampling2D(scale_factor=(3, 3)),
            ),
            nn.Sequential(
                ConvLayer(
                    in_channels=factor_cc * self.depth_channels[0], out_channels=self.depth_channels[0],
                    kernel_size=(3, 1), stride=(1, 1), padding=(1, 0), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
                ),
                ConvLayer(
                    in_channels=self.depth_channels[0], out_channels=self.out_channels,
                    kernel_size=(1, 3), stride=(1, 1), padding=(0, 1), padding_mode=padding_mode,
                    use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=0.
                ),
            )
        ])


def InputLR(data_channels=4, in_channels=64, out_channels=64,
            dropout=0.25, leaky_slope=0.05,
            use_bn=True, use_oa=False,
            padding_mode='reflection'):
    net = nn.Sequential(
        ConvLayer(
            in_channels=data_channels, out_channels=in_channels,
            kernel_size=(5, 5), stride=(1, 1), padding=(2, 2), padding_mode=padding_mode,
            use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
        ),
        ConvLayer(
            in_channels=in_channels, out_channels=out_channels,
            kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
            use_bn=use_bn, use_oa=use_oa, leaky_slope=leaky_slope, dropout=0.
        )
    )
    return net


def InputLRSeq(data_channels=4, in_channels=64, out_channels=64,
            dropout=0.25, leaky_slope=0.05,
            use_bn=True, use_oa=False,
            padding_mode='reflection'):
    net = nn.Sequential(
        ConvLayer(
            in_channels=data_channels, out_channels=in_channels,
            kernel_size=(5, 1), stride=(1, 1), padding=(2, 0), padding_mode=padding_mode,
            use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
        ),
        ConvLayer(
            in_channels=in_channels, out_channels=in_channels,
            kernel_size=(1, 5), stride=(1, 1), padding=(0, 2), padding_mode=padding_mode,
            use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=0.
        ),
        ConvLayer(
            in_channels=in_channels, out_channels=in_channels,
            kernel_size=(3, 1), stride=(1, 1), padding=(1, 0), padding_mode=padding_mode,
            use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
        ),
        ConvLayer(
            in_channels=in_channels, out_channels=out_channels,
            kernel_size=(1, 3), stride=(1, 1), padding=(0, 1), padding_mode=padding_mode,
            use_bn=use_bn, use_oa=use_oa, leaky_slope=leaky_slope, dropout=0.
        )
    )
    return net


def InputHR(data_channels=2, in_channels=64, out_channels=64,
            dropout=0.25, leaky_slope=0.05,
            use_bn=True, use_oa=False,
            padding_mode='reflection'):
    net = nn.Sequential(
        ConvLayer(
            in_channels=data_channels, out_channels=in_channels,
            kernel_size=(5, 5), stride=(2, 3), padding=(2, 2), padding_mode=padding_mode,
            use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
        ),
        ConvLayer(
            in_channels=in_channels, out_channels=out_channels,
            kernel_size=(3, 3), stride=(2, 1), padding=(1, 1), padding_mode=padding_mode,
            use_bn=use_bn, use_oa=use_oa, leaky_slope=leaky_slope, dropout=0.
        ),
    )
    return net


def UpresNet(in_channels=64, out_channels=64,
            dropout=0.25, leaky_slope=0.05,
            use_bn=True, use_oa=False,
            padding_mode='reflection'):
    net = nn.Sequential(
        Resampling2D(scale_factor=(2, 1)),
        ConvLayer(
            in_channels=in_channels, out_channels=in_channels,
            kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
            use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
        ),
        Resampling2D(scale_factor=(1, 3)),
        ConvLayer(
            in_channels=in_channels, out_channels=in_channels,
            kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
            use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
        ),
        Resampling2D(scale_factor=(2, 1)),
        ConvLayer(
            in_channels=in_channels, out_channels=in_channels,
            kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
            use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
        ),
        ConvLayer(
            in_channels=in_channels, out_channels=out_channels,
            kernel_size=(3, 3), stride=(1, 1), padding=(1, 1),
            use_bn=use_bn, use_oa=use_oa, leaky_slope=leaky_slope, dropout=0.
        ),
    )
    return net


def UpresNetSmall(in_channels=64, out_channels=64,
            dropout=0.25, leaky_slope=0.05,
            use_bn=True, use_oa=False,
            padding_mode='reflection'):
    net = nn.Sequential(
        Resampling2D(scale_factor=(2, 1)),
        ConvLayer(
            in_channels=in_channels, out_channels=in_channels,
            kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
            use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
        ),
        Resampling2D(scale_factor=(1, 3)),
        ConvLayer(
            in_channels=in_channels, out_channels=in_channels,
            kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=padding_mode,
            use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
        ),
        Resampling2D(scale_factor=(2, 1)),
        ConvLayer(
            in_channels=in_channels, out_channels=out_channels,
            kernel_size=(3, 3), stride=(1, 1), padding=(1, 1),
            use_bn=use_bn, use_oa=use_oa, leaky_slope=leaky_slope, dropout=0.
        ),
    )
    return net


def UpresNetSmallSeq(in_channels=64, out_channels=64,
            dropout=0.25, leaky_slope=0.05,
            use_bn=True, use_oa=False,
            padding_mode='reflection'):
    net = nn.Sequential(
        Resampling2D(scale_factor=(2, 1)),
        ConvLayer(
            in_channels=in_channels, out_channels=in_channels,
            kernel_size=(3, 1), stride=(1, 1), padding=(1, 0), padding_mode=padding_mode,
            use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
        ),
        ConvLayer(
            in_channels=in_channels, out_channels=in_channels,
            kernel_size=(1, 3), stride=(1, 1), padding=(0, 1), padding_mode=padding_mode,
            use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=0.
        ),
        Resampling2D(scale_factor=(1, 3)),
        ConvLayer(
            in_channels=in_channels, out_channels=in_channels,
            kernel_size=(3, 1), stride=(1, 1), padding=(1, 0), padding_mode=padding_mode,
            use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
        ),
        ConvLayer(
            in_channels=in_channels, out_channels=in_channels,
            kernel_size=(1, 3), stride=(1, 1), padding=(0, 1), padding_mode=padding_mode,
            use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=0.
        ),
        Resampling2D(scale_factor=(2, 1)),
        ConvLayer(
            in_channels=in_channels, out_channels=in_channels,
            kernel_size=(3, 1), stride=(1, 1), padding=(1, 0), padding_mode=padding_mode,
            use_bn=use_bn, use_oa=True, leaky_slope=leaky_slope, dropout=dropout
        ),
        ConvLayer(
            in_channels=in_channels, out_channels=out_channels,
            kernel_size=(1, 3), stride=(1, 1), padding=(0, 1),
            use_bn=use_bn, use_oa=use_oa, leaky_slope=leaky_slope, dropout=0.
        ),
    )
    return net


class OutputGaussianMu(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(OutputGaussianMu, self).__init__()
        self.output_mu = nn.Conv2d(
            in_channels=in_channels, out_channels=out_channels,
            kernel_size=(1, 1), stride=(1, 1), padding=(0, 0),
        )

    def forward(self, features):
        output = self.output_mu(features)
        return output, torch.zeros_like(output)


class OutputGaussianDiagVar(OutputGaussianMu):
    def __init__(self, in_channels, out_channels):
        super(OutputGaussianDiagVar, self).__init__(in_channels, out_channels)
        self.output_logVar = nn.Conv2d(
            in_channels=in_channels, out_channels=out_channels,
            kernel_size=(1, 1), stride=(1, 1), padding=(0, 0),
        )

    def forward(self, features):
        return self.output_mu(features), self.output_logVar(features)


class CholeskyConv2d(nn.Conv2d):
    def __init__(self, data_channels, kernel_size, num_kernels):
        super(CholeskyConv2d, self).__init__(in_channels=data_channels, out_channels=data_channels*num_kernels, kernel_size=kernel_size,
                                           stride=(1,1), padding=(0,0), bias=False, groups=data_channels)
        bound = self.kernel_size[1]//2
        self.mask = torch.ones_like(self.weight, requires_grad=False)
        self.mask[:, :, 0, bound] = 0.

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        return F.conv2d(x, self.weight * self.mask, stride=self.stride, padding=self.padding, bias=self.bias)


class OutputGaussianStructured(OutputGaussianMu):
    def __init__(self, in_channels, out_channels, num_kernels, k):
        super(OutputGaussianStructured, self).__init__(in_channels, out_channels)
        self.data_channels = out_channels
        self.num_kernels = num_kernels
        self.covWeights = nn.Conv2d(
            in_channels=in_channels, out_channels=self.num_kernels*self.data_channels,
            kernel_size=(1, 1), stride=(1, 1), padding=(0, 0),
        )
        self.logDiag = nn.Conv2d(
            in_channels=in_channels, out_channels=self.data_channels,
            kernel_size=(1, 1), stride=(1, 1), padding=(0,0)
        )
        self.padding = nn.ZeroPad2d((k, k, 0, 2 * k))
        self.covBlocksDiag = CholeskyConv2d(
            data_channels=self.data_channels, kernel_size=(k + 1, 2 * k + 1), num_kernels=self.num_kernels
        )
        blocks = []
        for channel in range(self.data_channels - 1):
            blocks += [
                nn.Conv2d(
                    in_channels=self.data_channels-(1+channel), out_channels=self.num_kernels,
                    kernel_size=(2 * k + 1), padding=(2 * k + 1)
                )
            ]
        self.covBlocksNonDiag = nn.ModuleList(blocks)

    def forward(self, features):
        return self.output_mu(features), self.output_logDiag(features), features

    def apply_Lt(self, data, features):
        shape = data.size()[-2:]
        output = torch.exp(self.logDiag(features)) * data
        weights = self.covWeights(features).view(-1, self.data_channels, self.num_kernels, shape[0], shape[1])
        layers = self.covBlocksDiag(self.padding(data)).view(-1, self.data_channels, self.num_kernels, shape[0], shape[1])
        output += torch.sum(weights * layers, dim=2)
        layers = []
        for channel, block in enumerate(self.covBlocksNonDiag):
            layers += [block(data[:,(channel + 1):, :, :])]
        layers = torch.cat(layers, dim=1)
        output += torch.sum(weights * layers, dim=2)
        return output


