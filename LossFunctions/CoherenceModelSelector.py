import os
import torch
from .CoherenceModels import PolynomialInterpolator


class CoherenceModelSelector(object):
    def __init__(self, config):
        self.config = config

    def select(self):
        print('[INFO]: Selecting coherence model for temporal coherence loss.')
        netName = self.config['training']['coherenceModel']['name']
        if netName == 'PolynomialInterpolator':
            net = self._get_PolynomialInterpolator()
        else:
            net = self._load_pretrained_model()
        return net

    def _load_pretrained_model(self):
        print('[INFO]: Loading pretrained model from directory.')
        netName = self.config['training']['coherenceModel']['name']
        runName, runNumber = self._select_run(netName)
        fileName, epoch = self._select_epoch(netName, runName)
        modelPath = os.path.join('results', netName, 'records', runName, 'models', fileName)
        net = torch.load(modelPath)['model']
        self._assert_model_compatibility(net)
        print('[INFO]: Used coherence network: {}, run {}, epoch {}'.format(netName, runNumber, epoch))
        return net

    def _select_run(self, netName):
        netDir = os.path.join('results', netName)
        assert os.path.exists(netDir) and os.path.isdir(netDir), \
            '[ERROR] Pretrained networks of type <{}> are not available.'
        recordsDir = os.path.join(netDir, 'records')
        folders = sorted(filter(lambda fd: os.path.isdir(os.path.join(recordsDir, fd)), os.listdir(recordsDir)))
        runNumber = self.config['training']['coherenceModel']['run']
        if runNumber < 0:
            assert -runNumber <= len(folders), \
                '[ERROR] Negative run number exceeds range of available training runs for <{}>.'.format(netName)
            runName = folders[runNumber]
            runNumber = int(runName[-5:])
        else:
            runNumbers = [int(f.split('_')[-1]) for f in folders]
            assert runNumber in runNumbers, \
                '[ERROR] Specified run number is not available for <{}>.'.format(netName)
            runName = folders[runNumbers.index(runNumber)]
        return runName, runNumber

    def _select_epoch(self, netName, runName):
        runNumber = int(runName[-5:])
        modelsDir = os.path.join('results', netName, 'records', runName, 'models')
        files = sorted(filter(lambda f: f.endswith('.pth'), os.listdir(modelsDir)))
        epoch = self.config['training']['coherenceModel']['epoch']
        if epoch < 0:
            assert -epoch <= len(files), \
                '[ERROR] Negative epoch index exceeds range of available training epochs for {}, run {}.'.format(
                    netName, runNumber
                )
            fileName = files[epoch]
            epoch = int(fileName.split('_')[-1][:-4])
        else:
            epochNumbers = [int(f.split('_')[-1][:-4]) for f in files]
            assert epoch in epochNumbers, \
                '[ERROR] Specified epoch number is not available for <{}>, run {}.'.format(netName, runNumber)
            fileName = files[epochNumbers.index(epoch)]
        return fileName, epoch

    def _assert_model_compatibility(self, net):
        assert net.numStepsPast == self.config['coherence']['numStepsPast'] and \
               net.numStepsFuture == self.config['coherence']['numStepsFuture'] and \
               net.stepSizePast == self.config['coherence']['stepSizePast'] and \
               net.stepSizeFuture == self.config['coherence']['stepSizeFuture'], \
            '[ERROR] Coherence model incompatible with <config> coherence requirements.'
        return 0

    def _get_PolynomialInterpolator(self):
        return PolynomialInterpolator(
            data_channels=self.config['data']['shapeTargets'][0],
            numStepsPast=self.config['coherence']['numStepsPast'],
            numStepsFuture=self.config['coherence']['numStepsFuture'],
            stepSizePast=self.config['coherence']['stepSizePast'],
            stepSizeFuture=self.config['coherence']['stepSizeFuture'],
        )