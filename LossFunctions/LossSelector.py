import torch.nn as nn
from .MaskedLosses import *
from .CoherenceLosses import L1CoherenceLoss, MSECoherenceLoss, MixedLoss


class LossSelector(object):
    def __init__(self, configOpts):
        self.config = configOpts

    def select(self):
        lossName = self.config['training']['loss']['name']
        loss = self._select_loss(lossName, 'deviation')
        if self.config['training']['coherenceModel'] is not None:
            lossName = self.config['training']['coherenceModel']['loss']['name']
            coherenceLoss = self._select_loss(lossName + 'Coherence', 'coherence')
            weight = self.config['training']['coherenceModel']['weight']
            loss = MixedLoss(loss, coherenceLoss, coherence_weight=weight)
        return loss

    def select_loss_function(self, name, mode):
        return self._select_loss(name, mode)

    def _select_loss(self, lossName, mode):
        try:
            lossCreationRoutine = getattr(self, '_get_{}'.format(lossName))
        except AttributeError:
            raise NotImplementedError('[ERROR] Loss function <{}> not implemented.'.format(lossName))
        else:
            loss = lossCreationRoutine()
            print('[INFO]: Used {} loss: {}'.format(mode, loss.__class__.__name__))
            return loss

    def _get_L1(self):
        return MaskedL1Loss()

    def _get_MSE(self):
        return MaskedMSELoss()

    def _get_RMSE(self):
        return MaskedRMSELoss()

    def _get_L2(self):
        return MaskedMSELoss()

    def _get_SmoothL1(self):
        return MaskedSmoothL1Loss()

    def _get_CosineDistance(self):
        return MaskedCosineDistanceLoss()

    def _get_CosineSimilarity(self):
        return MaskedCosineSimilarity()

    def _get_CosineNorm(self):
        weight_cos = self.config['training']['loss']['cosineNorm']['weight_cosine']
        weight_norm = self.config['training']['loss']['cosineNorm']['weight_norm']

        return MaskedCosineNormLoss(weight_cos=weight_cos, weight_norm=weight_norm)

    def _get_Norm(self):
        return MaskedNormLoss()

    def _get_L1Coherence(self):
        return L1CoherenceLoss(config=self.config)

    def _get_MSECoherence(self):
        return MSECoherenceLoss(config=self.config)

    def _get_L2Coherence(self):
        return MSECoherenceLoss(config=self.config)
