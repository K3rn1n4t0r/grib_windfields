import torch.nn.functional as F
from torch.nn.modules.loss import _Loss
from .CoherenceModelSelector import CoherenceModelSelector
from .MaskedLosses import masked_l1_loss, masked_mse_loss


class _CoherenceLoss(_Loss):
    def __init__(self, config=None, size_average=None, reduce=None, reduction='mean'):
        super(_CoherenceLoss, self).__init__(size_average, reduce, reduction)
        self.coherenceModel = CoherenceModelSelector(config).select()
        self.coherenceModel.eval()


class L1CoherenceLoss(_CoherenceLoss):
    def __init__(self, config=None, size_average=None, reduce=None, reduction='mean'):
        super(L1CoherenceLoss, self).__init__(config, size_average, reduce, reduction)

    def forward(self, series, mask=None):
        if mask is None:
            loss = F.l1_loss(
                series[self.coherenceModel.idxPresent], self.coherenceModel(series),
                reduction=self.reduction
            )
        else:
            loss = masked_l1_loss(
                series[self.coherenceModel.idxPresent], self.coherenceModel(series), mask,
                reduction=self.reduction
            )
        return loss


class MSECoherenceLoss(_CoherenceLoss):
    def __init__(self, config=None, size_average=None, reduce=None, reduction='mean'):
        super(MSECoherenceLoss, self).__init__(config, size_average, reduce, reduction)

    def forward(self, series, mask=None):
        if mask is None:
            loss = F.mse_loss(
                series[self.coherenceModel.idxPresent], self.coherenceModel(series),
                reduction=self.reduction
            )
        else:
            loss = masked_mse_loss(
                series[self.coherenceModel.idxPresent], self.coherenceModel(series), mask,
                reduction=self.reduction
            )
        return loss


class MixedLoss(_Loss):
    def __init__(self, deviation_loss, coherence_loss, coherence_weight=1.,
                 size_average=None, reduce=None, reduction='mean'):
        super(MixedLoss, self).__init__(size_average, reduce, reduction)
        self.deviation = deviation_loss
        self.coherence = coherence_loss
        self.coherenceWeight = coherence_weight

    def forward(self, predictions, targets):
        return self.deviation(predictions, targets) + self.coherenceWeight * self.coherence(predictions)