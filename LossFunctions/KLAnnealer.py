class KLAnnealer(object):
    def __init__(self, eps_start=0., eps_end=1., increment = None, epochs = None, offset = 5):
        self.step_curr = 0
        self.eps_start = eps_start
        self.eps_end = eps_end
        self.eps_curr = eps_start
        self.eps_last = eps_start
        if increment is None:
            assert epochs is not None
            self.increment = (eps_end - eps_start) / epochs
        else:
            assert epochs is None
            self.increment = increment
        self.offset = offset

    def step(self):
        self.step_curr += 1
        if self.step_curr < self.offset:
            eps = self.eps_start
        else:
            if self.increment > 0:
                eps = min(self.eps_curr + self.increment, self.eps_end)
            else:
                eps = max(self.eps_curr + self.increment, self.eps_end)
        self.eps_last = self.eps_curr
        self.eps_curr = eps

    def get_eps(self, mode='training'):
        return self.eps_last if mode=='validation' else self.eps_curr

