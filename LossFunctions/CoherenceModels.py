import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from scipy.linalg import circulant


class _CoherenceModel(nn.Module):
    def __init__(self, data_channels=2,
                 numStepsPast=2, numStepsFuture=2,
                 stepSizePast=1, stepSizeFuture=1):
        super(_CoherenceModel, self).__init__()
        self.dataChannels = data_channels
        self.numStepsPast = numStepsPast
        self.numStepsFuture = numStepsFuture
        self.stepSizePast = stepSizePast
        self.stepSizeFuture = stepSizeFuture
        self.selection = list(np.arange(numStepsPast + numStepsFuture + 1))
        self.selection.pop(numStepsPast)
        self.idxPresent = numStepsPast

    def _reshape(self, input):
        return input[self.selection].transpose(0,1).flatten(start_dim=1, end_dim=2)


class PolynomialInterpolator(_CoherenceModel):
    def __init__(self, data_channels=2,
                 numStepsPast=2, numStepsFuture=2,
                 stepSizePast=1, stepSizeFuture=1):
        super(PolynomialInterpolator, self).__init__(
            data_channels,
            numStepsPast, numStepsFuture,
            stepSizePast, stepSizeFuture,
        )
        self.weight = self._get_weight()

    def forward(self, input):
        return F.conv2d(self._reshape(input), self.weight)

    def _get_weight(self):
        nodes = np.concatenate((
            - np.flip(np.arange(self.numStepsPast) + 1) * self.stepSizePast,
            (np.arange(self.numStepsFuture) + 1) * self.stepSizeFuture
        ))
        exponents = np.arange(len(nodes))
        phi = np.expand_dims(nodes, axis=1) ** exponents
        c = np.linalg.inv(phi)[0]
        c_ext = np.zeros(self.dataChannels * len(c))
        c_ext[::self.dataChannels] = c
        weight = torch.as_tensor(np.transpose(circulant(c_ext))[:self.dataChannels].astype(np.float32))
        weight = weight.view(self.dataChannels, self.dataChannels * len(c), 1, 1)
        return nn.parameter.Parameter(weight)


class MinimalNet(_CoherenceModel):
    def __init__(self, data_channels=4, out_channels=2, kernel_size = 5, dilation=1,
                 numStepsPast=2, numStepsFuture=2, stepSizePast=1, stepSizeFuture=1):
        super(MinimalNet, self).__init__(
            data_channels,
            numStepsPast, numStepsFuture,
            stepSizePast, stepSizeFuture
        )
        if isinstance(kernel_size, int):
            kernel_size = (kernel_size, kernel_size)
        if isinstance(dilation, int):
            dilation = (dilation, dilation)
        padding = (dilation[0] * (kernel_size[0]//2), dilation[1] * (kernel_size[1]//2))
        self.conv = nn.Conv2d(data_channels, out_channels, kernel_size=kernel_size, padding=padding, dilation=dilation)

    def forward(self, x):
        return self.conv(self._reshape(x))
