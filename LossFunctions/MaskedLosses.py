import warnings
import torch
import torch.nn as nn
from torch.nn import _reduction as _Reduction
from torch.nn import functional as F
import math


def tensor_reduction(tensor, reduction='mean', mask=None):
    if reduction == 'none':
        return tensor

    if mask is not None:
        mask_sum = mask.sum()
        ret = torch.sum(tensor) / mask_sum if reduction == 'mean' else torch.sum(tensor)
    else:
        ret = torch.mean(tensor) if reduction == 'mean' else torch.sum(tensor)

    return ret


def masked_l1_loss(input, target, mask, size_average=None, reduce=None, reduction='mean'):
    if not (target.size() == input.size()):
        warnings.warn("Using a target size ({}) that is different to the input size ({}). "
                      "This will likely lead to incorrect results due to broadcasting. "
                      "Please ensure they have the same size.".format(target.size(), input.size()),
                      stacklevel=2)
    if size_average is not None or reduce is not None:
        reduction = _Reduction.legacy_get_string(size_average, reduce)
    ret = mask * torch.abs(input - target)
    ret = tensor_reduction(ret, reduction, mask)
    return ret


def masked_mse_loss(input, target, mask, size_average=None, reduce=None, reduction='mean'):
    if not (target.size() == input.size()):
        warnings.warn("Using a target size ({}) that is different to the input size ({}). "
                      "This will likely lead to incorrect results due to broadcasting. "
                      "Please ensure they have the same size.".format(target.size(), input.size()),
                      stacklevel=2)
    if size_average is not None or reduce is not None:
        reduction = _Reduction.legacy_get_string(size_average, reduce)
    ret = mask * ((input - target) ** 2)
    ret = tensor_reduction(ret, reduction, mask)
    return ret


def _smooth_l1_loss(input, target, mask):
    t = mask * torch.abs(input - target)
    return torch.where(t < 1, 0.5 * t ** 2, t - 0.5)


def masked_smooth_l1_loss(input, target, mask, size_average=None, reduce=None, reduction='mean'):
        if not (target.size() == input.size()):
            warnings.warn("Using a target size ({}) that is different to the input size ({}). "
                          "This will likely lead to incorrect results due to broadcasting. "
                          "Please ensure they have the same size.".format(target.size(), input.size()),
                          stacklevel=2)
        if size_average is not None or reduce is not None:
            reduction = _Reduction.legacy_get_string(size_average, reduce)
        if target.requires_grad:
            ret = _smooth_l1_loss(input, target, mask)
            ret = tensor_reduction(ret, reduction, mask)
        else:
            expanded_input, expanded_target = torch.broadcast_tensors(input, target)
            ret = torch._C._nn.smooth_l1_loss(expanded_input, expanded_target, _Reduction.get_enum(reduction))
        return ret


def masked_cosine_similarity_loss(input, target, mask, dim=1, eps=1.0e-6, reduction='mean'):
    if not (target.size() == input.size()):
        warnings.warn("Using a target size ({}) that is different to the input size ({}). "
                      "This will likely lead to incorrect results due to broadcasting. "
                      "Please ensure they have the same size.".format(target.size(), input.size()),
                      stacklevel=2)
    input_mod = input * mask if mask is not None else input
    target_mod = target * mask if mask is not None else target

    prod = input_mod * target_mod

    norm_input = torch.norm(input_mod, dim=dim)
    norm_target = torch.norm(target_mod, dim=dim)

    # use epsilon to avoid division by zero
    norm_eps = torch.full_like(norm_input, eps)
    # if norm_input.is_cuda:
    #     norm_eps = torch.full(norm_input.shape, eps, device='cuda:0')
    # else:
    #     norm_eps = torch.full(norm_input.shape, eps)

    denom = torch.max(norm_input * norm_target, norm_eps)
    ret = torch.sum(prod, dim=dim) / denom
    ret = tensor_reduction(ret, reduction, mask)

    return ret


def masked_radians_similarity_loss(input, target, mask, dim=1, eps=1.0e-6, reduction='mean'):
    ret = masked_cosine_similarity_loss(input, target, mask, dim, eps, reduction)

    angular_dist = torch.acos(ret)
    return angular_dist


def masked_norm_loss(input, target, mask, dim=1, reduction='mean'):
    if not (target.size() == input.size()):
        warnings.warn("Using a target size ({}) that is different to the input size ({}). "
                      "This will likely lead to incorrect results due to broadcasting. "
                      "Please ensure they have the same size.".format(target.size(), input.size()),
                      stacklevel=2)

    if mask is not None:
        ret = torch.abs(torch.norm(target * mask, dim=dim) - torch.norm(input * mask, dim=dim))
    else:
        ret = torch.abs(torch.norm(target, dim=dim) - torch.norm(input, dim=dim))

    ret = tensor_reduction(ret, reduction, mask)
    return ret


class MaskedL1Loss(nn.L1Loss):
    def __init__(self, size_average=None, reduce=None, reduction='mean'):
        super(MaskedL1Loss, self).__init__(size_average=size_average, reduce=reduce, reduction=reduction)

    def forward(self, prediction, target, mask=None):
        if mask is None:
            loss = F.l1_loss(prediction, target, reduction=self.reduction)
        else:
            loss = masked_l1_loss(prediction, target, mask, reduction=self.reduction)
        return loss


class MaskedMSELoss(nn.MSELoss):
    def __init__(self, size_average=None, reduce=None, reduction='mean'):
        super(MaskedMSELoss, self).__init__(size_average=size_average, reduce=reduce, reduction=reduction)

    def forward(self, prediction, target, mask=None):
        if mask is None:
            loss = F.mse_loss(prediction, target, reduction=self.reduction)
        else:
            loss = masked_mse_loss(prediction, target, mask, reduction=self.reduction)
        return loss


class MaskedRMSELoss(nn.MSELoss):
    def __init__(self, size_average=None, reduce=None, reduction='mean'):
        super(MaskedRMSELoss, self).__init__(size_average=size_average, reduce=reduce, reduction=reduction)

    def forward(self, prediction, target, mask=None):
        if mask is None:
            loss = torch.sqrt(F.mse_loss(prediction, target, reduction=self.reduction))
        else:
            loss = torch.sqrt(masked_mse_loss(prediction, target, mask, reduction=self.reduction))
        return loss


class MaskedSmoothL1Loss(nn.SmoothL1Loss):
    def __init__(self, size_average=None, reduce=None, reduction='mean'):
        super(MaskedSmoothL1Loss, self).__init__(size_average=size_average, reduce=reduce, reduction=reduction)

    def forward(self, prediction, target, mask=None):
        if mask is None:
            loss = F.smooth_l1_loss(prediction, target, reduction=self.reduction)
        else:
            loss = masked_smooth_l1_loss(prediction, target, mask, reduction=self.reduction)
        return loss


class MaskedCosineDistanceLoss(nn.Module):
    def __init__(self, reduction='mean', eps=1.0e-8):
        super(MaskedCosineDistanceLoss, self).__init__()
        self.reduction = reduction
        self.eps = eps

    def forward(self, prediction, target, mask=None):
        loss = 1.0 - masked_cosine_similarity_loss(prediction, target, mask, reduction=self.reduction, eps=self.eps)
        return loss


class MaskedCosineSimilarity(nn.Module):
    def __init__(self, reduction='mean', eps=1.0e-8):
        super(MaskedCosineSimilarity, self).__init__()
        self.reduction = reduction
        self.eps = eps

    def forward(self, prediction, target, mask=None):
        loss = masked_cosine_similarity_loss(prediction, target, mask, reduction=self.reduction, eps=self.eps)
        return loss


class MaskedNormLoss(nn.Module):
    def __init__(self, reduction='mean'):
        super(MaskedNormLoss, self).__init__()
        self.reduction = reduction

    def forward(self, prediction, target, mask=None):
        loss = masked_norm_loss(prediction, target, mask, reduction=self.reduction)
        return loss


class MaskedCosineNormLoss(nn.Module):
    def __init__(self, weight_cos=1., weight_norm=1., reduction='mean'):
        super(MaskedCosineNormLoss, self).__init__()
        self.reduction = reduction
        self.weight_cos = weight_cos
        self.weight_norm = weight_norm
        self.cos_loss = MaskedCosineDistanceLoss()
        self.norm_loss = MaskedNormLoss()

    def forward(self, prediction, target, mask=None):
        loss = self.cos_loss(prediction, target, mask) * self.weight_cos \
               + self.norm_loss(prediction, target, mask) * self.weight_norm
        return loss
