# https://towardsdatascience.com/different-types-of-regularization-on-neuronal-network-with-pytorch-a9d6faf4793e
# https://github.com/dizam92/pyTorchReg/tree/5ad3ca0a0cc7561741871a4135c2c08bc0bbef6c/src/regularization

class _Regularizer:
    def __init__(self, model):
        self.model = model

    def regularized_param(self, param_weights, loss):
        raise NotImplementedError

    def regularized_all_param(self, loss):
        raise NotImplementedError


class L1Regularizer(_Regularizer):
    def __init__(self, model, lambda_factor=0.01):
        super(L1Regularizer, self).__init__(model)
        self.lambda_factor = lambda_factor

    def regularized_param(self, param_weights, loss):
        loss_function = self.lambda_factor * L1Regularizer.__add_l1(var=param_weights)
        return loss_function

    def regularized_all_param(self, loss_function):
        for model_param_name, model_param_value in self.model.named_parameters():
            if model_param_name.endswith("weight"):
                loss_function += self.lambda_factor * L1Regularizer.__add_l1(var=model_param_value)

        return loss_function

    @staticmethod
    def __add_l1(var):
        return var.abs().sum()
